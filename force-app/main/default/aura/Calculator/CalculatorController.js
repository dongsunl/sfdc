({
    doInit : function(component, event, helper) {
        var opts = [
            { class: "optionClass", label: "USD", value: "1"},
            { class: "optionClass", label: "AED", value: "3.67" },
            { class: "optionClass", label: "ARS", value: "37.68" },
            { class: "optionClass", label: "AUD", value: "1.38" },
            { class: "optionClass", label: "BGN", value: "1.72" },
            { class: "optionClass", label: "BRL", value: "3.88" },
            { class: "optionClass", label: "CAD", value: "1.36" }, 
            { class: "optionClass", label: "CHF", value: "0.99" }, 
            { class: "optionClass", label: "CNY", value: "6.88" },
            { class: "optionClass", label: "COP", value: "3201.20" },
            { class: "optionClass", label: "CZK", value: "22.59" },
            { class: "optionClass", label: "DKK", value: "6.53" },
            { class: "optionClass", label: "EUR", value: ".87" },
            { class: "optionClass", label: "GBP", value: ".79" },
            { class: "optionClass", label: "HKD", value: "7.83" },
            { class: "optionClass", label: "HUF", value: "283.67" },
            { class: "optionClass", label: "IDR", value: "14522.49"},
            { class: "optionClass", label: "INR", value: "69.79"},
            { class: "optionClass", label: "JPY", value: "110.15" },
            { class: "optionClass", label: "KRW", value: "1182.06" },
            { class: "optionClass", label: "MXN", value: "19.64" },
            { class: "optionClass", label: "MYR", value: "4.13"},
            { class: "optionClass", label: "NOK", value: "8.69" },
            { class: "optionClass", label: "NZD", value: "1.49" },
            { class: "optionClass", label: "PEN", value: "3.38" },
            { class: "optionClass", label: "PHP", value: "52.55" },
            { class: "optionClass", label: "RUR", value: "69.53" }, 
            { class: "optionClass", label: "SEK", value: "8.95" },
            { class: "optionClass", label: "SGD", value: "1.36" },
            { class: "optionClass", label: "THB", value: "32.39" },
            { class: "optionClass", label: "TWD", value: "31.40" },
            { class: "optionClass", label: "UAH", value: "26.35" },
            { class: "optionClass", label: "VEF", value: "248138.96" },
            { class: "optionClass", label: "ZAR", value: "14.38" },
			{ class: "optionClass", label: "PLN", value: "3.76" },
			{ class: "optionClass", label: "VND", value: "23234.2" }            
        ];
        component.find("currency").set("v.options", opts);
        
        
    },
    calculateCurrency : function(component, event, helper) {
        helper.calculateCurrency(component);
    }
    
})