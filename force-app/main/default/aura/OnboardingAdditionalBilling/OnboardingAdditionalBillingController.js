({
    doInit : function(component, event, helper) {
        helper.getPicklistOptions(component);
    },
    handleOppInit : function(component, event, helper) {
        helper.reloadOpportunity(component);
    },
    handleClientInit : function(component, event, helper) {
        component.find("clientRecordLoader").reloadRecord();
    },
    handleOppUpdate : function(component, event, helper) {
        const changeType = event.getParams().changeType;
        if (changeType === "CHANGED") {
            helper.reloadOpportunity(component);
        }
    },
    handleConsumptionTaxCheck : function(component, event, helper) {
        const taxNumberField = component.find("abi-consumption-tax-number");
        if (component.find("abi-not-registered").get("v.checked")) {
            const country = component.get("v.tempOpp.Sales_Order_Shipping_Country__c");
            const taxCheckConfirmation = confirm("By checking this box, you agree that you have confirmed directly with your client that they are NOT registered for Consumption Tax in " + country + ". We need this information in order to be compliant with the tax laws of " + country + ".");
            component.set("v.tempOpp.Not_Registered_for_Consumption_Tax__c", taxCheckConfirmation);
            if (taxCheckConfirmation) {
                component.set("v.tempOpp.InternationalTaxNumber__c", null);
                component.set("v.taxNumberDisabled", true);
                taxNumberField.setCustomValidity(""); // Clears out custom validation when field is disabled so the error message doesn't appear once disabled
            }
        } else {
            component.set("v.tempOpp.Not_Registered_for_Consumption_Tax__c", false);
            component.set("v.taxNumberDisabled", false);
        }
        taxNumberField.reportValidity();
    },
    handleTermsChange : function(component, event, helper) {
        //Clear custom payment terms if payment terms selection is not Custom.
        if(component.get("v.tempOpp.Payment_Terms__c") !== 'Custom'){
           component.set("v.tempOpp.Custom_Payment_Terms__c", null);
           }
    },
    handleARRChange : function(component, event, helper) {
        if (component.get("v.tempOpp.ARR_NRR__c") === "NRR") {
            const arrChangeConfirmation = confirm("By checking Non Recurring Revenue a renewal will NOT be generated for next year.");
            if (!arrChangeConfirmation) {
                component.set("v.tempOpp.ARR_NRR__c", null);
            }
        }
    },
    handleAutoIncChange : function(component, event, helper) {
        const percentIncreaseField = component.find("abi-percent-increase");
        if (component.get("v.tempOpp.Renewal_Price_Increase__c") === "No") {
            component.set("v.tempOpp.Annual_Price_Increase__c", null);
            component.set("v.percentIncreaseDisabled", true);
            percentIncreaseField.setCustomValidity(""); // Clears out custom validation when field is disabled so the error message doesn't appear once disabled
        } else {
            component.set("v.percentIncreaseDisabled", false);
        }
        percentIncreaseField.reportValidity();
    },
    handleSave : function(component, event, helper) {
        
        let allValid = component.find("abi-field").reduce(function(validSoFar, field) {
            field.reportValidity();
            return validSoFar && field.checkValidity();
        }, true);
        // We need to manually check Consumption Tax Number and % Increase Amount validation since it may or may not be disabled
        const taxNumberField = component.find("abi-consumption-tax-number");
        const percentIncreaseField = component.find("abi-percent-increase");
        
        if (taxNumberField) {
            taxNumberField.reportValidity();
            allValid = allValid && taxNumberField.checkValidity()
        }
        if (percentIncreaseField) {
            percentIncreaseField.reportValidity();
            allValid = allValid && percentIncreaseField.checkValidity()
        }
        
      //Validate that demographics is 900 characters or fewer before trying to save and display error message if not.

        var demographicsValidity = true;

        if(component.get('v.tempOpp.RecordType.Name') == 'Panels'){

        var demographicsInput = component.find("abi-invoice-display");

            var demographicsValue = demographicsInput.get("v.value");

            demographicsValidity = demographicsValue.length < 901;

            if (demographicsValidity === true) {

                demographicsInput.setCustomValidity("");

            } else {

                demographicsInput.setCustomValidity("Invoice Display Text must be 900 characters or fewer.");

            }

            demographicsInput.reportValidity();

        }
        
        if (allValid && demographicsValidity)  {
            //Format incumbent value to be acceptable to database
            var incumbentvalue = component.get('v.incumbentSelections');
            console.log('incumbent selections' + incumbentvalue);
            var formattedincumbentvalue = '';
            for(var i=0; i<incumbentvalue.length; i++){
                formattedincumbentvalue += incumbentvalue[i] + ';';
            }
            console.log('incumbent value post format' + formattedincumbentvalue);
            component.set('v.tempOpp.Incumbent__c', formattedincumbentvalue);
            const studyTypes = component.get("v.studyTypeSelections");
            if (studyTypes) {
                component.set("v.tempOpp.Panel_Type_of_Project__c", studyTypes.join("; "));
            }
            if (component.get("v.tempOpp.RecordType.Name") === "Panels") {
                component.set("v.tempOpp.ARR_NRR__c", "NRR");
            }
            if (component.get("v.showConsumptionTaxNumber") && !component.get("v.tempOpp.Not_Registered_for_Consumption_Tax__c") && !component.get("v.taxNumberDisabled")) {
                component.set("v.tempOpp.InternationalTaxNumber__c", component.get("v.tempClient.Consumption_Tax_Number__c"));
            } else {
                component.set("v.tempOpp.InternationalTaxNumber__c", null);
                component.set("v.tempClient.Consumption_Tax_Number__c", null);
            }
            //Since Auto-Renew is hidden, if there is no value, need to default to "No"
            console.log('autorenew before if: ' + component.get("v.tempOpp.Auto_Renew__c"));
            if (!component.get("v.tempOpp.Auto_Renewal__c")) {
                component.set("v.tempOpp.Auto_Renewal__c", "No");
            }
            console.log('autorenew after if: ' + component.get("v.tempOpp.Auto_Renew__c"));
            component.set("v.showSpinner", true);
            component.find("oppRecordLoader").saveRecord($A.getCallback(function(saveResult) {
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                    // Save the consumption tax number on the related Client record.
                    helper.saveClient(component);
                } else if (saveResult.state === "INCOMPLETE") {
                    if(component.get('v.isLightningOut') === false){
                        component.find('addBillingNotifLib').showToast({
                            "title": "There was a problem saving the Opportunity",
                            "message": "User is offline, device doesn't support drafts.",
                            "variant": "error"
                        });
                    }
                    console.log("User is offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR") {
                    const errors = saveResult.error;
                    let errorMessage;
                    if (errors && errors[0] && errors[0].message) {
                        errorMessage = errors[0].message;
                    } else {
                        errorMessage = "Unknown error.";
                    }
                    if(component.get('v.isLightningOut') === false){
                        component.find('addBillingNotifLib').showToast({
                            "title": "There was a problem saving the Opportunity",
                            "message": "Error: " + errorMessage,
                            "variant": "error"
                        });
                    }
                    console.log("Problem saving record, error: " + JSON.stringify(saveResult.error));
                } else {
                    if(component.get('v.isLightningOut') === false){
                        component.find('addBillingNotifLib').showToast({
                            "title": "There was a problem saving the Opportunity",
                            "message": "Error: " + "Unknown problem, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error),
                            "variant": "error"
                        });
                    }
                    console.log("Unknown problem, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error));
                }
                component.set("v.showSpinner", false);
            }));
        }
    },
})