({
    getPicklistOptions : function(component) {
        const picklistOptionAction = component.get("c.generatePicklistJSON");
        picklistOptionAction.setParams({"sObjectType": "Opportunity", "fieldApiNames": ["Sales_Order_Billing_Country__c", "Sales_Order_Shipping_Country__c"]})
        picklistOptionAction.setCallback(this, function(response) {
            const state = response.getState();
            if (state === "SUCCESS") {
                const picklistJSON = JSON.parse(response.getReturnValue());
                if (picklistJSON.hasOwnProperty("Sales_Order_Billing_Country__c")) {
                    component.set("v.billingCountryOptions", picklistJSON["Sales_Order_Billing_Country__c"]);
                }
                if (picklistJSON.hasOwnProperty("Sales_Order_Shipping_Country__c")) {
                    component.set("v.shippingCountryOptions", picklistJSON["Sales_Order_Shipping_Country__c"]);
                }
            } else {
                console.log("There was a problem retrieving Contact picklist values: " + state + ", " + JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(picklistOptionAction);
    },
    prepopulateAddress : function(opp, client, overwrite) {
        console.log("PREPOPULATE THE ADDRESS");
        if (overwrite) {
            opp.Sales_Order_Bill_To_Name__c = client.hasOwnProperty("BillToName__c") ? client.BillToName__c : "";
            opp.Sales_Order_Billing_Street_Address__c = client.hasOwnProperty("Billing_Street_Address__c") ? client.Billing_Street_Address__c : "";
            opp.Sales_Order_Billing_City__c = client.hasOwnProperty("Billing_City__c") ? client.Billing_City__c : "";
            opp.Sales_Order_Billing_State_Province__c = client.hasOwnProperty("Billing_State_Province__c") ? client.Billing_State_Province__c : "";
            opp.Sales_Order_Billing_ZIP_Postal_Code__c = client.hasOwnProperty("Billing_Zip_Postal_Code__c") ? client.Billing_Zip_Postal_Code__c : "";
            opp.Sales_Order_Billing_Country__c = client.hasOwnProperty("BillingCountry2__c") ? client.BillingCountry2__c : "";
            opp.Sales_Order_Ship_To_Name__c = client.hasOwnProperty("Ship_To_Name__c") ? client.Ship_To_Name__c : "";
            opp.Sales_Order_Shipping_Street_Address__c = client.hasOwnProperty("Shipping_Street__c") ? client.Shipping_Street__c : "";
            opp.Sales_Order_Shipping_City__c = client.hasOwnProperty("Shipping_City__c") ? client.Shipping_City__c : "";
            opp.Sales_Order_Shipping_State_Province__c = client.hasOwnProperty("Shipping_State__c") ? client.Shipping_State__c : "";
            opp.Sales_Order_Shipping_ZIP_Postal_Code__c = client.hasOwnProperty("Shipping_Zip_Postal_Code__c") ? client.Shipping_Zip_Postal_Code__c : "";
            opp.Sales_Order_Shipping_Country__c = client.hasOwnProperty("ShippingCountry2__c") ? client.ShippingCountry2__c : "";
        } else {
            if (!opp.Sales_Order_Bill_To_Name__c && client.hasOwnProperty("BillToName__c")) {
                opp.Sales_Order_Bill_To_Name__c = client.BillToName__c;
            } 
            if (!opp.Sales_Order_Billing_Street_Address__c && client.hasOwnProperty("Billing_Street_Address__c")) {
                opp.Sales_Order_Billing_Street_Address__c = client.Billing_Street_Address__c;
            }
            if (!opp.Sales_Order_Billing_City__c && client.hasOwnProperty("Billing_City__c")) {
                opp.Sales_Order_Billing_City__c = client.Billing_City__c;
            }
            if (!opp.Sales_Order_Billing_State_Province__c && client.hasOwnProperty("Billing_State_Province__c")) {
                opp.Sales_Order_Billing_State_Province__c = client.Billing_State_Province__c;
            }
            if (!opp.Sales_Order_Billing_ZIP_Postal_Code__c && client.hasOwnProperty("Billing_Zip_Postal_Code__c")) {
                opp.Sales_Order_Billing_ZIP_Postal_Code__c = client.Billing_Zip_Postal_Code__c;
            }
            if (!opp.Sales_Order_Billing_Country__c && client.hasOwnProperty("BillingCountry2__c")) {
                opp.Sales_Order_Billing_Country__c = client.BillingCountry2__c;
            }
            if (!opp.Sales_Order_Ship_To_Name__c && client.hasOwnProperty("Ship_To_Name__c")) {
                opp.Sales_Order_Ship_To_Name__c = client.Ship_To_Name__c;
            }
            if (!opp.Sales_Order_Shipping_Street_Address__c && client.hasOwnProperty("Shipping_Street__c")) {
                opp.Sales_Order_Shipping_Street_Address__c = client.Shipping_Street__c;
            }
            if (!opp.Sales_Order_Shipping_City__c && client.hasOwnProperty("Shipping_City__c")) {
                opp.Sales_Order_Shipping_City__c = client.Shipping_City__c;
            }
            if (!opp.Sales_Order_Shipping_State_Province__c && client.hasOwnProperty("Shipping_State__c")) {
                opp.Sales_Order_Shipping_State_Province__c = client.Shipping_State__c;
            }
            if (!opp.Sales_Order_Shipping_ZIP_Postal_Code__c && client.hasOwnProperty("Shipping_Zip_Postal_Code__c")) {
                opp.Sales_Order_Shipping_ZIP_Postal_Code__c = client.Shipping_Zip_Postal_Code__c;
            }
            if (!opp.Sales_Order_Shipping_Country__c && client.hasOwnProperty("ShippingCountry2__c")) {
                opp.Sales_Order_Shipping_Country__c = client.ShippingCountry2__c;
            }
        }
		
        return opp;
    },
    validateStateAbbreviation : function(abb){
        //Validate state abbreviation  
        console.log('state abbreviation for validation ' + abb);
        var isValid = false;        
        var states =    ["AK","AL","AR","AS","AZ","CA","CO","CT","DC","DE",
                         "FL","GA","GU","HI","IA","ID","IL","IN","KS","KY",
                         "LA","MA","MD","ME","MH","MI","MN","MO","MS","MT",
                         "NC","ND","NE","NH","NJ","NM","NV","NY","OH","OK",
                         "OR","PA","PR","PW","RI","SC","SD","TN","TX","UT",
                         "VA","VI","VT","WA","WI","WV","WY"];
        for(var i=0;i< states.length;i++) {
            if(abb == states[i]) {
                isValid = true;                
            }
            console.log('isValid after comparison to ' + states[i] + '=' + isValid);
        }
        console.log('validity for ' + abb + ' = ' + isValid);
        return isValid;
    },
})