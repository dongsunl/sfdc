({

    doInit: function (component, event, helper) {
        var result=decodeURIComponent((new RegExp('[?|&]' + "Id" + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
        component.set("v.OpportunityId" , result) ;
        helper.getOpportunityObject (component);

    },
    handleOppChange : function(component, event, helper) {
        if (!component.get("v.initialBrandLoadComplete")) {
            helper.getBrands(component);
            if(component.get('v.linkedBrands').length === 1){
                component.set('v.selectedBrandId', component.get('v.linkedBrands')[0].Id);
              //  console.log('set initial brand id: ' + component.get('v.linkedBrands')[0].Id);
            }
        }
    },
    handleBrandInfoUnknownChange : function(component, event, helper) {
        const brandInfoUnknown = component.get("v.brandInfoUnknown");
        const completeEvent = component.getEvent("brandInfoComplete");
        const brandTabStyleClasses = component.get("v.brandTabStyleClasses");
        const toggledStyles = helper.toggleBrandTabStyleClasses(brandTabStyleClasses, brandInfoUnknown);

        component.set("v.brandTabStyleClasses", toggledStyles);
        /*
        component.set("v.ShowBrandSetupIcon", brandInfoUnknown || component.get("v.ShowBrandLinkedIcon"));
        completeEvent.setParams({
            "isComplete": brandInfoUnknown || component.get("v.ShowBrandSetupIcon")
        });
        completeEvent.fire();
        */
        if (brandInfoUnknown) {
            helper.sendCompleteEvent(component, true, true, true);
        } else {
            helper.sendCompleteEvent(component, component.get("v.brandSetupComplete"), component.get("v.linkedBrandsComplete"), component.get("v.brandAdminComplete"));
        }
    },
    handleBrandTabClick: function (component, event, helper) {

        // Find the closest anchor tag, which will either be the tab itself, or an element's parent anchor tag.
        // Required in case the user clicks a child element, like <abbr/>. Ensures our target element is always the tab.
        var target = event.target.closest("a");
        var elem = target.getAttribute("tabindex");

        helper.handleBrandTabChange(elem)

    },
    handleComponentEvent: function (component, event, helper) {
        //console.log ('***** Message received Brand Container ************** ');
        //If there is only one linked brand when the wizard is loaded, make that brand the selected brand.
      //  console.log('number of linked brands ' + component.get('v.linkedBrands').length);
        if(component.get('v.linkedBrands').length === 1 && !component.get('v.selectedBrandId')){
            component.set('v.selectedBrandId', component.get('v.linkedBrands')[0].Id);
         //   console.log('set selected brand id on handle component event: ' + component.get('v.linkedBrands')[0].Id);
        }
        if (event.getParam("OpportunityObj") == undefined) {
            const brandSetupComplete = event.getParam("BrandSetupComplete") != undefined ? event.getParam("BrandSetupComplete") : component.get("v.brandSetupComplete");
            const linkedBrandsComplete = event.getParam("LinkedBrandsComplete") != undefined ? event.getParam("LinkedBrandsComplete") : component.get("v.linkedBrandsComplete");
            //const brandAdminComplete = event.getParam("BrandAdminComplete") != undefined ? event.getParam("BrandAdminComplete") : component.get("v.brandAdminComplete");
            let brandAdminComplete = event.getParam("BrandAdminComplete");
            if (brandAdminComplete === undefined) {
                helper.getBrands(component, function() {
                    const linkedBrands = component.get("v.linkedBrands");
                    if (linkedBrands.length > 0) {
                        brandAdminComplete = linkedBrands.filter(function(brand) {
                            return !brand.Brand_Admins__c && !(brand.Existing_Brand_Admin_Username__c && brand.Existing_Brand_Admin_Brand_ID__c);
                        }).length === 0;
                    } else {
                        brandAdminComplete = component.get("v.opportunity.Total_Number_of_Brands_Being_Purchased__c") == 0;
                    }
                    
                });
            }
            component.set("v.brandSetupComplete", brandSetupComplete);
            component.set("v.linkedBrandsComplete", linkedBrandsComplete);
            component.set("v.brandAdminComplete", brandAdminComplete);

            //var isLinkedBrand = event.getParam("LinkedBrandsComplete") == undefined ? '' : event.getParam("LinkedBrandsComplete");

            component.set("v.ShowBrandLinkedIcon", linkedBrandsComplete);
            //var isBrandSetup = component.get("v.brandInfoUnknown") || (event.getParam("BrandSetupComplete") == undefined ? '' : event.getParam("BrandSetupComplete"));
            //var isBrandSetup = component.get("v.brandInfoUnknown") || event.getParam("BrandSetupComplete");

            component.set("v.ShowBrandSetupIcon", component.get("v.brandInfoUnknown") || brandSetupComplete);
            //var isBrandAdmin = event.getParam("BrandAdminComplete") == undefined ? '' : event.getParam("BrandAdminComplete");

            //if (isBrandAdmin != undefined) {
            //    component.set("v.ShowBrandAdminIcon", isBrandAdmin);
            //}

            /*
            const completeEvent = component.getEvent("brandInfoComplete");
            const brandInfoTabsToComplete = [];
            let tabsComplete = true;
            if (!component.get("v.brandInfoUnknown")) {
                if (!component.get("v.ShowBrandLinkedIcon")) {
                    brandInfoTabsToComplete.push("Brand Setup");
                    brandInfoTabsToComplete.push("Linked Brands");
                    tabsComplete = false;
                }
                if (!component.get("v.ShowBrandSetupIcon")) {
                    brandInfoTabsToComplete.push("Admin Setup");
                    tabsComplete = false;
                }
            }
            completeEvent.setParams({
                //"isComplete": component.get("v.brandInfoUnknown") || (component.get("v.ShowBrandSetupIcon") && component.get("v.ShowBrandLinkedIcon"))
                "isComplete": tabsComplete,
                "tabsToComplete": brandInfoTabsToComplete
            });
            completeEvent.fire();
            */
            if (component.get("v.brandInfoUnknown")) {
                helper.sendCompleteEvent(component, true, true, true);
            } else {
                helper.sendCompleteEvent(component, brandSetupComplete, linkedBrandsComplete, brandAdminComplete);
            }
        }
    },
    handleNewBrandEvent : function(component, event, helper) {
        helper.getBrands(component);
        component.set("v.selectedBrandId", event.getParam("brandId"));
    },
    handleDeleteBrandEvent : function(component, event, helper) {
        const deleteBrandId = event.getParam("brandId");
        const action = component.get("c.DeleteBrandRecord");
        component.set("v.showSpinner", true);
        action.setParams({
            "BrandId": deleteBrandId,
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
          //  console.log('##### received getBrands');
            if (state === "SUCCESS") {
                
                helper.getBrands(component);
                //If there is only one brand left after a Brand is deleted, make the one remaining brand left the selected brand.
                if(component.get('v.linkedBrands').length === 1){
                    component.set('v.selectedBrandId', component.get('v.linkedBrands')[0].Id);
                  //  console.log('set selected brand id after brand delete: ' + component.get('v.linkedBrands')[0].Id);
                }
                if(component.get('v.isLightningOut') === false){
                    component.find('brandContainerNotifLib').showToast({
                        "title": "Brand Deleted!",
                        "message": "The brand has been deleted successfully.",
                        "variant": "success"
                    });
                }
                if (component.get("v.selectedBrandId") === deleteBrandId) {
                    component.set('v.selectedBrandId', null);
                }
            } else {
                let errors = response.getError();
                let message = "Unknown error.";
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                if(component.get('v.isLightningOut') === false){
                    component.find('brandContainerNotifLib').showToast({
                        "title": "There was a problem deleting the brand.",
                        "message": "Error: " + message,
                        "variant": "error"
                    });
                }
                component.set("v.showSpinner", false);
            }
        });
        $A.enqueueAction(action);
    },
    handleDeleteBrandsEvent : function(component, event, helper) {
        const oppId = event.getParam("oppId");
        const deleteBrandsAction = component.get('c.deleteLinkedBrands');
        deleteBrandsAction.setParams({ oppId: event.getParam("oppId") });
        deleteBrandsAction.setCallback(this, function(response) {
            const state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.linkedBrands", []);
                component.set('v.selectedBrandId', null);
            } else {
                const errors = response.getError();
                let errorMessage;
                if (errors && errors[0] && errors[0].message) {
                    errorMessage = errors[0].message;
                } else {
                    errorMessage = "Unknown error.";
                }
                if(component.get('v.isLightningOut') === false){
                    component.find('brandContainerNotifLib').showToast({
                        "title": "There was a problem deleting Linked Brand records",
                        "message": "Error: " + errorMessage,
                        "variant": "error"
                    });
                }
                console.log("Error deleteing Linked Brand records, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error));
            }
        });
        $A.enqueueAction(deleteBrandsAction);
    },
    handleUnsavedChanges : function(component, event, helper) {
        const tabName = event.getParam("tabName");
        const showUnsavedIndicator = event.getParam("showUnsavedIndicator");
        switch(tabName) {
            case 'Admin Setup':
                component.set("v.showUnsavedIndicatorAdminSetup", showUnsavedIndicator);
                break;
            case 'Brands to Merge':
                component.set("v.showUnsavedIndicatorBrandsToMerge", showUnsavedIndicator);
                break;
            case 'Users to Move':
                component.set("v.showUnsavedIndicatorUsersToMove", showUnsavedIndicator);
                break;
            case 'Onboarding Notes':
                component.set("v.showUnsavedIndicatorOnboardingNotes", showUnsavedIndicator);
                break;
            default:
                break;
        }
    },
    handleClearUnsavedChanges : function(component, event, helper) {
        component.set("v.showUnsavedIndicatorAdminSetup", false);
        component.set("v.showUnsavedIndicatorBrandsToMerge", false);
        component.set("v.showUnsavedIndicatorUsersToMove", false);
        component.set("v.showUnsavedIndicatorOnboardingNotes", false);
    }
})