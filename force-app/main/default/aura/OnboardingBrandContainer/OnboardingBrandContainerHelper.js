({
    handleBrandTabChange: function ( tabNumber) {

        for (var i = 1; i <= 6; i++) {
            var element = document.getElementById("brand-subtab-tabitem-0" + i);
            element.classList.add("slds-hide");
            element.classList.remove("slds-show");
            element = document.getElementById("brand-tab-menu-0" + i);
            element.classList.remove("slds-is-active");

        }
        var element = document.getElementById("brand-subtab-tabitem-0" + tabNumber);
        element.classList.add("slds-show");
        element.classList.remove("slds-hide");
        element = document.getElementById("brand-tab-menu-0" + tabNumber );
        element.classList.add("slds-is-active");
    } ,

    getOpportunityObject : function (cmp) {
        // Prepare the action to load account record
        var action = cmp.get("c.getOpportunity");
        action.setParams({
            "oppId": cmp.get("v.OpportunityId")
        });
        // Configure response handler
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var opp = response.getReturnValue() ;
              //  console.log ('%%%%%%%%%%  Sending Opportunity Brand Container ')
                //console.log(opp);

                var cmpEvent = $A.get("e.c:OnboardingBrandContainerEvent");
                cmpEvent.setParams({
                    "OpportunityObj": opp
                });
                cmpEvent.fire();

            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);


    },
    getBrands : function(component, successCallback) {
        component.set("v.showSpinner", true);
        const action = component.get("c.getBrandItems");
        action.setParams({
            "opportunityId": component.get("v.opportunity.Id"),
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.linkedBrands", response.getReturnValue());
                component.set("v.initialBrandLoadComplete", true);
                if (successCallback) {
                    successCallback();
                }
            } else {
                console.log('Problem getting linked brands, response state: ' + state);
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
    },
    collectBrandSummary: function(component){
        //Call component method on Linked Brands component
        //Get the info back and set the brand summary parameters.
    },
    toggleBrandTabStyleClasses: function(classString, brandInfoUnknown) {
        let classList = classString.split(" ");
        if (brandInfoUnknown) {
            classList = classList.concat("slds-hide");
        } else {
            classList = classList.filter(function(tabClass) { return tabClass != "slds-hide" });
        }
        return classList.join(" ");
    },
    sendCompleteEvent: function(component, brandSetupComplete, linkedBrandsComplete, brandAdminComplete) {
        const brandInfoTabsToComplete = [];
        let tabsComplete = true;
        if (!brandSetupComplete) {
            brandInfoTabsToComplete.push("Brand Setup");
            tabsComplete = false;
        }
        if (!linkedBrandsComplete) {
            brandInfoTabsToComplete.push("Linked Brands");
            tabsComplete = false;
        }
        if (!brandAdminComplete) {
            brandInfoTabsToComplete.push("Admin Setup");
            tabsComplete = false;
        }
        const completeEvent = component.getEvent("brandInfoComplete");
        completeEvent.setParams({
            //"isComplete": cmp.get("v.brandInfoUnknown") || (cmp.get("v.ShowBrandSetupIcon") && cmp.get("v.ShowBrandLinkedIcon"))
            "isComplete": tabsComplete,
            "tabsToComplete": brandInfoTabsToComplete
        });
        completeEvent.fire();
    }
})