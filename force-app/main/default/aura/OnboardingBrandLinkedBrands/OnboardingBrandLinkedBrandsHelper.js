({
    showSpinner: function (cmp, statusFlag) {
        cmp.set('v.Spinner', statusFlag);
    },
    getBrands: function (cmp) {
       // console.log('##### calling getBrands');
        var action = cmp.get("c.getBrandItems");
        
        action.setParams({
            "opportunityId": cmp.get("v.OpportunityId"),
            
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
           // console.log('##### received getBrands');
            if (state === "SUCCESS") {
                var brands = response.getReturnValue();
             //   console.log('brands')
             //   console.log(brands);
                cmp.set("v.tooManyBrands", false);
                cmp.set("v.SelectedBrands", brands.length);
                cmp.set("v.brands", brands);
                
                
                this.checkIfComplete(cmp);
                this.showSpinner(cmp, false);
                
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    
    getOpportunityWithClient: function (cmp, event, helper) {
       // console.log('Billing Address');
      //  console.log(cmp.get('v.OpportunityId'));
        // Prepare the action to load account record
        var action = cmp.get("c.getOpportunity");
        action.setParams({
            "oppId": cmp.get("v.OpportunityId")
        });
        // Configure response handler
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set('v.NumOfBrandsBought', 1);
                
                
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    deleteBrands: function (cmp, Id) {
   //     console.log('##### calling getBrands');
        var action = cmp.get("c.DeleteBrandRecord");
        
        action.setParams({
            "BrandId": Id,
            
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
          //  console.log('##### received getBrands');
            if (state === "SUCCESS") {
                
                this.getBrands(cmp);
                if(component.get('v.isLightningOut') === false){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type": "success",
                        "message": "Brand deleted successfully.",
                        duration: ' 2000',
                        type: 'success',
                        mode: 'dismissible'
                        
                    });
                    toastEvent.fire();
                }
                cmp.set('v.selectedBrandId', null);
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    selectBrand: function (cmp, Id) {
        
        
        var action = cmp.get("c.getBrand");
        
        action.setParams({
            "Id": Id
            
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var cmpEvent = $A.get("e.c:OnboardingBrandEvent");
                cmpEvent.setParams({"Brand": response.getReturnValue(), "CurrentBrand": Id});
                cmpEvent.fire();
                if(component.get('v.isLightningOut') === false){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type": "success",
                        "message": "Brand selected.",
                        duration: '500',
                        type: 'success',
                        mode: 'dismissible'
                        
                    });
                    toastEvent.fire();
                }
                this.checkIfComplete(cmp);
                
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
        
    },
    checkIfComplete: function (cmp) {
        
        
        var brands = cmp.get("v.brands");
       // console.log('*************  brands check  ************* ');
      //  console.log(brands);
      //  console.log(cmp.get('v.NumOfBrandsBought'));
        
        if (brands != undefined || brands.length == 0) {
            var usersToMove = 0;
            var usersToCreate = 0;
            var brandsToMerge = 0;
            
            for (var i = 0; brands.length > i; i++) {
                if (brands[i].Users_To_Create__c != undefined) {
                    var tmp = brands[i].Users_To_Create__c.split(';');
                    if (tmp != undefined) {
                        usersToCreate += tmp.length -1 ;
                    }
                }
                if (brands[i].Users_To_Move__c != undefined) {
                    var tmp = brands[i].Users_To_Move__c.split(';');
                    if (tmp != undefined) {
                        usersToMove += tmp.length -1 ;
                    }
                }
                if (brands[i].Brands_To_Merge__c != undefined) {
                    var tmp = brands[i].Brands_To_Merge__c.split(';');
                    if (tmp != undefined) {
                        brandsToMerge += tmp.length -1 ;
                    }
                }
            }            
            var cmpEvent1 = $A.get("e.c:OnboardingSummaryEvent");
            cmpEvent1.setParams({
                "UsersToMove": usersToMove,
                "UsersToCreate" : usersToCreate ,
                "BrandsToMerge" : brandsToMerge
            });
            cmpEvent1.fire();
        }
        
        //Does Not Qualify
        if (brands == undefined || brands.length == 0 || brands.length != cmp.get('v.NumOfBrandsBought')) {
            
            var cmpEventContainer = $A.get("e.c:OnboardingBrandContainerEvent");
            cmpEventContainer.setParams({
                "LinkedBrandsComplete": false,
                "BrandSetupComplete": false,
                "BrandAdminComplete": false
                
            });
            cmpEventContainer.fire();
            var cmpEvent = $A.get("e.c:OnboardingSummaryEvent");
            cmpEvent.setParams({
                "fromBrandCount": true,
                "brandCount": 0,
                "FromBrandInformation": true,
                "BrandInfoComplete": false
                
            });
          //  console.log('##### sending from Brand Links');
            cmpEvent.fire();
          //  console.log('Brands not complete');            
            if (brands.length > cmp.get('v.NumOfBrandsBought')) {
                cmp.set("v.tooManyBrands", true)
                cmp.set("v.BrandsNeededToDelete", brands.length - cmp.get('v.NumOfBrandsBought'));
            } else {
                cmp.set("v.tooManyBrands", false);
                cmp.set("v.BrandsNeededToDelete", 0);
            }
            return;
        }
                cmp.set("v.tooManyBrands", false);
        cmp.set("v.BrandsNeededToDelete", 0);
        
        var adminValid = true;
        if (brands.length == cmp.get('v.NumOfBrandsBought')) {
            for (var i = 0; brands.length > i; i++) {
                console.log("BRAND ADMIN FOR", brands[i], brands[i].Brand_Admins__c, !brands[i].Brand_Admins__c, !(brands[i].Existing_Brand_Admin_Username__c && brands[i].Existing_Brand_Admin_Brand_ID__c));
                if (!brands[i].Brand_Admins__c && !(brands[i].Existing_Brand_Admin_Username__c && brands[i].Existing_Brand_Admin_Brand_ID__c)) {
                    adminValid = false;
                }
            }
        } else {
            adminValid = false;
        }
        
        
        var cmpEventContainer = $A.get("e.c:OnboardingBrandContainerEvent");
        cmpEventContainer.setParams({
            "LinkedBrandsComplete": true,
            "BrandSetupComplete": true,
            "BrandAdminComplete": adminValid
        });
        //console.log('Calling Linked Brands OnboardingBrandContainerEvent LINE 181');
        cmpEventContainer.fire();
        
        var cmpEvent = $A.get("e.c:OnboardingSummaryEvent");
        cmpEvent.setParams({
            "fromBrandCount": true,
            "brandCount": brands.length,
            "BrandInfoComplete": adminValid,
            "FromBrandInformation": true
        });
        cmpEvent.fire();
        
        
        /*
        else {
            var cmpEventContainer = $A.get("e.c:OnboardingBrandContainerEvent");
            cmpEventContainer.setParams({
                "LinkedBrandsComplete": false,
                "BrandSetupComplete": false,
                "BrandAdminComplete": false

            });
            cmpEventContainer.fire();
            var cmpEvent = $A.get("e.c:OnboardingSummaryEvent");
            cmpEvent.setParams({
                "fromBrandCount": true,
                "brandCount": 0,
                "BrandInfoComplete": true,
                "FromBrandInformation": false,

            });
            console.log('##### sending from Brand Links');
            cmpEvent.fire();
        }
        */
        
    }
})