({
    doInit : function(component, event, helper) {
        component.find("brandCreator").getNewRecord(
            "Brand__c", 
            null, 
            false,
            $A.getCallback(function() {
                component.set("v.simpleNewBrand.Name", component.get("v.clientName"));
                component.set("v.simpleNewBrand.Opportunity__c", component.get("v.opportunityId"));
            })
        );
        helper.getPicklistOptions(component);
    },
    handleClientNameChange : function(component, event, helper) {
        component.set("v.simpleNewBrand.Name", component.get("v.clientName"));
    },
    handleOppIdChange : function(component, event, helper) {
        const action = component.get("c.hasRC1Product");
        action.setParams({
            "oppId": component.get("v.opportunityId")
        });
        // Configure response handler
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.hasRC1Product", response.getReturnValue());
                console.log("Has RC1 Product?", component.get("v.hasRC1Product"));
            } else {
                console.log('Problem verifying if Opportunity has RC1 product, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
        if (component.get("v.simpleNewBrand")) {
            component.set("v.simpleNewBrand.Opportunity__c", component.get("v.opportunityId"));
            component.find("oppRecordLoader").reloadRecord(false);
        } 
    },
    handleCreateBrand : function(component, event, helper) {
        const allValid = component.find('brandinfo-field').reduce(function (validSoFar, inputCmp) {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);
        if (allValid) {
            component.set("v.showSpinner", true);
            component.set("v.simpleNewBrand.SingleUserorEntryLicense__c", component.get("v.hasRC1Product"));
            if (component.get("v.simpleNewBrand.Client_Has_Existing_Brand__c")) {
                component.set("v.simpleNewBrand.Requested_Brand_Id__c", null);
            } else {
                component.set("v.simpleNewBrand.Existing_Brand_Id__c", null);
            }
            component.find("brandCreator").saveRecord($A.getCallback(function(saveResult) {
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                    // record is saved successfully
                    if(component.get('v.isLightningOut') === false){
                        component.find('brandSetupNotifLib').showToast({
                            "title": "Brand Created!",
                            "message": "The brand has been created successfully.",
                            "variant": "success"
                        });
                    }
                    console.log("fire new brand event!");
                    const newBrandEvent = component.getEvent("newBrandEvent");
                    newBrandEvent.setParams({
                        "brandId": saveResult.recordId
                    });
                    newBrandEvent.fire();

                    component.find("brandCreator").getNewRecord(
                        "Brand__c", 
                        null, 
                        false,
                        $A.getCallback(function() {
                            component.set("v.simpleNewBrand.Name", component.get("v.clientName"));
                            component.set("v.simpleNewBrand.Opportunity__c", component.get("v.opportunityId"));
                            component.set("v.showSpinner", false);
                        })
                    );
                } else if (saveResult.state === "INCOMPLETE") {
                    // handle the incomplete state
                    if(component.get('v.isLightningOut') === false){
                        component.find('brandSetupNotifLib').showToast({
                            "title": "Brand Creation Failed.",
                            "message": "User is offline, device doesn't support drafts.",
                            "variant": "error"
                        });
                    }
                    component.set("v.showSpinner", false);
                } else {
                    // handle the error state
                    const errors = saveResult.error;
                    let errorMessage;
                    if (errors && errors[0] && errors[0].message) {
                        errorMessage = errors[0].message;
                    } else {
                        errorMessage = "Unknown error.";
                    }
                    if(component.get('v.isLightningOut') === false){
                        component.find('brandSetupNotifLib').showToast({
                            "title": "There was a problem saving the Brand",
                            "message": "Error: " + errorMessage,
                            "variant": "error"
                        });
                    }
                    console.log('Problem saving contact, error: ' + JSON.stringify(saveResult.error));
                    component.set("v.showSpinner", false);
                }
            }));
        }
    },
    handleUnknown: function(component, event, helper) {
        if (component.get("v.tempOpp.Brand_Info_Unknown_at_Time_of_Invoice__c")) {
            const brandInfoUnknownConfirmation = confirm("This client will NOT be onboarded at the time of invoicing. The Onboarding Team will reach out directly to the Sales Account Owner for information. \n\nAll currently linked brands will be deleted. Are you sure you want to continue?");
            if (!brandInfoUnknownConfirmation) {
                component.set("v.tempOpp.Brand_Info_Unknown_at_Time_of_Invoice__c", false);
                return;
            }
        }
        component.set("v.showSpinner", true);
        component.find("oppRecordLoader").saveRecord($A.getCallback(function(saveResult) {
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                if (component.get("v.tempOpp.Brand_Info_Unknown_at_Time_of_Invoice__c")) {
                    const deleteBrandsEvent = component.getEvent("deleteBrandsEvent");
                    deleteBrandsEvent.setParams({
                        "oppId": component.get("v.opportunityId")
                    });
                    deleteBrandsEvent.fire();
                }
            } else if (saveResult.state === "INCOMPLETE") {
                if(component.get('v.isLightningOut') === false){
                    component.find('brandSetupNotifLib').showToast({
                        "title": "There was a problem saving the Opportunity",
                        "message": "User is offline, device doesn't support drafts.",
                        "variant": "error"
                    });
                }
                console.log("User is offline, device doesn't support drafts.");
            } else if (saveResult.state === "ERROR") {
                const errors = saveResult.error;
                let errorMessage;
                if (errors && errors[0] && errors[0].message) {
                    errorMessage = errors[0].message;
                } else {
                    errorMessage = "Unknown error.";
                }
                if(component.get('v.isLightningOut') === false){
                    component.find('brandSetupNotifLib').showToast({
                        "title": "There was a problem saving the Opportunity",
                        "message": "Error: " + errorMessage,
                        "variant": "error"
                    });
                }
            } else {
                if(component.get('v.isLightningOut') === false){
                    component.find('brandSetupNotifLib').showToast({
                        "title": "There was a problem saving the Opportunity",
                        "message": "Error: " + "Unknown problem, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error),
                        "variant": "error"
                    });
                }
                console.log("Unknown problem, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error));
            }
            component.set("v.showSpinner", false);
        }));
    }
})