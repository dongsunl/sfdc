({
    getPicklistOptions : function(component) {
        const picklistOptionAction = component.get("c.generatePicklistJSON");
        picklistOptionAction.setParams({"sObjectType": "Brand__c", "fieldApiNames": ["Brand_Type__c","Data_Center__c"]})
        picklistOptionAction.setCallback(this, function(response) {
            const state = response.getState();
            if (state === "SUCCESS") {
                const picklistJSON = JSON.parse(response.getReturnValue());
                if (picklistJSON.hasOwnProperty("Brand_Type__c")) {
                    component.set("v.brandTypes", picklistJSON["Brand_Type__c"]);
                }
                if (picklistJSON.hasOwnProperty("Data_Center__c")) {
                    component.set("v.dataCenters", picklistJSON["Data_Center__c"]);
                }
            } else {
                console.log("There was a problem retrieving Contact picklist values: " + state + ", " + JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(picklistOptionAction);
    }
})