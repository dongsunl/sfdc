({
    doInit: function (cmp, event, helper) {
        /*
        var result = decodeURIComponent((new RegExp('[?|&]' + "Id" + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null;
        cmp.set("v.OpportunityId", result);
        cmp.set("v.BrandOptions", JSON.parse("[{\"label\": \"Request Brand\", \"value\" : \"Id\"},{\"label\": \"Single User/Entry License\", \"value\" : \"single\"}]"));
        helper.getBrandLabels(cmp);
        var newObject = {Name: "",Requested_Brand_Id__c: "" , Data_Center__c : "" ,  Users_To_Create__c : "" ,UserTypeToMove : "" , Username : "" , Users_To_Move__c: "" , Brand_Type__c: "", Type__c: "" , BrandToMerge: ""  , Client_Has_Existing_Brand__c: "" , BrandOptions: "" , Existing_Brand_Id__c:"" , existingbranduser: "" , Existing_Brand_Admin_Username__c : "" , Brand_Admin__c :""    } ;
        cmp.set("v.newbrand" , newObject ) ; 
        */
        helper.showSpinner(cmp, false);
		cmp.find("brandRecordLoader").reloadRecord();
    },
    brandChange: function(cmp, event, helper) {
        cmp.find("brandRecordLoader").reloadRecord();
    },
    handleBrandRecordUpdated: function(cmp,event,helper){
        //Handles loading data and parsint out brands to merge string from brand record.
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            //record is loaded
            var brand = cmp.get("v.simpleBrandRecord");
            //console.log('brand record data loaded' + JSON.stringify(brand));
            var brandstomerge = brand.Brands_To_Merge__c;
            var brandarray = [];
            if(!!brandstomerge){
                brandarray = brandstomerge.split(';');
            }
            cmp.set('v.SelectedBrandsToMerge', brandarray);
            //console.log('selected brands to merge' + JSON.stringify(cmp.get('v.SelectedBrandsToMerge')));
        } else if(eventParams.changeType === "CHANGED") {
            // record is changed
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
        }  
    },
    handleComponentEvent : function (cmp, event, helper) {
        /*
        var OppObj = event.getParam("OpportunityObj") ;
        console.log('%%%%%%  Brand Notes');
        var opp = new Object();
        opp['Id'] = OppObj.Id;
        opp['OnboardingNotes_New__c'] = OppObj.OnboardingNotes_New__c;
        console.log(opp);
        cmp.set("v.Opportunity", opp);
        */
    },
    handleBrandComponentEvent : function (cmp, event, helper) {
        /*
        var brandObj = event.getParam("Brand") ;
        var obj = new Array;
        if (brandObj.Brands_To_Merge__c != undefined) {
            var arrObj = brandObj.Brands_To_Merge__c.split(';');
            for (var c = 0; arrObj.length > c; c++) {
                if (arrObj[c].length > 0) {
                    obj.push(arrObj[c]);
                }
            }
        }

        cmp.set('v.SelectedBrandsToMerge' , obj);
        cmp.set ('v.newbrand' , brandObj);
        console.log('#########  BrandToMerge' );
        console.log ( event.getParam("Brand") ) ;
        console.log ( event.getParam("CurrentBrand") ) ;
        console.log(brandObj ) ;
        */
    },
    SaveBrandToMerge : function (cmp, event, helper) {
        helper.showSpinner(cmp, true);
        //Concatenate Brands to Merge array into string and assign to the Brands to Merge field.
        var tmpArr = cmp.get("v.SelectedBrandsToMerge");
        var tmpValues = '';
        for (var i = 0 ; i < tmpArr.length; i++) {
            tmpValues += tmpArr[i] + ';'
        }
        tmpValues = tmpValues.substring(0, tmpValues.length - 1);
        cmp.set("v.simpleBrandRecord.Brands_To_Merge__c" , tmpValues);
        //Fire application event with ID of updated brand.
        var appEvent = $A.get("e.c:OnboardingBrandUpdatedEvent");
        appEvent.setParams({
            "brandId" : cmp.get('v.selectedBrandId') });
        appEvent.fire();
        //Save brand record via Lightning Data Service.
        cmp.find("brandRecordLoader").saveRecord($A.getCallback(function(saveResult) {
            helper.showSpinner(cmp, false);//Hide spinner
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                console.log('brand save result ' + saveResult.state);
                if(cmp.get('v.isLightningOut') === false){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type" : "success",
                        "message": "Brand(s) to Merge updated."
                    });
                    toastEvent.fire();
                }
                const unsavedChangesEvent = cmp.getEvent("unsavedChangesEvent");
                unsavedChangesEvent.setParams({
                    "tabName": "Brands to Merge",
                    "showUnsavedIndicator": false
                });
                unsavedChangesEvent.fire();
				cmp.find("brandRecordLoader").reloadRecord();                
            } else if (saveResult.state === "INCOMPLETE") {
                console.log("User is offline, device doesn't support drafts.");
            } else if (saveResult.state === "ERROR") {
                console.log('Problem saving record, error: ' + JSON.stringify(saveResult.error));
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
            }
        }));
        
        //helper.saveBrandRecord(cmp);
    } ,
    AddBrand: function (cmp, event, helper) {

        var errorMessage = '';
        var allValid = true;

        if (cmp.find('BrandId').get('v.validity').valueMissing ) {
            allValid = false;
            cmp.find('BrandId').showHelpMessageIfInvalid();
        }
        if (cmp.find('BrandId').get('v.validity').patternMismatch ) {
            allValid = false;
            cmp.find('BrandId').showHelpMessageIfInvalid();
        }


        if (allValid) {

            var users = cmp.get("v.newbrand.Brands_To_Merge__c");
            if (users == undefined) {
                users = "";
            }

            //if (cmp.get("v.newBrandEntry") == undefined ) {
            //    alert('The user data needs to be entered');
            //    return;
            //}
            console.log('******* users ********* ');
            console.log(users) ;
            console.log(users.length) ;

            if (users.length > 0) {
                users += ';' + cmp.get("v.newBrandEntry")  ;
            } else {
                users =  cmp.get("v.newBrandEntry") ;
            }
            //brandToMergeCount++ ;
            //cmp.set("v.newbrand.Brands_To_Merge__c", users);
            var obj = cmp.get('v.SelectedBrandsToMerge');
            obj.push( cmp.get("v.newBrandEntry")) ;
            cmp.set('v.SelectedBrandsToMerge' , obj);
            cmp.set("v.newBrandEntry", "");
            //if (confirm("Do you want to create add another User?")) {
            //    var ele = cmp.find('BrandId');
                //ele.focus();

            //}
            const unsavedChangesEvent = cmp.getEvent("unsavedChangesEvent");
            unsavedChangesEvent.setParams({
                "tabName": "Brands to Merge",
                "showUnsavedIndicator": true
            });
            unsavedChangesEvent.fire();
        } else {
            if(cmp.get('v.isLightningOut') === false){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Required fields are missing. Please update and save again.",
                    duration:' 2000',
                    mode: 'dismissible'
                    
                });
                toastEvent.fire();
            }
        }
        

    },
    removeBrandToMerge : function (cmp, event, helper) {

        var dataToRemove = event.target.dataset.index ;
        var tmpArr = cmp.get("v.SelectedBrandsToMerge");
        var newArr = new Array();
        for (var i = 0 ; i < tmpArr.length; i++) {
            if (tmpArr[i] != dataToRemove) {
                newArr.push ( tmpArr[i] );
            }
        }
        cmp.set("v.SelectedBrandsToMerge", newArr );
        const unsavedChangesEvent = cmp.getEvent("unsavedChangesEvent");
        unsavedChangesEvent.setParams({
            "tabName": "Brands to Merge",
            "showUnsavedIndicator": true
        });
        unsavedChangesEvent.fire();
    },
    editBrandToMerge : function (cmp, event, helper) {

        var dataToRemove = event.target.dataset.index ;

        var tmpArr = cmp.get("v.SelectedBrandsToMerge");
        console.log(tmpArr);
        var idx = tmpArr.indexOf(dataToRemove) ;
        tmpArr.splice(idx, 1);
        console.log(tmpArr);
        cmp.set("v.newBrandEntry", dataToRemove );
        cmp.get("v.SelectedBrandsToMerge" , tmpArr);


    }
})