({
    showSpinner : function (cmp , statusFlag) {
        cmp.set('v.Spinner' , statusFlag);
    } ,
    saveBrandRecord: function (cmp) {

        var action = cmp.get("c.SaveBrandRecord");
        console.log ('Starting savingg brands to merge');
        var tmpArr = cmp.get("v.SelectedBrandsToMerge");
        var tmpValues = '';
        for (var i = 0 ; i < tmpArr.length; i++) {
            tmpValues += tmpArr[i] + ';'
        }
        tmpValues = tmpValues.substring(0, tmpValues.length - 2);
        cmp.set("v.newbrand.Brands_To_Merge__c" , tmpValues);

        action.setParams({
            "dataString": JSON.stringify(cmp.get("v.newbrand")),
            "OppId": cmp.get("v.OpportunityId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                //cmp.set("v.NewContactObj", response.getReturnValue());
                //cmp.set("v.isContactSelected", true);
                window.scrollTo(0, 0);
                var brandData = response.getReturnValue();
                var cmpEvent = $A.get("e.c:OnboardingBrandEvent");
                cmpEvent.setParams({"Brand": brandData, "CurrentBrand": brandData.Id});
                cmpEvent.fire();
                /*
                var cmpEvent = $A.get("e.c:OnboardingSummaryEvent");
                cmpEvent.setParams({
                    "fromBrandCount": true,
                    "brandToMergeCount": brandToMergeCount ,

                });
                console.log('##### sending from Brand Links') ;
                cmpEvent.fire();
                */
                this.showSpinner(cmp, false);
                if(cmp.get('v.isLightningOut') === false){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type": "success",
                        "message": "Brand Record updated.",
                        duration:' 2000',
                        type: 'success',
                        mode: 'dismissible'
                    });
                    toastEvent.fire();
                }

                /*
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success Message',
                    message: 'Brand Record Created',
                    messageTemplate: 'Brand Record Created',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'pester'
                });
                toastEvent.fire();
                */
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    getBrandLabels: function (cmp) {

        // Prepare the action to load account record
        var action = cmp.get("c.getBrandLabels");


        // Configure response handler
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                cmp.set("v.BrandMap", response.getReturnValue());
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    }
})