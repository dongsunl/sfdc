({
    doInit: function (cmp, event, helper) {

        var result=decodeURIComponent((new RegExp('[?|&]' + "Id" + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
        cmp.set("v.OpportunityId" , result) ;
        helper.getClientList (cmp, event, helper ) ;
        helper.getBillingCountriesList (cmp, event, helper ) ;
        helper.getShippingCountriesList (cmp, event, helper ) ;
        helper.getAccountBillingData (cmp, event, helper ) ;



    } ,

    createNewClient : function (cmp, event, helper) {
        var isChecked = cmp.find("NewClientCheckBox").get("v.checked");
        cmp.set('v.CreateNew' , isChecked) ;

    },
    differentBillingAddress : function (cmp, event, helper) {
        var isChecked = cmp.find("iDifferentBillingAddress").get("v.checked");
        cmp.set('v.DifferentBillingAddress' , isChecked) ;

    } ,
    differentEndUser : function (cmp, event, helper) {
        var isChecked = cmp.find("iDifferentEndUser").get("v.checked");
        cmp.set('v.DifferentEndUser' , isChecked) ;

    } ,
    saveRecord : function (cmp, event, helper) {
        console.log(cmp.get('v.ClientData')) ;
        console.log('******** calling controller') ;
        helper.saveClient(cmp);
    } ,
    handleSelectClient: function (cmp, event, helper) {
        helper.getSelectedClient(cmp);
    },
    handleRemoveClient: function (cmp, event, helper) {
        helper.removeSelectedClient(cmp);
    }
})