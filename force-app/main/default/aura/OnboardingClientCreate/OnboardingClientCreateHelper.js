({
    getClientList : function(cmp, event, helper) {

        // Prepare the action to load account record
        var action = cmp.get("c.getClients");

        action.setParams({"opportunityId": cmp.get('v.OpportunityId')});

        // Configure response handler
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {

                cmp.set("v.ClientList", JSON.parse(response.getReturnValue()));
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    getBillingCountriesList : function(cmp, event, helper) {

        // Prepare the action to load account record
        var action = cmp.get("c.getBillingCountries");

        // Configure response handler
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {

                cmp.set("v.BillingCountries", JSON.parse(response.getReturnValue()));
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    getShippingCountriesList : function(cmp, event, helper) {

        // Prepare the action to load account record
        var action = cmp.get("c.getShippingCountries");

        // Configure response handler
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {

                cmp.set("v.ShippingCountries", JSON.parse(response.getReturnValue()));
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    getAccountBillingData : function(cmp) {

        // Prepare the action to load account record
        var action = cmp.get("c.getBillingInfo");
        action.setParams({"opportunityId": cmp.get('v.OpportunityId')});
        // Configure response handler
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                console.log ('Response getBillingInfo') ;
                console.log( response.getReturnValue() ) ;
                cmp.set("v.ClientData", response.getReturnValue());
                console.log(cmp.get('v.ClientData')) ;
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    } ,
    saveClient : function(cmp) {
        var action = cmp.get("c.SaveClientRecord");
        var dataString = JSON.stringify(cmp.get('v.ClientData')) ;
        console.log(dataString) ;
        action.setParams({"dataString": dataString});
        // Configure response handler
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {

                var cmpEvent = $A.get("e.c:OnboardingSummaryEvent");
                console.log(cmp.get("v.ClientData"));
                cmpEvent.setParams({"ClientObj": cmp.get("v.ClientData"), "ClientSelected":true , "FromClient" : true});
                cmpEvent.fire();
                cmp.set("v.isSelected" , true);
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    getSelectedClient : function(cmp) {
        var action = cmp.get("c.getSelectedClient");
        action.setParams({"clientId": cmp.get("v.SelectedClient")});
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.ClientData", response.getReturnValue());
                console.log(response.getReturnValue());
                console.log('Sending the event');
                // = cmp.getEvent("cmpEvent");
                var cmpEvent = $A.get("e.c:OnboardingSummaryEvent");
                console.log(cmp.get("v.ClientData"));
                cmpEvent.setParams({"ClientObj": cmp.get("v.ClientData"), "ClientSelected":true , "FromClient" : true ,  "SelectedClientId" : cmp.get("v.SelectedClient") });
                cmpEvent.fire();
                cmp.set("v.isSelected" , true);
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    removeSelectedClient : function(cmp) {
        var cmpEvent = $A.get("e.c:OnboardingSummaryEvent");
        console.log(cmp.get("v.ClientData"));
        cmpEvent.setParams({"ClientObj": null , "ClientSelected":false , "FromClient" : true});
        cmpEvent.fire();
        cmp.set("v.isSelected" , false);
        cmp.set("v.CreateNew" , false );
        this.getAccountBillingData ( cmp);
    }


})