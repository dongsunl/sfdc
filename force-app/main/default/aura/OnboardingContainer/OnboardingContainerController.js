({
    doInit: function(cmp, event, helper) {
        if(cmp.get('v.recordId') != undefined && cmp.get('v.recordId') != null){
            cmp.set("v.opportunityId", cmp.get('v.recordId'));
            cmp.find("opportunityData").reloadRecord(true);
        }
         var oppIdToSend;
        if (!cmp.get("v.opportunityId")) {
            const oppId = decodeURIComponent((new RegExp('[?|&]' + "Id" + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
            cmp.set("v.opportunityId", oppId);
            cmp.find("opportunityData").reloadRecord(true);
             oppIdToSend = oppId;
        }else{oppIdToSend = cmp.get('v.opportunityId');}
        //Send Opportunity Id to Finish and Bill Component
        var fabCmp = cmp.find("fabcontainer");
        if(!!fabCmp){
            fabCmp.refreshOppId(oppIdToSend);
        }
    },
    reloadOpportunity: function(component, event, helper) {
        component.find("opportunityData").reloadRecord(true);  
        component.set("v.showSpinner", true);
    },
    saveOpportunity: function(component, event, helper) {
        const oppObject = event.getParam("oppObject");
        const opportunityData = component.find("opportunityData");
        component.set("v.simpleRecord", oppObject);
        component.set("v.showSpinner", true);
        opportunityData.saveRecord($A.getCallback(function(saveResult) {
            if (saveResult.state === "INCOMPLETE") {
                if(component.get('v.isLightningOut') === false){
                    component.find('containerNotifLib').showToast({
                        "title": "There was a problem saving the Opportunity",
                        "message": "User is offline, device doesn't support drafts.",
                        "variant": "error"
                    });
                }
                console.log("User is offline, device doesn't support drafts.");
                component.find("opportunityData").reloadRecord(false); 
            } else if (saveResult.state === "ERROR") {
                const errors = saveResult.error;
                let errorMessage;
                if (errors && errors[0] && errors[0].message) {
                    errorMessage = errors[0].message;
                } else {
                    errorMessage = "Unknown error.";
                }
                if(component.get('v.isLightningOut') === false){
                    component.find('containerNotifLib').showToast({
                        "title": "There was a problem saving the Opportunity",
                        "message": "Error: " + errorMessage,
                        "variant": "error"
                    });
                }
                console.log("Problem saving record, error: " + JSON.stringify(saveResult.error));
                component.find("opportunityData").reloadRecord(false); 
            } else if (saveResult.state !== "SUCCESS" && saveResult.state !== "DRAFT") {
                if(component.get('v.isLightningOut') === false){
                    component.find('containerNotifLib').showToast({
                        "title": "There was a problem saving the Opportunity",
                        "message": "Error: " + "Unknown problem, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error),
                        "variant": "error"
                    });
                }
                component.find("opportunityData").reloadRecord(false); 
                console.log("Unknown problem, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error));
            }
        }));
    },
    handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "CHANGED") {
            // get the fields that changed for this record
            var changedFields = eventParams.changedFields;
          //  console.log('Fields that are changed: ' + JSON.stringify(changedFields));
            // record is changed, so refresh the component (or other component logic)
            if(component.get('v.isLightningOut') === false){
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Opportunity Updated!",
                    "message": "The Opportunity has been updated successfully.",
                    "variant": "success"
                });
            
            resultsToast.fire();
            }
            component.find("opportunityData").reloadRecord(false, $A.getCallback(function(reloadResult) {
                component.set("v.showSpinner", false);
            })); 
        } else if(eventParams.changeType === "LOADED") {
            //const clientName = component.get("v.simpleRecord.Client__r.Name"); 
            //component.set("v.ShowClientIcon", clientName != false);
          //  console.log("Opportunity has loaded");
            helper.markTabsComplete(component, helper, component.get("v.simpleRecord"));
            component.set("v.showSpinner", false);
            component.set("v.showBrandInfoTab", component.get("v.simpleRecord.RecordType.Name") != 'Panels');
            if(component.get("v.simpleRecord.Products__c") != null){
                component.set("v.oppProductsContainDelighted", component.get("v.simpleRecord.Products__c").includes('Delighted'));//Hide brand info tab if Opportunity Products contains 'Delighted' by setting oppProductsContainDelighted to TRUE
            }
            // record is loaded in the cache
        } else if(eventParams.changeType === "REMOVED") {
           // console.log("REMOVED?");
            component.set("v.showSpinner", false);
            // record is deleted and removed from the cache
        } else if(eventParams.changeType === "ERROR") {
           // console.log("ERROR?:", component.get("v.error"));
            component.set("v.showSpinner", false);
            // there’s an error while loading, saving or deleting the record
        }
    },
    handleComponentEvent: function (cmp, event, helper) {
      //  console.log('*****  Message received from event ********** ');


        /*********************   Billing Information Tab  ***************/
        var counter = cmp.get('v.BillingInfoIconParent');

        var isFromClient = event.getParam("FromClient") == undefined ? '' : event.getParam("FromClient");
        if (isFromClient) {
            var clientSelected = event.getParam("ClientSelected") == undefined ? '' : event.getParam("ClientSelected");
        //    console.log('Client Selected: ');
         //   console.log(clientSelected);
            if (clientSelected != null) {
               // console.log(clientSelected);
                // set the handler attributes based on event data
                cmp.set("v.ShowClientIcon", clientSelected);
                clientSelected ? counter++ : counter--;

              //  console.log('Client object from event')
            }
        }
        var isFromBilling = event.getParam("FromBillingAddress") == undefined ? '' : event.getParam("FromBillingAddress");
        if (isFromBilling) {
            var contactSelected = event.getParam("BillingAddressSelected") == undefined ? '' : event.getParam("BillingAddressSelected");

            if (contactSelected != null) {

                // set the handler attributes based on event data
                cmp.set("v.ShowBillingAddressIcon", contactSelected);
                contactSelected ? counter++ : counter--;
              //  console.log('From Billing Address from event')
            }
        }
        var isFromBilling = event.getParam("FromBillingContact") == undefined ? '' : event.getParam("FromBillingContact");
        if (isFromBilling) {
            var contactSelected = event.getParam("BillingContactSelected") == undefined ? '' : event.getParam("BillingContactSelected");

            if (contactSelected != null) {

                // set the handler attributes based on event data
                cmp.set("v.ShowBillingContactIcon", contactSelected);
                contactSelected ? counter++ : counter--;
              //  console.log('From Billing Contact from event')
            }
        }

        var isAddBilling = event.getParam("FromAdditional") == undefined ? '' : event.getParam("FromAdditional");

        if (isAddBilling) {
            var isCompleted = event.getParam("AdditionalBillingComplete") == undefined ? '' : event.getParam("AdditionalBillingComplete");
           // console.log('isCompleted ' + isCompleted ) ;
            if (isCompleted != null) {

                // set the handler attributes based on event data
                cmp.set("v.ShowAdditionalBillingIcon", isCompleted);
                isCompleted ? counter++ : counter--;

              //  console.log('Additional Billing Object from event')
            }
        }
        cmp.set('v.BillingInfoIconParent' , counter);

        /*********************   End Billing Information Tab  ***************/



        var isFromBilling = event.getParam("FromBillingInfo") == undefined ? '' : event.getParam("FromBillingInfo");
        if (isFromBilling) {
            var contactSelected = event.getParam("BillingInfoComplete") == undefined ? '' : event.getParam("BillingInfoComplete");

            if (contactSelected != null) {

                // set the handler attributes based on event data
                cmp.set("v.BillingInfoIcon", contactSelected);
                //console.log(contactSelected);
              //  console.log('Billing Object from event')
            }
        }
        var isFromBilling = event.getParam("FromInformation") == undefined ? '' : event.getParam("FromInformation");
        if (isFromBilling) {
            var contactSelected = event.getParam("InformationComplete") == undefined ? '' : event.getParam("InformationComplete");

            if (contactSelected != null) {

                // set the handler attributes based on event data
                cmp.set("v.InformationIcon", contactSelected);
              //  console.log(contactSelected);
              //  console.log('Information from event')
            }
        }


        /*
        var isFromBillingAdmin = event.getParam("FromBrandInformation") == undefined ? '' : event.getParam("FromBrandInformation");
        if (isFromBillingAdmin) {
            var billingAdminObj = event.getParam("BrandInfoComplete") == undefined ? '' : event.getParam("BrandInfoComplete");
            console.log("HERE NOW!!!");
            console.log(billingAdminObj);
            // set the handler attributes based on event data
            cmp.set("v.BrandInformationIcon", billingAdminObj);

        }
        */





    },
    handleTab1: function (cmp, event, helper) {
        helper.handleBillingInformationTabChange ('1');
    },
    handleTab2: function (cmp, event, helper) {
        helper.handleBillingInformationTabChange ('2');
    },
    handleTab3: function (cmp, event, helper) {

        helper.handleBillingInformationTabChange ('3');
    },
    handleTab4: function (cmp, event, helper) {

        helper.handleBillingInformationTabChange ('4');
    } ,

    handleMainTab1: function (cmp, event, helper) {

        helper.handleMainTabChange ('1')
    },
    handleMainTab2: function (cmp, event, helper) {

        helper.handleMainTabChange ('2')
    },
    handleMainTab3: function (cmp, event, helper) {
        helper.handleMainTabChange ('3')
    },
    handleMainTab4: function (cmp, event, helper) {
        helper.handleMainTabChange ('4')

    },

    returnToOpportunity : function (cmp, event, helper) {
        var result=decodeURIComponent((new RegExp('[?|&]' + "Id" + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
        var type =decodeURIComponent((new RegExp('[?|&]' + "Type" + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
        if (type = '006') {
            var returnUrl = 'https://' + location.hostname + '/' + result ;
        } else {
            var returnUrl = 'https://' + location.hostname + '/apex/RedirectionManager?sobject=opportunity&id=' + result + '&action=finish_and_bill';
        }

        window.location.replace(returnUrl );
    },
    handleBrandInfoComplete : function(component, event, helper) {
        //console.log("GOT HERE");
       // console.log(event.getParam("isComplete"));
        component.set("v.BrandInformationIcon", event.getParam("isComplete"));
        component.set("v.brandInfoTabsToComplete", event.getParam("tabsToComplete"));
        helper.checkTabsToComplete(component);
    }
})