({
    handleBillingInformationTabChange: function ( tabNumber) {

        for (var i = 1; i < 5; i++) {
            var element = document.getElementById("subtab-tabpanel-0" + i);
            element.classList.add("slds-hide");
            element.classList.remove("slds-show");
            element = document.getElementById("tab-menu-0" + i);
            element.classList.remove("slds-is-active");

        }
        var element = document.getElementById("subtab-tabpanel-0" + tabNumber);
        element.classList.add("slds-show");
        element.classList.remove("slds-hide");
        element = document.getElementById("tab-menu-0" + tabNumber );
        element.classList.add("slds-is-active");
    } ,
    handleMainTabChange: function (tabNumber) {
      //  console.log('tabNumber: ' + tabNumber);
        for (var i = 1; i <= 4; i++) {
         //   console.log(i);
            var element = document.getElementById("maintab-tabpanel-0" + i);
            if (element) {
                element.classList.add("slds-hide");
                element.classList.remove("slds-show");
                element = document.getElementById("maintab-menu-0" + i);
                element.classList.remove("slds-is-active");
            }
        }
        var element = document.getElementById("maintab-tabpanel-0" + tabNumber );
        element.classList.add("slds-show");
        element.classList.remove("slds-hide");
        element = document.getElementById("maintab-menu-0" + tabNumber);
        element.classList.add("slds-is-active");
    },
    handleBrandTabChange: function ( tabNumber) {

        for (var i = 1; i <= 6; i++) {
            var element = document.getElementById("brand-subtab-tabitem-0" + i);
            element.classList.add("slds-hide");
            element.classList.remove("slds-show");
            element = document.getElementById("brand-tab-menu-0" + i);
            element.classList.remove("slds-is-active");

        }
        var element = document.getElementById("brand-subtab-tabitem-0" + tabNumber);
        element.classList.add("slds-show");
        element.classList.remove("slds-hide");
        element = document.getElementById("brand-tab-menu-0" + tabNumber );
        element.classList.add("slds-is-active");
    },
    markTabsComplete: function(component, helper, fields) {
        component.set("v.ShowClientIcon", fields.Client__c && fields.Client__r.Name);
        component.set("v.ShowBillingAddressIcon", this.checkRequiredAddressFields(fields));
        component.set("v.ShowBillingContactIcon", !!fields.Billing_Contact__c);
        component.set("v.ShowAdditionalBillingIcon", this.checkRequiredAdditionalBillingFields(component, fields));
        component.set("v.ShowBillingInfoIcon", this.checkBillingInformationTabs(component));
        component.set("v.ShowSummaryIcon", fields.OnboardingWizardUsed__c);
        helper.checkTabsToComplete(component);
    },
    checkRequiredAddressFields: function(fields) {
        return fields.Sales_Order_Bill_To_Name__c && fields.Sales_Order_Billing_Street_Address__c &&
               fields.Sales_Order_Billing_Street_Address__c && fields.Sales_Order_Billing_City__c &&
               fields.Sales_Order_Billing_Country__c && fields.Sales_Order_Ship_To_Name__c &&
               fields.Sales_Order_Shipping_Street_Address__c && fields.Sales_Order_Shipping_City__c &&
               fields.Sales_Order_Shipping_Country__c
    },
    checkRequiredAdditionalBillingFields : function(component, fields) {
        let billingFieldsComplete = fields.Invoice_Email_Language__c && fields.Payment_Terms__c && fields.ARR_NRR__c;
        if (billingFieldsComplete && component.get("v.simpleRecord.RecordType.Name") !== 'Panels') {
            billingFieldsComplete = billingFieldsComplete && fields.Auto_Renewal__c && fields.Is_this_a_non_cancelable_multi_year_deal__c && ((fields.Renewal_Price_Increase__c === "Yes" && fields.Annual_Price_Increase__c) || fields.Renewal_Price_Increase__c === "No");
            if(component.get('v.oppProductsContainDelighted') == false){
                billingFieldsComplete = billingFieldsComplete && fields.Incumbent__c && fields.Primary_Competitor__c;
            }
        } else if (billingFieldsComplete && component.get("v.simpleRecord.RecordType.Name") === 'Panels') {
            billingFieldsComplete = billingFieldsComplete && fields.Estimated_Project_End_Date__c && fields.Study_Type__c && fields.Panel_Demographics_Invoice_Display__c;
        }
        return billingFieldsComplete;
    },
    checkBillingInformationTabs : function(component) {
        return component.get("v.ShowClientIcon") && component.get("v.ShowBillingAddressIcon") &&
               component.get("v.ShowBillingContactIcon") && component.get("v.ShowAdditionalBillingIcon");
    },
    checkTabsToComplete : function(component) {
        let tabsToCompleteList = [];
        if (!component.get("v.ShowClientIcon")) {
            tabsToCompleteList.push("Client");
        }
        if (!component.get("v.ShowBillingAddressIcon")) {
            tabsToCompleteList.push("Billing Address");
        }
        if (!component.get("v.ShowBillingContactIcon")) {
            tabsToCompleteList.push("Billing Contact");
        }
        if (!component.get("v.ShowAdditionalBillingIcon")) {
            tabsToCompleteList.push("Additional Billing Information");
        }
    //    console.log("TABS TO COMPLETE LIST", tabsToCompleteList);
        if(component.get('v.oppProductsContainDelighted') == false && component.get("v.simpleRecord.RecordType.Name") !== 'Panels'){
            tabsToCompleteList = tabsToCompleteList.concat(component.get("v.brandInfoTabsToComplete"));
        }
     //   console.log("FULL TABS TO COMPLETE LIST", tabsToCompleteList);
        component.set("v.tabsToComplete", tabsToCompleteList.join(", "));
    }
})