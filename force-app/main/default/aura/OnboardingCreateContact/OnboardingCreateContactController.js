({
    doInit : function(component, event, helper) {
        helper.getPicklistOptions(component);
        helper.getNewContactRecord(component);
    },
    handleEmailChange : function(component, event, helper) {
        if (component.get("v.showRequestedUsername")) {
            const email = component.get("v.contactFields.Email");
            const username = component.get("v.contactFields.Requested_Username__c");
            if (username != email) {
                component.set("v.contactFields.Requested_Username__c", email);
            }
        }
    },
    handleCreateContact : function(component, event, helper) {
        const allValid = component.find("new-contact-field").reduce(function(validSoFar, field) {
        	field.reportValidity();
        	return validSoFar && field.checkValidity();
        }, true);
        if (allValid) {
            component.set("v.contactFields.AccountId", component.get("v.accountId"));
            if (component.get("v.clientId")) {
                component.set("v.contactFields.Client__c", component.get("v.clientId"));
            }
            if (component.get("v.showRequestedUsername") && !component.get("v.contactFields.Requested_Username__c")) {
                component.set("v.contactFields.Requested_Username__c", component.get("v.contactFields.Email"));
            }
            if (!component.get("v.contactFields.Status__c")) {
                component.set("v.contactFields.Status__c", "Active Customer");
            } 
            if (!component.get("v.contactFields.LeadSource")) {
                component.set("v.contactFields.LeadSource", "Outbound - Sales");
            }
            helper.saveContact(component, helper);
        }
    },
})