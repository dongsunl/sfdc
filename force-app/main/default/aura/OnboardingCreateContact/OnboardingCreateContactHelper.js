({
    getPicklistOptions : function(component) {
        const picklistOptionAction = component.get("c.generatePicklistJSON");
        picklistOptionAction.setParams({"sObjectType": "Contact", "fieldApiNames": ["Contact_Role__c", "Status__c", "LeadSource"]})
        picklistOptionAction.setCallback(this, function(response) {
            const state = response.getState();
            if (state === "SUCCESS") {
                const picklistJSON = JSON.parse(response.getReturnValue());
                if (picklistJSON.hasOwnProperty("Contact_Role__c")) {
                    component.set("v.contactLevels", picklistJSON["Contact_Role__c"]);
                }
                if (picklistJSON.hasOwnProperty("Status__c")) {
                    component.set("v.contactStatuses", picklistJSON["Status__c"]);
                }
                if (picklistJSON.hasOwnProperty("LeadSource")) {
                    component.set("v.contactSources", picklistJSON["LeadSource"]);
                }
            } else {
                console.log("There was a problem retrieving Contact picklist values: " + state + ", " + JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(picklistOptionAction);
    },
    getNewContactRecord : function(component) {
        component.find("contactCreator").getNewRecord(
            "Contact",
            null,
            false,
            $A.getCallback(function() {
                const rec = component.get("v.contact");
                const error = component.get("v.contactError");
                if (error || (rec === null)) {
                    if(component.get("v.isLightningOut") === false){
                        component.find('billingContactNotifLib').showToast({
                            "title": "Contact Form Initialization Error",
                            "message": "Error initializing Contact record template: " + error,
                            "variant": "error"
                        });
                    }
                    return;
                }
                component.set("v.contactFields.Status__c", "Active Customer");
                component.set("v.contactFields.LeadSource", "Outbound - Sales");
            })
        );
    },
    saveContact : function(component, helper) {
        component.set("v.showSpinner", true);
        component.find("contactCreator").saveRecord($A.getCallback(function(saveResult) {
            const saveEvent = component.getEvent("contactSaved");
            saveEvent.setParams({
                "saveResponse": saveResult,
                "savedContactId": saveResult.recordId
            });
            saveEvent.fire();
            const state = saveResult.state;
            if (state === "SUCCESS" || state === "DRAFT") {
                helper.getNewContactRecord(component);
            }
            component.set("v.showSpinner", false);
        }));
    }
})