({
    doInit: function (cmp, event, helper) {
        //var result=decodeURIComponent((new RegExp('[?|&]' + "Id" + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
        //cmp.set("v.OpportunityId" , result) ;
        helper.getOpportunityLines(cmp, event, helper );
        //helper.getOpportunityWithClient(cmp);

    },
    handleOppChange : function(component, event, helper) {
        component.set("v.opportunityId", component.get("v.opportunity.Id"));
        component.set("v.billingContactId", component.get("v.opportunity.Billing_Contact__c"));
        component.find("opportunityRecordLoader").reloadRecord();
        component.find("billingContactRecordLoader").reloadRecord();
        if (!component.get("v.oppLinesLoadComplete")) {
            helper.getOpportunityLines(component, event, helper);
        }
        if (!component.get("v.oppPageRef")) {
            const navService = component.find("navService");
            const pageRef = {
                "type": "standard__recordPage",
                "attributes": {
                    "recordId": component.get("v.opportunityId"),
                    "objectApiName": "Opportunity",
                    "actionName": "view"
                }
            }
            component.set("v.oppPageRef", pageRef);
        }
    },
    handleComponentEvent: function (cmp, event, helper) {
      //  console.log('Summary handle event');

       if (event.getParam("brandCount") != undefined ) {

            cmp.set("v.BrandCount", event.getParam("brandCount") );
        }

        if (event.getParam("brandUsetToSetupCount") != undefined ) {
            cmp.set("v.BrandsToMerge", event.getParam("UsersToMoveCount") );
        }

        var isFromClient = event.getParam("FromClient") == undefined ? '' : event.getParam("FromClient");
        if (isFromClient) {
            var clientObj = event.getParam("ClientObj") == undefined ? '' : event.getParam("ClientObj");


            // set the handler attributes based on event data
            cmp.set("v.ClientData", clientObj);

        }
        var isFromBilling = event.getParam("FromBilling") == undefined ? '' : event.getParam("FromBilling");
        if (isFromBilling) {
            var billingObj = event.getParam("BillingContact") == undefined ? '' : event.getParam("BillingContact");

            // set the handler attributes based on event data
            cmp.set("v.BillingContact", billingObj);

        }
        var isFromBrand = event.getParam("fromBrandCount") == undefined ? '' : event.getParam("fromBrandCount");
      //  console.log('$$$$$$$ isFromBrand' + isFromBrand );
      //  console.log(isFromBrand );
        if (isFromBrand) {
         //   console.log('##### received from Brand Links') ;
            // set the handler attributes based on event data
            //cmp.set("BrandCount", event.getParam("brandCount") );

        }

        var isFromBillingAdmin = event.getParam("FromBrandInformation") == undefined ? '' : event.getParam("FromBrandInformation");
        if (isFromBillingAdmin) {
            var billingAdminObj = event.getParam("BrandInfoComplete") == undefined ? '' : event.getParam("BrandInfoComplete");

            // set the handler attributes based on event data
            cmp.set("v.BrandInformationIcon", billingAdminObj);

        }
        var counter = cmp.get("v.BillingInfoIconParent") ;

        var isFromClient = event.getParam("FromClient") == undefined ? '' : event.getParam("FromClient");
        if (isFromClient) {
            var clientSelected = event.getParam("ClientSelected") == undefined ? '' : event.getParam("ClientSelected");
          //  console.log('Client Selected: ');
          //  console.log(clientSelected);
            if (clientSelected != null) {
             //   console.log(clientSelected);
                // set the handler attributes based on event data
                cmp.set("v.ShowClientIcon", clientSelected);
                clientSelected ? counter++ : counter--;

              //  console.log('Client object from event')
            }
        }
        var isFromBilling = event.getParam("FromBillingAddress") == undefined ? '' : event.getParam("FromBillingAddress");
        if (isFromBilling) {
            var contactSelected = event.getParam("BillingAddressSelected") == undefined ? '' : event.getParam("BillingAddressSelected");

            if (contactSelected != null) {

                // set the handler attributes based on event data
                cmp.set("v.ShowBillingAddressIcon", contactSelected);
                contactSelected ? counter++ : counter--;
              //  console.log('From Billing Address from event')
            }
        }
        var isFromBilling = event.getParam("FromBillingContact") == undefined ? '' : event.getParam("FromBillingContact");
        if (isFromBilling) {
            var contactSelected = event.getParam("BillingContactSelected") == undefined ? '' : event.getParam("BillingContactSelected");

            if (contactSelected != null) {

                // set the handler attributes based on event data
                cmp.set("v.ShowBillingContactIcon", contactSelected);
                contactSelected ? counter++ : counter--;
              //  console.log('From Billing Contact from event')
            }
        }

        var isAddBilling = event.getParam("FromAdditional") == undefined ? '' : event.getParam("FromAdditional");

        if (isAddBilling) {
            var isCompleted = event.getParam("AdditionalBillingComplete") == undefined ? '' : event.getParam("AdditionalBillingComplete");
           // console.log('isCompleted ' + isCompleted ) ;
            if (isCompleted != null) {

                // set the handler attributes based on event data
                cmp.set("v.ShowAdditionalBillingIcon", isCompleted);
                isCompleted ? counter++ : counter--;

              //  console.log('Additional Billing Object from event')
            }
        }
        cmp.set('v.BillingInfoIconParent' , counter);

        if (cmp.get("v.BillingInfoIconParent") == 4 &&   cmp.get("v.BrandInformationIcon" ) )  {
            cmp.set("v.VerifyToggle" , false) ;

        }
        if ( event.getParam("UsersToMove") != undefined ) {

            cmp.set('v.UsersToMoveCount' , event.getParam("UsersToMove") );
        }
        if ( event.getParam("UsersToCreate") != undefined ) {

            cmp.set('v.AdminCount' , event.getParam("UsersToCreate") );
        }
        if ( event.getParam("BrandsToMerge") != undefined ) {

            cmp.set('v.BrandsToMerge' , event.getParam("BrandsToMerge") );
        }





    },
    hideProducts : function (cmp, event, helper) {
        document.getElementById("hideProducts").style.display = "block";
        document.getElementById("showProducts").style.display = "none";
        document.getElementById("OppProducts").style.display = "none";
        document.getElementById("productTable").style.visibility = "collapse";
       // console.log('hide products');
    } ,

    showProducts : function (cmp, event, helper) {
        document.getElementById("hideProducts").style.display = "none";
        document.getElementById("showProducts").style.display = "block";
        document.getElementById("OppProducts").style.display = "block";
        document.getElementById("productTable").style.visibility = "visible";
        //console.log('show products');
    } ,

    toggleProductTable : function (component, event, handler) {
        component.set("v.showProductTable", !component.get("v.showProductTable"));
        component.set("v.toggleIcon", component.get("v.showProductTable") ? "utility:chevrondown" : "utility:chevronright");
        document.getElementById("OppProducts").classList.toggle("slds-hide");
    },

    handleRemoveBillingContact : function (cmp, event, helper) {
        /*
        var cmpEvent = $A.get("e.c:OnboardingSummaryEvent");
        cmpEvent.setParams({"BillingContact": null , "ContactSelected":false , "FromBilling" : true});
        cmpEvent.fire();
        */
        cmp.set("v.simpleOpportunity.Billing_Contact__c", null);
        cmp.set("v.showSpinner", true);
        cmp.find("opportunityRecordLoader").saveRecord($A.getCallback(function(saveResult) {
            if (saveResult.state === "INCOMPLETE") {
                if(component.get('v.isLightningOut') === false){
                    cmp.find('summaryNotifLib').showToast({
                        "title": "There was a problem saving the Opportunity",
                        "message": "User is offline, device doesn't support drafts.",
                        "variant": "error"
                    });
                }
                console.log("User is offline, device doesn't support drafts.");
            } else if (saveResult.state === "ERROR") {
                const errors = saveResult.error;
                let errorMessage;
                if (errors && errors[0] && errors[0].message) {
                    errorMessage = errors[0].message;
                } else {
                    errorMessage = "Unknown error.";
                }
                if(component.get('v.isLightningOut') === false){
                    cmp.find('summaryNotifLib').showToast({
                        "title": "There was a problem saving the Opportunity",
                        "message": "Error: " + errorMessage,
                        "variant": "error"
                    });
                }
                console.log("Problem saving record, error: " + JSON.stringify(saveResult.error));
            } else if (saveResult.state !== "SUCCESS" && saveResult.state !== "DRAFT") {
                if(component.get('v.isLightningOut') === false){
                    cmp.find('summaryNotifLib').showToast({
                        "title": "There was a problem saving the Opportunity",
                        "message": "Error: " + "Unknown problem, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error),
                        "variant": "error"
                    });
                }
                console.log("Unknown problem, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error));
            }
            cmp.set("v.showSpinner", false);
        }));
    } ,
    handleRemoveClient : function (cmp, event, helper) {
        /*
        var cmpEvent = $A.get("e.c:OnboardingSummaryEvent");
        cmpEvent.setParams({"ClientObj": null , "ClientSelected":false , "FromClient" : true});
        cmpEvent.fire();
        */
        const simpleOpportunity = cmp.get("v.simpleOpportunity");
        simpleOpportunity.Client__c = null;
        simpleOpportunity.Sales_Order_Bill_To_Name__c = null;
        simpleOpportunity.Sales_Order_Billing_Street_Address__c = null;
        simpleOpportunity.Sales_Order_Billing_City__c = null;
        simpleOpportunity.Sales_Order_Billing_State_Province__c = null;
        simpleOpportunity.Sales_Order_Billing_ZIP_Postal_Code__c = null;
        simpleOpportunity.Sales_Order_Billing_Country__c = null;
        simpleOpportunity.Sales_Order_Shipping_Street_Address__c = null;
        simpleOpportunity.Sales_Order_Shipping_City__c = null;
        simpleOpportunity.Sales_Order_Shipping_State_Province__c = null;
        simpleOpportunity.Sales_Order_Shipping_ZIP_Postal_Code__c = null;
        simpleOpportunity.Sales_Order_Shipping_Country__c = null;
        cmp.set("v.simpleOpportunity", simpleOpportunity);
        cmp.set("v.showSpinner", true);
        cmp.find("opportunityRecordLoader").saveRecord($A.getCallback(function(saveResult) {
            if (saveResult.state === "INCOMPLETE") {
                if(component.get('v.isLightningOut') === false){
                    cmp.find('summaryNotifLib').showToast({
                        "title": "There was a problem saving the Opportunity",
                        "message": "User is offline, device doesn't support drafts.",
                        "variant": "error"
                    });
                }
                console.log("User is offline, device doesn't support drafts.");
            } else if (saveResult.state === "ERROR") {
                const errors = saveResult.error;
                let errorMessage;
                if (errors && errors[0] && errors[0].message) {
                    errorMessage = errors[0].message;
                } else {
                    errorMessage = "Unknown error.";
                }
                if(component.get('v.isLightningOut') === false){
                    cmp.find('summaryNotifLib').showToast({
                        "title": "There was a problem saving the Opportunity",
                        "message": "Error: " + errorMessage,
                        "variant": "error"
                    });
                }
                console.log("Problem saving record, error: " + JSON.stringify(saveResult.error));
            } else if (saveResult.state !== "SUCCESS" && saveResult.state !== "DRAFT") {
                if(component.get('v.isLightningOut') === false){
                    cmp.find('summaryNotifLib').showToast({
                        "title": "There was a problem saving the Opportunity",
                        "message": "Error: " + "Unknown problem, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error),
                        "variant": "error"
                    });
                }
                console.log("Unknown problem, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error));
            }
            cmp.set("v.showSpinner", false);
        }));
    } ,
    confirmAndClose : function (component, event, helper) {
        //Need logic to Save
        //var result=decodeURIComponent((new RegExp('[?|&]' + "Id" + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
        //window.location.replace("/" + result);
        component.set("v.showSpinner", true);
        if (component.get("v.enableVerify")) {
            component.set("v.simpleOpportunity.OnboardingWizardUsed__c", true);
            component.find("opportunityRecordLoader").saveRecord($A.getCallback(function(saveResult) {
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                    event.preventDefault();
                    ///if (component.get("v.isLightningOut")) {
                    //    window.location.href = "/" + component.get("v.opportunityId");
                    //} else {
                    //    const navService = component.find("navService");
                    //    const pageRef = component.get("v.oppPageRef");
                    //    navService.navigate(pageRef);
                    //}
                    var completeEvent = component.getEvent("summarycomplete");
                    completeEvent.fire();
                    const fabReloadEvent = $A.get("e.c:fab_Reload");
                    fabReloadEvent.fire();
                } else if (saveResult.state === "INCOMPLETE") {
                    if(component.get('v.isLightningOut') === false){
                        component.find('summaryNotifLib').showToast({
                            "title": "There was a problem saving the Opportunity",
                            "message": "User is offline, device doesn't support drafts.",
                            "variant": "error"
                        });
                    }
                    console.log("User is offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR") {
                    const errors = saveResult.error;
                    let errorMessage;
                    if (errors && errors[0] && errors[0].message) {
                        errorMessage = errors[0].message;
                    } else {
                        errorMessage = "Unknown error.";
                    }
                    if(component.get('v.isLightningOut') === false){
                        component.find('summaryNotifLib').showToast({
                            "title": "There was a problem saving the Opportunity",
                            "message": "Error: " + errorMessage,
                            "variant": "error"
                        });
                    }
                    console.log("Problem saving record, error: " + JSON.stringify(saveResult.error));
                } else {
                    if(component.get('v.isLightningOut') === false){
                        component.find('summaryNotifLib').showToast({
                            "title": "There was a problem saving the Opportunity",
                            "message": "Error: " + "Unknown problem, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error),
                            "variant": "error"
                        });
                    }
                    console.log("Unknown problem, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error));
                }
                component.set("v.showSpinner", false);
            }));
        } else {
            if(component.get('v.isLightningOut') === false){
                component.find('summaryNotifLib').showToast({
                    "title": "Unable to Complete Verification",
                    "message": "The Billing Information and Brand Information tabs must be completed before verification. Please complete all tabs and re-verify.",
                    "variant": "error"
                });
            }
            component.set("v.showSpinner", false);
        }
    }
    
    
})