({
    getOpportunityLines : function(cmp, event, helper ) {

        // Prepare the action to load account record
        var action = cmp.get("c.getOppLineItems");
        var currencyIsoCode = cmp.get("v.opportunity.CurrencyIsoCode") ? cmp.get("v.opportunity.CurrencyIsoCode") : "USD"

        action.setParams({"opportunityId": cmp.get('v.opportunityId')});

        // Configure response handler
        action.setCallback(this, function(response) {
            var state = response.getState();

            var formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: currencyIsoCode,
                minimumFractionDigits: 2
            })
            if(state === "SUCCESS") {
				//console.log ( response.getReturnValue() );
				var items = response.getReturnValue() ;
                cmp.set("v.OppProductItems", items );
                var total = 0;
                for (var i = 0 ; i < items.length ; i++ ) {
                    total += items[i].TotalPrice;

                }

                cmp.set("v.TotalProducts" , formatter.format(total));
                cmp.set("v.oppLinesLoadComplete", true);

            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    getOpportunityWithClient : function (cmp, event, helper) {
     //   console.log('Billing Address');
    //    console.log(cmp.get('v.OpportunityId') );
        // Prepare the action to load account record
        var action = cmp.get("c.getOpportunity");
        action.setParams({
            "oppId": cmp.get("v.OpportunityId")
        });
        // Configure response handler
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var opp = response.getReturnValue();

                cmp.set("v.ClientData", opp);
              //  console.log(opp);
                //this.showSpinner(cmp, false);
                //ocument.getElementById("main-content").style.display = "block";
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    } ,
})