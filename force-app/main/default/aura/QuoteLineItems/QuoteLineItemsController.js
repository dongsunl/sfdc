({
    doInit : function(component, event, helper) {
        console.log('QuoteLineItems: doInit');
        console.log(component.get('v.recordId'));
        var action = component.get('c.getQuoteLines');
        action.setParams({
            quoteId : component.get('v.recordId')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            if(state == 'SUCCESS') {
                component.set('v.quoteLineItems', response.getReturnValue());
                component.set('v.countOfQLIs', response.getReturnValue().length);
                //console.log(response.getReturnValue());
                component.set('v.spinner', false);
            }
        });
        component.set('v.spinner', true);
        $A.enqueueAction(action);
    },

    handleRecordUpdated : function(component, event, helper) {

    }
})