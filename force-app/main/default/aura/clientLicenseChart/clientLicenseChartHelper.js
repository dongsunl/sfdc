({
    doInit : function(component, event, helper) 
    {
        var action = component.get("c.getClientLicensesJSON");
        
         action.setParams({ accountId : component.get('v.recordId') });

        action.setCallback(this,function(response) { 
            var state = response.getState();
            if (state === "SUCCESS") {   
               var dataObj= response.getReturnValue();
               component.set("v.data",dataObj);
               // helper.donutchart(component,event,helper);
                helper.Linechart(component,event,helper);
            } else if (state === "ERROR") { 
                var errors = response.getError();
                if (errors) {
                   
                    if (errors[0] && errors[0].message) {
                        throw new Error("Error" + errors[0].message);
                    }
                } else {
                    throw new Error("Unknown Error");
                }
            }
            
        });
        
        $A.enqueueAction(action);
    },
   
    donutchart : function(component,event,helper) {
        var jsonData = component.get("v.data");
        
        var dataObj = JSON.parse(jsonData);
        console.log(dataObj);
        new Highcharts.Chart({
            chart: {
                renderTo: component.find("donutchart").getElement(),
                type: 'bar'
                
            },
           
            title: {
                text: component.get("v.chartTitle")
            },
            subtitle: {
                text: component.get("v.chartSubTitle")
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                   
                }
            },
            series: [{
                type: 'bar',
                name:'Bundle__c',
                data:dataObj
            }]
            
        });
        
    },
    
    Linechart : function(component,event,helper) {
        var jsonData = component.get("v.data");
        var dataObj = JSON.parse(jsonData);
        var categories=[];
        console.log(dataObj);
        dataObj.forEach(function(d){
            categories.push(d.name);
        })
        console.log(categories);
      //  component.set('v.xAxisCategories',dataObj.name);
        new Highcharts.Chart({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                renderTo: component.find("linechart").getElement(),
                type: 'bar'
            },
            title: {
                text: component.get("v.chartTitle")
            },
            subtitle: {
                text: component.get("v.chartSubTitle")
            },
            xAxis: {
                categories: categories
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            series: [{
                type: 'bar',
                data:dataObj
            }]
            
        });
        
    },
})