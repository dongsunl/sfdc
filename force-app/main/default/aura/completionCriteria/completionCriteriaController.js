({
    doInit: function(component, event, helper) {
        var criteria = component.get("v.criteria");
        if (criteria.isComplete) {
            component.set("v.iconName", "utility:check");
            component.set("v.iconVariant", "brand");
        }
    },
	handleSelection : function(component, event, helper) {
		helper.executeSelection(component);
	}
})