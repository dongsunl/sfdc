({
	doInit : function(component, event, helper) {
    //    console.log('ClientLicenseLineItem Controller: Init');
		//Get the price into the correct currency format
		//console.log('price formatting method running');
        var currencycode = component.get("v.license.CurrencyIsoCode");
        var price = component.get("v.license.Invoice_Amount__c");
        var locale = component.get("v.license.Culture_Code__c");
        var priceformatted = price.toLocaleString(locale, { style: 'currency', currency: currencycode });
        component.set("v.price", priceformatted);
	}
})