({
    doInit : function(component, event, helper) {
       // console.log('ClientLicenseTable Controller: Init');
        //console.log('get groupings');
        component.set("v.showspinner", true);
        var actionacomplete = false;
        var actionbcomplete = false;
        var action = component.get("c.getLicenseGroupings");
        var groupingsinitialized = false;
        action.setParams({ 
            accountid : component.get("v.accountId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var groupings = response.getReturnValue();
                //    console.log('groupings: ' + groupings);
                component.set("v.groupings", groupings);
                actionacomplete = true;
                if(actionbcomplete){
                    component.set("v.showspinner", false);
                    if(!groupingsinitialized){
                        groupingsinitialized = true;
                        helper.setUpGroupings(component);}
                }
                
            }
            else if (state === "INCOMPLETE") {
                //console.log('load license groups incomplete');
                if(actionbcomplete){
                    component.set("v.showspinner", false);}
                actionacomplete = true;
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error loading license groups");
                    }            
                    if(actionbcomplete){
                        component.set("v.showspinner", false);}
                    actionacomplete = true;
                }
        });
        $A.enqueueAction(action);
        
        //Get licenses
        var baction = component.get("c.getLicenses");
        baction.setParams({ 
            accountid : component.get("v.accountId")
        });
        baction.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var licenses = response.getReturnValue();
                component.set("v.licenses", licenses);
                //console.log('clientlicensetable licenses ' + JSON.stringify(component.get("v.licenses")));
                if(actionacomplete){
                    component.set("v.showspinner", false);
                    if(!groupingsinitialized){
                        groupingsinitialized = true;
                        helper.setUpGroupings(component);
                    }                    
                }
                actionbcomplete = true;
            }
            else if (state === "INCOMPLETE") {
               // console.log('load licenses operation incomplete');
                if(actionacomplete){
                    component.set("v.showspinner", false);}
                actionbcomplete = true;
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error loading client licenses");
                    }            
                    if(actionacomplete){
                        component.set("v.showspinner", false);}
                    actionbcomplete = true;
                }
        });
        $A.enqueueAction(baction);
    },
    returnClients : function(component, event, helper){
       // console.log('client licenses returnClients method running');
        var clients = [];//Array for clients (deduplicated)
        var licensegroups = component.find("licensegroup"); //Array of license groups
        if(licensegroups.length === undefined && !!licensegroups){//If there is only one license group
          //  console.log('there is only one licenses group');
            if(!!licensegroups.returnClient()){
                clients.push(licensegroups.returnClient());
            }
            else{console.log('group is not selected');}
        }
        else{
           // console.log('there are multiple license groups');
            var clientsSeen = {};//Object to use to compare in deduplication.
            for(var i = 0;i < licensegroups.length;i++){                
                //Add if not already in array
                if(!!licensegroups[i].returnClient()){
                    var currentclient = licensegroups[i].returnClient();
                    console.log('current client: ' + JSON.stringify(currentclient));
                    if(clientsSeen[currentclient.Id] !== currentclient.Name + ' ' + currentclient.Bundle){
                        clients.push(currentclient);
                        clientsSeen[currentclient.Id] = currentclient.Name + ' ' + currentclient.Bundle;
                    }
                }
                else{'group is not selected'};
            }
        }
       // console.log('deduped clients: ' + JSON.stringify(clients));
        return clients;
    },
    returnAllLicenses : function(component, event, helper){
     //   console.log('client licenses returnAllLicenses method running');
        var allLicenses = [];//Array for licenses from all groups selected
        var licensegroups = component.find("licensegroup"); //Array of license groups
        if(licensegroups.length === undefined && !!licensegroups){//If there is only one license group
            //console.log('there is only one license group');
            allLicenses = licensegroups.returnGroupLicenses();
        }
        else{
            //console.log('there is more than one license group');
            for(var i = 0;i < licensegroups.length;i++){                
                if(!!licensegroups[i].returnGroupLicenses()){
                    var currentgroup = licensegroups[i].returnGroupLicenses();
                    //console.log('current license group: ' + JSON.stringify(currentgroup));
                    allLicenses = allLicenses.concat(currentgroup);
                    
                }
            }
        }        
        //console.log('all selected licenses: ' + JSON.stringify(allLicenses));
        return allLicenses;
    },
    handleSelectEvent : function(component, event, helper) {
     //   console.log('ClientLicenseTable Controller: handleSelectEvent');
        // console.log('license table component handling select event: ' + JSON.stringify(event.getParams()));
        var selectedgroup = event.getParam("group");
        //  console.log('selected group' + selectedgroup);
        component.set("v.selectedgroup", selectedgroup);
    },
    handleGroupInfo : function(component, event, helper) {
       // console.log('ClientLicenseTable Controller: handleGroupInfo');
        // console.log('complete group info received: ' + JSON.stringify(event.getParams()));
        var newgroupinfo = event.getParam("group");
        //Old group format: ClientId|BundleId|LicenseEndDate|Status|ClientName|BundleName.
        //New group format: ClientId|BundleId|LicenseEndDate|Status|ClientName|BundleName|Start|Value.
        var oldgroupinfo = newgroupinfo.substring(0,newgroupinfo.lastIndexOf('|'));//Remove the value
        oldgroupinfo = oldgroupinfo.substring(0,oldgroupinfo.lastIndexOf('|'));//Remove the start
        var groupings = component.get("v.groupings");
        var groupindex = groupings.indexOf(oldgroupinfo);
        if(groupindex > -1){
            groupings.splice(groupindex,1,newgroupinfo);
            component.set("v.groupings", groupings);  
        }
    },
    clientSort : function(component, event, helper) {
      //  console.log('ClientLicenseTable Controller: clientSort');
        //  console.log('client sort triggered');
        var column = 'client';
        helper.columnSort(component, column);
    },
    bundleSort : function(component, event, helper) {
      //  console.log('ClientLicenseTable Controller: bundleSort');
        var column = 'bundle';
        helper.columnSort(component, column);
    },
    startSort : function(component, event, helper) {
        //  console.log('ClientLicenseTable Controller: startSort');
        var column = 'start';
        helper.columnSort(component, column);
    },
    endSort : function(component, event, helper) {
      //  console.log('ClientLicenseTable Controller: endSort');
        var column = 'end';
        helper.columnSort(component, column);
    },
    statusSort : function(component, event, helper) {
       // console.log('ClientLicenseTable Controller: statusSort');
        var column = 'status';
        helper.columnSort(component, column);
    },
    valueSort : function(component, event, helper) {
        // console.log('ClientLicenseTable Controller: valueSort');
        var column = 'value';
        helper.columnSort(component, column);
    } 
})