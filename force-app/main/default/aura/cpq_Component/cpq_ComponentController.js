({     
    afterScriptsLoaded : function (component,event,helper){
       // console.log('CMP CTRL: afterScriptsLoaded');
        //console.log(component.get('v.quoteType'))
        //  component.set("v.showspinner", true);
        //console.log('CMP CTRL: afterScriptsLoaded - callServer getXmQuoting');
        var compEvent = component.getEvent("loading");
        compEvent.fire();
        helper.callServer(
            component,
            "c.getXmQuoting",
            function(response){
                component.set('v.quotingPermissions', response);
                //console.log('CMP CTRL: afterScriptsLoaded - CALL HLPR setAvailableExperiences');
                helper.setAvailableExperiences(component, event, helper, response);
            },{
                
            },true
        );
        //console.log('CMP CTRL: afterScriptsLoaded - callServer getOpportunity');
        helper.callServer(
            component,
            "c.getOpportunity",
            function(response){
                component.set('v.opportunity', response);
                component.set('v.restrictedStatus', response.Restricted_Access_Status__c)
            },
            {
                opportunityId : component.get('v.opportunityId')
            }
        );
        //console.log('CMP CTRL: afterScriptsLoaded - callServer getLoc');
        helper.callServer(
            component,
            "c.getLoc",
            function(response){
                component.set('v.locale', response);
            },
            {
            },true
        );
         //console.log('CMP CTRL: afterScriptsLoaded - callServer getct');
        helper.callServer(
            component,
            "c.getct",
            function(response){
                component.set('v.currencyConversionRates', response);
                _.filter(response, function(value,key) { 
                    if(key === component.get('v.opportunitycurrencycode')){
                        component.set('v.currency', value);
                    }
                });
                //component.set("v.showspinner", false);
                
            },
            {
            },true
        );
         //console.log('CMP CTRL: afterScriptsLoaded - callServer getOppTeam');
        helper.callServer(
            component,
            "c.getOppTeam",
            function(response){
                if(response===false){
                    component.set('v.editable',true);
                }else{
                    component.set('v.editable',false);
                }
                var compEvent = component.getEvent("doneloading");
                compEvent.fire();
            },
            {
                opportunityId:component.get('v.opportunityId'),
                accountId:component.get('v.accountId')
            },true
        );
        
        helper.callServer(
            component,
            "c.getDealDeskAccess",
            function(response) {
                component.set('v.dealDeskAccess', response);
            },
            {
                
            }
        )
    },
    
    selectExperience : function(component, event, helper){  
        component.set("v.showspinner", true);
         //console.log('------------------SELECT EXPERIENCE------------------'); 
        //console.log('CMP CTRL: selectExperience'); 
        var opportunityId = component.get('v.opportunityId');
        component.set('v.opportunityId',opportunityId);
        component.set('v.quoteType','New License Quote');
        component.set('v.actionType','Create');
        
        var exp = component.find('expSelect').get('v.value');
        var experiences = component.get('v.experienceMetaData');
        var experience = _.filter(experiences, {'Code__c' : exp});
       // console.log(exp);
      //  console.log(experience);
        component.set('v.experience', experience);
        component.set('v.exp',exp);  
        component.set("v.showspinner", false);
    },
    
    onAddExperience : function(component, event, helper){ 
        //console.log('CMP CTRL: addExperience');
        var selectedPriceList = event.getParam('selectedPriceList').replace('__c', '');
        var priceListOptions = component.get('v.priceListOptions');
        selectedPriceList = _.findIndex(priceListOptions, ['label', selectedPriceList]);
        if (selectedPriceList > -1) {
            priceListOptions[selectedPriceList].selected = true;
        }
        var exp = event.getParam('experience');
        var experiences = component.get('v.experienceMetaData');
        var experience = _.filter(experiences, {'Code__c' : exp});
        component.set('v.xmProducts', event.getParam('xmProducts'));
        component.set('v.experience', experience);
        component.set('v.exp',exp);  
        component.set('v.quoteId',event.getParam('quoteId'));
        component.set('v.quoteType','New License Quote');
        component.set('v.actionType','Add');
        component.set('v.selectedPriceList', event.getParam('selectedPriceList').replace('__c', ''));
        component.set('v.priceListOptions', priceListOptions);
        component.set('v.quoteName', event.getParam('quoteName'));
        component.set('v.quoteStartDate', event.getParam('quoteStartDate'));
        component.set('v.quoteEndDate', event.getParam('quoteEndDate'));
        component.set('v.quoteTerm', event.getParam('quoteTerm'));
    },
    
    onEditExperience : function(component, event, helper){ 
        //console.log('CMP CTRL: editExperience');
        component.set('v.exp',event.getParam('experience'));
        var xmProducts = event.getParam('xmProducts').split('; ');
        var bundleName;
        _.filter(xmProducts, function(product) {
            if(product.includes(event.getParam('experience'))) {
                bundleName = product;
            } else if(product.includes('360') && event.getParam('experience')==='X360') {
                bundleName = product;
            }
        })
        component.set('v.bundleName', bundleName);
        var quoteType = event.getParam('quoteType');
        if(quoteType != 'New License') {
            component.set('v.quoteType', 'Upgrade License');
            if(quoteType == 'Merge Licenses') {
                component.set('v.upgradeType', 'merge');
            } else if(quoteType == 'Core Upgrade - Same Renewal Date' || quoteType == 'Non-Core Add-On') {
                component.set('v.upgradeType', 'keep-dates');
            } else if(quoteType == 'Core Upgrade - Change Renewal Date') {
                component.set('v.upgradeType', 'change-dates');
            } else if(quoteType == 'License Change at Renewal') {
                component.set('v.upgradeType', 'on-renewal');
            }
        }
        else {
            component.set('v.quoteType', 'New License Quote');
        }
        var selectedPriceList;
        if(event.getParam('selectedPriceList')===undefined){
            selectedPriceList = 'Corporate';
        }else{
            selectedPriceList = event.getParam('selectedPriceList').replace('__c', '');
        }
        var priceListOptions = component.get('v.priceListOptions');
        var selectedPriceListIndex = _.findIndex(priceListOptions, ['label', selectedPriceList]);
        if (selectedPriceListIndex > -1) {
            priceListOptions[selectedPriceListIndex].selected = true;
        }
        component.set('v.relatedClients', event.getParam('relatedClients'));
        component.set('v.xmProducts', event.getParam('xmProducts'));
        component.set('v.discountApproved',event.getParam('discountApproved'));
        component.set('v.quoteId',event.getParam('quoteId'));
        component.set('v.actionType','Edit');
        component.set('v.selectedPriceList', selectedPriceList);
        component.set('v.priceListOptions', priceListOptions);
        component.set('v.quoteName', event.getParam('quoteName'));
        component.set('v.quoteStartDate', event.getParam('quoteStartDate'));
        component.set('v.quoteEndDate', event.getParam('quoteEndDate'));
        component.set('v.quoteTerm', event.getParam('quoteTerm'));
         //console.log('CMP CTRL: editExperience - CALL HLPR getQuoteLineItems');
        helper.getQuoteLineItems(component,event,helper);
    },
    
    // SHOW SPINNER
    spinnerShow : function (component, event, helper) {
         //   console.log('CMP CTRL: spinnerShow');
        var m = component.find('modalspinner');
        $A.util.removeClass(m, "slds-hide");
    },
    
    // HIDE SPINNER
    spinnerHide : function (component, event, helper) {
       // console.log('CMP CTRL: spinnerHide');
        var m = component.find('modalspinner');
        $A.util.addClass(m, "slds-hide");
    },
    
    onQuoteSave : function(component,event,helper){  
       // console.log('CMP CTRL: onQuoteSave');
        var valueReset = '';
       // component.set("v.showspinner", true);
        component.set('v.quoteType',event.getParam('quoteType'));
        component.set('v.quoteSaved',true);
        component.set('v.exp',valueReset);
        //console.log('CMP CTRL: onQuoteSave - callServer getXmQuoting');
        helper.callServer(
            component,
            "c.getXmQuoting",
            function(response){
                component.set('v.quotingPermissions', response);
                //console.log('CMP CTRL: onQuoteSave - CALL HLPR setAvailableExperiences');
                helper.setAvailableExperiences(component, event, helper, response);
              //  component.set("v.showspinner", false);
            }
            
        );
    },
    
    onLicenseReceive: function(component, event, helper) { 
      //  console.log('CMP CTRL: onLicenseReceive');
      //  console.log(event.getParam('clientid'));
      //  console.log(event.getParam('clients'));
        var exp;
        var bundleId;
        var coreQuantity;
        var maxBundle;
        var bundleName;
        var d = new Date();
        var merge = event.getParam('merge');
        var licenses = event.getParam('licenses');
        var currency = 1;
        var licenseARR = 0;
        //console.log('CMP CTRL: onLicenseReceive - callServer getct');
        helper.callServer(
            component,
            "c.getct",
            function(response) {
               // console.log(response);
                
                var coreQuantity = 0;
                _.forEach(licenses,function(license){
                   // console.log(license);
                    _.filter(response, function(value,key) {
                        if(key === license.CurrencyIsoCode){
                            license.licenseCurrency = value;
                        }
                    });
                    license.MergeTerm = moment(license.License_End_Date__c).add(1, 'days').month() - moment(d).month();
                    license.TermLength = moment(license.License_End_Date__c).add(1, 'days').month() - moment(license.License_Start_Date__c).month();
                    if(license.MergeTerm <= 0) {
                        if(moment(license.License_End_Date__c).add(1, 'days').month() != moment(d).month() || moment(license.License_End_Date__c).add(1, 'days').year() != moment(d).year()) {
                            license.MergeTerm = license.MergeTerm + 12;
                        } else {
                            license.MergeTerm = 0;
                        }
                    }
                    if(license.TermLength <= 0) {
                        if(moment(license.License_End_Date__c).month() != moment(license.License_Start_Date__c).month() || moment(license.License_End_Date__c).add(1, 'days').year() != moment(license.License_Start_Date__c).year()) {
                            license.TermLength = license.TermLength + 12;
                        }
                    }
                    license.ProductName = license.Bundle_Product__r.Display_Label__c;
                    license.Quantity__c = license.Quantitiy__c;
                    license.Quantity = license.Quantitiy__c;
                    license.newQuantity = license.Quantitiy__c;
                    license.PriceEach = license.Invoice_Amount__c / license.Quantitiy__c;
                    license.PriceTotal = license.Invoice_Amount__c;
                    license.Comparison = license.Product__r.Upgrade_Comparison__c+license.Bundle_Product__r.User_Input_Required__c;
                    if(license.Core_Product__c === true){
                        if(bundleId == undefined || license.Bundle__r.Sort_Order__c > maxBundle) {
                            bundleId = license.Bundle__c;
                            bundleName = license.Bundle__r.Name;
                            maxBundle = license.Bundle__r.Sort_Order__c;
                            exp = license.Bundle__r.Experience__c;
                            currency = license.licenseCurrency;
                        }
                        coreQuantity += license.Quantitiy__c;
                    }
                    if(license.Product__r.Revenue_Type__c = 'Recurring') {
                
                licenseARR += license.Invoice_Amount__c / license.licenseCurrency * currency;
            }
                });
              //  console.log(licenses);
                if(bundleName==undefined || bundleId==undefined) {
                        bundleName = licenses[0].Bundle__r.Name;
                        bundleId = licenses[0].Bundle__c;
                        exp = licenses[0].Bundle__r.Experience__c;
                    }
                component.set('v.licenseCurrencyCode', event.getParam('licenseCurrencyCode'));
                if(!merge) {
                    component.set('v.start', licenses[0].License_Start_Date__c);
                    component.set('v.end', licenses[0].License_End_Date__c);
                }
                component.set('v.licenseCurrency',currency);
                component.set('v.licenseCoreQty',coreQuantity);
                component.set('v.licenseARR', licenseARR);
                component.set('v.exp',exp);
                component.set('v.bundleName', bundleName);
                component.set('v.licenseExp', exp);
                component.set('v.bundleId', bundleId);
                component.set('v.licenses', event.getParam('licenses'));
              // console.log('CMP CTRL: onLicenseReceive - SET selectedTier');
                component.set('v.selectedTier', coreQuantity);
                component.set('v.upgradeType', event.getParam('upgradechoice'));
                
                component.set('v.actionType', 'Create');
                
                component.set('v.clientId', event.getParam('clientid'));
                
                component.set('v.clients', event.getParam('clients'));
                
                component.set('v.coreDiscountPercentage', 1);
               
                component.set('v.nonCoreDiscountPercentage', 1);
                
                component.set('v.quoteType', 'Upgrade License');
               
            });
    },
    
})