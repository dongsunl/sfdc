({
    
    setOutput : function(component, event, helper) {
        //console.log('CNF CTRL: setOutput');
        var cmpMsg = component.find("msg");
        $A.util.removeClass(cmpMsg, 'hide');
        var expdate = component.find("expdate").get("v.value");
        var oDate = component.find("oDate");
        oDate.set("v.value", expdate);
    },
    
    onQliUpdate : function(component,event,helper){ 
        //console.log('CNF CTRL: QliUpdate');
        var techQlis = [];
        var qlis = _.flatten(event.getParam('qlis'));
        techQlis.push(qlis);
        techQlis = _.flattenDeep(techQlis);
        component.set('v.techQlis',techQlis);
        component.set('v.qliTotal',event.getParam('qliTotal'));
        component.set('v.netTotal',event.getParam('netTotal'));
        component.set('v.arr',event.getParam('arr'));
        component.set('v.selectedBundle', event.getParam('selectedBundle'));   
    },
    
    onServiceUpdate: function(component,event,helper){
        console.log('CNF CTRL: ServiceUPdate'); 
        component.set('v.serviceQlis',event.getParam('serviceQlis'));
        component.set('v.serviceBundles',event.getParam('serviceBundles'));
        component.set('v.techQlis',event.getParam('techQlis'));
        component.set('v.qliTotal',event.getParam('qliTotal'));
        component.set('v.netTotal',event.getParam('netTotal'));
        component.set('v.arr',event.getParam('arr'));
        component.set('v.selectedBundle', event.getParam('selectedBundle'));
    },
    
    onSave : function(component,event,helper){
        //console.log('CNF CTRL: onSave');
        component.set('v.saving',true);
        component.set('v.redirectToQuote',true);
        helper.getServiceRecordTypes(component,event,helper);        
    },
    
    onSaveAndNew : function(component,event,helper){
        //console.log('CNF CTRL: onSaveAndNew');
        component.set('v.saving',true);
        helper.getServiceRecordTypes(component,event,helper);       
    },
    
    onCancel : function(component,event,helper){
        //console.log('CNF CTRL: Cancel');
        var quoteNameReset = 'Quote ';
        var selectedExpReset = '';
        var compEvent = component.getEvent("quoteSaved");
        location.reload();
    },
    
    onBundleChange : function(component, event, helper) {
        //console.log('CNF CTRL: BundleChange');
        component.set('v.selectedBundle', event.getParam('selectedBundle'));
    },
    
    rulesValidation: function(component,event,helper){
        //console.log('CNF CTRL: rulesValidation');
        var preventSave = false;
        if(event.getParam('messageCount') > 0){
            preventSave = true;
        }
        component.set('v.preventSave',preventSave);
    },
    
    onClone : function (component,event,helper){
        component.set('v.actionType','Clone');
        component.set('v.saving',true);
        component.set('v.redirectToQuote',true);
        helper.getServiceRecordTypes(component,event,helper);
    },
    
    onErrorClose : function(component,event,helper){
        var quoteId = component.get('v.quoteId');
        if(component.get('v.redirectToQuote')===true){
            var openloc = '/apex/QuoteLayoutFinder?Id='+quoteId; 
            location.reload();
            window.open(openloc);
        }else{
            location.reload();
        }
    }
    
    
})