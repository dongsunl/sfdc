({
    getServiceRecordTypes : function(component,event,helper){
        //console.log('CNF HLPR: getServiceRecordTypes');        
        var action = component.get('c.getServiceRecordType');     
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                component.set('v.serviceRecordTypes',dataObj);                
                helper.getPriceBookEntry(component,event,helper);
            }  
            else if (state === 'ERROR') {
                var header = 'Service Record Type Retrieval Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    getPriceBookEntry : function(component,event,helper){
        //console.log('CNF HLPR: getPriceBookEntry');
        var serviceBundles = 0;
        var techQlis = component.get('v.techQlis');
        var currency = component.get('v.opportunity.CurrencyIsoCode');
        var products = [];
        _.forEach(techQlis, function(value) {
            products.push(value.Related_Product__c);
            serviceBundles += value.Count_of_Service_Bundles__c;
        });
        var quoteType = component.get('v.quoteType');
        var actionType = component.get('v.actionType');       
        
        var action = component.get('c.getPriceBookEntry');        
        action.setParams({
            "products" : products,
            "priceBook" : '01s500000001tHMAAY',
            "currencyCode" : currency
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                techQlis =  _.forEach(techQlis, function(value){
                    _.forEach(dataObj, function(resp){
                        if(value.Related_Product__c == resp.Product2Id){
                            value.PriceBookEntry = resp.Id;
                        }
                    });
                });
                component.set('v.techQlis',techQlis);                
                if(quoteType === 'New License Quote'){
                    if(actionType === 'Create'|| actionType === 'Clone'){
                        helper.createQuote(component,event,helper);  
                    } else if(actionType === 'Add'){
                        helper.addToQuote(component,event,helper);  
                    } else if(actionType === 'Edit'){
                        helper.editQuote(component,event,helper);  
                    }
                } else if(quoteType === 'Upgrade License'){
                    if(actionType === 'Create'|| actionType === 'Clone'){
                        helper.createUpgradeQuote(component,event,helper);
                    } else if(actionType === 'Edit'){
                        helper.editUpgradeQuote(component,event,helper)
                    } else if(actionType === 'Add'){
                        helper.addToUpgradeQuote(component,event,helper)
                    }
                }
            }  
            else if (state === 'ERROR') {
                var header = 'PriceBook Entry Retreival Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action);         
    },
    
    createQuote : function(component, event, helper){
        //console.log('CNF HLPR: CreateQuote');     
        var Quote = component.get('v.quote');
        var bundleName = component.get('v.selectedBundle')[0].Name;
        var serviceQlis = component.get('v.serviceQlis');
        var templateCodes = '';
        var newItem = {};
        var serviceBundles = _.flattenDeep(component.get('v.serviceBundles'));
        var partnerContact = '';
        if(serviceBundles.length>0){
            partnerContact = serviceBundles[0].PartnerContact;
        }
        _.forEach(serviceBundles, function(value) {
            if(value.Conga_Template_Code__c != null) {
                templateCodes+=value.Conga_Template_Code__c + ',';
            }
        });
        templateCodes = templateCodes.slice(0,-1);
        newItem = JSON.parse(JSON.stringify(Quote));
        newItem.Name = component.get('v.quoteName');
        newItem.OpportunityId = component.get('v.opportunity.Id');
        newItem.Pricebook2Id = '01s500000001tHMAAY';
        newItem.License_Term_in_months__c = component.get('v.quoteTerm');
        newItem.Quote_Type__c = "New License";
        newItem.License_Length_Input__c = component.get('v.quoteTerm');
        newItem.XM_Products__c = component.get('v.selectedBundle')[0].Name;
        newItem.PostBundleDiscountAmount__c  = component.get('v.netTotal');
        newItem.License_Start_Date__c = component.get('v.quoteStartDate');
        newItem.License_End_Date__c = component.get('v.quoteEndDate');
        newItem.Last_Configuration__c = new Date();
        newItem.Service_Template_Codes__c = templateCodes;
        newItem.ChosenPriceTable__c = component.get('v.selectedPriceList') + '__c';
        newItem.Partner_SOW_Contact__c = partnerContact;
        newItem.Payment_Terms__c = 'Net 30';
        if(component.get('v.trial')) {
            newItem.Pilot__c = true;
        }            
        var action = component.get('c.saveQuoteRecord');        
        
        action.setParams({
            "record" : newItem,
            "startDate" : component.get('v.quoteStartDate')
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();            
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                var recordId = dataObj.Id;
                if(component.get('v.experience')[0].MasterLabel == 'Research Services') {
                    helper.syncQuote(component, event, helper, recordId);
                }               
                component.set('v.quoteId',recordId);
                component.set('v.savedQuote',dataObj);
                helper.createQuoteLineItem(component,recordId,helper);
            }  
            else if (state === 'ERROR') {
                var header = 'Quote Save Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
        
    },    
    
    createUpgradeQuote : function(component, event, helper){
        //console.log('CNF HLPR: CreateUpgradeQuote');
        var licenses = _.keyBy(component.get('v.licenses'),'Comparison');
        var techQlis = _.keyBy(component.get('v.techQlis'),'Comparison');        
        var deletedLicenses;       
        var exp = component.get('v.exp');
        var arr = component.get('v.techARR');
        var experiences = component.get('v.experienceMetaData');
        var experience = _.filter(experiences, {'Code__c' : exp});
        component.set('v.experience', experience);
        var qrField = component.get('v.experience[0].QR_Field__c');
        
        var Quote = component.get('v.quote');
        var bundleName = component.get('v.selectedBundle')[0].Name;
        var serviceQlis = component.get('v.serviceQlis');
        var quoteType = component.get('v.quoteType');
        var templateCodes = '';
        var newItem = {};
        var serviceBundles = _.flattenDeep(component.get('v.serviceBundles'));
        var partnerContact = '';
        if(serviceBundles.length>0){
            partnerContact = serviceBundles[0].PartnerContact;
        }
        var upgradeType = component.get('v.upgradeType');
        var Quote_Type__c;
        var qlis = component.get('v.techQlis');
        
        _.forEach(qlis, function(qli) {
            if(qli.Related_Product__r.ProductCode === 'CreditM') {
                Quote_Type__c = 'Core Upgrade - Same Renewal Date';
                component.set('v.coreUpgrade',true);
            }
        });
        if(upgradeType === 'keep-dates' && Quote_Type__c == undefined) {
            Quote_Type__c = 'Non-Core Add-On';
        }
        else if (upgradeType === 'change-dates') {
            Quote_Type__c = 'Core Upgrade - Change Renewal Date';
            component.set('v.coreUpgrade',true);
            var delProds =  _.difference(_.keys(licenses), _.keys(techQlis) ); 
            _.forEach(delProds, function(value) {
                var l = _.find(licenses,['Comparison',value]);
                deletedLicenses += l.ProductName+';';
            });
        }
            else if(upgradeType === 'on-renewal') {
                Quote_Type__c = 'License Change at Renewal';
                component.set('v.coreUpgrade',true);
                var delProds =  _.difference(_.keys(licenses), _.keys(techQlis) ); 
                
                _.forEach(delProds, function(value) {
                    var l = _.find(licenses,['Comparison',value]);
                    deletedLicenses += l.ProductName+';';
                });
            }
                else if(upgradeType == 'merge') {
                    Quote_Type__c = 'Merge Licenses';
                    component.set('v.coreUpgrade', true);
                }
        _.forEach(serviceBundles, function(value) {
            if(value.Conga_Template_Code__c != null) {
                templateCodes+=value.Conga_Template_Code__c + ',';
            }
        });
        if(component.get('v.coreUpgrade') != true || component.get('v.upgradeType') === 'keep-dates') {
            arr = 0;
        }
        templateCodes = templateCodes.slice(0,-1);
        newItem = JSON.parse(JSON.stringify(Quote));
        newItem.Name = component.get('v.quoteName');
        newItem.OpportunityId = component.get('v.opportunity.Id');
        newItem.Pricebook2Id = '01s500000001tHMAAY';
        newItem.License_Term_in_months__c = component.get('v.quoteTerm');
        newItem.Quote_Type__c = Quote_Type__c;
        newItem.License_Length_Input__c = component.get('v.quoteTerm');
        newItem.XM_Products__c = component.get('v.selectedBundle')[0].Name;
        newItem.PostBundleDiscountAmount__c  = component.get('v.netTotal');
        newItem.License_Start_Date__c = component.get('v.quoteStartDate');
        newItem.License_End_Date__c = component.get('v.quoteEndDate');
        newItem.Last_Configuration__c = new Date();
        newItem.Service_Template_Codes__c = templateCodes;
        newItem.ChosenPriceTable__c = component.get('v.selectedPriceList') + '__c';
        newItem.Related_Client__c = component.get('v.clientId');
        newItem.Current_License_End_Date__c = component.get('v.end');
        newItem.License_Items_to_be_Removed__c = deletedLicenses;
        newItem.Partner_SOW_Contact__c = partnerContact;
        newItem.Payment_Terms__c = 'Net 30';
        if(component.get('v.trial')) {
            newItem.Pilot__c = true;
        }
        
        var action = component.get('c.saveUpgradeQuoteRecord');        
        
        action.setParams({
            "record" : newItem,
            "qrField" : qrField,
            "arr" : arr,
            "startDate" : component.get('v.quoteStartDate')
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();            
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                
                var recordId = dataObj.Id;
                if(component.get('v.experience')[0].MasterLabel == 'Research Services') {
                    helper.syncQuote(component, event, helper, recordId);
                }
                helper.createRelatedClients(component, event, helper, recordId);
                
                component.set('v.quoteId',recordId);
                helper.createQuoteLineItem(component,recordId,helper);
            }  
            else if (state === 'ERROR') {
                var header = 'Quote Save Error';
                helper.processErrors(component,dataObj,header);
            }
        }));
        $A.enqueueAction(action);
    },
    
    editQuote : function(component, event, helper){
        //console.log('CNF HLPR: editQuote');
        var Quote = component.get('v.quote');
        var bundleName = component.get('v.selectedBundle')[0].Name;
        var newItem = {};
        var serviceBundles = _.flattenDeep(component.get('v.serviceBundles'));
        var partnerContact = '';
        if(serviceBundles.length>0){
            partnerContact = serviceBundles[0].PartnerContact;
        }
        var templateCodes = '';
        _.forEach(serviceBundles, function(value) {
            if(value.Conga_Template_Code__c != null) {
                templateCodes+=value.Conga_Template_Code__c + ',';
            }
        });
        
        var prods = component.get('v.xmProducts');
        var curProd = prods.split("; ");
        var xp = component.get('v.selectedBundle')[0].Name;
        var existingBundle = component.get('v.quoteLines')[0].Bundle_Lookup__r.Name;
        
        curProd = _.pull(curProd, existingBundle);
        curProd.push(xp);
        curProd.sort();
        var curProd2 = '';                    
        for(var i = 0; i < curProd.length; i++){ 
            if(i === 0){
                curProd2 = curProd[i];
            } else{
                curProd2 = curProd2+'; '+curProd[i];
            }  
        }
        templateCodes = templateCodes.slice(0,-1);
        newItem = JSON.parse(JSON.stringify(Quote));
        newItem.Id = component.get('v.quoteId');
        newItem.Name = component.get('v.quoteName');
        newItem.Last_Configuration__c = new Date();
        newItem.License_Start_Date__c = component.get('v.quoteStartDate');
        newItem.License_End_Date__c = component.get('v.quoteEndDate');
        newItem.License_Term_in_months__c = component.get('v.quoteTerm');
        newItem.XM_Products__c = curProd2;
        newItem.ChosenPriceTable__c = component.get('v.selectedPriceList') + '__c';
        newItem.Partner_SOW_Contact__c = partnerContact;
        newItem.CPQ_Completed__c = false;
        newItem.Service_Template_Codes__c = templateCodes;
        if(component.get('v.trial')) {
            newItem.Pilot__c = true;
        }
        if(component.get('v.discountApproved')===true){
            newItem.Discount_Approval_Status__c = 'Not Submitted';
            newItem.Requested_Discount_Price__c = null;
            newItem.Requested_Discount_Amount__c = null;
            newItem.Requested_Discount__c = null;
            newItem.Discretionary_Discount__c = null;
            newItem.Free_Months_Included__c = null;
            newItem.DiscountRequestNotes_New__c = null;
            newItem.Discount_Submit_Date_Time__c = null;
            newItem.Discount_Status_Finalized_Date_Time__c = null;
            newItem.Summit_Discount_Amount__c = null;
        }       
        
        var action = component.get('c.editRecord');        
        
        action.setParams({
            "record" : newItem
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                component.set('v.savedQuote',dataObj);
                var recordId = dataObj.Id;               
                helper.deleteQuoteLineItems(component,recordId,helper);
            }  
            else if (state === 'ERROR') {
                var header = 'Quote Save Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action);     
    },
    
    editUpgradeQuote : function(component, event, helper){
        //console.log('CNF HLPR: editUgradeQuote');        
        var licenses = _.keyBy(component.get('v.licenses'),'Comparison');
        var techQlis = _.keyBy(component.get('v.techQlis'),'Comparison');
        var deletedLicenses;        
        var arr = component.get('v.techARR');
        var exp = component.get('v.exp');
        var experiences = component.get('v.experienceMetaData');
        var experience = _.filter(experiences, {'Code__c' : exp});
        component.set('v.experience', experience);
        var qrField = component.get('v.experience[0].QR_Field__c');
        var Quote = component.get('v.quote');
        var bundleName = component.get('v.selectedBundle')[0].Name;
        var newItem = {};
        var serviceBundles = _.flattenDeep(component.get('v.serviceBundles'));
        var partnerContact = '';
        if(serviceBundles.length>0){
            partnerContact = serviceBundles[0].PartnerContact;
        }
        var templateCodes = '';        
        var prods = component.get('v.xmProducts');
        var curProd = prods.split("; ");
        var xp = component.get('v.selectedBundle')[0].Name;
        var existingBundle = component.get('v.quoteLines')[0].Bundle_Lookup__r.Name;        
        curProd = _.pull(curProd, existingBundle);
        curProd.push(xp);
        curProd.sort();
        var curProd2 = '';                    
        for(var i = 0; i < curProd.length; i++){ 
            if(i === 0){
                curProd2 = curProd[i];
            } else{
                curProd2 = curProd2+'; '+curProd[i];
            }  
        }        
        _.forEach(serviceBundles, function(value) {
            if(value.Conga_Template_Code__c != null) {
                templateCodes+=value.Conga_Template_Code__c + ',';
            }
        });
        if(component.get('v.upgradeType') === 'keep-dates') {
            arr = 0;
        }
        templateCodes = templateCodes.slice(0,-1);
        newItem = JSON.parse(JSON.stringify(Quote));
        newItem.Id = component.get('v.quoteId');
        newItem.Name = component.get('v.quoteName');
        newItem.Last_Configuration__c = new Date();
        newItem.ChosenPriceTable__c = component.get('v.selectedPriceList') + '__c';
        newItem.XM_Products__c = curProd2;
        newItem.Partner_SOW_Contact__c = partnerContact;
        newItem.CPQ_Completed__c = false;
        newItem.Service_Template_Codes__c = templateCodes;
        if(component.get('v.trial')) {
            newItem.Pilot__c = true;
        }        
        var action = component.get('c.editUpgradeRecord');  
        action.setParams({
            "record" : newItem,
            "qrField" : qrField,
            "arr" : arr
        });        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                var recordId = dataObj.Id;     
                component.set('v.savedQuote',dataObj);                
                helper.deleteQuoteLineItems(component,recordId,helper);
            }  
            else if (state === 'ERROR') {
                var header = 'Quote Save Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action);
        
    },
    
    addToUpgradeQuote : function(component, event, helper){
        //console.log('CNF HLPR: addToUpgradeQuote');        
        var Quote = component.get('v.quote');
        var bundleName = component.get('v.selectedBundle')[0].Name;
        var newItem = {};
        var arr = component.get('v.arr');
        var exp = component.get('v.exp');
        var experiences = component.get('v.experienceMetaData');
        var experience = _.filter(experiences, {Code__c : exp});
        component.set('v.experience', experience);
        var qrField = component.get('v.experience[0].Code__c');
        var serviceBundles = _.flattenDeep(component.get('v.serviceBundles'));
        var partnerContact = '';
        if(serviceBundles.length>0){
            partnerContact = serviceBundles[0].PartnerContact;
        }
        var templateCodes = '';
        _.forEach(serviceBundles, function(value) {
            if(value.Conga_Template_Code__c != null) {
                templateCodes+=value.Conga_Template_Code__c + ',';
            }
        });
        if(component.get('v.discountApproved')===true){
            newItem.Discount_Approval_Status__c = 'Not Submitted';
            newItem.Requested_Discount_Price__c = null;
            newItem.Requested_Discount_Amount__c = null;
            newItem.Requested_Discount__c = null;
            newItem.Discretionary_Discount__c = null;
            newItem.Free_Months_Included__c = null;
            newItem.DiscountRequestNotes_New__c = null;
            newItem.Discount_Submit_Date_Time__c = null;
            newItem.Discount_Status_Finalized_Date_Time__c = null;
            newItem.Summit_Discount_Amount__c = null;
            newItem.CPQ_Completed__c = false;
        }
        templateCodes = templateCodes.slice(0,-1);
        newItem = JSON.parse(JSON.stringify(Quote));
        newItem.Id = component.get('v.quoteId');
        newItem.Name = component.get('v.quoteName');
        newItem.Last_Configuration__c = new Date();
        newItem.Service_Template_Codes__c = templateCodes;
        newItem.Partner_SOW_Contact__c = partnerContact;
        newItem.ChosenPriceTable__c = component.get('v.selectedPriceList') + '__c';
        var action = component.get('c.editUpgradeRecord');      
        action.setParams({
            "record" : newItem,
            "qrField" : qrField,
            "arr" : arr
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                var recordId = dataObj.Id;
                component.set('v.savedQuote',dataObj);
                helper.createQuoteLineItem(component,recordId,helper);
            }  
            else if (state === 'ERROR') {
                var header = 'Quote Save Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action);
    },           
    
    addToQuote : function(component, event, helper){
        //console.log('CNF HLPR: AddtoQuote');
        var Quote = component.get('v.quote');
        var bundleName = component.get('v.selectedBundle')[0].Name;
        var newItem = {};
        var serviceBundles = _.flattenDeep(component.get('v.serviceBundles'));
        var partnerContact = '';
        if(serviceBundles.length>0){
            partnerContact = serviceBundles[0].PartnerContact;
        }
        var templateCodes = '';
        _.forEach(serviceBundles, function(value) {
            if(value.Conga_Template_Code__c != null) {
                templateCodes+=value.Conga_Template_Code__c + ',';
            }
        });        
        var prods = component.get('v.xmProducts');
        var curProd = prods.split("; ");
        var xp = component.get('v.selectedBundle')[0].Name;
        curProd.push(xp);
        curProd.sort();
        var curProd2 = ''; 
        for(var i = 0; i < curProd.length; i++){ 
            if(i === 0){
                curProd2 = curProd[i];
            } else{
                curProd2 = curProd2+'; '+curProd[i];
            }
        }
        templateCodes = templateCodes.slice(0,-1);
        newItem = JSON.parse(JSON.stringify(Quote));
        newItem.Id = component.get('v.quoteId');
        newItem.Name = component.get('v.quoteName');
        newItem.Last_Configuration__c = new Date();
        newItem.XM_Products__c = curProd2;
        newItem.ChosenPriceTable__c = component.get('v.selectedPriceList') + '__c';
        newItem.Partner_SOW_Contact__c = partnerContact;
        newItem.CPQ_Completed__c = false;
        newItem.Service_Template_Codes__c = templateCodes;        
        var action = component.get('c.editRecord');   
        action.setParams({
            "record" : newItem
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                var recordId = dataObj.Id;
                component.set('v.savedQuote',dataObj);
                helper.createQuoteLineItem(component,recordId,helper);
            }  
            else if (state === 'ERROR') {
                var header = 'Quote Save Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action);
    },
    
    deleteQuoteLineItems : function(component,quoteId,helper){
        //console.log('CNF HLPR: deleteQuoteLines');
        var exp = component.get('v.exp');     
        var bundleId = component.get('v.selectedBundle')[0].Id;       
        var action = component.get('c.deleteQlis');   
        action.setParams({
            "quoteId" : quoteId,
            "bundleId" : bundleId,
            "experience" : exp
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();               
                helper.createQuoteLineItem(component,quoteId,helper);
            }  
            else if (state === 'ERROR') {
                var header = 'Quote Line Item Delete Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action);
    },
    
    createQuoteLineItem : function(component, quoteId,helper) {
        //console.log('CNF HLPR: CreateQuoteLineItems');        
        var licenses = _.keyBy(component.get('v.licenses'),'Comparison');
        var techQlis = _.keyBy(component.get('v.techQlis'),'Comparison');
        var matchQlis = '';
        var diffQlis = '';
        var actionType = component.get('v.actionType')
        var Related_License_Item__c = '';
        if(component.get('v.coreUpgrade') === true){
            matchQlis =  _.intersection(_.keys(techQlis), _.keys(licenses));
            diffQlis = _.difference(_.keys(techQlis), _.keys(licenses));
            var coreLicense = _.findIndex(_.flatten(component.get('v.licenses')), {'Core_Product__c' : true});
            if(coreLicense > -1) {
                Related_License_Item__c = component.get('v.licenses')[coreLicense].Id;
            }
        }
        else if(component.get('v.quoteType')==='Upgrade License'){
            diffQlis = _.difference(_.keys(techQlis), _.keys(licenses));
        }
        var allTechQlis = component.get('v.techQlis');
        _.forEach(allTechQlis,function(t){
            _.forEach(matchQlis,function(m){
                if(t.Comparison === m){
                    var l = _.find(licenses,['Comparison',m]);
                    t.NewBusiness = t.PriceTotal - l.PriceTotal;
                    t.Renewal = l.PriceTotal;
                    t.LicenseChange = t.Quantity__c - l.Quantity__c;
                }
            });
            _.forEach(diffQlis,function(d){              
                if(t.Comparison === d){                    
                    var l = _.find(licenses,['Comparison',d]);
                    t.NewBusiness = t.PriceTotal;
                    t.Renewal = 0;
                    t.LicenseChange = t.Quantity__c;
                }
            });
        });         
        var itemList = [];
        var QuoteLineItem = component.get('v.obj');         
        var serviceBundles = 0;        
        _.forEach(allTechQlis,function(value){
            if(value.Name.includes('Markup')){
                //console.log('do not create')
            }else{
                var newItem = {};
                newItem = JSON.parse(JSON.stringify(QuoteLineItem));
                newItem.QuoteId = quoteId;
                newItem.Quantity = value.Quantity__c;
                newItem.UnitPrice = value.PriceTotal / value.Quantity__c;
                if(value.PricingType__c === 'Core') {
                    newItem.List_Price__c = value.PriceTotal / value.Quantity__c / component.get('v.coreDiscountPercentage');
                } else {
                    newItem.List_Price__c = value.PriceTotal / value.Quantity__c / component.get('v.nonCoreDiscountPercentage');
                }
                newItem.ListPrice = value.PriceTotal;
                newItem.PricebookEntryId = value.PriceBookEntry;
                newItem.Bundle_Lookup__c = value.Related_Bundle__c;
                newItem.Bundle_NS_Id__c = value.Bundle_NS_Id__c;
                if(actionType === 'Edit' || actionType === 'Clone') {
                    newItem.Bundle_Product__c = value.Bundle_Product__c;
                }
                else {
                    newItem.Bundle_Product__c = value.Id;
                }
                newItem.New_Business_Value__c = value.NewBusiness;
                newItem.Renewal_Value__c = value.Renewal;
                if(value.LicenseChange === '' || value.LicenseChange === undefined){
                    newItem.License_Change__c = 0;  
                }else{
                    newItem.License_Change__c = value.LicenseChange;  
                }
                if(component.get('v.selectedPriceList') !== 'Corporate') {
                    newItem.Max_Bundle_Discount__c = 0;
                    newItem.Actual_Bundle_Discount__c = 0;
                }
                else {
                    newItem.Max_Bundle_Discount__c = value.BundleDiscount / value.Quantity__c;                   
                }
                newItem.Revenue_Recognition_Rule__c = value.Related_Product__r.Revenue_Recognition_Rule__c;
                newItem.Revenue_Roll_Up__c = value.Related_Product__r.Revenue_Roll_Up__c;
                newItem.Product2Id = value.Related_Product__c;
                if(value.Related_Product__r.NS_Item_ID__c !== undefined) {
                    newItem.NS_Item_ID__c = value.Related_Product__r.NS_Item_ID__c;
                }
                else {
                    newItem.NS_Item_ID__c = value.Related_Bundle__r.NS_Item_ID__c;
                }
                newItem.External_Display_Name__c = value.External_Display_Name__c; 
                if(value.ProductName === 'Proration Credit'){
                    newItem.Proration_Amount__c = value.PriceTotal;
                }
                if(value.ProductName === 'Current Maintenance Credit') {
                    newItem.Related_License_Item__c = Related_License_Item__c;
                    newItem.Credit_Type__c = 'Current Maintenance Credit';
                }
                if(value.Related_Product__r.Unlimited_Responses__c === true) {
                    newItem.Unlimited_Responses__c = 'TRUE';
                }
                if(component.get('v.upgradeType') === 'merge' && value.Related_Product__r.ProductCode.includes('CreditT')) {
                    newItem.Credit_Type__c = 'Merged License Credit';
                }
                if(value.Unlimited_Users__c) {
                    newItem.Unlimited_Users__c = true;
                }
                newItem.Exclude_from_Quote_Display__c = value.Exclude_from_PDF__c;
                if(value.Multiplicity_Key__c != undefined) {
                    newItem.Multiplicity_Key__c = value.Multiplicity_Key__c;
                }
                if(value.FederalID != undefined) {
                    newItem.Federal_ID__c = value.FederalID;
                }
                if(value.DoNotRecreate != true) {
                    itemList.push(newItem);
                }                
                serviceBundles += value.Count_of_Service_Bundles__c;
            }
            
        });
        var recordId = quoteId;        
        var action = component.get('c.saveRecords');  
        action.setParams({
            "records" : itemList
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue(); 
                if(serviceBundles > 0){ 
                    if(component.get('v.quoteType')==='New License Quote'){
                        if(actionType==='Create' || actionType === 'Clone'){
                            helper.createService(component,recordId,helper,dataObj); 
                        } else if(actionType==='Add'){
                            helper.createService(component,recordId,helper,dataObj);
                        }
                            else if(actionType==='Edit'){
                                helper.deleteServices(component,recordId,helper,dataObj,serviceBundles,quoteId);
                            }
                    } else if(component.get('v.quoteType')==='Upgrade License'){
                        if(actionType==='Create' || actionType === 'Clone'){
                            helper.createService(component,recordId,helper,dataObj); 
                        } else if(actionType==='Add'){
                            helper.createService(component,recordId,helper,dataObj);
                        }
                            else if(actionType==='Edit'){
                                helper.deleteServices(component,recordId,helper,dataObj,serviceBundles,quoteId);
                            }
                    }
                }else{                    
                    if(actionType === 'Edit') {
                        helper.deleteServices(component, recordId, helper, dataObj, serviceBundles, quoteId);
                    }
                    else {
                        var quoteId = component.get('v.quoteId');
                        helper.cpqCompleted(component,quoteId)
                    }
                    var compEvent = component.getEvent("quoteSaved");                    
                }
            }  
            else if (state === 'ERROR') {
                var header = 'Quote Line Item Save Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action);
        
    },
    
    fireQuoteSaved : function(component,event,helper){
        //console.log('CNF HLPR: fireQuoteSaved');
        var compEvent = component.getEvent("quoteSaved");
        if(component.get('v.redirectToQuote')===true){
            var quoteId = component.get('v.quoteId');
            var openloc = '/apex/QuoteLayoutFinder?Id='+quoteId; 
            location.reload();
            window.open(openloc);
        }else{
            location.reload();
        }
    },
    
    createService : function(component, quoteId, helper, quoteLineItemsCreated){  
        //console.log('CNF HLPR: CreateService');
        var itemList = [];
        var Service = component.get('v.service');        
        var serviceBundles = component.get('v.serviceBundles');
        _.forEach(serviceBundles,function(value){
            var newItem = {};
            newItem = JSON.parse(JSON.stringify(Service));
            _.forEach(component.get('v.serviceRecordTypes'),function(rt){
                if(value.Bundle_Type__c === rt.Name){
                    newItem.RecordTypeId = rt.Id;
                }
            })
            newItem.Opportunity__c = component.get('v.opportunity.Id');
            newItem.Quote__c = quoteId;
            newItem.CurrencyIsoCode = component.get('v.opportunitycurrencycode');
            newItem.Name = value.Name;
            newItem.Service_Type__c = value.Bundle_Type__c;
            newItem.Package__c = value.Id;
            newItem.Custom__c = value.Custom__c;
            newItem.Partner_Name__c = value.Partner_Name__c;
            newItem.Q_Partner__c = value.Q_Partner__c;
            newItem.Partner_Availability__c = value.Partner_Availability__c;
            newItem.Qualtrics_Invoicing__c = value.Qualtrics_Invoicing__c;
            if(value.Custom__c == false && value.Partner_Name__c == undefined) {
                newItem.Provider_Selected__c = true;
            } else {
                newItem.Provider_Selected__c = value.Provider_Selected__c;
            }
            newItem.List_Price__c = value.ServiceOriginalPrice;
            newItem.Partner_Percentage_of_Cost__c = value.PartnerDiscount;
            newItem.Duration__c = value.Duration__c;
            if(value.Custom__c === false){
                newItem.Status__c = 'SOW in QSO';
            }
            if(value.Multiplicity_Key__c != undefined) {
                newItem.Multiplicity_Key__c = value.Multiplicity_Key__c;
            }
            _.forEach(quoteLineItemsCreated,function(qli){
                if(value.Bundle_Product_Prerequisite__c === qli.Bundle_Product__c && (value.Multiplicity_Key__c === qli.Multiplicity_Key__c || value.Multiplicity_Key__c == undefined)){
                    newItem.Quote_Line_Item__c = qli.Id;
                    newItem.Bundle__c = qli.Bundle_Lookup__c;
                }
                if(value.Markup_Bundle_Product__c === qli.Bundle_Product__c) {
                    newItem.Markup_Line_Item__c = qli.Id;
                    newItem.Markup_Amount__c = qli.ListPrice;
                }
                if(value.Maintenance_Bundle_Product__c === qli.Bundle_Product__c && (value.Multiplicity_Key__c === qli.Multiplicity_Key__c || value.Multiplicity_Key__c == undefined)) {
                    newItem.Maintenance_Line_Item__c = qli.Id;
                }
            });
            if(value.DoNotRecreate != true) {
                itemList.push(newItem);
            }
        });        
        var action = component.get('c.saveRecords');  
        action.setParams({
            "records" : itemList
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                var servicesCreated = dataObj;      
                helper.createServiceLineItem(component, servicesCreated,helper, quoteLineItemsCreated, quoteId);
            }  
            else if (state === 'ERROR') {
                var header = 'Service Save Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    }, 
    
    createServiceLineItem : function(component, servicesCreated, helper, quoteLineItemsCreated, quoteId) { 
        //console.log('CNF HLPR: createServiceLine');
        var exp = component.get('v.exp');        
        var itemList = [];
        var ServiceLineItem = component.get('v.serviceLineItem');        
        var serviceQlis = _.flattenDeep(component.get('v.serviceQlis'));
        var techQlis = _.flattenDeep(component.get('v.techQlis'));
        var actionType = component.get('v.actionType')
        if(exp ==='RS'){
            serviceQlis.forEach(function(sq){
                techQlis.forEach(function(tq){
                    if(tq.ProductCode===sq.ProductCode){
                        sq.Cost__c = tq.PriceEach;
                        sq.Total_Price__c = tq.PriceTotal;
                        sq.List_Price__c = tq.PriceTotal;
                        sq.Quantity__c = tq.Quantity__c;
                        sq.PriceEach = tq.PriceEach;
                        sq.PriceTotal = tq.PriceTotal;
                        sq.ServiceOriginalPrice = tq.PriceEach;
                    }
                });
            });
        }
        _.forEach(serviceQlis,function(value){
            var newItem = {};
            newItem = JSON.parse(JSON.stringify(ServiceLineItem));
            newItem.Quantity__c = value.Quantity__c;
            newItem.Cost__c = value.PriceEach;
            newItem.Total_Price__c = value.PriceTotal;
            newItem.List_Price__c = value.ServiceOriginalPrice;
            newItem.Name = value.Name; 
            newItem.CurrencyIsoCode = component.get('v.opportunitycurrencycode');            
            newItem.Maintenance__c = value.Maintenance__c;
            if(actionType === 'Edit' || actionType === 'Clone') {
                newItem.Service_Bundle_Product__c = value.Service_Bundle_Product__c;
            }
            else {
                newItem.Service_Bundle_Product__c = value.Id;
            }
            newItem.Related_Quote__c = quoteId;            
            _.forEach(servicesCreated,function(services){
                if(value.Related_Service_Bundle__c === services.Package__c && (value.Multiplicity_Key__c === services.Multiplicity_Key__c || value.Multiplicity_Key__c === undefined)){
                    newItem.Service__c = services.Id;
                } 
            });
            if(value.Multiplicity_Key__c != undefined) {
                newItem.Multiplicity_Key__c = value.Multiplicity_Key__c;
            }
            if(value.DoNotRecreate != true) {
                itemList.push(newItem);
            }
        });          
        var action = component.get('c.saveRecords'); 
        action.setParams({
            "records" : itemList
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                var serviceLineItems = dataObj;
                helper.updateQuoteLineItems(component, event, helper, servicesCreated, quoteLineItemsCreated);
            }  
            else if (state === 'ERROR') {
                var header = 'Service Component Save Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    deleteServices: function(component,recordId,helper,quoteLineItemsCreated,serviceBundles,quoteId){
        //console.log('CNF HLPR: deleteServices');      
        var action = component.get('c.deleteSlis');  
        action.setParams({
            "quoteId" : recordId
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();               
                if(serviceBundles >= 1) {
                    helper.createService(component,recordId,helper,quoteLineItemsCreated); 
                }
                else {
                    var quoteId = component.get('v.quoteId');
                    helper.cpqCompleted(component,quoteId)  
                }
            }  
            else if (state === 'ERROR') {
                var header = 'Service Delete Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    updateServices : function(component, servicesCreated, quoteLineItemsCreated, helper) {
       // console.log('CNF HLPR: updateServices');      
        var serviceBundles = component.get('v.serviceBundles');
        var services = [];
        _.forEach(servicesCreated, function(service) {
            _.forEach(serviceBundles, function(bundle) {
                if(service.Package__c == bundle.Id) {
                    service.Q_Partner__c = bundle.Q_Partner__c;
                    service.Partner_Availability__c = bundle.Partner_Availability__c;
                    service.Qualtrics_Invoicing__c = bundle.Qualtrics_Invoicing__c;
                    service.Provier_Selected__c = bundle.Provider_Selected__c;
                }
                services.push(service);
            });
        });       
        var action = component.get('c.editRecords');   
        action.setParams({
            "records" : services
        });        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                helper.updateQuoteLineItems(component, event, helper, servicesCreated, quoteLineItemsCreated);
            }  
            else if (state === 'ERROR') {
                var header = 'Service Save Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action);         
    },
    
    updateQuoteLineItems : function(component, event, helper, servicesCreated, quoteLineItemsCreated) {
       // console.log('CNF HLPR: updateQuoteLineItems');       
        var qlis = [];
        _.forEach(servicesCreated, function(service) {
            _.forEach(quoteLineItemsCreated, function(qli) {
                if(service.Quote_Line_Item__c == qli.Id) {
                    qli.Service__c = service.Id;
                    if(service.Custom__c == false  && service.Partner_Name__c != undefined) {
                        if(!(service.Partner_Name__c.includes('Qualtrics'))) {
                            qli.Partner__c = service.Q_Partner__c;
                            qli.Total_Partner_Amount__c = service.List_Price__c * service.Partner_Percentage_of_Cost__c;
                        }
                        if(service.Qualtrics_Invoicing__c == false) {
                            qli.ListPrice = 0;
                            qli.List_Price__c = 0;
                            qli.UnitPrice = 0;
                        }
                    }
                    qlis.push(qli);
                }
                if(service.Markup_Line_Item__c == qli.Id) {
                    qli.Service__c = service.Id;
                    if(service.Custom__c == false && service.Partner_Name__c != undefined) {
                        if(!(service.Partner_Name__c.includes('Qualtrics'))) {
                            qli.Partner__c = service.Q_Partner__c;
                            qli.Total_Partner_Amount__c = service.List_Price__c * service.Partner_Percentage_of_Cost__c;
                        }
                    }
                    qli.Exclude_from_Quote_Display__c = true;
                    qlis.push(qli);
                }
                if(service.Maintenance_Line_Item__c == qli.Id) {
                    qli.Service__c = service.Id;
                    qli.Total_Partner_Amount__c = 0;
                    qlis.push(qli);
                }
            });
        });
        
        helper.callServer(
            component,
            "c.editRecords",
            function(response) {
                var quoteId = component.get('v.quoteId');
                helper.cpqCompleted(component,quoteId)               
            },
            {
                records : qlis
            });
    },
    
    syncQuote : function(component, event, helper, quoteId) {
        //console.log('CNF HLPR: syncQuote');
        var action = component.get('c.syncQuote');          
        action.setParams({
            "quoteId" : quoteId,
            "opportunityId" : component.get('v.opportunityId')
        });        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();                
            }  
            else if (state === 'ERROR') {
                var header = 'Quote Sync Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action);         
    },
    
    createRelatedClients : function(component, event, helper, quoteId) {
        //console.log('CNF HLPR: createRelatedClients');
        var clients = component.get('v.clients');        
        var relatedClients = [];
        var clientObj = component.get('v.relatedClient');
        _.forEach(clients, function(client) {
            var relatedClient = {};
            relatedClient = JSON.parse(JSON.stringify(clientObj));
            relatedClient.Client__c = client.Id;
            relatedClient.Quote__c = quoteId;
            relatedClient.Bundle__c = client.Bundle;
            if(component.get('v.upgradeType') === 'merge') {
                relatedClient.Merge_Client__c = true;
            }
            else {
                relatedClient.Merge_Client__c = false;
            }
            relatedClient.End_Date__c = client.EndDate;
            relatedClients.push(relatedClient);
        });        
        var action = component.get('c.saveRecords');           
        action.setParams({
            "records" : relatedClients
        });        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
            }  
            else if (state === 'ERROR') {
                var header = 'Related Client Creation Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action);
    },
    
    cpqCompleted : function (component,quoteId){
        var action = component.get('c.cpqComplete'); 
        action.setParams({
            "quoteId" : quoteId
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                if(component.get('v.redirectToQuote')===true){
                    var openloc = '/apex/QuoteLayoutFinder?Id='+quoteId; 
                    location.reload();
                    window.open(openloc);
                }else{
                    location.reload();
                }
            }  
            else if (state === 'ERROR') {
                var header = 'CPQ Complete Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    processErrors : function(component,response,header){
        //console.log('MCH: processErrors');
        const errors = response.getError();
        let message = 'Unknown error'; 
        if (errors && Array.isArray(errors) && errors.length > 0) {
            const error = errors[0];
            if (typeof error.message != 'undefined') {
                message = error.message;
            } else if (typeof error.pageErrors != 'undefined' && Array.isArray(error.pageErrors) && error.pageErrors.length > 0) {
                const pageError = error.pageErrors[0];
                if (typeof pageError.message != 'undefined') {
                    message = pageError.message;
                }
            }
        }
        component.set('v.errorHeader',header);
        component.set('v.errorBody',message);
        component.set('v.saving',false);
        component.set('v.isError',true);        
    },
})