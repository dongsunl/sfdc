({
    setAvailableBundles : function(component, event, helper, quotingPermissions) {
      //  console.log('CT HLPR: setAvailableBundles');
        var bundleName = component.get('v.bundleName');
        
        var exclude;
        var excludedBundles = [];
        excludedBundles.push(_.split(quotingPermissions[0].Excluded_Bundles__c,';'));
        excludedBundles = _.flatten(excludedBundles);
        var xmProducts = component.get('v.xmProducts');
        var exp = component.get('v.exp');
        if(component.get('v.quoteType') == 'Upgrade License') {
            //console.log('CT HLPR: setAvailableBundles - callServer getUpgradeBundles');
            helper.callServer(
                component,
                "c.getUpgradeBundles",
                function(response) {
                    var zeroBundle = _.findIndex(response, {'Bundle__c' : '0'});
                    if(zeroBundle > -1 && component.get('v.upgradeType') != 'merge' && !bundleName.includes('0')) {
                        _.pullAt(response, zeroBundle);
                    }
                    component.set('v.availableTechBundles', response);
                    if(component.get('v.bundleChanged') === false) {
                        if(component.get('v.actionType') === 'Edit') {
                            if(component.get('v.bundleId') == undefined && !(bundleName == undefined)) {
                                var bundle = _.filter(response, {'Name' : bundleName});
                                component.set('v.bundleId', bundle[0].Id);
                            }
                            //console.log('CT HLPR: setAvailableBundles - CALL HLPR setSelectedBundle');
                            helper.setSelectedBundle(component, event, helper, component.get('v.bundleId'));
                        }
                        else {
                            //console.log('CT HLPR: setAvailableBundles - CALL HLPR setSelectedBundle');
                            helper.setSelectedBundle(component, event, helper, component.get('v.bundleId'));
                            //console.log('CT HLPR: setAvailableBundles - CALL HLPR setLicenseTotal');
                            helper.setLicenseTotal(component, event, helper);
                        }
                    }
                },
                {
                    excludedBundles : excludedBundles,
                    bundleId : component.get('v.bundleId'),
                    bundleName : bundleName
                }
            );
        }
        else if (component.get('v.quoteType') == 'New License Quote') {
            //console.log('CT HLPR: setAvailableBundles - callServer getBundles');
            helper.callServer( 
                component,
                "c.getBundles",
                function(response) {
                    component.set('v.availableTechBundles', response);
                    if(component.get('v.actionType') === 'Create' || component.get('v.actionType') === 'Add') {
                        //console.log('CT HLPR: setAvailableBundles - CALL HLPR setSelectedBundle');
                        helper.setSelectedBundle(component, event, helper, response[0].Id);
                    }
                    
                    if(component.get('v.bundleChanged')===false){
                        if(component.get('v.actionType')==='Edit'){
                            if(component.get('v.quoteLines').length == 0 && !(bundleName == undefined)) {
                                var bundle = _.filter(response, {'Name' : bundleName});
                                
                                //console.log('CT HLPR: setAvailableBundles - CALL HLPR setSelectedBundle');
                                helper.setSelectedBundle(component, event, helper, bundle[0].Id);
                            }else {
                                //console.log('CT HLPR: setAvailableBundles - CALL HLPR setSelectedBundle');
                                helper.setSelectedBundle(component, event, helper, component.get('v.quoteLines')[0].Bundle_Lookup__c);
                            }
                        }
                    }
                },
                {
                    selectedExperience : component.get('v.selectedExperience'),
                    excludedBundles : excludedBundles
                }
            );
        }
    },
    
    setSelectedBundle : function(component,event,helper,bundleId){
       // console.log('CT HLPR: setSelectedBundle');        
        var selectedBundle = _.filter(component.get('v.availableTechBundles'), {'Id' : bundleId});        
        selectedBundle[0].selected = true;        
        component.set('v.selectedBundle',selectedBundle[0]);       
        component.set('v.selectedBundleId',bundleId);        
        component.set('v.selectedExperience', selectedBundle[0].Experience__c);        
        if(selectedBundle[0].Unlimited_Responses__c == true) {
            component.set('v.unlimitedResponsesTier', 'true');   
        }         
        component.set('v.unlimitedResponses', selectedBundle[0].Unlimited_Responses__c);       
        var currentExperienceMeta = _.filter(component.get('v.experienceMetaData'),{'Code__c' : component.get('v.selectedExperience')});        
        component.set('v.experience', currentExperienceMeta);          
        component.set('v.coreEntryType', currentExperienceMeta[0].Core_Entry_Type__c);       
        if(component.get('v.tierChanged')===false && component.get('v.quoteType') !== 'Upgrade License'){
            if(currentExperienceMeta[0].Core_Entry_Type__c === 'Enter' && component.get('v.actionType')!=='Edit'){
                component.set('v.enteredTier',currentExperienceMeta[0].Core_Minimum__c); 
            }  
        }        
        if(bundleId !== ''){  
            //console.log('CT HLPR: setSelectedBundle - CALL HLPR getBundleProducts');
            helper.getBundleProducts(component,event,helper);
            //console.log('CT HLPR: setSelectedBundle - CALL HLPR getServiceBundles');
            helper.getServiceBundles(component,event,helper);
            //console.log('CT HLPR: setSelectedBundle - CALL HLPR getPricing');
            helper.getPricing(component,event,helper);
        }        
    },
    
    setSelectedTier : function(component,event,helper,tier){
        //console.log('CT HLPR: setSelectedTier');
        var currentExperienceMeta = _.filter(component.get('v.experienceMetaData'),{'Code__c' : component.get('v.selectedExperience')});
        var coreType = currentExperienceMeta[0].Core_Entry_Type__c;
        var selectedTier = component.get('v.selectedTier');
        var enteredTier = component.get('v.enteredTier');
        var originalTier = component.get('v.originalTier');
        if(coreType === 'Enter'){
            component.set('v.enteredTier',tier);
            //console.log('CT HLPR: setSelectedTier - SET SelectedTier');
            component.set('v.selectedTier', tier);
        } else{
            //console.log('CT HLPR: setSelectedTier - SET SelectedTier');
            component.set('v.selectedTier',tier);
            component.set('v.coreQuantity', tier);
        }
        if(originalTier === undefined) {
            component.set('v.originalTier', tier);
        }
        helper.onTierChangeEvent(component,event,helper,tier);
    },
    
    getBundleProducts : function(component,event,helper) {
        //console.log('CT HLPR: getBundleProducts');  
        //console.log('CT HLPR: getBundleProducts - callServer getProducts');  
        helper.callServer( 
            component,
            "c.getProducts",
            function(response) {
                //console.log(response);
                _.forEach(response,function(r){
                    r.ProductName=r.Related_Product__r.Name;
                    r.Quantity = r.Quantity__c * r.Quantity_Multiplier__c
                    r.Available = true;
                    r.Exclude_from_PDF__c = false;
                    if(r.Type__c === 'Tech' || r.Type__c === 'Products'){
                        r.groupSortOrder = 1;
                        if(r.PricingType__c === 'Core') {
                            r.sortOrder = 1;
                        }
                        else if(r.PricingType__c === 'UserD') {
                            r.sortOrder = 2;
                        }
                            else if(r.User_Input_Required__c === true) {
                                r.sortOrder = 3;
                            }
                                else if(r.Availability__c === 'Available' || r.Availability__c === 'Restricted' || r.Availability__c === 'Historic') {
                                    r.sortOrder = 4;
                                }
                                    else if(r.PricingType__c === 'Cred') {
                                        r.sortOrder = 6;
                                    }
                                        else {
                                            r.sortOrder = 5;
                                        }
                    }else if(r.Type__c === 'Events') {
                        r.groupSortOrder = 2;
                    }
                        else if(r.Type__c === 'Implementation') {
                            r.groupSortOrder = 3;
                        }
                            else if(r.Type__c === 'Build Services') {
                                r.groupSortOrder = 4;
                            }
                                else if(r.Type__c === 'Engineering Services') {
                                    r.groupSortOrder = 5;
                                }
                                    else if (r.Type__c === 'Conjoint') {
                                        r.groupSortOrder = 6;
                                    }
                                        else if(r.Type__c === 'SME') {
                                            r.groupSortOrder = 7;
                                        }
                                            else if(r.Type__c === 'Xccelerate') {
                                                r.groupSortOrder = 8;
                                            }
                                                else {
                                                    r.groupSortOrder = 9;
                                                }
                });
                var products =  _.sortBy(response,['groupSortOrder','sortOrder']);
                //console.log('CT HLPR: getBundleProducts - CALL HLPR getEditablePrices');  
                helper.getEditablePrices(component,event,helper,products);
                
            },
            {
                bundleId : component.get('v.selectedBundle')[0].Id
            }
        );                
    },
    
    getEditablePrices : function(component, event, helper,allBundleProducts) {
        //console.log('CT HLPR: getEditablePrices');
        if(allBundleProducts.length>0){
            var quotingPermissions = component.get('v.quotingPermissions');
            var priceEditable = quotingPermissions[0].Price_Editable__c;
            var editablePrices = [];
            var priceEachVisible = false;
            editablePrices.push(_.split(priceEditable, ','));
            editablePrices = _.flatten(editablePrices);
            _.forEach(allBundleProducts,function(t){
                var editable = _.indexOf(editablePrices, t.Type__c);
                if(editable > -1 || (component.get('v.dealDeskAccess') && t.Type__c == 'Tech')) {
                    t.priceEditable = true;   
                    priceEachVisible = true;
                }
            });
            component.set('v.priceEachVisible',priceEachVisible);
            // component.set('v.techQlis',techQlis);
            component.set('v.allBundleProducts', allBundleProducts);
            var productIds = _.uniq(_.map(allBundleProducts,'Related_Product__c'));
            //console.log('CT HLPR: getEditablePrices - CALL HLPR getProducts');
            helper.getProducts(component,event,helper,productIds);
        }
    },
    
    getProducts : function(component,event,helper,productIds) {
       // console.log('CT HLPR: getProducts'); 
        //console.log('CT HLPR: getProducts - callServer getSystemProducts'); 
        helper.callServer( 
            component,
            "c.getSystemProducts",
            function(response) { 
                var systemProducts = response;
                //console.log('CT HLPR: getProducts - CALL HLPR createProductsArray'); 
                helper.createProductsArray(component,event,helper,systemProducts);
            },
            {
                productIds : productIds
            }
        );                
    },
    
    createProductsArray : function(component,event,helper,systemProducts) {
        //console.log('CT HLPR: createProductsArray'); 
        var prods = component.get('v.allBundleProducts');
        var prods2 = [];
        _.forEach(prods,function(q){    
            _.forEach(systemProducts,function(s){
                if(q.Related_Product__c == s.Id ){
                    var obj = Object.assign({}, s);
                    obj = Object.assign(obj, q);
                    obj.Bundle_Product__c = q.Id;
                    obj.QuantityEditMode = false;
                    obj.PriceEditMode = false;
                    obj.Comparison = obj.Upgrade_Comparison__c+obj.User_Input_Required__c;
                    prods2.push(obj);
                }
            });
        });
        // console.log(prods2);
        component.set('v.allBundleProducts', prods2);
        //console.log('CT HLPR: createProductsArray - CALL HLPR setProducts');   
        helper.setProducts(component,event,helper);
    },
    
    setProducts : function(component,event,helper){
        //console.log('CT HLPR: setProducts');   
        var allBundleProducts = component.get('v.allBundleProducts');
        var availableTechProducts = [];
        var groupedProducts=[];
        var quoteType = component.get('v.quoteType');
        var upgradeType = component.get('v.upgradeType');
        var actionType = component.get('v.actionType');
        var bundleChanged = component.get('v.bundleChanged');
        var bundle = component.get('v.selectedBundle');
        var restrictedStatus = component.get('v.restrictedStatus');
       
        if(quoteType==='New License Quote'){
            _.forEach(allBundleProducts,function(product){
                if(product.Availability__c === 'Available' || (component.get('v.dealDeskAccess') && product.Availability__c != 'Included' && product.Default_Service__c != true)){
                    availableTechProducts.push(product);
                } else if(product.Availability__c === 'Restricted' && restrictedStatus==='Approved' && product.Default_Service__c !== true){
                    availableTechProducts.push(product);
                }
            });
        }
        else{
            _.forEach(allBundleProducts,function(product){
                if(product.Upgrade_Availability__c === 'Available' || (component.get('v.dealDeskAccess') && product.Availability__c != 'Included' && product.Default_Service__c != true)){
                    availableTechProducts.push(product);
                } else if(product.Upgrade_Availability__c === 'Restricted' && restrictedStatus==='Approved'){
                    availableTechProducts.push(product);
                }else if(product.Upgrade_Availability__c ==='Historic' || product.Upgrade_Availability__c === 'Restricted'){
                    _.forEach(component.get('v.licenses'),function(license){
                        if(license.Comparison === product.Comparison){
                            availableTechProducts.push(product);
                        }
                    });
                }
                    else if(upgradeType==='keep-dates' && product.ProductName==="Additional Dashboard Users"){
                        availableTechProducts.push(product);
                    }
            });
        }
        
       
        var grp = _.groupBy(availableTechProducts,'Type__c');
        _.mapKeys(grp,function(value,key){
            var newGroup = {}
            newGroup.Name = key;
            if(key === 'Tech' || key === 'Products') {
                newGroup.isexpanded = true;
            }
            groupedProducts.push(newGroup);
        });
        //console.log('quoteLines');
        var quoteLineItems = _.forEach(component.get('v.quoteLines'), function(qli) {
            return qli.Multiplicity_Key__c == undefined;
        });
        
        var techProdRemoves = [];
        
        if(bundleChanged === false) {
            if(actionType === 'Edit') {
                availableTechProducts.forEach(function(a){
                    quoteLineItems.forEach(function(q){
                        if(a.Comparison === q.Comparison && a.Add_Multiple__c === false){
                            techProdRemoves.push(a);
                        }
                    })
                })
                availableTechProducts = _.differenceBy(availableTechProducts, techProdRemoves,'Id');
            }
            else {
                availableTechProducts.forEach(function(a){
                    component.get('v.techQlis').forEach(function(q){
                        if(a.Comparison === q.Comparison && a.Add_Multiple__c === false){
                            techProdRemoves.push(a);
                        }
                    })
                })
                availableTechProducts = _.differenceBy(availableTechProducts, techProdRemoves, 'Id');
            }
        }
        else {
        }
        component.set('v.groupedProducts2', groupedProducts);
        if(quoteType!='Upgrade License'){
            availableTechProducts = _.filter(availableTechProducts, {'Default_Service__c' : false}); 
        }
        component.set('v.availableTechProducts', availableTechProducts);
        setTimeout($A.getCallback(
            () => component.set('v.openSection', groupedProducts[0])
        ));
        // var bundle = component.get('v.selectedBundle');
        if(bundleChanged===false){
            if(quoteType==='New License Quote' && actionType!=='Edit'){ 
                //console.log('CT HLPR: setProducts - CALL HLPR onBundleChangeEvent');  
                helper.onBundleChangeEvent(component,event,helper);
            }else if(upgradeType==='merge' && actionType != 'Edit' && (bundle[0].Bundle__c != '0' || bundle[0].Academic_Tier_Comparison__c == undefined)){
                //console.log('CT HLPR: setProducts - CALL HLPR calculateMergeCredits');  
                helper.calculateMergeCredits(component,event,helper);
                //console.log('CT HLPR: setProducts - CALL HLPR onBundleChangeEvent');  
                helper.onBundleChangeEvent(component, event, helper);
            }else if((component.get('v.upgradeType')==='merge' && component.get('v.actionType')==='Edit' && (bundle[0].Bundle__c != '0' || bundle[0].Academic_Tier_Comparison__c == undefined)) || component.get('v.quoteType')==='Upgrade License' && component.get('v.actionType')==='Edit'){
                helper.setLicenseTotal(component,event,helper);
                //helper.onBundleChangeEvent(component, event, helper);
            }
        }else{
            if(upgradeType==='merge' && actionType!= 'Edit' && (bundle[0].Bundle__c != '0' || bundle[0].Academic_Tier_Comparison__c == undefined)) {
                //console.log('CT HLPR: setProducts - CALL HLPR calculateMergeCredits');  
                helper.calculateMergeCredits(component, event, helper);
                //console.log('CT HLPR: setProducts - CALL HLPR onBundleChangeEvent');  
                helper.onBundleChangeEvent(component, event, helper);
            }
            else {
                //console.log('CT HLPR: setProducts - CALL HLPR onBundleChangeEvent');  
                helper.onBundleChangeEvent(component,event,helper);
            }
        }
        
    },
    
    getPricing : function(component,event,helper) {
      //  console.log('CT HLPR: getPricing');
        var licenses = component.get('v.licenses');
        var upgradeType = component.get('v.upgradeType');
        var bundle = component.get('v.selectedBundle');
        var cultureCode = component.get('v.opportunityculturecode');
        var currencyCode = component.get('v.opportunitycurrencycode');
        var currency = component.get('v.currency');
        //console.log('CT HLPR: getPricing - callServer getPricing');
        helper.callServer( 
            component,
            "c.getPricing",
            function(response) { 
                //console.log('--------------pricing-------------');
                //console.log(response);
                var priceList = component.get('v.selectedPriceList')+'__c';
                var netTotal = 0;
                var allPricing = _.forEach(response,function(value){
                    if(priceList !== 'Corporate__c') {
                        value.Bundle_Discount_Amount__c = 0;
                        component.set('v.netTotal', component.get('v.qliTotal'));
                    }
                    else {
                        value.Bundle_Discount_Amount__c = value.Bundle_Discount_Amount__c * currency;
                        _.forEach(component.get('v.techQlis'), function(qli) {
                            if(qli.PricingType__c == value.Pricing_Type__c && qli.TierQty >= value.Minimum__c && qli.TierQty <= value.Maximum__c) {
                                qli.BundleDiscount = qli.Quantity__c * value.Bundle_Discount_Amount__c;
                                netTotal += qli.PriceTotal - qli.BundleDiscount;
                            }
                        });
                        component.set('v.netTotal', netTotal);
                    }
                    value.FedRampCarahsoft__c = value.FedRampCarahsoft__c * currency;
                    value.FedRamp__c = value.FedRamp__c * currency;
                    value.GSACarahsoft__c = value.GSACarahsoft__c * currency;
                    value.GSA__c = value.GSA__c * currency;
                    value.List_Price__c = value.List_Price__c * currency;
                    
                    if(priceList === 'Corporate__c'){
                        component.set('v.allPricing', response);
                    }
                });
                component.set('v.allPricing', response);
                var core =  _.filter(response,{'Pricing_Type__c' : 'Core'});
                component.set('v.coreTiers',core);
                var dbuser =  _.filter(response,{'Pricing_Type__c' : 'UserD'});
                //console.log('dbuser');
                //console.log(dbuser);
                component.set('v.dashboardUserTiers',dbuser);
                var pv =  _.filter(response,{'Pricing_Type__c' : 'PV'});
                component.set('v.pvTiers',pv);
                if(component.get('v.bundleChanged')===false){
                    if(component.get('v.quoteType') === 'Upgrade License') {
                        var coreTiers = component.get('v.coreTiers')
                        _.forEach(coreTiers, function(tier) {
                            if(tier.Maximum__c == component.get('v.selectedTier')) {
                                tier.selected = true;
                            }
                        });
                        component.set('v.coreTiers', coreTiers);
                        if(component.get('v.actionType') === 'Edit') {
                            //console.log('CT HLPR: getPricing - CALL HLPR setQuoteLineItems');
                            helper.setQuoteLineItems(component, event, helper);
                        }
                        else if((bundle[0].Id === licenses[0].Bundle__c && component.get('v.actionType') != 'Edit' && (upgradeType === 'change-dates' || upgradeType === 'on-renewal')) || (upgradeType === 'merge' && (bundle[0].Bundle__c != '0' || bundle[0].Academic_Tier_Comparison__c == undefined))) {
                            //console.log('CT HLPR: getPricing - CALL HLPR getUpgradeLines');
                            helper.getUpgradeLines(component, event, helper);
                        }
                    } 
                    else if (component.get('v.actionType') === 'Edit') {
                        //console.log('CT HLPR: getPricing - CALL HLPR setQuoteLineItems');
                        helper.setQuoteLineItems(component, event, helper);
                    }
                }
                else if(upgradeType === 'merge' && (bundle[0].Bundle__c != '0' || bundle[0].Academic_Tier_Comparison__c == undefined)) {
                    //console.log('CT HLPR: getPricing - CALL HLPR getUpgradeLines');
                    helper.getUpgradeLines(component, event, helper);
                }
                
            },
            {
                bundleId : component.get('v.selectedBundle')[0].Id   
            }
        );                 
    },    
    
    getServiceBundles : function(component,event,helper){ 
       // console.log('CT HLPR: getServiceBundles');
        //console.log('CT HLPR: getServiceBundles - callServer getServiceBundles');
        helper.callServer( 
            component,
            "c.getServiceBundles",
            function(response) {
              //   console.log(response);
                response.forEach(function(r){
                    r.PartnerEmail = '';
                    r.PartnerContact = '';
                });
                component.set('v.availableServiceBundles',response);  
                //console.log(response);
                //console.log('CT HLPR: getServiceBundles - CALL HLPR getServiceComponents');
                helper.getServiceComponents(component,event,helper);  
            },
            {
                bundle : component.get('v.selectedBundle')[0].Name
            }
        );
    },
    
    getServiceComponents : function(component,event,helper) {
        //console.log('CT HLPR: getServiceComponents');
        var serviceBundles = component.get('v.availableServiceBundles');
        var serviceComponents = [];
        //console.log(component.get('v.selectedBundle')[0].Name);
        //console.log('CT HLPR: getServiceComponents - callServer getServiceProducts');
        helper.callServer( 
            component,
            "c.getServiceProducts",
            function(response) {
             //   console.log('SERVICE COMPONENTS');
             //   console.log(response);
                _.forEach(response,function(r){
                    r.ProductName=r.Related_Product__r.Name;
                    r.Available = true;
                    _.forEach(serviceBundles,function(s){
                        if(r.Related_Service_Bundle__c == s.Id ){
                            var obj = Object.assign({}, s);
                            obj = Object.assign(obj, r);
                            obj.Service_Bundle_Product__c = obj.Id;
                            serviceComponents.push(obj);
                        }
                    });
                });
                component.set('v.allServiceProducts',serviceComponents);
                var productIds = _.uniq(_.map(response,'Related_Product__c'));
                //console.log('CT HLPR: getServiceComponents - CALL HLPR getServiceProducts');
                helper.getServiceProducts(component,event,helper,productIds);
            },
            {
                bundle : component.get('v.selectedBundle')[0].Name
            }
        );                
    },
    
    getServiceProducts : function(component,event,helper,productIds) {
      //  console.log('CT HLPR: getServiceProducts');  
        //console.log('CT HLPR: getServiceProducts - callServer getSystemProducts');  
        helper.callServer( 
            component,
            "c.getSystemProducts",
            function(response) { 
                var systemProducts = response;
               // console.log(systemProducts)
                //console.log('CT HLPR: getServiceProducts - CALL HLPR createServiceProductsArray');
                helper.createServiceProductsArray(component,event,helper,systemProducts);
            },
            {
                productIds : productIds
            }
        );                
    },
    
    createServiceProductsArray : function(component,event,helper,systemProducts) {
      //  console.log('CT HLPR: createServiceProductsArray');
        var prods = component.get('v.allServiceProducts');
        var qli2 = [];
        _.forEach(prods,function(q){    
            _.forEach(systemProducts,function(s){
                if(q.Related_Product__c == s.Id ){
                    var obj = Object.assign({}, s);
                    obj = Object.assign(obj, q);
                    qli2.push(obj);
                }
            });
        });
        component.set('v.allServiceProducts',qli2);
        if(component.get('v.bundleChanged')===false){
            if(component.get('v.actionType')==='Edit'){
                helper.getService(component,event,helper); 
            }  
        }
        component.set("v.showspinner", false);
        //console.log('CT HLPR: createServiceProductsArray - CALL HLPR serviceProductsSet1');
        helper.serviceProductsSet1(component,event,helper);
    },
    
    getQuoteLineItems: function(component, event, helper){
       // console.log('CT HLPR: getQuoteLineItems');
        //console.log('CT HLPR: getQuoteLineItems - CALL HLPR setQuoteLineItems');
        helper.setQuoteLineItems(component, event, helper);
    },
    
    setQuoteLineItems: function(component, event, helper){
      //  console.log('CT HLPR: setQuoteLineItems');
        var quoteLines = component.get('v.quoteLines');
     //   console.log(quoteLines)
        var prods = component.get('v.allBundleProducts');
        var qlis = [];
        var total = 0;
        var netTotal = 0;	
        var arr = 0;
        var tier = 0;
        _.forEach(quoteLines,function(r){
            _.forEach(prods,function(p){
                if(p.Id == r.Bundle_Product__c ){
                    var obj = Object.assign({}, p);
                    obj = Object.assign(obj, r);
                    obj.Comparison = obj.Related_Product__r.Upgrade_Comparison__c+obj.User_Input_Required__c;
                    qlis.push(obj);
                    total += r.PriceTotal;
                    netTotal += r.PriceTotal - r.Max_Bundle_Discount__c;
                    if(r.Product2.Revenue_Type__c == 'Recurring'){
                        arr+=r.PriceTotal;
                    }
                    if(p.Picklist_Value__c === true && p.PricingType__c === 'UserD'){
                        //console.log('CT HLPR: setQuoteLineItems - CALL HLPR fetchPickListVal');
                        helper.fetchPickListVal(component, 'Maximum__c', 'dashboardUserTiersOptions');
                    }else if(p.Picklist_Value__c === true && p.PricingType__c === 'PV'){
                        //console.log('CT HLPR: setQuoteLineItems - CALL HLPR fetchPickListVal2');
                        helper.fetchPickListVal2(component, 'Maximum__c', 'pvTiersOptions');
                    }
                }
                
            });
            if(r.Bundle_Product__r.PricingType__c === 'Core') {
                tier += r.Quantity;
            }
        });
        
        //console.log(qlis);
        var qlisSorted =  _.sortBy(qlis,['groupSortOrder','sortOrder']);
        component.set('v.qliTotal',total);
        component.set('v.netTotal',netTotal);
        component.set('v.arr',arr);
        component.set('v.quoteLines', qlisSorted);
        //console.log('CT HLPR: setQuoteLineItems - CALL HLPR productsSetEvent');
        helper.productsSetEvent(component,event,helper, tier);
    },
    
    getService: function(component, event, helper){
      //  console.log('CT HLPR: getService');
        //NOTE: THIS IS ONLY MEANT TO PULL IN STANDARD SERVICES
        var bundles = component.get('v.availableServiceBundles');
        var newServices = [];
        //console.log('CT HLPR: getService - callServer getQService');
        helper.callServer(
            component,
            "c.getQService",
            function(response){
              //  console.log('bundles');
             //   console.log(bundles);
             //   console.log('response');
            //    console.log(response);
                _.forEach(response,function(r){                    
                    _.forEach(bundles,function(b){
                        if(b.Id == r.Package__c ){
                            var obj = Object.assign({}, r);
                            obj = Object.assign(obj, b );
                            obj.PriceEach = obj.List_Price__c;
                            obj.PriceTotal = obj.List_Price__c;
                            obj.ServiceOriginalPrice = obj.List_Price__c;
                            obj.PartnerDiscount = obj.Partner_Percentage_of_Cost__c;
                            if(obj.Markup_Bundle_Product__c!==undefined){
                                obj.Markup_Bundle_Product__c = obj.Markup_Line_Item__r.Bundle_Product__c;
                            }
                            
                            if(obj.Custom__c == true) {
                                obj.DoNotRecreate = true;
                            }
                            newServices.push(obj);
                        }
                    });
                });
              //  console.log('newServices');
              //  console.log(newServices);
                
                component.set('v.selectedServiceBundles',newServices);
                component.set('v.service', newServices);
                if(response.length >= 1) {
                    //console.log('CT HLPR: getService - CALL HLPR getServiceLineItems');
                    helper.getServiceLineItems(component, event, helper);
                }
            },
            {
                quoteId : component.get('v.quoteId')
            }
        );
    },
    
    getServiceLineItems: function(component, event, helper){
      //  console.log('CT HLPR: getServiceLineItems');
        var products = component.get('v.allServiceProducts')
        var newProducts = [];
        //console.log('CT HLPR: getServiceLineItems - callServer getServiceComponents');
        helper.callServer(
            component,
            "c.getServiceComponents",
            function(response){
                //console.log(response);
                _.forEach(response,function(r){
                    _.forEach(products,function(b){
                        if(b.Id == r.Service_Bundle_Product__c ){
                            var obj = Object.assign({}, b);
                            obj = Object.assign(obj, r);
                            obj.PriceEach = obj.Cost__c;
                            obj.Quantity = obj.Quantity__c;
                            obj.PriceTotal = obj.Total_Price__c;
                            obj.PartnerDiscount = obj.Service__r.Partner_Percentage_of_Cost__c;
                            obj.ServiceOriginalPrice = obj.List_Price__c;
                            if(obj.Service__r.Custom__c == true) {
                                obj.DoNotRecreate = true;
                            }
                            newProducts.push(obj);
                        }
                    });
                });  
                component.set('v.serviceQlis', newProducts);
                component.set('v.serviceComponents', newProducts);
                //console.log('CT HLPR: getServiceLineItems - CALL HLPR serviceProductsSetEvent');
                helper.serviceProductsSetEvent(component,event,helper);
            },
            {
                quoteId : component.get('v.quoteId')
            }
        );
    },
    
    getLicenseLineItems: function(component, event, helper){
      //  console.log('CT HLPR: getLicenseLineItems');
        var licenses = component.get('v.licenses');
        var bundleId;
        var tier;
        _.forEach(licenses,function(r){
            if(r.Bundle_Product__r.PricingType__c  === 'Core'){
                bundleId = r.Bundle__c;
                tier = r.Quantitiy__c;
            }
        });
        //console.log('CT HLPR: getLicenseLineItems - SET selectedTier');
        component.set('v.selectedTier', tier);
        component.set('v.coreQuantity', tier);
        helper.setSelectedBundle(component,event,helper,bundleId);
        helper.setSelectedTier(component,event,helper,tier);
        component.set('v.quoteLines', licenses);
        component.set('v.quoteType','Upgrade License');
    },
    
    assignLicenseLines : function(component,event,helper){
       // console.log('CT HLPR: assignLicenseLines');
        var prods = component.get('v.allBundleProducts');
        var licenses = component.get('v.licenses');
        if(component.get('v.quoteType')==='Upgrade License'){
            if(component.get('v.licenses').length>0){
                var bundleId;
                var tier;
                _.forEach(_.flatten(component.get('v.licenses')),function(value){
                    if(value.Core_Product__c === true){
                        bundleId = value.Bundle__c;
                        tier = value.Quantity__c;
                        //console.log('CT HLPR: assignLicenseLines - SET selectedTier');
                        component.set('v.selectedTier', tier);
                        component.set('v.coreQuantity', tier);
                    }
                });
                //console.log('CT HLPR: getLicenseLineItems - CALL HLPR setSelectedBundle');
                helper.setSelectedBundle(component,event,helper,bundleId);
            }
        }
        _.forEach(licenses,function(l){
            _.forEach(prods,function(p){
                if(p.Id == l.Bundle_Product__c ){
                    var obj = Object.assign({}, l);
                    obj = Object.assign(obj, p);
                    
                }
            });
        });
        licenses =  _.sortBy(licenses,['groupSortOrder','sortOrder']);
    },
    
    productsSetEvent: function(component,event,helper,tier){
       // console.log('CT HLPR: productsSetEvent');
        //console.log(component.get('v.bundleChanged'));
        //console.log(component.get('v.actionType'));
        if(component.get('v.bundleChanged')===false){
            if(component.get('v.actionType')==='Edit'){
                component.set('v.techQlis',component.get('v.quoteLines'));
                //console.log('CT HLPR: productsSetEvent - CALL HLPR setSelectedTier');
                helper.setSelectedTier(component, event, helper, tier);
            }
            else if(component.get('v.quoteType')==='Upgrade License'){
            }
        }
        var compEvent = component.getEvent("doneloading");
        //console.log('CT HLPR: productsSetEvent - FIRE compEvent doneloading');
        compEvent.fire();
    },
    
    serviceProductsSetEvent: function(component,event,helper){
       // console.log('CT HLPR: serviceProductsSetEvent');
        //console.log(component.get('v.techQlis'));
        //console.log(component.get('v.qliTotal'));
        //console.log(component.get('v.netTotal'));
        //console.log(component.get('v.arr'));
       
        if(component.get('v.bundleChanged')===false){
            if(component.get('v.actionType')==='Edit'){
                component.set('v.selectedServiceBundles',component.get('v.service'));
                component.set('v.serviceQlis',component.get('v.serviceComponents'));
                //console.log('CT HLPR: serviceProductsSetEvent - CALL HLPR updateCombinedQlis');
                helper.updateCombinedQlis(component,event,helper);
             //   console.log('afterupdateevent')
             //     console.log(component.get('v.serviceQlis'))
      // console.log(component.get('v.selectedServiceBundles'))
                var appEvent = component.getEvent('serviceUpdate');    
                appEvent.setParams({
                    serviceQlis: component.get('v.serviceQlis'),
                    serviceBundles: component.get('v.selectedServiceBundles'),
                    techQlis: component.get('v.techQlis'),
                    qliTotal: component.get('v.qliTotal'),
                    netTotal: component.get('v.netTotal'),
                    arr: component.get('v.arr'),
                    selectedBundle: component.get('v.selectedBundle')
                });
                //console.log('CT HLPR: serviceProductsSetEvent - FIRE appEvent serviceUpdate');
              
                appEvent.fire();
            }
        }
        
    },
    
    onBundleChangeEvent : function(component,event,helper){
       // console.log('CT HLPR: BundleChangeEvent');
        var appEvent = $A.get("e.c:cpq_BundleChange");
        appEvent.setParams({
            coreTiers : component.get('v.coreTiers'),
            allBundleProducts: component.get('v.allBundleProducts'),
            allPricing: component.get('v.allPricing'),
            dashboardUserTiers : component.get('v.dashboardUserTiers'),
            selectedBundle: component.get('v.selectedBundle')
        });
        //console.log('CT HLPR: BundleChangeEvent - FIRE appEvent cpq_BundleChange');
        appEvent.fire();
    },
    
    onTierChangeEvent : function(component,event,helper,tier){
       // console.log('CT HLPR: TierChangeEvent');
        var appEvent = $A.get("e.c:cpq_TierChange");
        appEvent.setParams({
            selectedTier : tier
        });
        //console.log('CT HLPR: BundleChangeEvent - FIRE appEvent cpq_TierChange');
        appEvent.fire();
    },
    
    setLicenseTotal : function(component, event, helper) {
      //  console.log('CT HLPR: setLicenseTotal');
        var licenses = _.flattenDeep(component.get('v.licenses'));
        var currency = component.get('v.currency');
        var licenseARR = 0;
        var licenseTotal = 0;
        var techARR = 0;
        var uniqueProducts = [];
        var groupedLicenses = [];
        var dashboardUsers = 0;
        _.forEach(licenses, function(l) {
            var index = _.indexOf(uniqueProducts, l.Product__r.ProductCode);
            if(index > -1 && !(l.Product__r.ProductCode.toUpperCase().includes('MAINT'))) {
                groupedLicenses[index].Quantitiy__c += l.Quantitiy__c;
                groupedLicenses[index].PriceTotal += l.Invoice_Amount__c / l.licenseCurrency * currency;
            }
            else {
                l.PriceTotal = l.Invoice_Amount__c / l.licenseCurrency * currency;
                uniqueProducts.push(l.Product__r.ProductCode);
                groupedLicenses.push(l);
            }
            if(l.Bundle_Product__r.PricingType__c === 'UserD') {
                dashboardUsers += l.Quantitiy__c;
            }
            if(component.get('v.upgradeType')==='keep-dates'){
                component.set('v.dbUsersLicense',dashboardUsers);
            }else{
                component.set('v.dashboardUsers', dashboardUsers); 
            }
            licenseTotal += l.Invoice_Amount__c / l.licenseCurrency * currency;
            if(l.Product__r.Revenue_Type__c === 'Recurring') {
                licenseARR += l.Invoice_Amount__c / l.licenseCurrency * currency;
                if(l.Bundle_Product__r.Type__c == 'Tech') {
                    techARR += l.Invoice_Amount__c / l.licenseCurrency * currency;
                }
            }
        });
        component.set('v.originalLicenses', licenses);
        component.set('v.licenses', groupedLicenses);
        component.set('v.licenseTotal', licenseTotal);
        component.set('v.licenseARR', licenseARR);
        component.set('v.techARR', techARR);
    },
    
    getUpgradeLines : function(component, event, helper) {
      //  console.log('CT HLPR: getUpgradeLines');
        var priceList = component.get('v.selectedPriceList');
        var bundleId = component.get('v.bundleId');
        var bundle = component.get('v.selectedBundle');
        var licenses = component.get('v.licenses');
      //  console.log(licenses);
        var upgradeType = component.get('v.upgradeType');
        var bundleProducts = component.get('v.allBundleProducts');
        var price = component.get('v.allPricing');
        price = _.flatten(price); 
        var group;
        var priceGroup = [];
        var tier;
        var techQlis = [];
        var total = 0;
        var netTotal = 0;
        var arr = 0;
        var selectedTier = Number(component.get('v.selectedTier'));
        var hasMaintenance = false;
        
        _.forEach(bundleProducts, function(bp) {
            _.forEach(licenses, function(l) {
                if(bp.Id == l.Bundle_Product__c && upgradeType != 'merge') {
                    bp.Quantity__c = l.Quantitiy__c;
                    if(bp.Quantity__c > bp.Related_Product__r.Maximum__c) {
                        bp.Quantity__c = bp.Related_Product__r.Maximum__c;
                    }
                    if(bp.Related_Product__r.ProductCode.toUpperCase().includes('MAINT')) {
                        bp.PriceEach = l.Invoice_Amount__c / l.licenseCurrency * component.get('v.currency');
                        bp.PriceTotal = bp.PriceEach * bp.Quantity__c;
                        bp.Availability__c = 'Included';
                        bp.PriceUpdate = true;
                        bp.License = l.Id;
                        bp.Multiplicity_Key__c = undefined;
                        bp.BundleDiscount = 0;
                        hasMaintenance = true;
                    }
                    bp.Comparison = bp.Related_Product__r.Upgrade_Comparison__c + bp.User_Input_Required__c;
                    var techQli = Object.assign({}, bp);
                    techQli.Bundle_Product__c = bp.Id;
                    techQlis.push(techQli);
                }
                else if (upgradeType === 'merge' && bp.Related_Product__r.Merge_Comparison__c == l.Product__r.Merge_Comparison__c && l.Product__r.Merge_Comparison__c != undefined && ((bp.Availability__c === l.Bundle_Product__r.Availability__c) || (bp.Availability__c == 'Restricted' || l.Bundle_Product__r.Availability__c == 'Restricted' || bp.Availability__c == undefined || l.Bundle_Product__r.Availability__c == undefined))) {
                    if((l.Core_Product__c == true && bundle[0].Unlimited_Responses__c != true) || l.Bundle_Product__r.User_Input_Required__c == true) {
                        bp.Quantity__c = l.Quantitiy__c;
                        if(bp.Quantity__c > bp.Related_Product__r.Maximum__c) {
                            bp.Quantity__c = bp.Related_Product__r.Maximum__c;
                        }
                    }
                    bp.Comparison = bp.Related_Product__r.Upgrade_Comparison__c + bp.User_Input_Required__c;
                    techQlis.push(bp);
                }
            });
        });
        if(hasMaintenance && component.get('v.upgradeType') === 'change-dates'){
            var creditProduct = _.filter(component.get('v.allBundleProducts'), function(bp) {
                return bp.Related_Product__r.ProductCode === 'CreditES';
            })[0];
            //console.log(creditProduct);
            var qli = Object.assign({}, creditProduct);
            var sum = -1 * _.sumBy(licenses, function(l) {
                if(l.Product__r.ProductCode.toUpperCase().includes('MAINT')) {
                    return l.Invoice_Amount__c / l.licenseCurrency * component.get('v.currency') * (l.MergeTerm / l.TermLength);
                }
            });
            qli.PriceEach = sum;
            qli.PriceTotal = sum;
            qli.PriceUpdate = true;
            qli.BundleDiscount = 0;
            qli.Availability__c = 'Included';
            techQlis.push(qli);
        }
       // console.log(techQlis);
        group = _.groupBy(techQlis, 'Price_Based_On__c');
        _.forEach(group, function(value,key){
            _.filter(techQlis ,function(o){
                if(o.PricingType__c === key){
                    priceGroup[key] = o.Quantity__c;
                }
                else if(priceGroup[key] === undefined) {
                    priceGroup[key] = selectedTier;
                }
            }); 
        });
        var qli2 = _.flatten(techQlis);
        qli2 = _.forEach(qli2,function(o){
            _.forIn(priceGroup,function(value,key){
                if(o.Price_Based_On__c == key){
                    o.TierQty = value;
                }
                if(o.Quantity_Multiplied_by_Core__c == true) {
                    o.Quantity__c = value;
                }
            });
            if(o.Picklist_Value__c === true && o.PricingType__c === 'UserD'){
                //console.log('CT HLPR: getUpgradeLines - CALL HLPR fetchPickListVal');
                helper.fetchPickListVal(component, 'Maximum__c', 'dashboardUserTiersOptions');
            }else if(o.Picklist_Value__c === true && o.PricingType__c === 'PV'){
                //console.log('CT HLPR: getUpgradeLines - CALL HLPR fetchPickListVal2');
                helper.fetchPickListVal2(component, 'Maximum__c', 'pvTiersOptions');
            }
        });
        var qli3 = _.forEach(qli2,function(o){
            _.forEach(price,function(p){                
                if(o.PricingType__c == p.Pricing_Type__c && o.TierQty >= p.Minimum__c && o.TierQty <= p.Maximum__c){
                    if(o.PriceUpdate != true) {
                        if(priceList == undefined || priceList == 'Corporate') {
                            o.PriceEach = p.List_Price__c;
                            o.BundleDiscount = o.Quantity__c * p.Bundle_Discount_Amount__c;
                        }
                        else if (priceList == 'GSA') {
                            o.PriceEach = p.GSA__c;
                            o.BundleDiscount = 0;
                        }
                            else if (priceList == 'GSACarahsoft') {
                                o.PriceEach = p.GSACarahsoft__c;
                                o.BundleDiscount = 0;
                            }
                                else if (priceList == 'FedRamp') {
                                    o.PriceEach = p.FedRamp__c;
                                    o.BundleDiscount = 0;
                                }
                                    else if (priceList == 'FedRampCarahsoft') {
                                        o.PriceEach = p.FedRampCarahsoft__c;
                                        o.BundleDiscount = 0;
                                    }
                        if(o.Related_Product__r.Eligible_for_Discount__c && o.PricingType__c==='Core') {
                            o.PriceEach = o.PriceEach * component.get('v.coreDiscountPercentage');
                        } else if (o.Related_Product__r.Eligible_for_Discount__c && o.PricingType__c != 'Core') {
                            o.PriceEach = o.PriceEach * component.get('v.nonCoreDiscountPercentage');
                        }
                        o.Unlimited_Users__c = p.Unlimited__c;
                        o.PriceTotal = o.Quantity__c * o.PriceEach;
                    }
                    if(o.DisplayEdited != true) {
                        if(o.Unlimited_Users__c === true){
                            o.External_Display_Name__c = o.Display_Label__c + ' (Unlimited)';
                        } else if (o.Quantity__c === 1 && o.User_Input_Required__c == false){
                            o.External_Display_Name__c = o.Display_Label__c;
                        } else {
                            o.External_Display_Name__c = o.Display_Label__c + ' ' + o.Display_Variable__c + ' ' + o.Quantity__c;
                        }
                    }
                    total += o.PriceTotal;
                    netTotal += o.PriceTotal - o.BundleDiscount;
                    if(o.Related_Product__r.Revenue_Type__c == 'Recurring'){
                        arr+=o.PriceTotal;
                    }
                }                
            });
        });
        qli3 =  _.sortBy(qli3,['groupSortOrder','sortOrder']);
       // console.log(qli3);
      //  console.log(arr);
        component.set('v.techQlis', qli3); 
        component.set('v.qliTotal',total);
        component.set('v.netTotal',netTotal);
        component.set('v.arr',arr);
        var groupedProducts = [];
        var grp = _.groupBy(qli3,'Type__c');
        _.mapKeys(grp,function(value,key){
            groupedProducts.push(key);
        });   
        
        component.set('v.groupedProducts', groupedProducts);
        //helper.sortQlis(component, event, helper);
    },
    
    fetchPickListVal: function(component, fieldName, picklistOptsAttributeName) {   
       // console.log('CT HLPR: fetchPicklistVal');
        var licenseDBUsers = component.get('v.dashboardUsers');
        var opts = [];
        var allValues = component.get("v.dashboardUserTiers");
        if(licenseDBUsers !== undefined) {
            opts.push({
                class: "optionClass",
                label: licenseDBUsers,
                value: licenseDBUsers
            });
        }
        
        for (var i = 0; i < allValues.length; i++) {
            opts.push({
                class: "optionClass",
                label: allValues[i].Maximum__c,
                value: allValues[i].Maximum__c
            });
        }
        component.set("v." + picklistOptsAttributeName, opts);        
    },
    
    fetchPickListVal2: function(component, fieldName, picklistOptsAttributeName) {   
      //  console.log('CT HLPR: fetchPicklistVal2');
        var opts = [];
        var allValues = component.get("v.pvTiers");
        
        for (var i = 0; i < allValues.length; i++) {
            opts.push({
                class: "optionClass",
                label: allValues[i].Maximum__c,
                value: allValues[i].Maximum__c
            });
        }
        component.set("v." + picklistOptsAttributeName, opts);        
    },
    
    updateAvailabilityFromRules: function(component,event,helper,availability){
       // console.log('CT HLPR: updateAvailabilityFromRules');
        var allBundleProducts = component.get('v.allBundleProducts');
        var availableTechProducts = component.get('v.availableTechProducts');
        var allServiceProducts = component.get('v.allServiceProducts');
        var techQlis = component.get('v.techQlis');
        var groupedProducts=[];
        _.forEach(availability,function(a){
            _.forEach(allBundleProducts,function(p){
                if(p.Type__c === 'Tech' || p.Type__c === 'Products'){
                    p.groupSortOrder = 1;
                    if(p.PricingType__c === 'Core') {
                        p.sortOrder = 1;
                    }
                    else if(p.PricingType__c === 'UserD') {
                        p.sortOrder = 2;
                    }
                        else if(p.User_Input_Required__c === true) {
                            p.sortOrder = 3;
                        }
                            else if(p.Availability__c === 'Available' || p.Availability__c === 'Restricted' || p.Availability__c === 'Historic') {
                                p.sortOrder = 4;
                            }
                                else if(p.PricingType__c === 'Cred') {
                                    p.sortOrder = 6;
                                }
                                    else {
                                        p.sortOrder = 5;
                                    }
                }else if(p.Type__c === 'Events') {
                    p.groupSortOrder = 2;
                }
                    else if(p.Type__c === 'Implementation') {
                        p.groupSortOrder = 3;
                    }
                        else if(p.Type__c === 'Build Services') {
                            p.groupSortOrder = 4;
                        }
                            else if(p.Type__c === 'Engineering Services') {
                                p.groupSortOrder = 5;
                            }
                                else if (p.Type__c === 'Conjoint') {
                                    p.groupSortOrder = 6;
                                }
                                    else if(p.Type__c === 'SME') {
                                        p.groupSortOrder = 7;
                                    }
                                        else if(p.Type__c === 'Xccelerate') {
                                            p.groupSortOrder = 8;
                                        } 
                                            else if(p.Type__c === 'XM Solutions') {
                                                p.groupSortOrder = 9;
                                            }
                                                else {
                                                    p.groupSortOrder = 10;
                                                }
                if(p.Id === a.product){
                    if(a.isAvailable === "true"){
                        p.Available = true;
                        p.Availability__c = 'Available';
                        p.Upgrade_Availability__c = 'Available';
                        var availableIndex = _.indexOf(availableTechProducts, p);
                        var techQliIndex = _.findIndex(techQlis, function(qli) {
                            return qli.Bundle_Product__c == p.Id;
                        });
                        if(availableIndex === -1 && techQliIndex === -1) {
                            availableTechProducts.push(p);
                        }
                    }else{
                        p.Available = false;
                        var availableIndex = _.indexOf(availableTechProducts, p);
                        if (availableIndex > -1) {
                            _.pullAt(availableTechProducts, availableIndex);
                        }
                    }
                }
            });
            _.forEach(allServiceProducts,function(s){
                if(s.Id === a.product){
                    if(a.isAvailable === "true"){
                        s.Availability__c = 'Available';
                    }else{
                        s.Availability__c = 'Restricted';
                    }
                }
            });
        });
        availableTechProducts = _.sortBy(availableTechProducts, ['groupSortOrder', 'sortOrder']);
        var grp = _.groupBy(availableTechProducts, 'Type__c');
        _.mapKeys(grp,function(value,key){
            var newGroup = {}
            newGroup.Name = key;
            if(key === 'Tech' || key === 'Products') {
                newGroup.isexpanded = true;
            }
            groupedProducts.push(newGroup);
        });
        //console.log(availableTechProducts);
        //console.log(groupedProducts);
        component.set('v.groupedProducts2',groupedProducts);
        component.set('v.allServiceProducts',allServiceProducts);
        component.set('v.availableTechProducts',availableTechProducts);  
       
        //console.log('AFTER AVAILABILITY UPDATE');
        //console.log(component.get('v.allServiceProducts'));
        ;        
    },
    
    serviceProductsSet1: function(component,event,helper){
       // console.log('CT HLPR: serviceProductsSet1');
        var appEvent = $A.get("e.c:cpq_ServiceProductsSet");
        appEvent.setParams({
        });
        //console.log('CT HLPR: serviceProductsSet1 - FIRE appEvent cpq_ServiceProductsSet');
        appEvent.fire();
    },
    
    updateCombinedQlis: function(component,event,helper){
      //  console.log('CT HLPR: updateCombinedQlis');
        
        var techQlis = [];
        var serviceQlis = [];
        var combinedQlis = [];
        var selectedServiceBundles = [];
        _.forEach(component.get('v.techQlis'),function(t){  
            var techQli = {};
            techQli.TechBundle = t.Related_Bundle__c;
            techQli.TechId = t.Id;
            techQli.TechName = t.ProductName;     
            techQli.Quantity = t.Quantity__c;
            techQli.BundleKey = t.Bundle_Key__c;
            techQli.Multiplicity_Key__c = t.Multiplicity_Key__c;
            techQli.Revenue_Type__c = t.Revenue_Type__c;
            techQli.Type__c = t.Type__c;
            techQli.BundleDiscount = t.BundleDiscount;
            techQlis.push(techQli);
        });
        _.forEach(component.get('v.serviceQlis'),function(s){
            var serviceQli = {};
            serviceQli.TechId = s.Bundle_Product_Prerequisite__c;
            serviceQli.ServiceBundle = s.Related_Service_Bundle__c;
            serviceQli.ServiceId = s.Id;
            serviceQli.ServiceName = s.ProductName;
            serviceQli.Quantity = s.Quantity__c;
            serviceQlis.push(serviceQli);
        });
        _.forEach(component.get('v.selectedServiceBundles'),function(sb){
         //   console.log('sb')
        //    console.log(sb)
            var selectedServiceBundle = {};
            selectedServiceBundle.Id = sb.Id;
            selectedServiceBundle.Custom__c = sb.Custom__c;
            selectedServiceBundle.BundleType = sb.Bundle_Type__c;
            selectedServiceBundle.ProviderSelected = sb.Provider_Selected__c;
            selectedServiceBundle.Name = sb.Name;
            selectedServiceBundle.PartnerDiscount = sb.PartnerDiscount;
            selectedServiceBundles.push(selectedServiceBundle);
       //     console.log('selectedServiceBundle')
      //      console.log(selectedServiceBundle)
      //      console.log('selectedServiceBundles')
      //      console.log(selectedServiceBundles)
        });
        
        var appEvent = $A.get("e.c:cpq_SendToRulesEngine");   
        appEvent.setParams({
            arr: component.get('v.arr'),
            netTotal: component.get('v.netTotal'),
            techQlis: techQlis,
            serviceQlis: serviceQlis,
            selectedServiceBundles: selectedServiceBundles,
            allBundleProducts: component.get('v.allBundleProducts'),
            allServiceProducts: component.get('v.allServiceProducts'),
            quoteType: component.get('v.quoteType'),
            licenseExp: component.get('v.licenseExp'),
            upgradeType: component.get('v.upgradeType')
            
        });
        //console.log('CT HLPR: updateCombinedQlis - FIRE appEvent cpq_SendToRulesEngine');
        appEvent.fire(); 
        
    },
    
    sortQlis : function(component, event, helper) {
      //  console.log('CT HLPR: sortQlis');
        var techQlis = component.get('v.techQlis');
        var sortable = [];
        var groupedProducts=[];
        var grp = _.groupBy(techQlis,'Type__c');
        _.mapKeys(grp,function(value,key){
            groupedProducts.push(key);
        });
        component.set('v.groupedProducts', groupedProducts);        
       
        var tier = component.get('v.selectedTier');
        var tierNumber = Number(tier); 
        //  console.log(tier);        
        //   console.log(component.get('v.coreTiers'));
        var tierInTiers =  _.findIndex(component.get('v.coreTiers'),{'Maximum__c':tierNumber});      
        //  console.log(tierInTiers);
        if(tierInTiers === -1){
            component.set('v.matchingTier',false);
        }else{
            component.set('v.matchingTier',true);
        }
        
    },
    
    calculateMergeCredits : function(component, event, helper) {
       // console.log('CT HLPR: calculateMergeCredits');
        var licenses = component.get('v.originalLicenses');
        var techQlis = component.get('v.techQlis');
        var total = 0;
        var netTotal = 0;
        var arr = 0;
        var amount = 0;
        var currency = component.get('v.currency');
        var creditType = 'CreditT';
        var bundleProducts = component.get('v.allBundleProducts');
        var creditProduct = _.filter(bundleProducts, {'ProductCode' : creditType})[0];
        var creditLine = _.findIndex(techQlis, {'Bundle_Product__c' : creditProduct.Id});
        _.forEach(licenses, function(l) {
            amount = amount + ((l.Invoice_Amount__c / l.licenseCurrency * currency) * (l.MergeTerm / 12));
        });
        if(creditLine === -1) {
            creditProduct.ProductName = creditProduct.Related_Product__r.Name;
            creditProduct.Quantity__c = 1;
            creditProduct.BundleDiscount = 0;
            creditProduct.PriceUpdate = true;
            creditProduct.PriceEach = -1 * amount;
            creditProduct.PriceTotal = -1 * amount;
            creditProduct.Availability__c = 'Included';
            techQlis.push(creditProduct);
        }
        else {
            techQlis[creditLine].PriceUpdate = true;
            techQlis[creditLine].PriceEach = -1 * amount;
            techQlis[creditLine].PriceTotal = -1 * amount;
            techQlis[creditLine].Availability__c = 'Included';
        }
        techQlis =  _.sortBy(techQlis,['groupSortOrder','sortOrder']);
        component.set('v.techQlis', techQlis);
        _.forEach(techQlis, function(o) {
            total += o.PriceTotal;
            netTotal += o.PriceTotal - o.BundleDiscount;
            if(o.Related_Product__r.Revenue_Type__c == 'Recurring'){
                arr+=o.PriceTotal;
            }
        });
        component.set('v.mergeTermsChanged', false);
        component.set('v.qliTotal',total);
        component.set('v.netTotal',netTotal);
        component.set('v.arr',arr);
        // helper.sortQlis(component, event, helper);
    }
})