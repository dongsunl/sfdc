({
    fetchPickListVal: function(component, fieldName, picklistOptsAttributeName) {   
        //console.log('InlineEdit Helper: fetchPicklistVal');
        var opts = [];
        var allValues = component.get("v.dashboardUserTiers");
        if (allValues != undefined && allValues.length > 0) {
            opts.push({
                class: "optionClass",
                label: "--- None ---",
                value: ""
            });
        }
        for (var i = 0; i < allValues.length; i++) {
            opts.push({
                class: "optionClass",
                label: allValues[i].Maximum__c,
                value: allValues[i].Maximum__c
            });
        }
        component.set("v." + picklistOptsAttributeName, opts);        
    },
    
    fetchPickListVal2: function(component, fieldName, picklistOptsAttributeName) {   
        //console.log('InlineEdit Helper: fetchPicklistVal2');
        var opts = [];
        var allValues = component.get("v.pvTiers");
        if (allValues != undefined && allValues.length > 0) {
            opts.push({
                class: "optionClass",
                label: "--- None ---",
                value: ""
            });
        }
        for (var i = 0; i < allValues.length; i++) {
            opts.push({
                class: "optionClass",
                label: allValues[i].Maximum__c,
                value: allValues[i].Maximum__c
            });
        }
        component.set("v." + picklistOptsAttributeName, opts);        
    },
    
    getEditablePrices : function(component, event, helper) {
        //console.log('InlineEdit Helper: getEditablePrices');
        var type = component.get('v.singleRec.Type__c');
        var quotingPermissions = component.get('v.quotingPermissions');
        var priceEditable = quotingPermissions[0].Price_Editable__c;
        var editablePrices = [];
        editablePrices.push(_.split(priceEditable, ','));
        editablePrices = _.flatten(editablePrices);
        var editable = _.indexOf(editablePrices, type);
        if(editable > -1) {
            component.set('v.priceEditable', true);
        }
    },
    
    updateQlis : function(component, event, helper) {
        //console.log('InlineEdit: updateQlis');
        var singleRec = component.get('v.singleRec');
        var records = [];
        records.push(singleRec);
        var appEvent = component.getEvent("inlineEdit");
        appEvent.setParams({
            singleRecord : records,
            deleteFeature: component.get('v.deleteFeature')
        });
        appEvent.fire();
    }
    
})