({
    doInit : function(component, event, helper) {
        var amount = component.get("v.amount");
        var locale = component.get("v.culturecode");
        var currencycode = component.get("v.currencycode");
        var formattedamount = amount.toLocaleString(locale, { style: 'currency', currency: currencycode });
        component.set("v.formattedamount", formattedamount);
    },
    
    selectChange : function(component, event, helper) {
        var selected = component.get("v.selected");
        if(selected){
            var selectEvent = component.getEvent("optionselect");
            selectEvent.setParams({
                partner : component.get("v.partner"),
                qualtricsinvoice : component.get("v.qualtricsinvoice"),
                partnerSelected : true
            });
            selectEvent.fire();
        }
    },
})