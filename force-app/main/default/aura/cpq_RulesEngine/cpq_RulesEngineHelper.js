({
    checkIfBundleProductIncluded : function(techQlis, productNameIncludes){
        var bundleproductIncluded = false;
        if(productNameIncludes.toUpperCase().includes('SERVICES MAINTENANCE')) {
            var countOfES = 0;
            var countOfMaint = 0;
            //console.log(techQlis);
            _.forEach(techQlis, function(qli) {
                if(qli.TechName.toUpperCase().includes('SERVICES MAINTENANCE') && qli.Multiplicity_Key__c != undefined) {
                    countOfMaint++;
                } else if(qli.TechName.toUpperCase().includes('ENGINEERING SERVICES') && qli.Multiplicity_Key__c != undefined) {
                    countOfES++;
                }
            });
            //console.log(countOfMaint);
            //console.log(countOfES);
            if(countOfES > countOfMaint || (countOfES === 0 && countOfMaint === 0)) {
                bundleproductIncluded = false
            } else {
                bundleproductIncluded = true
            }
        } else if(productNameIncludes.toUpperCase().includes('ENGINEERING SERVICES')) {
            var countOfES = 0;
            var countOfMaint = 0;
            //console.log(techQlis);
            _.forEach(techQlis, function(qli) {
                if(qli.TechName.toUpperCase().includes('SERVICES MAINTENANCE') && qli.Multiplicity_Key__c != undefined){
                    countOfMaint++;
                } else if(qli.TechName.toUpperCase().includes('ENGINEERING SERVICES') && qli.Multiplicity_Key__c != undefined) {
                    countOfES++;
                }
            });
            //console.log(countOfMaint)
            //console.log(countOfES);
            if(countOfES < countOfMaint || (countOfES === 0 && countOfMaint === 0)) {
                bundleproductIncluded = false;
            } else {
                bundleproductIncluded = true;
            }
        } else {
            //console.log(techQlis);
            //console.log(productNameIncludes);
            for(var i=0; i < techQlis.length; i++){
                if(bundleproductIncluded == true){break;}
                var bundleprodname = techQlis[i].TechName
                if(bundleprodname == productNameIncludes){
                    bundleproductIncluded = true;
                }
                
            }
        }
        //console.log(techQlis);
        return bundleproductIncluded;
    },
    
    checkIfServiceBundleProductIncluded : function(serviceQlis, productNameIncludes) {
        var servicebundleproductincluded = false;
        for(var i = 0; i < serviceQlis.length; i++) {
            if(servicebundleproductincluded == true) {break;}
            var servicebundleprodname = serviceQlis[i].ServiceName;
            if(servicebundleprodname.includes(productNameIncludes)) {
                servicebundleproductincluded = true;
            }
        }
        return servicebundleproductincluded;
    },
    
    checkIfLicenseProductIncluded: function(licenses, productName){
        var licenseProductIncluded = false;
        if(licenses != undefined) {
            for(var i = 0; i < licenses.length; i++){
                var licenseProductName = licenses[i].product;
                //console.log(licenseProductIncluded);
                if(licenseProductName == productName){
                    licenseProductIncluded = true;
                }
            }
        }
        return licenseProductIncluded;
    },
    
    updateAvailabilityForServiceBundleProduct : function(availability, servicebundles, allServiceProducts, product, availabilitytype){
        
        for(var n=0; n < allServiceProducts.length; n++){
            if(allServiceProducts[n].Name == product){
                var av = {};
                av.product = allServiceProducts[n].Id;
                av.isAvailable = availabilitytype;
                availability.push(av);
            }
        }
        
        return availability;
    },
    
    updateAvailabilityForBundleProduct : function(availability, bundleid, bundleproducts, product, availabilitytype){
        for(var i=0; i < bundleproducts.length; i++){
            if(bundleproducts[i].Related_Bundle__c == bundleid && bundleproducts[i].Related_Product__r.Name == product){
                var av = {};
                av.product = bundleproducts[i].Id;
                av.isAvailable = availabilitytype;
                availability.push(av);
            }
        }
        return availability;
    },
    
    serviceLineItemUpdate : function(serviceQlis, lineItemUpdates, productname, price, quantity, maxquantity){
        //   console.log('Rules Helper: serviceLineItemUpdate');
        
        for(var i=0; i < serviceQlis.length; i++){
            if(serviceQlis[i].ServiceName == productname){
                var liUpdate = {};
                liUpdate.product = serviceQlis[i].ServiceId;
                if(price != null) {
                    liUpdate.price = price*serviceQlis[i].PartnerDiscount;
                }
                liUpdate.quantity = quantity;
                liUpdate.maxQuantity = maxquantity;
                lineItemUpdates.push(liUpdate);
            }
        }
        // console.log(lineItemUpdates);
        return lineItemUpdates;
    },
    
    techLineItemUpdate : function(techQlis, lineItemUpdates, productname, price, totalPrice, quantity, maxquantity){
        for(var i=0; i < techQlis.length; i++){
            if(techQlis[i].TechName == productname){
                var liUpdate = {};
                liUpdate.product = techQlis[i].TechId;
                liUpdate.price = price;
                liUpdate.totalPrice = totalPrice;
                liUpdate.quantity = quantity;
                liUpdate.maxQuantity = maxquantity;
                lineItemUpdates.push(liUpdate);
            }
        }
        return lineItemUpdates;
    },
    
    processAddRemoveRuleServiceBP : function(addremoves, techQlis, serviceQlis, servicebundles, servicebundleproducts, productName, bundleIncludes) {
        //   console.log('process add remove service bp');
        
        if(servicebundles.length > 0){
            var bundleproductincluded = this.checkIfBundleProductIncluded(techQlis, bundleIncludes);
            
            var servicebundleproductincluded = this.checkIfServiceBundleProductIncluded(serviceQlis, bundleIncludes);
            
            if(bundleproductincluded && !servicebundleproductincluded){
                
                for(var i=0; i < servicebundles.length; i++){
                    for(var n=0; n < servicebundleproducts.length; n++){
                        if(servicebundleproducts[n].Related_Service_Bundle__c == servicebundles[i].Id && servicebundleproducts[n].Name == productName && servicebundles[i].Custom__c != true){
                            var addition = {};
                            addition.toBeAdded = servicebundleproducts[n].Id;
                            addition.addTo = servicebundleproducts[n].Related_Service_Bundle__c;
                            addition.type = "Add";
                            addition.PartnerDiscount = servicebundles[i].PartnerDiscount;
                            addition.techOrService = 'Service';
                            addremoves.push(addition);
                        }
                    }
                }
                
            }
            if(!bundleproductincluded){
                var servicebundleproduct = false;
                for(var i=0; i < serviceQlis.length; i++){
                    //    console.log('currently searching for service bundle: ' + bundleIncludes);
                    //    console.log('currently reviewing serviceQli: ' + JSON.stringify(serviceQlis[i]));
                    if(servicebundleproduct == true){break;}
                    var servicebundleprodname = serviceQlis[i].ServiceName;
                    if(servicebundleprodname.includes(productName)){
                        servicebundleproduct = true;
                    }
                }
                // console.log(bundleIncludes + ' contained in service products: ' + servicebundleproduct);
                if(servicebundleproduct){
                    for(var i=0; i < serviceQlis.length; i++){
                        //   console.log('reviewing ServiceQli with name: ' + serviceQlis[i].ServiceName);
                        //  console.log('trying to match product name' + productName);
                        if(serviceQlis[i].ServiceName.includes(productName)){
                            console.log('ServiceQli/productName match for ' + productName);
                            var removal = {};
                            removal.toBeRemoved = serviceQlis[i].ServiceId;
                            removal.removeFrom = serviceQlis[i].ServiceBundle;
                            removal.type = "Remove";
                            addremoves.push(removal);
                            //   console.log('removal has been pushed to array');
                        }
                    }
                }
            }
        }
        // console.log(addremoves);
        return addremoves;
    },
    
    checkCoreQuantity : function(techQlis, coreQuantity){
        var selectedQuantity = 0;
        var coreIndex = _.findIndex(techQlis, function(qli){
            return qli.TechName.includes('Core');
        })
        if(coreIndex > -1){
            selectedQuantity = techQlis[coreIndex].Quantity;
        }
        var coreHighEnough = false;
        if(selectedQuantity >= coreQuantity){
            coreHighEnough = true;
        }
        return coreHighEnough;
    },
    
    processAddRemoveRuleTechBP : function(addremoves, techQlis, bundleproducts, bundlename, productName, bundleIncludes, selectedServiceBundles, serviceBundleName, coreQuantity) {
        //console.log('processAddRemoveRuleTechBP');
        selectedServiceBundles = _.flatten(selectedServiceBundles);
        var serviceBundle = _.findLastIndex(selectedServiceBundles, function(bundle) {
            return bundle.Name.includes(serviceBundleName);
        });
        //console.log(selectedServiceBundles);
        //console.log(serviceBundle);
        //console.log(bundleproducts);
        if(techQlis.length > 0){
            var productInQlis = this.checkIfBundleProductIncluded(techQlis, productName); //Check to see if the product to add is already included in the tech Qlis. 
            //console.log(productName);
            //console.log(productInQlis);
            var bundleproductincluded = this.checkIfBundleProductIncluded(techQlis, bundleIncludes); //Check to see if the bundle product that triggers the addition is in the tech Qlis
            //console.log(bundleIncludes);
            //console.log(bundleproductincluded);
            var coreHighEnough;
            if(coreQuantity != undefined){
                coreHighEnough = this.checkCoreQuantity(techQlis, coreQuantity);
            }
            if(bundleproductincluded && !productInQlis && (coreHighEnough || coreHighEnough == undefined)){
                for(var i=0; i < bundleproducts.length; i++){
                    if(bundleproducts[i].Name.includes(productName) && bundleproducts[i].Related_Bundle__r.Name == bundlename){
                        var addition = {};
                        addition.toBeAdded = bundleproducts[i].Id;
                        addition.type = "Add";
                        if(serviceBundle > -1) {
                            addition.addTo = selectedServiceBundles[serviceBundle].Bundle_Product_Prerequisite__c;
                            addition.Multiplicity_Key__c = selectedServiceBundles[serviceBundle].Multiplicity_Key__c;
                            addition.PartnerDiscount = selectedServiceBundles[serviceBundle].PartnerDiscount;
                        }
                        
                        addition.techOrService = 'Tech';
                        addremoves.push(addition);
                    }
                }
            }
            if(!bundleproductincluded || coreHighEnough == false){
                var bundleproduct = false;
                for(var i=0; i < techQlis.length; i++){
                    //console.log('currently searching for service bundle: ' + bundleIncludes);
                    //console.log('currently reviewing serviceQli: ' + JSON.stringify(techQlis[i]));
                    if(bundleproduct == true){break;}
                    var bundleprodname = techQlis[i].TechName;
                    if(productName.toUpperCase().includes('SERVICES MAINTENANCE')) {
                        if(bundleprodname.includes(productName) && techQlis[i].Multiplicity_Key__c != undefined) {
                            bundleproduct = true;
                        }
                    }
                    else if(bundleprodname.includes(productName)){
                        bundleproduct = true;
                    }
                }
                //console.log(techQlis);
                //console.log(bundleproduct);
                if(bundleproduct){
                    for(var i=0; i < techQlis.length; i++){
                        //console.log('reviewing ServiceQli with name: ' + serviceQlis[i].ServiceName);
                        //console.log('trying to match product name' + productName);
                        if(techQlis[i].TechName.includes(productName)){
                            //    console.log('ServiceQli/productName match for ' + productName);
                            var removal = {};
                            removal.toBeRemoved = techQlis[i].TechId;
                            removal.type = "Remove";
                            removal.techOrService = 'Tech';
                            removal.Multiplicity_Key__c = techQlis[i].Multiplicity_Key__c;
                            addremoves.push(removal);
                            //   console.log('removal has been pushed to array');
                        }
                    }
                }
            }
        }
        //console.log(addremoves);
        return addremoves;
    }
})