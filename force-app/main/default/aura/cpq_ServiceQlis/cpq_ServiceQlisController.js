({
    doInit: function (component, event, helper) {    
      //  console.log('Service Controller: Init');
        helper.getServiceBundles(component,event,helper);
    },
    
    onServiceBundleChange : function(component, event, helper) {
       // console.log('Service Controller: ServiceBundleChange');
        var bundleId = component.find('bundleSelect').get('v.value');
        component.set('v.selectedServiceBundle', _.filter(availableServiceBundles, {'Id' : bundleId}));
        helper.getIncludedServiceComponents(component,event,helper);  
        helper.getAvailableServiceComponents(component,event,helper);          
    },
    
    onFeatureSelect : function(component,event,helper){      
       // console.log('Service Controller: Feature Select');
        var target=event.getSource();
        var feature = target.get('v.value'); 
        var addedFeatures = component.get('v.addedFeatures');
        var currentServiceQlis = component.get('v.currentServiceQlis');   
        var selectedServiceBundleProducts = component.get('v.selectedServiceBundleProducts'); 
        var inc = _.filter(selectedServiceBundleProducts,{'Id': feature}); 
        currentServiceQlis.push(inc[0]);  
        addedFeatures.push(inc[0]);
        component.set('v.addedFeatures',addedFeatures);
        var featureRemove = _.findIndex(selectedServiceBundleProducts,{'Id': feature});  
        _.pullAt(selectedServiceBundleProducts,featureRemove);
        component.set('v.featureSelected',feature);
        component.set('v.featureAddOrRemove','Add');
        component.set('v.currentServiceQlis',currentServiceQlis);
        component.set('v.selectedServiceBundleProducts',selectedServiceBundleProducts);
        var digitalFeedbackDomainQuantity = 0;
        var digitalFeedbackInterceptQuantity = 0;
         _.forEach(_.flatten(currentServiceQlis),function(s){
            if(s.ProductName === 'CX Website Feedback Domain' || s.ProductName === 'RC Website Feedback Domain'){
                digitalFeedbackDomainQuantity = s.Quantity__c * s.Quantity_Multiplier__c;
            }else if(s.ProductName === 'CX Website Feedback Intercept' || s.ProductName === 'RC Website Feedback Domain'){
                digitalFeedbackInterceptQuantity = s.Quantity__c * s.Quantity_Multiplier__c;
            }
        });
        component.set('v.digitalFeedbackDomainQuantity',digitalFeedbackDomainQuantity);
        component.set('v.digitalFeedbackInterceptQuantity',digitalFeedbackInterceptQuantity);
        helper.updatePrice(component,event,helper);
         helper.updateCombinedQlis(component,event,helper);
    },
    
    onSave : function(component,event,helper){
       // console.log('Service Controller: onSave');   
         var addedFeatures = [];
        component.set('v.addedFeatures',addedFeatures);
        var appEvent = component.getEvent('serviceQliUpdate');
        appEvent.setParams({
            currentServiceQlis: component.get('v.currentServiceQlis'),
            serviceQliTotal: component.get('v.serviceQliTotal'),
            selectedServiceBundle: component.get('v.selectedServiceBundle')
        });
        appEvent.fire(); 
    },
    
    onClose: function(component,event,helper){
       // console.log('Service controller: onClose');
        var addedFeatures = component.get('v.addedFeatures');
        var currentServiceQlis = component.get('v.currentServiceQlis'); 
        if(addedFeatures.length>0){
            addedFeatures.forEach(function(a){
               // console.log(a);
                var featureRemove = _.findIndex(currentServiceQlis,{'Id': a.Id}); 
               // console.log(featureRemove);
                _.pullAt(currentServiceQlis,featureRemove);
                //console.log(currentServiceQlis);
            });
        }
        component.set('v.currentServiceQlis',currentServiceQlis);
        addedFeatures = [];
        component.set('v.addedFeatures',addedFeatures);
        var total=0;
        var currentServiceQlis = _.forEach(currentServiceQlis,function(o){    
            total += o.PriceTotal;
        });
       
        component.set('v.serviceQliTotal',total);
         var appEvent = component.getEvent('serviceQliUpdate');
        appEvent.setParams({
            currentServiceQlis: component.get('v.currentServiceQlis'),
            serviceQliTotal: component.get('v.serviceQliTotal'),
            selectedServiceBundle: component.get('v.selectedServiceBundle')
        });
        appEvent.fire(); 
    },
    
    onPriceListChange: function(component,event,helper){
      //  console.log('Service Controller: PriceListChange');
        helper.updatePrice(component,event,helper);
    },
    
    onQuantityChange : function(component,event,helper){ 
       // console.log('ServiceLIneEdit Controller: QuantityChange');
        var quantity = event.getSource().get("v.value").trim();
        var recordId = event.getSource().get("v.name").trim();
        var currentServiceQlis = component.get('v.currentServiceQlis');
        var total=0;
        var digitalFeedbackDomainQuantity = 0;
        var digitalFeedbackInterceptQuantity = 0;
        _.forEach(currentServiceQlis,function(s){
            if(s.Id === recordId){
                var max = s.Max_Cap__c;
                var min = s.Min_Cap__c;
                if(quantity > max){
                    alert('The quantity cannot be higher than '+max);
                    quantity = max;
                }
                if(quantity < min){
                    alert('The quantity cannot be lower than '+min);
                    quantity = min;
                }
                s.Quantity__c = quantity;
                s.PriceTotal = s.PriceEach * quantity;
                // component.find('Quantity').set('v.value',quantity);
            }
            total += s.PriceTotal;
            
         if(s.ProductName === 'CX Website Feedback Domain' || s.ProductName === 'RC Website Feedback Domain'){
            // console.log(s.Quantity__c);
                digitalFeedbackDomainQuantity = s.Quantity__c * s.Quantity_Multiplier__c;
            }else if(s.ProductName === 'CX Website Feedback Intercept' || s.ProductName === 'RC Website Feedback Intercept'){
                digitalFeedbackInterceptQuantity = s.Quantity__c * s.Quantity_Multiplier__c;
            }
        });
     
        component.set('v.digitalFeedbackDomainQuantity',digitalFeedbackDomainQuantity);
        component.set('v.digitalFeedbackInterceptQuantity',digitalFeedbackInterceptQuantity);
        component.set('v.currentServiceQlis', currentServiceQlis); 
        component.set('v.serviceQliTotal',total);
        helper.updateCombinedQlis(component,event,helper);
    },
    
    onPriceChange : function(component,event,helper){
       // console.log('ServiceLIneEdit Controller: PriceChange');
        var price = event.getSource().get("v.value").trim();
        var recordId = event.getSource().get("v.name").trim();
        var currentServiceQlis = component.get('v.currentServiceQlis');
        var total=0;
        _.forEach(currentServiceQlis,function(s){
            if(s.Id === recordId){
                s.PriceEach = price;
                s.PriceTotal = price * s.Quantity__c;
                s.PriceUpdate = true;
                //  component.find('PriceEach').set('v.value',price);
            }
            total += s.PriceTotal;
        });
        component.set('v.currentServiceQlis', currentServiceQlis);
        component.set('v.serviceQliTotal',total);
         helper.updateCombinedQlis(component,event,helper);
    },  
    
    deleteFeature: function(component,event,helper){
      //  console.log('ServiceLIneEdit Controller: deleteFeature');
        var target=event.getSource();
        var feature = target.get('v.value');  
        var currentServiceQlis = component.get('v.currentServiceQlis');
        var selectedServiceBundleProducts = component.get('v.selectedServiceBundleProducts'); 
        var inc = _.filter(currentServiceQlis,{'Id': feature}); 
        selectedServiceBundleProducts.push(inc[0]); 
        selectedServiceBundleProducts = _.sortBy(selectedServiceBundleProducts, 'Name');
        var featureRemove = _.findIndex(currentServiceQlis,{'Id': feature});  
        _.pullAt(currentServiceQlis,featureRemove);
        
        component.set('v.selectedServiceBundleProducts',selectedServiceBundleProducts);
        helper.updatePrice(component,event,helper);
         helper.updateCombinedQlis(component,event,helper);
    },

    onSearch : function(component, event, helper) {
       // console.log('ServiceQlis Controller: onSearch');
        var searchKey = event.getSource().get('v.value');
        var availableFeatures = component.get('v.selectedServiceBundleProducts');
        var matchingFeatures = _.filter(availableFeatures, function(product) {
            return product.Related_Product__r.Name.toUpperCase().includes(searchKey.toUpperCase());
        });
        if(searchKey != '') {
            component.set('v.matchingFeatures', matchingFeatures);
        }
    }
})