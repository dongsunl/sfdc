({
    
    onBundleChange : function(component, event, helper){
        console.log('TQ CTRL: onBundleChange');
        component.set('v.coreDiscountPercentage', 1);
        component.set('v.nonCoreDiscountPercentage', 1);
        var selectedTier = component.get('v.selectedTier');
        var coreTiers = event.getParam('coreTiers');
        var coreQuantity = Number(component.get('v.selectedTier'));
        var bundle = component.get('v.selectedBundle');
        var quoteType = component.get('v.quoteType');
        var upgradeType = component.get('v.upgradeType');
        var dashboardUsers = component.get('v.dashboardUsers');
        var licenses = component.get('v.licenses');
        var selectedServiceBundles = [];
        var serviceQlis = [];
        var tierMatched = false;
        var allBundleProducts = component.get('v.allBundleProducts');
        var fromRules = false;
        component.set('v.selectedServiceBundles', selectedServiceBundles);
        component.set('v.serviceQlis', serviceQlis);
        
        if(coreTiers.length >= 1) {            
            _.forEach(coreTiers, function(tier) {
                if(tier.Maximum__c == selectedTier) {
                    tier.selected = true;
                    tierMatched = true;
                }
            });
            if(component.get('v.experience')[0].Core_Entry_Type__c === 'Enter' && selectedTier != undefined) {                
                component.set('v.enteredTier', coreQuantity);
                tierMatched = true;
            }
            if((!(coreQuantity > 1)) || tierMatched === false) {                
                coreQuantity = coreTiers[0].Maximum__c;
                coreTiers[0].selected = true;
            }
            component.set('v.coreTiers', coreTiers);
        }
        
        if(upgradeType != 'merge') {
            var techQlis = [];
            component.set('v.techQlis', techQlis);
        }
        else {
            var techQlis = component.get('v.techQlis');            
        }
        allBundleProducts.forEach(function(product) {
            if(product.Availability__c === 'Included' || (product.Default_Service__c === true && quoteType!=='Upgrade License')) {
                var productInQlis = _.findIndex(techQlis, {'Related_Product__c' : product.Related_Product__c});
                if(productInQlis === -1) {
                    if(product.PricingType__c === 'Core') {
                        product.TierQty = coreQuantity;
                        product.Quantity__c = coreQuantity;
                    }
                    else if(product.PricingType__c === 'UserD' && dashboardUsers !== undefined && dashboardUsers != 0) {
                        product.Quantity__c = dashboardUsers;
                    }
                        else {
                            product.TierQty = 1;
                        }                    
                    product.PriceUpdate = false;
                    techQlis.push(product);
                    if(product.Count_of_Service_Bundles__c > 0) {
                        //console.log('***TQ CTRL: onBundleChange - CALL HLPR addServiceBundles');
                        helper.addServiceBundles(component, event, helper, product);
                    }
                    if(product.Picklist_Value__c === true && product.PricingType__c === 'UserD'){
                        //console.log('***TQ CTRL: onBundleChange - CALL HLPR fetchPickListVal');
                        helper.fetchPickListVal(component, 'Maximum__c', 'dashboardUserTiersOptions');
                    }else if(product.Picklist_Value__c === true && product.PricingType__c === 'PV'){
                        //console.log('***TQ CTRL: onBundleChange - CALL HLPR fetchPickListVal2');
                        helper.fetchPickListVal2(component, 'Maximum__c', 'pvTiersOptions');
                    }
                }
                
            } else if(product.Default_Service__c && component.get('v.experience')[0].Code__c != component.get('v.licenseExp')) {                
                techQlis.push(product);
                helper.addServiceBundles(component, event, helper, product);
            }
        });
        if(techQlis.length>0){            
            if(quoteType === 'Upgrade License' && bundle[0].Academic_Tier_Comparison__c === licenses[0].Tier__c && bundle[0].Academic_Tier_Comparison__c != undefined){
                _.forEach(licenses, function(l) {
                    if(l.Bundle_Product__r.PricingType__c === 'Core') {
                        coreQuantity = l.Quantitiy__c;
                        //console.log('TQ CTRL: onBundleChange - SET enteredTier');
                        component.set('v.enteredTier', coreQuantity);
                        if(component.get('v.upgradeType') != 'keep-dates') {
                            helper.updatePrice(component, event, helper, techQlis,fromRules);
                        }
                    }
                });
            }else{
                //console.log('TQ CTRL: onBundleChange - CALL HLPR updatePrice');
                helper.updatePrice(component, event, helper, techQlis,fromRules);
                var hasMaintenance = false;
                _.forEach(licenses, function(l) {
                    if(l.Product__r.ProductCode.toUpperCase().includes('MAINT')) {
                        hasMaintenance = true;
                    }
                });
                if(hasMaintenance) {
                    console.log('***hasMaintenance');
                    helper.createMaintenanceLines(component, event, helper);
                }
            }
            /* if(quoteType === 'New License Quote') {
                console.log('TQ CTRL: onBundleChange - CALL HLPR updatePrice');
                helper.updatePrice(component, event, helper, techQlis);
            }
            else if(upgradeType === 'merge') {
                console.log('TQ CTRL: onBundleChange - CALL HLPR updatePrice');
                helper.updatePrice(component, event, helper, techQlis);
            }
                else if(quoteType === 'Upgrade License') {
                    if(bundle[0].Id !== licenses[0].Bundle__c && bundle[0].Academic_Tier_Comparison__c !== licenses[0].Tier__c || bundle[0].Id !== licenses[0].Bundle__c && bundle[0].Academic_Tier_Comparison__c === undefined){
                        if(upgradeType === 'keep-dates') {
                            var creditType = 'CreditM';
                            var licenseARR = -1*component.get('v.licenseARR') / licenses[0].licenseCurrency * component.get('v.currency');
                            console.log('TQ CTRL: onBundleChange - CALL HLPR updatePrice');
                            helper.updatePrice(component, event, helper, techQlis);
                            console.log('TQ CTRL: onBundleChange - CALL HLPR createCreditLine');
                            helper.createCreditLine(component, event, helper, licenseARR, creditType);
                        }
                        else {
                            console.log('TQ CTRL: onBundleChange - CALL HLPR updatePrice');
                            helper.updatePrice(component, event, helper, techQlis);
                      //  }
                    }
                    else if (bundle[0].Academic_Tier_Comparison__c === licenses[0].Tier__c) {
                        _.forEach(licenses, function(l) {
                            if(l.Bundle_Product__r.PricingType__c === 'Core') {
                                coreQuantity = l.Quantitiy__c;
                                console.log('TQ CTRL: onBundleChange - SET enteredTier');
                                component.set('v.enteredTier', coreQuantity);
                            }
                        });
                    }
                }*/
       }
    },
    
    onTierChange : function(component, event, helper){
        console.log('TQ CTRL: onTierChange');
        var selectedTier = Number(event.getParam('selectedTier'));
        if(component.get('v.originalTier')!= undefined && component.get('v.originalTier')!=selectedTier) {
            component.set('v.coreDiscountPercentage', 1);
            component.set('v.nonCoreDiscountPercentage', 1);
        }
        //console.log(component.get('v.dashboardUsers'));
        var qli = component.get('v.techQlis');
        var coreQuantity = selectedTier;
        //if(qli.length>0){
        //  var selectedTier = Number(component.get('v.selectedTier'));  
        var coreTiers = component.get('v.coreTiers');
        var tierMatched = false;
        var upgradeType = component.get('v.upgradeType');
        var quoteType = component.get('v.quoteType');
        var actionType = component.get('v.actionType');
        var techQlis = component.get('v.techQlis');
        var dashboardUsers = component.get('v.dashboardUsers');
        var allBundleProducts = component.get('v.allBundleProducts');
        var licenses = component.get('v.licenses');
        var selectedBundle = component.get('v.selectedBundle');
        var fromRules = false;
        var hasMaintenance = false;
        if(coreTiers.length >= 1) {
            _.forEach(coreTiers, function(tier) {
                if(tier.Maximum__c == selectedTier) {
                    tier.selected = true;
                    tierMatched = true;
                }else{
                    tier.selected = false;
                }
            });
            if(((!(coreQuantity > 1)) || tierMatched === false) && component.get('v.experience').Core_Entry_Type__c === 'Select') {
                coreQuantity = coreTiers[0].Maximum__c;
                coreTiers[0].selected = true;
            }
            component.set('v.coreTiers', coreTiers);
        }
        
        _.find(qli, function(o){
            if(o.PricingType__c === 'Core'){
                o.Quantity__c = selectedTier;
                o.Quantity = selectedTier;
            }
        });
        if(quoteType === 'Upgrade License' && upgradeType != 'merge' /*&& actionType!='Edit'*/) {
            var licenses = component.get('v.licenses');
            var coreTier = 0;
            var bundleId = selectedBundle[0].Id;
            _.forEach(licenses, function(l) {
                if(l.Bundle_Product__r.PricingType__c === 'Core') {
                    coreTier = l.Quantitiy__c;
                }
            });
            if(selectedTier !== coreTier && licenses != undefined && ((bundleId === licenses[0].Bundle__c) || (selectedBundle[0].Academic_Tier_Comparison__c === licenses[0].Tier__c)) /*&& upgradeType === 'keep-dates'*/) {
                // var techQlis = component.get('v.techQlis');
                _.forEach(allBundleProducts, function(product) {
                    if(product.Upgrade_Availability__c === 'Included') {
                        if(product.PricingType__c === 'Core') {
                            product.TierQty = coreQuantity;
                            product.Quantity__c = coreQuantity;
                        }
                        else if(product.PricingType__c === 'UserD' && dashboardUsers !== undefined && dashboardUsers != 0) {
                            product.Quantity__c = dashboardUsers;
                        }
                            else { 
                                product.TierQty = 1;
                            }
                        product.PriceUpdate = false;
                        var index = _.findIndex(techQlis, function(qli) {
                            return qli.Bundle_Product__c == product.Id;
                        });
                        if(!(index > -1)) {
                            //console.log(techQlis);
                            //console.log(product);
                            techQlis.push(product);
                        }
                        if(product.Count_of_Service_Bundles__c > 0) {
                            //console.log('TQ CTRL: onTierChange - CALL HLPR addServiceBundles');
                            helper.addServiceBundles(component, event, helper, product);
                        }
                        if(product.Picklist_Value__c === true && product.PricingType__c === 'UserD'){
                            //console.log('TQ CTRL: onTierChange - CALL HLPR fetchPickListVal');
                            helper.fetchPickListVal(component, 'Maximum__c', 'dashboardUserTiersOptions');
                        }else if(product.Picklist_Value__c === true && product.PricingType__c === 'PV'){
                            //console.log('TQ CTRL: onTierChange - CALL HLPR fetchPickListVal2');
                            helper.fetchPickListVal2(component, 'Maximum__c', 'pvTiersOptions');
                        }
                        
                    }
                });
                if(upgradeType === 'keep-dates'){
                    var creditType = 'CreditM';
                    console.log('****con licenseARR before: ', component.get('v.licenseARR'));
                    console.log('****Are you htting this logic too?');
                    var licenseARR = -1*component.get('v.licenseARR'); 
                    console.log('****con licenseARR after: ', licenseARR);
                    console.log('****con licenseCurrency: ', licenses[0]);
                    console.log('****con currency: ', component.get('v.currency'));
                    console.log('****con FinallicenseARR: ', licenseARR);
                    //console.log('TQ CTRL: onTierChange - CALL HLPR createCreditLine');
                    helper.createCreditLine(component, event, helper, licenseARR, creditType,techQlis,fromRules);
                }
                //  helper.updatePrice(component, event, helper, techQlis);
                //  helper.createCreditLine(component, event, helper, licenseARR, creditType);
            }
            /*else if(selectedTier !== coreTier && ((bundleId === licenses[0].Bundle__c) || (component.get('v.selectedBundle')[0].Academic_Tier_Comparison__c === licenses[0].Tier__c)) && component.get('v.upgradeType') !== 'keep-dates') {
                var techQlis = component.get('v.techQlis');
                _.forEach(allBundleProducts, function(product) {
                    if(product.Upgrade_Availability__c === 'Included') {
                        if(product.PricingType__c === 'Core') {
                            product.TierQty = coreQuantity;
                            product.Quantity__c = coreQuantity;
                        }
                        else if(product.PricingType__c === 'UserD' && component.get('v.dashboardUsers') !== undefined && component.get('v.dashboardUsers') != 0) {
                            product.Quantity__c = component.get('v.dashboardUsers');
                        }
                            else {
                                product.TierQty = 1;
                            }
                        product.PriceUpdate = false;
                        techQlis.push(product);
                        if(product.Count_of_Service_Bundles__c > 0) {
                            helper.addServiceBundles(component, event, helper, product);
                        }
                        if(product.Picklist_Value__c === true && product.PricingType__c === 'UserD'){
                            helper.fetchPickListVal(component, 'Maximum__c', 'dashboardUserTiersOptions');
                        }else if(product.Picklist_Value__c === true && product.PricingType__c === 'PV'){
                            helper.fetchPickListVal2(component, 'Maximum__c', 'pvTiersOptions');
                        }
                        
                    }
                });
                helper.updatePrice(component, event, helper, techQlis);
                //helper.getUpgradeLines(component, event, helper, selectedTier);
            }*/
        }
        // else {
        //console.log('TQ CTRL: onTierChange - CALL HLPR updatePrice');
        helper.updatePrice(component,event,helper, qli,fromRules);
        //console.log('CHECKING FOR MAINTENANCE');
        _.forEach(licenses, function(l) {
            if(l.Product__r.ProductCode.toUpperCase().includes('MAINT')) {
                hasMaintenance = true;
            }
        });
        //console.log(hasMaintenance);
        if(hasMaintenance) {
            helper.createMaintenanceLines(component, event, helper);
        }
        //  }
        //}
    },
    
    onEnteredTierChange : function(component, event, helper){
        console.log('TQ CTRL: onEnteredTierChange');
        var qli = component.get('v.techQlis'); 
        var fromRules = false;
        if(qli.length>0){
            var enteredTier = Number(component.get('v.enteredTier'));       
            _.find(qli, function(o){
                if(o.PricingType__c === 'Core'){
                    o.Quantity__c = enteredTier;
                    o.Quantity = enteredTier;
                }
            });
            //console.log('TQ CTRL: onEnteredTierChange - CALL HLPR updatePrice');
            helper.updatePrice(component,event,helper, qli,fromRules);
        }
        
    },
    
    onFeatureSelect : function(component,event,helper){
        console.log('TQ CTRL: FeatureSelect');         
        var qli = component.get('v.techQlis');
        //console.log(component.get('v.serviceQlis'))
        
        
        
        var prods = component.get('v.allBundleProducts');
        var feature = event.getParam('pk');
        var featureAddOrRemove = '';
        var featureInQlis = _.findIndex(qli,{'Comparison': feature});
        var newProduct = _.filter(prods,{'Comparison': feature});
        var fromRules = false;
        if(newProduct[0].Add_Multiple__c) {
            var countOfProduct = 0;
            _.forEach(qli, function(q) {
                if(q.Bundle_Product__c === newProduct[0].Id) {
                    countOfProduct++;
                }
            });
            newProduct[0].Multiplicity_Key__c = countOfProduct + 1;
        }
       // console.log('newProduct');
      //  console.log(newProduct[0]);        
        if(featureInQlis === -1 || newProduct[0].Add_Multiple__c){
            featureAddOrRemove = 'Add';
            newProduct[0].TierQty = 1;
            newProduct[0].PriceUpdate = false;
            newProduct[0].PriceEach = 0;
            newProduct[0].PriceTotal = 0;
            newProduct[0].BundleDiscount = 0;
            qli.push(newProduct[0]);
            component.set('v.featureSelected',newProduct[0].Id);
            component.set('v.featureAddOrRemove',featureAddOrRemove);
          //  console.log('newProductservicebundles');
       // console.log(newProduct[0].Count_of_Service_Bundles__c);  
            if(newProduct[0].Count_of_Service_Bundles__c > 0) {
                console.log('TQ CTRL: FeatureSelect - CALL HLPR addServiceBundles');
                helper.addServiceBundles(component, event, helper, newProduct[0]);
            }
            console.log('TQ CTRL: FeatureSelect CALL HLPR updatePrice'); 
            helper.updatePrice(component,event,helper, qli,fromRules);
        }
    },
    
    onServiceUpdate : function(component,event,helper){ 
        console.log('TQ CTRL: onServiceUpdate');
        var techQlis = component.get('v.techQlis');
        var total=0;
        var netTotal=0;
        var arr=0;
        var digitalFeedbackDomainQuantity;
        var digitalFeedbackInterceptQuantity;
        var fromRules = false;
        _.forEach(techQlis,function(value){
            if(value.Bundle_Product__c == event.getParam('selectedServiceBundle')[0].Bundle_Product_Prerequisite__c){ 
                value.PriceEach = event.getParam('serviceQliTotal');
                value.PriceTotal = event.getParam('serviceQliTotal');
                value.PriceUpdate = true;
                value.isOpen = false;
            }
            // total += value.PriceTotal;
            //  netTotal += value.PriceTotal - value.BundleDiscount;
            //  if(value.Related_Product__r.Revenue_Type__c == 'Recurring'){
            //      arr+=value.PriceTotal;
            //  }
        });
        //component.set('v.qliTotal',total);
        // component.set('v.netTotal',netTotal);
        // component.set('v.arr',arr);
        _.forEach(component.get('v.selectedServiceBundles'),function(bundle){
            if(bundle.Id == event.getParam('selectedServiceBundle')[0].Id){ 
                bundle.PriceTotal = event.getParam('serviceQliTotal');
                bundle.ServiceOriginalPrice = event.getParam('serviceQliTotal') / bundle.PartnerDiscount;
            }
        });
        // component.set('v.techQlis',techQlis);
        var currentServiceQlis = _.flatten(event.getParam('currentServiceQlis'));
        var serviceQlis = component.get('v.serviceQlis');
        var compareServiceQlis = _.filter(serviceQlis,['Related_Service_Bundle__c',event.getParam('selectedServiceBundle')[0].Id]);
        var additions = _.differenceBy(currentServiceQlis,compareServiceQlis,'Id');
        serviceQlis.push(additions);
        var subtractions = _.differenceBy(compareServiceQlis,currentServiceQlis,'Id');
        _.forEach(subtractions,function(s){
            var remove = _.findIndex(serviceQlis,{'Id': s.Id});
            _.pullAt(serviceQlis,remove);            
        });
        /*  component.set('v.serviceQlis',_.flatten(serviceQlis))
        _.forEach(_.flatten(serviceQlis),function(s){
            if(s.ProductName === 'CX Website Feedback Domain'){
                digitalFeedbackDomainQuantity = s.Quantity__c * s.Quantity_Multiplier__c;
            }else if(s.ProductName === 'CX Website Feedback Intercept'){
                digitalFeedbackInterceptQuantity = s.Quantity__c * s.Quantity_Multiplier__c;
            }else{
                digitalFeedbackDomainQuantity = 0;
                digitalFeedbackInterceptQuantity = 0;
            }
        });
        component.set('v.digitalFeedbackDomainQuantity',digitalFeedbackDomainQuantity);
        component.set('v.digitalFeedbackInterceptQuantity',digitalFeedbackInterceptQuantity);*/
        //console.log('TQ CTRL: onServiceUpdate - CALL HLPR serviceUpdateEvent');
        helper.serviceUpdateEvent(component,event,helper,fromRules);
        //console.log('TQ CTRL: onServiceUpdate - CALL HLPR calculateQliTotals');
        helper.calculateQliTotals(component,event,helper,techQlis,fromRules);
        // helper.updateCombinedQlis(component,event,helper);
    },
    
    onQuoteLineChange: function(component,event,helper){
        console.log('TQ CTRL: onQuoteLineChange');
        var techQlis = [];
        var fromRules = false;
        if(component.get('v.actionType')==='Edit'){
            _.forEach(component.get('v.quoteLines'),function(line){
                _.forEach(component.get('v.allBundleProducts'),function(product){
                    if(product.Id === line.Bundle_Product__c){
                        product.Quantity__c = line.Quantity
                        product.TierQty = 1;
                        product.PriceUpdate = false;
                        techQlis.push(product);
                        if(product.Count_of_Service_Bundles__c > 0){
                            helper.addServiceBundles(component,event,helper,product);
                        }
                    }
                });
            });
            //console.log('TQ CTRL: onQuoteLineChange - CALL HLPR updatePrice');
            helper.updatePrice(component,event,helper,techQlis,fromRules);  
        }
    },
    
    onTermChange : function(component, event, helper) {
        console.log('TQ CTRL: onTermChange');
        var techQlis = component.get('v.techQlis');
        var fromRules = false;
        if(techQlis.length>0){
            //console.log('TQ CTRL: onTermChange - CALL HLPR updatePrice');
            helper.updatePrice(component, event, helper, techQlis,fromRules);
        }
        
    },
    
    changeCredits : function(component, event, helper) {
        console.log('TQ CTRL: changeCredits');
        //console.log('TQ CTRL: changeCredits - CALL HLPR calculateCredits');
        helper.calculateCredits(component, event, helper);
    },
    
    onPartnerSelection : function(component, event, helper) {
        console.log('TQ CTRL: onPartnerSelection');
        var selectedPartners = event.getParam('selectedpartners');
        var partners = [];
        //console.log(selectedPartners);
        _.forEach(selectedPartners, function(partner) {
            partners.push(partner.partneravailability);
        });
        console.log('partners')
        console.log(partners)
        component.set('v.selectedPartners', partners);
        var serviceBundles = component.get('v.selectedServiceBundles');
        var techQlis = component.get('v.techQlis');
        var serviceQlis = _.flattenDeep(component.get('v.serviceQlis'));
        var prods = component.get('v.allBundleProducts');
        //var featureInQlis = _.findIndex(techQlis, {'Type__c' : 'Markup'}); This had to be updated 9/25 because new markup bundle products were added
        var featureInQlis = _.findIndex(techQlis, function(qli) {
            return qli.Related_Product__r.ProductCode.includes('ImpMarkup');
        });
        if(featureInQlis > -1) {
            _.pullAt(techQlis, featureInQlis);
        }
        console.log(techQlis);
        //console.log(markupLine);
        console.log(serviceQlis);
        var total = 0;
        var netTotal = 0;
        var arr = 0;
        console.log(selectedPartners);
        console.log(serviceBundles);
        if(selectedPartners.length > 0) {
            _.forEach(techQlis, function(qli) {
                _.forEach(selectedPartners, function(partner) {
                    _.forEach(serviceBundles, function(service) {
                        if(partner.servicebundle.Id == service.Id) {
                            service.Q_Partner__c = partner.partneravailability.Partner__c;
                            service.Partner_Availability__c = partner.partneravailability.Id;
                            service.Qualtrics_Invoicing__c = partner.qualtricsinvoice;
                            service.Provider_Selected__c = true;
                            service.Partner_Name__c = partner.partneravailability.Partner_Name__c;
                            service.PriceTotal = partner.servicebundle.ServiceOriginalPrice * (partner.partneravailability.Percentage_of_Cost__c / 100);
                            service.PartnerDiscount = (partner.partneravailability.Percentage_of_Cost__c / 100);
                            service.PartnerContact  = partner.partneravailability.Partner__r.Partner_SOW_Contact__c;
                            service.LeadTime = partner.partneravailability.Lead_Time__c
                        }
                        //console.log(service);
                        if(service.Bundle_Product_Prerequisite__c == qli.Bundle_Product__c) {
                            qli.PriceEach = service.PriceTotal;
                            qli.PriceTotal = service.PriceTotal;
                            qli.PriceUpdate = true;
                            qli.PartnerName = service.Partner_Name__c;
                            qli.LeadTime = service.LeadTime;
                        }
                        if(service.Bundle_Product_Prerequisite__c == qli.Bundle_Product__c && partner.servicebundle.Id == service.Id) {
                            if(partner.qualtricsinvoice == true && !(partner.partneravailability.Partner_Name__c.includes('Qualtrics'))) {
                                qli.HasMarkupLine = true;
                                //var newProduct = _.filter(prods, {'Type__c' : 'Markup'}); This had to be updated 9/25 because new markup bundle products were added
                                var newProduct = _.filter(prods, function(prod) {
                                    return prod.Related_Product__r.ProductCode.includes('ImpMarkup');
                                });
                                newProduct[0].PriceEach = service.PriceTotal * (partner.partneravailability.Markup_Percentage__c / 100);
                                newProduct[0].PriceTotal = service.PriceTotal * (partner.partneravailability.Markup_Percentage__c / 100);
                                newProduct[0].Quantity__c = 1;
                                newProduct[0].TierQty = 1;
                                newProduct[0].BundleDiscount = 0;
                                newProduct[0].MarkupPercentage = (partner.partneravailability.Markup_Percentage__c / 100);
                                newProduct[0].PriceUpdate = true;
                                newProduct[0].Availability__c = 'Included';
                                techQlis.push(newProduct[0]);
                                service.Markup_Bundle_Product__c = newProduct[0].Id;
                                total += newProduct[0].PriceTotal;
                                netTotal += newProduct[0].PriceTotal;
                            }
                        }
                        _.forEach(serviceQlis, function(sqli) {
                            if(sqli.Related_Service_Bundle__r.Id === service.Id && service.Bundle_Product_Prerequisite__c == qli.Bundle_Product__c) {
                                sqli.PartnerDiscount = service.PartnerDiscount;
                            }
                        });
                    });
                });
                total += qli.PriceTotal;
                netTotal += qli.PriceTotal - qli.BundleDiscount;
                if(qli.Related_Product__r.Revenue_Type__c == 'Recurring'){
                    arr+=qli.PriceTotal;
                }
            });
            var groupedProducts = [];
            var grp = _.groupBy(techQlis,'Type__c');
            _.mapKeys(grp,function(value,key){
                groupedProducts.push(key);
            });               
            component.set('v.groupedProducts', groupedProducts);
        }
        else {
            _.forEach(techQlis, function(qli) {
                _.forEach(serviceBundles, function(service) {
                    service.Q_Partner__c = '';
                    service.Partner_Availability__c = '';
                    service.Qualtrics_Invoicing__c = false;
                    if(service.Custom__c != true) {
                        service.Provider_Selected__c = false;
                    }
                    service.PriceTotal = service.ServiceOriginalPrice;
                    service.PartnerDiscount = 1;
                    if(service.Bundle_Product_Prerequisite__c == qli.Bundle_Product__c) {
                        qli.PriceEach = service.ServiceOriginalPrice;
                        qli.PriceTotal = service.ServiceOriginalPrice;
                        qli.PartnerName = 'None';
                    }
                    _.forEach(serviceQlis, function(sqli) {
                        if(sqli.Related_Service_Bundle__r.Id === service.Id) {
                            //sqli.PriceEach = sqli.ServiceOriginalPrice;
                            //sqli.PriceTotal = sqli.ServiceOriginalPrice;
                            sqli.PartnerDiscount = 1;
                        }
                    });
                });
                total += qli.PriceTotal;
                netTotal += qli.PriceTotal - qli.BundleDiscount;
                if(qli.Related_Product__r.Revenue_Type__c == 'Recurring'){
                    arr+=qli.PriceTotal;
                }
            });
        }
        //console.log(techQlis);
        component.set('v.qliTotal', total);
        component.set('v.arr', arr);
        component.set('v.netTotal', netTotal);
        // helper.sortQlis(component, event, helper);
        //console.log('TQ CTRL: onPartnerSelection - CALL HLPR updateServicePrice');
        helper.updateServicePrice(component, event, helper, serviceQlis);
    },
    
    onQuantityChange : function(component,event,helper){ 
        console.log('TQ CTRL: onQuantityChange');
        var quantity = event.getSource().get("v.value").trim();
        var recordId = event.getSource().get("v.name").trim();
        var techQlis = component.get('v.techQlis');
        var fromRules = false;
        if(techQlis.length>0){
            _.forEach(techQlis,function(s){
                if(s.Id === recordId){
                    var max = s.Max_Cap__c;
                    var min = s.Min_Cap__c;
                    if(quantity > max){
                        alert('The quantity cannot be higher than '+max);
                        quantity = max;
                    }
                    if(quantity < min){
                        alert('The quantity cannot be lower than '+min);
                        quantity = min;
                    }
                    s.Quantity__c = quantity;
                    s.PriceTotal = s.PriceEach * quantity;
                }
            });
            //console.log('TQ CTRL: onQuantityChange - CALL HLPR updatePrice');
            helper.updatePrice(component,event,helper,techQlis,fromRules);
            // helper.updateCombinedQlis(component,event,helper);
        }
        
    },
    
    onPriceChange : function(component,event,helper){
        console.log('ServiceLIneEdit Controller: PriceChange');
        var price = event.getSource().get("v.value").trim();
        var recordId = event.getSource().get("v.name").trim();
        var techQlis = component.get('v.techQlis');
        
        //console.log('techQlis');
        //console.log(techQlis);
        var fromRules = false;
        if(techQlis.length>0){
            _.forEach(techQlis,function(s){
                if(s.Id === recordId){
                    //console.log(s);
                    s.PriceEach = price;
                    s.PriceTotal = price * s.Quantity__c;
                    s.PriceUpdate = true;
                }
            });     
            //console.log('TQ CTRL: onPriceChange - CALL HLPR updatePrice');
            helper.updatePrice(component,event,helper,techQlis,fromRules);
            //    helper.updateCombinedQlis(component,event,helper);
        }
        
    },  
    
    onQuantitySelectChange : function(component,event,helper){
        console.log('TQ CTRL: onQuantitySelectChange');
        var quantity = event.getSource().get("v.value").trim();
        var recordId = event.getSource().get("v.name").trim();
        var techQlis = component.get('v.techQlis');
        var fromRules = false;
        if(techQlis.length>0){
            _.forEach(techQlis,function(s){
                if(s.Id === recordId){
                    s.Quantity__c = quantity;
                    s.PriceTotal = s.PriceEach * quantity;
                }
            });
            //console.log('TQ CTRL: onQuantitySelectChange - CALL HLPR updatePrice');
            helper.updatePrice(component,event,helper,techQlis,fromRules);
            //  helper.updateCombinedQlis(component,event,helper);
        }
    }, 
    
    openModel: function(component, event, helper) {   
        console.log('TQ CTRL: Open Modal');
        var recordId = event.getSource().get("v.value").trim();
        //  console.log(recordId);
        var techQlis = component.get('v.techQlis');
        
        _.forEach(techQlis,function(s){
            if(s.Id === recordId){
                // console.log(s);
                s.isOpen = true;
            }
        });     
        component.set('v.techQlis',techQlis);
        // component.set("v.isOpen", true); 
    },
    
    onClose: function(component, event, helper) {
        console.log('TQ CTRL: onClose');
        component.set("v.isOpen", false);
    },
    
    deleteFeature: function(component,event,helper){
        console.log('TQ CTRL: deleteFeature');
        // console.log(component.get('v.selectedServiceBundles'));
        var target=event.getSource();
        var feature = target.get('v.value'); 
        var featureComparison = target.get('v.name');  
        var techQlis = component.get('v.techQlis');
        var fromRules = false;
        //console.log(feature);
        //console.log(techQlis);
        var inc = _.filter(techQlis,{'Comparison': featureComparison});
        if(inc[0].Multiplicity_Key__c != undefined){
            component.set('v.Multiplicity_Key__c', inc[0].Multiplicity_Key__c);
        }
        // console.log(inc);
        var featureRemove = _.findIndex(techQlis,{'Comparison': featureComparison});  
        _.pullAt(techQlis,featureRemove);
        if(inc[0].Count_of_Service_Bundles__c > 0){
            //console.log('TQ CTRL: deleteFeature - CALL HLPR removeServiceBundles');
            helper.removeServiceBundles(component,event,helper,inc);
        }
        // console.log(techQlis);
        //console.log('TQ CTRL: deleteFeature - CALL HLPR updatePrice');
        helper.updatePrice(component,event,helper,techQlis,fromRules);
        //console.log('TQ CTRL: deleteFeature - CALL HLPR runAvUpdateEvent');
        helper.runAvUpdateEvent(component,event,helper,inc);
        //console.log(inc[0].Count_of_Service_Bundles__c );
        //   helper.updateCombinedQlis(component,event,helper);
    },
    
    rulesAddRemove: function(component,event,helper){
        console.log('TQ CTRL: rulesAddRemove');
        var additions = event.getParam('additions');
        var removals = event.getParam('removals');
        
        //console.log(additions);
        //   console.log(removals);
        
        if(additions.length > 0){
            //  console.log('add');
            _.forEach(additions, function(a){
                //   console.log(a);
                var type = a.techOrService;
                var productId = a.toBeAdded;
                var serviceBundle = a.addTo;
                var PartnerDiscount = a.PartnerDiscount;
                var Multiplicity_Key__c = a.Multiplicity_Key__c;
                //  console.log(type);
                if(type === 'Tech'){
                    //console.log('TQ CTRL: rulesAddRemove - CALL HLPR addTechComponentsFromRules');
                    helper.addTechComponentsFromRules(component,event,helper,productId, PartnerDiscount, serviceBundle, Multiplicity_Key__c);
                }else{
                    var bundleId = a.addTo;
                    var Multiplicity_Key__c = a.Multiplicity_Key__c;
                    //console.log('TQ CTRL: rulesAddRemove - CALL HLPR addServiceComponentsFromRules');
                    helper.addServiceComponentsFromRules(component,event,helper,bundleId,productId, PartnerDiscount, Multiplicity_Key__c);
                }
                
            });
            
        }
        if(removals.length>0){
            //console.log('remove'); 
            _.forEach(removals, function(r){
                var type = r.techOrService;
                var productId = r.toBeRemoved;
                var Multiplicity_Key__c = r.Multiplicity_Key__c;
                //console.log(r);
                
                if(type === 'Tech'){
                    //console.log('TQ CTRL: rulesAddRemove - CALL HLPR removeTechComponentsFromRules');
                    helper.removeTechComponentsFromRules(component,event,helper,productId, Multiplicity_Key__c);
                }else{
                    var bundleId = r.removeFrom;
                    var Multiplicity_Key__c = r.Multiplicity_Key__c;
                    //console.log('TQ CTRL: rulesAddRemove - CALL HLPR removeServiceComponentsFromRules');
                    helper.removeServiceComponentsFromRules(component,event,helper,bundleId,productId, Multiplicity_Key__c);
                }
                
            });
        }
        
    },
    
    rulesPricing: function(component,event,helper){
        console.log('TQ CTRL: rulesPricing');
        //console.log(event.getParam('lineItemUpdates'));
        _.forEach(event.getParam('lineItemUpdates'),function(l){
            var productId = l.product;
            var maxQuantity = l.maxQuantity;
            var price = l.price;
            var quantity = l.quantity;
            var totalPrice = l.totalPrice;
            var tech = l.tech;
            var multiplicityKey = l.multiplicityKey;
            if(maxQuantity != null){
                
            }
            if(price != null){
                //console.log('TQ CTRL: rulesPricing - CALL HLPR updateServicePriceFromRules');
                helper.updateServicePriceFromRules(component,event,helper,productId,price);
            }
            if(totalPrice != null){
                //console.log('TQ CTRL: rulesPricing - CALL HLPR updateServiceTotalPriceFromRules');
                helper.updateServiceTotalPriceFromRules(component,event,helper,productId,totalPrice);
            }
            if(quantity != null){
                if(tech != true){
                    //console.log('TQ CTRL: rulesPricing - CALL HLPR updateQuantityFromRules');
                    helper.updateQuantityFromRules(component,event,helper,productId,quantity);
                }
                else{
                    //console.log('TQ CTRL: rulesPricing - CALL HLPR updateTechQuantityFromRules');
                    helper.updateTechQuantityFromRules(component,event,helper,productId,quantity,multiplicityKey);  
                }
            }
        })
    },
    
    rulesValidation: function(component,event,helper){
        
        //console.log('TQ CTRL: rulesValidation DOES NOTHING');
    },
    
    servicesSet: function(component,event,helper){
        console.log('**TQ CTRL: servicesSet');
        console.log(component.get('v.serviceQlis'))
        var selectedServiceBundles = component.get('v.selectedServiceBundles');
         console.log(selectedServiceBundles);
        var techQlis = component.get('v.techQlis');
        
        _.forEach(techQlis,function(product){
            if(product.Count_of_Service_Bundles__c > 0) {
                _.forEach(selectedServiceBundles,function(s){
                    if(product.Id === s.Bundle_Product_Prerequisite__c){
                        //console.log('TQ CTRL: servicesSet - CALL HLPR addServiceComponents');
                        helper.addServiceComponents(component,event,helper,s);
                    }
                });
            }
        });
    },
    
    onPriceListChange : function(component, event, helper) {
        console.log('TQ CTRL: onPriceListChange - DEFAULT Set on DT (Corporate)');
        var techQlis = component.get('v.techQlis');
        var fromRules = false;
        //console.log('TQ CTRL: onPriceListChange - CALL HLPR updatePrice');
        helper.updatePrice(component, event, helper, techQlis,fromRules);
    },
    
    onDisplayNameChange : function(component, event, helper) {
        console.log('Tech Controller: onDisplayNameChange');
        var displayName = event.getSource().get('v.value');
        var qli = event.getSource().get('v.name');
        var techQli = _.filter(component.get('v.techQlis'), {'Bundle_Product__c' : qli});
        techQli[0].DisplayEdited = true;
    },
    
    trial : function(component, event, helper) {
        helper.updatePrice(component, event, helper, component.get('v.techQlis'));
    }
})