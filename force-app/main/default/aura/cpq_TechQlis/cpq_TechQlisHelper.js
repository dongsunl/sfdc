({
    updatePrice : function(component, event, helper, qli,fromRules) {  
        console.log('****TQ HLPR: updatePrice - START');
        //onsole.log(qli);
        //console.log(component.get('v.coreTiers'));
        var priceList = component.get('v.selectedPriceList');
        var dbUsers = component.get('v.dbUsersLicense');
        var licenses = component.get('v.licenses');
        var licenseCostEach = component.get('v.licenseARR')/ component.get('v.licenseCoreQty');
        var newCostEach = 0;
        var quoteType = component.get('v.quoteType');

        
        if(qli.length>0){
            console.log('****qli.length>0')
            var selectedTier = Number(component.get('v.selectedTier'));
            var price = component.get('v.allPricing');    
            console.log('****selectedTier: ', JSON.parse(JSON.stringify(selectedTier))); 
            console.log('****price before: ', JSON.parse(JSON.stringify(price)));
            price = _.flatten(price); 
            
            var group;
            console.log('****qli: ', JSON.parse(JSON.stringify(qli)));
            group = _.groupBy(qli,'Price_Based_On__c');
            console.log('****group: ', group);
            var priceGroup = [];
            _.forEach(group, function(value,key){
                _.filter(qli,function(o){
                    if(o.PricingType__c === key){
                        console.log('****PricingType__c === key: ', o, key);
                        if(key === 'UserD'){
                            if(component.get('v.upgradeType')==='keep-dates') {
                                
                                if(dbUsers!= undefined) {
                                    
                                    priceGroup[key] = Number(o.Quantity__c) + dbUsers;
                                } else {
                                    
                                    priceGroup[key] = Number(o.Quantity__c);
                                }
                            } else {
                                priceGroup[key] = Number(o.Quantity__c);
                            }
                        }else{
                            priceGroup[key] = Number(o.Quantity__c);
                        }
                    }
                    else if(priceGroup[key] === undefined) {
                        priceGroup[key] = selectedTier;
                    }
                }); 
            });
            var qli2= _.flatten(qli);
            console.log('****qli2: ', JSON.parse(JSON.stringify(qli2)));
            qli2 = _.forEach(qli2,function(o){
                _.forIn(priceGroup,function(value,key){
                    if(o.Price_Based_On__c == key){
                        o.TierQty = value;
                    }
                    if(o.Quantity_Multiplied_by_Core__c == true) {
                        o.Quantity__c = value;
                    }
                });
                if(o.Picklist_Value__c === true && o.PricingType__c === 'UserD'){
                    //console.log('TQ HLPR: updatePrice - CALL HLPR fetchPickListVal');
                    helper.fetchPickListVal(component, 'Maximum__c', 'dashboardUserTiersOptions');
                }else if(o.Picklist_Value__c === true && o.PricingType__c === 'PV'){
                    //console.log('TQ HLPR: updatePrice - CALL HLPR fetchPickListVal2');
                    helper.fetchPickListVal2(component, 'Maximum__c', 'pvTiersOptions');
                }
            });
            //var total=0;
            //var netTotal=0;
            //var arr=0;
            //var surveyCount = 0;
            //var dashboardUsers = 0;
            //var employeeCount = 0;
            var qli3 = _.forEach(qli2,function(o){
                
                /*if(o.ProductName === 'Dashboard Users'){
                    dashboardUsers = o.Quantity__c * o.Quantity_Multiplier__c;
                }else if(o.ProductName === 'EX Core'){
                    employeeCount = o.Quantity__c * o.Quantity_Multiplier__c;
                }else if(o.ProductName === '360 Core'){
                    employeeCount = o.Quantity__c * o.Quantity_Multiplier__c;
                }else if(o.ProductName === 'VOC Solution - K12- Tier A'){
                    employeeCount = o.Quantity__c * o.Quantity_Multiplier__c;
                }else if(o.ProductName === 'Ad-hoc Survey'){
                    surveyCount += o.Quantity__c * o.Quantity_Multiplier__c;
                }else if(o.ProductName === 'Pulse/Engagement'){
                    surveyCount += o.Quantity__c * o.Quantity_Multiplier__c;
                }else if(o.ProductName === 'Pulse (Additional)'){
                    surveyCount += o.Quantity__c * o.Quantity_Multiplier__c;
                }*/
                _.forEach(price,function(p){                
                    if(o.PricingType__c == p.Pricing_Type__c && o.TierQty >= p.Minimum__c && o.TierQty <= p.Maximum__c ){
                        if( o.PriceUpdate != true){
                            
                            if (o.PricingType__c === "Core"){
                                newCostEach = p.List_Price__c;
                            }
                            if(o.PricingType__c !== 'Core' && o.PricingType__c !=='UserD' && (o.Availability__c === 'Included' || o.Upgrade_Availability__c === 'Included')){
                                o.Quantity__c = p.Quantity__c;
                            }
                            if(o.PricingType__c === "HIS1" || o.PricingType__c === "HIS1.1"){
                                if(quoteType==='Upgrade License'){
                                    o.PriceEach = licenseCostEach * p.Price_Multiplier__c * (component.get('v.currency') / component.get('v.licenseCurrency'));
                                }else{
                                    o.PriceEach = newCostEach * p.Price_Multiplier__c;
                                }
                                
                            }else{
                                if(priceList == undefined || priceList == 'Corporate') {
                                    o.PriceEach = p.List_Price__c;
                                    o.BundleDiscount = o.Quantity__c * p.Bundle_Discount_Amount__c;
                                }
                                else if (priceList == 'GSA') {
                                    o.PriceEach = p.GSA__c;
                                    o.BundleDiscount = 0;
                                    o.FederalID = p.CarahsoftID__c;
                                }
                                    else if (priceList == 'GSACarahsoft') {
                                        o.PriceEach = p.GSACarahsoft__c;
                                        o.BundleDiscount = 0;
                                        o.FederalID = p.CarahsoftID__c;
                                    }
                                        else if (priceList == 'FedRamp') {
                                            o.PriceEach = p.FedRamp__c;
                                            o.BundleDiscount = 0;
                                            o.FederalID = p.FedRAMP_CarahsoftID__c;
                                        }
                                            else if (priceList == 'FedRampCarahsoft') {
                                                o.PriceEach = p.FedRampCarahsoft__c;
                                                o.BundleDiscount = 0;
                                                o.FederalID = p.FedRAMP_CarahsoftID__c;
                                            }
                            }
                            if(o.Related_Product__r.Eligible_for_Discount__c && o.PricingType__c==='Core') {
                                o.PriceEach = o.PriceEach * component.get('v.coreDiscountPercentage');
                            } else if (o.Related_Product__r.Eligible_for_Discount__c && o.PricingType__c != 'Core') {
                                o.PriceEach = o.PriceEach * component.get('v.nonCoreDiscountPercentage');
                            }
                            if(component.get('v.trial') && o.Type__c === 'Tech') {
                                o.PriceEach = 0;
                                o.BundleDiscount = 0;
                            }
                            o.Unlimited_Users__c = p.Unlimited__c;
                            o.PriceTotal = o.Quantity__c * o.PriceEach;
                        }
                        if(o.DisplayEdited != true) {
                            if(o.FederalID != undefined) {
                                o.External_Display_Name__c = o.FederalID + ' - ';
                            } else {
                                o.External_Display_Name__c = '';
                            }
                            if(o.Unlimited_Users__c === true){
                                o.External_Display_Name__c = o.External_Display_Name__c + o.Display_Label__c + ' (Unlimited)';
                            } else if (o.Quantity__c === 1 && o.User_Input_Required__c == false){
                                o.External_Display_Name__c = o.External_Display_Name__c + o.Display_Label__c;
                            } else {
                                o.External_Display_Name__c = o.External_Display_Name__c + o.Display_Label__c + ' ' + o.Display_Variable__c + ' ' + o.Quantity__c;
                            }
                        }
                        
                        /*  total += o.PriceTotal;
                        netTotal += o.PriceTotal - o.BundleDiscount;
                        if(o.Related_Product__r.Revenue_Type__c == 'Recurring'){
                            arr+=o.PriceTotal;
                        }*/
                    }                
                });
            });
            //console.log(qli3);
            //component.set('v.surveyCount',surveyCount);
            //component.set('v.dashboardUsers',dashboardUsers);
            //component.set('v.employeeCount',employeeCount);
            //component.set('v.qliTotal',total);
            //component.set('v.netTotal',netTotal);
            //component.set('v.arr',arr);
            //component.set('v.techQlis',qli3);
            //helper.sortQlis(component, event, helper);
            //console.log('TQ HLPR: updatePrice - CALL HLPR calculateProration');
            helper.calculateProration(component, event, helper,qli3,fromRules);
            
            if(component.get('v.upgradeType') === 'keep-dates' && component.get('v.licenses')!= undefined && ((component.get('v.licenses')[0].Bundle__c !== component.get('v.selectedBundle')[0].Id && component.get('v.selectedBundle')[0].Academic_Tier_Comparison__c !== component.get('v.licenses')[0].Tier__c) || (component.get('v.selectedBundle')[0].Id !== component.get('v.licenses')[0].Bundle__c && component.get('v.selectedBundle')[0].Academic_Tier_Comparison__c === undefined))) {
                console.log('****Ready for credit process');
                var creditType = 'CreditM';
                var amount = -1*component.get('v.licenseARR');
                
                //console.log('TQ HLPR: updatePrice - CALL HLPR createCreditLine');
                helper.createCreditLine(component, event, helper, amount, creditType,qli3,fromRules);
                
            }
            // helper.updateCombinedQlis(component,event,helper);
        }
        
    },
    
    calculateQliTotals : function(component,event,helper,techQlis,fromRules){
        console.log('****TQ HLPR: calculateQliTotals - START');
        techQlis =  _.sortBy(techQlis,['groupSortOrder','sortOrder']);
        var total=0;
        var netTotal=0;
        var arr=0;
        var surveyCount = 0;
        var dashboardUsers = 0;
        var employeeCount = 0;
        var groupedProducts=[];
        
        var grp = _.groupBy(techQlis,'Type__c');
        _.mapKeys(grp,function(value,key){
            groupedProducts.push(key);
        });   
        
        component.set('v.groupedProducts', groupedProducts);
        var selectedServiceBundles = component.get('v.selectedServiceBundles');
        var selectedPartners = component.get('v.selectedPartners');
        
        selectedServiceBundles.forEach(function(sb){
            selectedPartners.forEach(function(p){
                if(sb.Id === p.Service_Bundle__c){
                    p.Bundle_Product_Prerequisite__c = sb.Bundle_Product_Prerequisite__c;
                }
            });
        });
        console.log(techQlis)
        techQlis.forEach(function(t){
            total += t.PriceTotal;
            netTotal += t.PriceTotal - t.BundleDiscount;
            if(t.Related_Product__r.Revenue_Type__c == 'Recurring'){
                arr+=t.PriceTotal;
            }
            if(t.ProductName === 'Dashboard Users' ){
                dashboardUsers = t.Quantity__c * t.Quantity_Multiplier__c;
            }else if(t.ProductName === 'EX Core' || t.ProductName === '360 Core' || t.ProductName === 'VOC Solution - K12- Tier A'){
                employeeCount = t.Quantity__c * t.Quantity_Multiplier__c;
            }
                else if(t.ProductName === 'Ad-hoc Survey' || t.ProductName === 'Pulse/Engagement' ||t.ProductName === 'Pulse (Additional)'){
                    surveyCount += t.Quantity__c * t.Quantity_Multiplier__c;
                }
            if(selectedPartners.length===0){
                t.PartnerName = 'None';
            }else{
                selectedPartners.forEach(function(p){
                    if(t.Bundle_Product__c  === p.Bundle_Product_Prerequisite__c ){
                        t.PartnerName = p.Partner_Name__c;
                        t.LeadTime = p.Lead_Time__c;
                    }
                    else{
                        t.PartnerName = 'None';
                    }
                });
            }
        });
        
        //console.log('TQ HLPR: calculateQliTotals - SET surveyCount');
        component.set('v.surveyCount',surveyCount);
        //console.log('TQ HLPR: calculateQliTotals - SET dashboardUsers');
        component.set('v.dashboardUsers',dashboardUsers);
        //console.log(dashboardUsers);
        //console.log('TQ HLPR: calculateQliTotals - SET employeeCount');
        component.set('v.employeeCount',employeeCount);
        console.log('TQ HLPR: calculateQliTotals - CALL HLPR qliUpdateEvent');
        helper.qliUpdateEvent(component, event, helper,techQlis,total,netTotal,arr,fromRules);
        //helper.serviceUpdateEvent(component, event, helper, fromRules);
        
    },
    
    qliUpdateEvent: function(component,event,helper,techQlis,total,netTotal,arr,fromRules){
        console.log('TQ HLPR: qliUpdateEvent');
         console.log(techQlis);
        var appEvent = component.getEvent('qliUpdate'); 
        appEvent.setParams({
            qlis: techQlis,//component.get('v.techQlis'),
            qliTotal: total,//component.get('v.qliTotal'),
            netTotal: netTotal,//component.get('v.netTotal'),
            arr: arr,//component.get('v.arr'),
            selectedBundle: component.get('v.selectedBundle')            
        });
        //console.log('TQ HLPR: qliUpdateEvent - FIRE qliUpdate');
        appEvent.fire(); 
        component.set('v.arr', arr);
        if(fromRules === false){
            //console.log('TQ HLPR: updatePrice - CALL HLPR updateCombinedQlis');
            helper.updateCombinedQlis(component,event,helper);
        }
        
    },
    
    runAvUpdateEvent: function(component,event,helper,singleRec){
        console.log('TQ HLPR: runAvUpdateEvent');
        // console.log(singleRec);
        var appEvent = component.getEvent('availableTechProductsUpdate');
        appEvent.setParams({
            singleRec: singleRec
        });
        //console.log('TQ HLPR: runAvUpdateEvent - FIRE availableTechProductsUpdate');
        appEvent.fire();
    },
    
    addServiceBundles : function(component,event,helper,techProduct){
        console.log('TQ HLPR: addServiceBundles');
     //   console.log('techProduct');
     //    console.log(techProduct);
        var newProduct = _.filter(component.get('v.availableServiceBundles'), {'Bundle_Product_Prerequisite__c' : techProduct.Id})[0];
     //    console.log('newProduct');
     //    console.log(newProduct);
        var service = Object.assign({}, newProduct);
     //    console.log('service');
    //     console.log(service);
        service.PriceTotal = techProduct.PriceTotal;
        if(techProduct.Multiplicity_Key__c != undefined) {
            service.Multiplicity_Key__c = techProduct.Multiplicity_Key__c;
        }
        service.PartnerDiscount = 1;
        if(service.Custom__c) {
            service.Provider_Selected__c = true;
        }
    //    console.log('service');
    //    console.log(service);
        var selectedServiceBundles = component.get('v.selectedServiceBundles');
     //   console.log('selectedServiceBundles');
    //    console.log(selectedServiceBundles);
        selectedServiceBundles.push(service);        
   //    console.log('selectedServiceBundles1');
    //   console.log(selectedServiceBundles[1]);
        var selectedServiceBundles2 = [];
        
      // selectedServiceBundles.forEach(function(ssb){
       //     selectedServiceBundles2.push(ssb)
      //      console.log(ssb)
      //  })
        
    // console.log('selectedServiceBundles2');
     //   console.log(selectedServiceBundles2);
        helper.addServiceComponents(component, event, helper, service);
        //console.log(component.get('v.techQlis'));
        //console.log(selectedServiceBundles);
        /*_.forEach(component.get('v.availableServiceBundles'),function(bundle){
            if(bundle.Bundle_Product_Prerequisite__c === techProduct.Id){
                bundle.PriceTotal = techProduct.PriceTotal;
                //bundle.ServiceOriginalPrice = 0;
                if(techProduct.Multiplicity_Key__c != undefined) {
                    bundle.Multiplicity_Key__c = techProduct.Multiplicity_Key__c;
                }
                bundle.PartnerDiscount = 1;
                if(bundle.Custom__c === true) {
                    bundle.Provider_Selected__c = true;
                }
                //console.log('-------------BUNDLE-----------');
                //console.log(bundle);
                selectedServiceBundles.push(bundle);
                //console.log('TQ HLPR: addServiceBundles - CALL HLPR addServiceComponents');
                console.log(selectedServiceBundles); 
                helper.addServiceComponents(component,event,helper,bundle);
            }
        });*/
    },
    
    addServiceComponents : function(component,event,helper,bundle){
        console.log('TQ HLPR: addServiceComponents');
        var allServiceProducts = component.get('v.allServiceProducts');
        var serviceQlis = component.get('v.serviceQlis');
      //   console.log('serviceQlis');
      //  console.log(serviceQlis);
     //   console.log('bundle');
     //   console.log(bundle);
        var newProducts = _.filter(allServiceProducts, {'Related_Service_Bundle__c' : bundle.Id});
     //   console.log('newProducts');
    //    console.log(newProducts);
        _.forEach(newProducts, function(product) {
            if(product.Availability__c === 'Included') {
              //  console.log('product')
             //   console.log(product)
                var item = Object.assign({}, product);
                item.PriceEach = 0;
                item.ServiceOriginalPrice = 0;
                item.PriceTotal = 0;
                item.BundleDiscount = 0;
                item.TierQty = 1;
                item.PriceUpdate = false;
                item.PartnerDiscount = 1;
                if(bundle.Multiplicity_Key__c != undefined) {
                    item.Multiplicity_Key__c = bundle.Multiplicity_Key__c;
                }
              //  console.log('item')
             //   console.log(item)
                serviceQlis.push(item);
            }
        });
      //  console.log('serviceQlis');
      //  console.log(serviceQlis);
        /*_.forEach(allServiceProducts,function(product){
            if(bundle.Id === product.Related_Service_Bundle__c ){
                if(product.Availability__c === 'Included'){
                    product.PriceEach = 0;
                    product.ServiceOriginalPrice = 0;
                    product.PriceTotal = 0;
                    product.BundleDiscount = 0;
                    product.TierQty = 1;
                    product.PriceUpdate = false;
                    product.PartnerDiscount = 1;
                    if(bundle.Multiplicity_Key__c != undefined) {
                        product.Multiplicity_Key__c = bundle.Multiplicity_Key__c;
                    }
                    serviceQlis.push(product);
                }
            }
        });*/
        //console.log('TQ HLPR: addServiceComponents - CALL HLPR updateServicePrice');
        helper.updateServicePrice(component,event,helper,serviceQlis);  
    },
    
    updateServicePrice : function(component, event, helper,serviceQlis) {  
        console.log('TQ HLPR: updateServicePrice');
      //  console.log('serviceQlis')
     //   console.log(serviceQlis)
        var priceList = component.get('v.selectedPriceList');
        var selectedTier = Number(component.get('v.selectedTier'));
        var price = component.get('v.allPricing');     
        price = _.flatten(price);  
        var group = _.groupBy(serviceQlis,'Price_Based_On__c');    
        var priceGroup = [];
        var fromRules = false;
        _.forEach(group, function(value,key){
            _.filter(serviceQlis,function(o){
                if(o.PricingType__c === key){
                    priceGroup[key] = o.Quantity__c;
                }
            }); 
        });
        var qli2= _.flatten(serviceQlis);
      //  console.log('qli2')
      //  console.log(qli2)
        qli2 = _.forEach(qli2,function(o){
            _.forIn(priceGroup,function(value,key){
                if(o.Price_Based_On__c == key){
                    o.TierQty = value;
                }
            });
        });  
        
        var total=0;
        var netTotal=0;
        var arr=0;
        var digitalFeedbackDomainQuantity = 0;
        var digitalFeedbackInterceptQuantity = 0;
        var qli3 = _.forEach(qli2,function(o){  
            if(o.ProductName === 'CX Website Feedback Domain' || o.ProductName === 'RC Website Feedback Domain'){
                digitalFeedbackDomainQuantity = o.Quantity__c * o.Quantity_Multiplier__c;
            }else if(o.ProductName === 'CX Website Feedback Intercept' || o.ProductName === 'RC Website Feedback Intercept'){
                digitalFeedbackInterceptQuantity = o.Quantity__c * o.Quantity_Multiplier__c;
            }
            _.forEach(price,function(p){                
                if(o.PricingType__c == p.Pricing_Type__c && o.TierQty >= p.Minimum__c && o.TierQty <= p.Maximum__c && o.PriceUpdate != true){
                    //console.log(o);
                    if(priceList == undefined || priceList == 'Corporate') {
                        o.ServiceOriginalPrice = p.List_Price__c;
                        o.PriceEach = p.List_Price__c * o.PartnerDiscount;
                    }
                    else if (priceList == 'GSA') {
                        o.ServiceOriginalPrice = p.GSA__c;
                        o.PriceEach = p.GSA__c * o.PartnerDiscount;
                    }
                        else if (priceList == 'GSACarahsoft') {
                            o.ServiceOriginalPrice = p.GSACarahsoft__c;
                            o.PriceEach = p.GSACarahsoft__c * o.PartnerDiscount;
                        }
                            else if (priceList == 'FedRamp') {
                                o.ServiceOriginalPrice = p.FedRamp__c;
                                o.PriceEach = p.FedRamp__c * o.PartnerDiscount;
                            }
                                else if (priceList == 'FedRampCarahsoft') {
                                    o.ServiceOriginalPrice = p.FedRampCarahsoft__c;
                                    o.PriceEach = p.FedRampCarahsoft__c * o.PartnerDiscount;
                                }
                    o.PriceTotal = o.Quantity__c * o.PriceEach;
                }
                total += o.PriceTotal;
                netTotal += o.PriceTotal - o.BundleDiscount;
                if(o.Related_Product__r.Revenue_Type__c == 'Recurring'){
                    arr+=o.PriceTotal;
                }
            });
        });
      //  console.log('qli3')
     //   console.log(qli3)
        //Danny changes 7.26
        var techQlis = component.get('v.techQlis');
        var techQliTotal = 0;
        var techNetTotal = 0;
        var techArr = 0;
        _.forEach(techQlis, function(value) {
            techQliTotal += value.PriceTotal;
            techNetTotal += value.PriceTotal - value.BundleDiscount;
            if(value.Related_Product__r.Revenue_Type__c == 'Recurring'){
                techArr+=value.PriceTotal;
            }
        });
        component.set('v.qliTotal', techQliTotal);
        component.set('v.netTotal', techNetTotal);
        component.set('v.arr', techArr);
        component.set('v.digitalFeedbackDomainQuantity',digitalFeedbackDomainQuantity);
        component.set('v.digitalFeedbackInterceptQuantity',digitalFeedbackInterceptQuantity);
        component.set('v.serviceQlis', qli3);
        //console.log('TQ HLPR: updateServicePrice - CALL HLPR serviceUpdateEvent');
        //helper.calculateQliTotals(component, event, helper, component.get('v.techQlis'), fromRules);
        helper.serviceUpdateEvent(component,event,helper,fromRules);
        // helper.updateCombinedQlis(component,event,helper);
    },
    
    serviceUpdateEvent : function(component,event,helper,fromRules){
        console.log('TQ HLPR: ServiceUpdateFromTechEvent');
        var appEvent = component.getEvent('serviceUpdate');    
        console.log(component.get('v.techQlis'));
        appEvent.setParams({
            serviceQlis: component.get('v.serviceQlis'),
            serviceBundles: component.get('v.selectedServiceBundles'),
            techQlis: component.get('v.techQlis'),
            qliTotal: component.get('v.qliTotal'),
            netTotal: component.get('v.netTotal'),
            arr: component.get('v.arr'),
            selectedBundle: component.get('v.selectedBundle')
        });
        console.log('TQ HLPR: ServiceUpdateFromTechEvent - FIRE serviceUpdate');
        appEvent.fire(); 
        
        if(fromRules === false){
            console.log('TQ HLPR: ServiceUpdateFromTechEvent CALL HLPR updateCombinedQlis');
            helper.updateCombinedQlis(component,event,helper);
        }
    },
    
    removeServiceBundles: function(component,event,helper,newProduct){
        console.log('TQ HLPR: removeServiceBundles'); 
        var selectedServiceBundles = _.flatten(component.get('v.selectedServiceBundles'));
        var availableServiceBundles = component.get('v.availableServiceBundles');
        var techQlis = component.get('v.techQlis');
        var qliTotal = component.get('v.qliTotal');
        var netTotal = component.get('v.netTotal');
        if(newProduct[0].HasMarkupLine) {
            //var markupLine = _.findIndex(techQlis, {'Type__c' : 'Markup'}); This had to be updated 9/25 because new markup bundle products were added
            var markupLine = _.findIndex(techQlis, function(qli) {
                return qli.Related_Product__r.ProductCode.includes('ImpMarkup'); 
            });
            if(markupLine > -1) {
                qliTotal = qliTotal - techQlis[markupLine].PriceTotal;
                netTotal = qliTotal - techQlis[markupLine].PriceTotal;
                component.set('v.qliTotal', qliTotal);
                component.set('v.netTotal', netTotal);
                _.pullAt(techQlis, markupLine);
                var countOfMarkupProducts = _.size(_.filter(techQlis, function(qli) {
                    return qli.Type__c == 'Markup';
                }));
                if(countOfMarkupProducts == 0) {
                    var groupedProducts = component.get('v.groupedProducts');
                    component.set('v.groupedProducts', _.pull(groupedProducts, 'Markup'));
                }
            }
        }
        if(selectedServiceBundles.length > 0) {
            var bundleInServices = _.findIndex(selectedServiceBundles,{'Bundle_Product_Prerequisite__c': newProduct[0].Bundle_Product__c});
            var bundleInAvailableServices = _.findIndex(availableServiceBundles, {'Bundle_Product_Prerequisite__c' : newProduct[0].Bundle_Product__c});
            availableServiceBundles[bundleInAvailableServices].Q_Partner__c = undefined;
            availableServiceBundles[bundleInAvailableServices].Partner_Availability__c = null;
            availableServiceBundles[bundleInAvailableServices].Provider_Selected__c = false;
            availableServiceBundles[bundleInAvailableServices].Partner_Name__c = null;
            availableServiceBundles[bundleInAvailableServices].PartnerDiscount = 1;
            var bundleId = selectedServiceBundles[bundleInServices].Id;
            _.pullAt(selectedServiceBundles,bundleInServices); 
            component.set('v.selectedServiceBundles',selectedServiceBundles);
            //console.log('TQ HLPR: removeServiceBundles - CALL HLPR removeServiceComponents'); 
            helper.removeServiceComponents(component,event,helper,bundleId);
        }
    },
    
    removeServiceComponents : function(component,event,helper,bundleId){
        console.log('TQ HLPR: removeServiceComponents');
        var serviceQlis = component.get('v.serviceQlis');  
        _.pullAllBy(serviceQlis, [{'Related_Service_Bundle__c':bundleId}],'Related_Service_Bundle__c');
        //console.log('TQ HLPR: removeServiceComponents - CALL HLPR serviceUpdateEvent');
        helper.serviceUpdateEvent(component,event,helper);
    },
    
    calculateProration : function(component, event, helper, techQlis,fromRules) {
        console.log('****TQ HLPR: calculateProration');
        var creditType = 'CreditP';
        // var techQlis = component.get('v.techQlis');
        var quoteTerm = component.get('v.quoteTerm');
        var nonCoreTotal = 0;
        var prorationAmount = 0;
        let licenses = component.get('v.licenses');
        if(quoteTerm != 12) {
            _.forEach(techQlis, function(qli) {
                let index = _.findIndex(licenses, function(license) {
                    return qli.Related_Product__r.Upgrade_Comparison__c === license.Product__r.Upgrade_Comparison__c;
                });
                if(qli.Related_Product__r.Eligible_for_Proration__c && index === -1) {
                    nonCoreTotal += parseFloat(qli.PriceTotal);
                    qli.Proration_Amount__c = -1*qli.PriceEach*(12-quoteTerm)/12;
                }
            });
            prorationAmount = -1*nonCoreTotal*(12-quoteTerm)/12;
            if(prorationAmount > 0 || prorationAmount < 0) {
                console.log('****TQ HLPR: calculateProration - CALL HLPR createCreditLine');
                helper.createCreditLine(component, event, helper, prorationAmount, creditType,techQlis,fromRules);
            }
            else {
                console.log('****TQ HLPR: calculateProration - CALL HLPR deleteCreditLine');
                helper.deleteCreditLine(component, event, helper, creditType,techQlis,fromRules);
            }
        }
        else {
            console.log('****TQ HLPR: calculateProration - CALL HLPR deleteCreditLine');
            helper.deleteCreditLine(component, event, helper, creditType,techQlis,fromRules);
        }
    },
    
    createCreditLine : function(component, event, helper, amount, creditType,techQlis,fromRules) {
        console.log('****TQ HLPR: createCreditLine');
          var techQlis = component.get('v.techQlis');
        var allBundleProducts = component.get('v.allBundleProducts');
        var bundleId = component.get('v.selectedBundle')[0].Id;
        var alreadyHasCredit;
        var creditLine = {};
        var creditProduct;
        var total=0;
        var netTotal=0;
        var arr=0;
        /*helper.callServer(
            component,
            "c.getProducts",
            function(response) {*/
        //var bundleProducts = response;
        _.forEach(allBundleProducts, function(bp) {
            if(bp.Related_Product__r.ProductCode == creditType) {
                creditProduct = bp;
            } 
        });
        creditLine = _.filter(allBundleProducts, {'Id' : creditProduct.Id})[0];
        if(component.get('v.actionType') === 'Edit') {
            alreadyHasCredit = _.findIndex(techQlis, {'Bundle_Product__c' : creditLine.Id});
        }
        else {
            alreadyHasCredit = _.findIndex(techQlis, {'Id' : creditLine.Id});
        }
        if(alreadyHasCredit > -1) {
            techQlis[alreadyHasCredit].PriceEach = amount;
            techQlis[alreadyHasCredit].PriceTotal = amount;
        }
        else {
            creditLine.ProductName = creditLine.Related_Product__r.Name;
            creditLine.PriceEach = amount;
            creditLine.PriceTotal = amount;
            creditLine.Quantity__c = 1;
            creditLine.BundleDiscount = 0;
            creditLine.Comparison = creditLine.Related_Product__r.Upgrade_Comparison__c+creditLine.User_Input_Required__c;
            creditLine.PriceUpdate = true;
            creditLine.Availability__c = 'Included';
            techQlis.push(creditLine);
        }
        //console.log('TQ HLPR: createCreditLine - CALL HLPR calculateQliTotals');
        helper.calculateQliTotals(component,event,helper,techQlis,fromRules);
        /* component.set('v.techQlis', techQlis);
                _.forEach(techQlis, function(o) {
                    total += o.PriceTotal;
                    netTotal += o.PriceTotal - o.BundleDiscount;
                    if(o.Related_Product__r.Revenue_Type__c == 'Recurring'){
                        arr+=o.PriceTotal;
                    }
                });
                component.set('v.qliTotal',total);
                component.set('v.netTotal',netTotal);
                component.set('v.arr',arr);
                helper.sortQlis(component, event, helper);
                helper.qliUpdateEvent(component, event, helper);
            },
            {
                bundleId : bundleId
            }
        );*/
    },
    
    deleteCreditLine : function(component, event, helper, creditType,techQlis,fromRules) {
        console.log('TQ HLPR: deleteCreditLine');
        //  var techQlis = component.get('v.techQlis');
       
        var creditLine;
       
        var total=0;
       
        var netTotal=0;
       
        var arr=0;
      
        _.forEach(techQlis, function(qli) {
           
            if(qli.Related_Product__r.ProductCode == creditType) {
              
                creditLine = _.findIndex(techQlis, {'Id' : qli.Id});
               
                techQlis.splice(creditLine, 1);
              
            }
         
            /* else {
                total += qli.PriceTotal;
                netTotal += qli.PriceTotal - qli.BundleDiscount;
                if(qli.Related_Product__r.Revenue_Type__c == 'Recurring'){
                    arr+= qli.PriceTotal;
                }
            }*/
        });
       
        console.log('TQ HLPR: deleteCreditLine - CALL HLPR calculateQliTotals');
        helper.calculateQliTotals(component,event,helper,techQlis,fromRules);
        /* component.set('v.techQlis', techQlis);
        component.set('v.qliTotal', total);
        component.set('v.netTotal', netTotal);
        component.set('v.arr', arr);
        helper.qliUpdateEvent(component, event, helper);*/
    },
    
    calculateCredits : function(component, event, helper,fromRules) {
        console.log('TQ HLPR: calculateCredits');
        var creditType = 'CreditT';
        var credits = component.get('v.unusedCredits');
        var licenses = component.get('v.originalLicenses');
        var quoteTerm = licenses[0].MergeTerm;
        var term = quoteTerm / licenses[0].TermLength;
        var techQlis = component.get('v.techQlis');
        var currency = component.get('v.currency');
        var coreQuantity = 0;
        var responses = 0;
        var total = 0;
        var proration = 0;
        var amount = 0;
        _.forEach(licenses, function(l) {
            if(l.Core_Product__c == true) {
                coreQuantity += l.Quantitiy__c;
            }
            if(l.Product__r.Revenue_Type__c === 'Recurring') {
                total += l.Invoice_Amount__c;
            }
            if(l.Proration_Amount__c === undefined) {
                proration += 0;
            }
            else {
                proration += l.Proration_Amount__c;
            }
        });
        if(coreQuantity > 0) {
            responses = credits / coreQuantity;
        }
        if(responses < term){
            amount = -1*(((total+proration)*responses)) / licenses[0].licenseCurrency * currency;
        }
        else if (responses > term) {
            amount = -1*(((total+proration)*term)) / licenses[0].licenseCurrency * currency;
        }
        if(amount > 0 || amount < 0) {
            //console.log('TQ HLPR: calculateCredits - CALL HLPR createCreditLine');
            helper.createCreditLine(component, event, helper, amount, creditType,techQlis,fromRules);
        } 
        else {
            //console.log('TQ HLPR: calculateCredits - CALL HLPR deleteCreditLine');
            helper.deleteCreditLine(component, event, helper, creditType,techQlis,fromRules);
        }
    },
        
    updateCombinedQlis: function(component,event,helper){
        console.log('TQ HLPR: updateCombinedQlis');
        //console.log(_.flattenDeep(component.get('v.serviceQlis')));
        //console.log(component.get('v.techQlis'));
        //console.log(component.get('v.selectedServiceBundles'));
        //console.log(component.get('v.selectedTier'));
        var techQlis = [];
        var serviceQlis = [];
        var combinedQlis = [];
        var selectedServiceBundles = [];
        var partnerAvailability = [];
        var licenses = [];
        _.forEach(component.get('v.techQlis'),function(t){  
            var techQli = {};
            techQli.TechBundle = t.Related_Bundle__c;
            techQli.TechId = t.Id;
            techQli.TechName = t.ProductName;     
            techQli.Quantity = t.Quantity__c;
            techQli.BundleKey = t.Bundle_Key__c;
            techQli.PriceTotal = t.PriceTotal;
            techQli.Multiplicity_Key__c = t.Multiplicity_Key__c;
            techQli.Revenue_Type__c = t.Revenue_Type__c;
            techQli.Type__c = t.Type__c;
            techQli.BundleDiscount = t.BundleDiscount;
            techQlis.push(techQli);
        });
        _.forEach(_.flattenDeep(component.get('v.serviceQlis')),function(s){
            var serviceQli = {};
            // console.log(component.get('v.serviceQlis'));
            serviceQli.TechId = s.Bundle_Product_Prerequisite__c;
            serviceQli.ServiceBundle = s.Related_Service_Bundle__c;
            serviceQli.ServiceId = s.Id;
            serviceQli.ServiceName = s.ProductName;
            serviceQli.Quantity = s.Quantity__c;
            serviceQli.PartnerDiscount = s.PartnerDiscount;
            serviceQlis.push(serviceQli);
        });
        _.forEach(component.get('v.selectedServiceBundles'),function(sb){
            var selectedServiceBundle = {};
            selectedServiceBundle.Id = sb.Id;
            selectedServiceBundle.BundleType = sb.Bundle_Type__c;
            selectedServiceBundle.ProviderSelected = sb.Provider_Selected__c;
            selectedServiceBundle.Name = sb.Name;
            selectedServiceBundle.PartnerDiscount = sb.PartnerDiscount;
            selectedServiceBundle.Bundle_Product_Prerequisite__c = sb.Bundle_Product_Prerequisite__c;
            selectedServiceBundle.Custom__c = sb.Custom__c;
            selectedServiceBundle.Multiplicity_Key__c = sb.Multiplicity_Key__c;
            selectedServiceBundles.push(selectedServiceBundle);
        });
        _.forEach(component.get('v.selectedPartners'),function(partner){
            var selectedPartnerAvailability = {};
            selectedPartnerAvailability.maxDealSize = partner.Maximum_Deal_Size__c;
            selectedPartnerAvailability.minDealSize = partner.Minimum_Deal_Size__c;
            partnerAvailability.push(selectedPartnerAvailability);
        });
        _.forEach(component.get('v.licenses'),function(l){
            var license = {};
            license.product = l.Product__r.Name;
            license.price = l.PriceTotal;
            licenses.push(license);
        });
        //console.log(component.get('v.selectedServiceBundles'));
        //console.log(component.get('v.techQlis'));
        var appEvent = $A.get("e.c:cpq_SendToRulesEngine");   
        appEvent.setParams({
            arr: component.get('v.arr'),
            netTotal: component.get('v.netTotal'),
            surveyCount: component.get('v.surveyCount'),
            dashboardUsers: component.get('v.dashboardUsers'),
            employeeCount: component.get('v.employeeCount'),
            digitalFeedbackDomainQuantity: component.get('v.digitalFeedbackDomainQuantity'),
            digitalFeedbackInterceptQuantity: component.get('v.digitalFeedbackInterceptQuantity'),
            featureSelected: component.get('v.featureSelected'),
            featureAddOrRemove: component.get('v.featureAddOrRemove'),
            techQlis: techQlis,
            serviceQlis: serviceQlis,
            selectedServiceBundles: selectedServiceBundles,
            partnerAvailability: partnerAvailability,
            allBundleProducts: component.get('v.allBundleProducts'),
            allServiceProducts: component.get('v.allServiceProducts'),
            quoteType: component.get('v.quoteType'),
            currency: component.get('v.currency'),
            licenseExp: component.get('v.licenseExp'),
            upgradeType: component.get('v.upgradeType'),
            coreQuantity: component.get('v.selectedTier'),
            licenses: licenses,
            quotingPermissions : component.get('v.quotingPermissions')
        });
        //  console.log('fireEvent');
        //console.log('TQ HLPR: updateCombinedQlis - FIRE cpq_SendToRulesEngine');
        appEvent.fire(); 
        
    },
    
    getUpgradeLines : function(component, event, helper, selectedTier) {
        console.log('TQ HLPR: getUpgradeLines');
        var priceList = component.get('v.selectedPriceList');
        var bundleId = component.get('v.bundleId');
        var licenses = component.get('v.licenses');
        var bundleProducts = component.get('v.allBundleProducts');
        var fromRules = false;
        // var price = component.get('v.allPricing');
        //  price = _.flatten(price); 
        // var group;
        // var priceGroup = [];
        // var tier;
        var techQlis = [];
        // var total = 0;
        //  var netTotal = 0;
        //  var arr = 0;
        _.forEach(bundleProducts, function(bp) {
            _.forEach(licenses, function(l) {
                if(component.get('v.selectedBundle')[0].Academic_Tier_Comparison__c === licenses[0].Tier__c && licenses[0].Tier__c !== undefined) {
                    if(bp.Related_Product__c === l.Product__c) {
                        if(bp.PricingType__c === 'Core') {
                            bp.Quantity__c = selectedTier;
                        }
                        else {
                            bp.Quantity__c = l.Quantitiy__c;
                        }
                        techQlis.push(bp);
                    }
                }
                else if(bp.Id == l.Bundle_Product__c) {
                    if(bp.PricingType__c == 'Core') {
                        bp.Quantity__c = selectedTier;
                    }
                    else {
                        bp.Quantity__c = l.Quantitiy__c;
                    }
                    techQlis.push(bp);
                }
            });
        });
        //console.log('TQ HLPR: getUpgradeLines - CALL HLPR updatePrice');
        helper.updatePrice(component,event,helper,techQlis,fromRules); 
        /* group = _.groupBy(techQlis, 'Price_Based_On__c');
        _.forEach(group, function(value,key){
            _.filter(techQlis ,function(o){
                if(o.PricingType__c === key){
                    priceGroup[key] = o.Quantity__c;
                }
            }); 
        });
        var qli2 = _.flatten(techQlis);
        qli2 = _.forEach(qli2,function(o){
            _.forIn(priceGroup,function(value,key){
                if(o.Price_Based_On__c == key){
                    o.TierQty = value;
                }
                if(o.Quantity_Multiplied_by_Core__c == true) {
                    o.Quantity__c = value;
                }
            });
        });
        var qli3 = _.forEach(qli2,function(o){    
            _.forEach(price,function(p){                
                if(o.PricingType__c == p.Pricing_Type__c && o.TierQty >= p.Minimum__c && o.TierQty <= p.Maximum__c && o.PriceUpdate != true){
                    if(priceList == undefined || priceList == 'Corporate') {
                        o.PriceEach = p.List_Price__c;
                        o.BundleDiscount = o.Quantity__c * p.Bundle_Discount_Amount__c;
                    }
                    else if (priceList == 'GSA') {
                        o.PriceEach = p.GSA__c;
                        o.BundleDiscount = 0;
                    }
                        else if (priceList == 'GSACarahsoft') {
                            o.PriceEach = p.GSACarahsoft__c;
                            o.BundleDiscount = 0;
                        }
                            else if (priceList == 'FedRamp') {
                                o.PriceEach = p.FedRamp__c;
                                o.BundleDiscount = 0;
                            }
                                else if (priceList == 'FedRampCarahsoft') {
                                    o.PriceEach = p.FedRampCarahsoft__c;
                                    o.BundleDiscount = 0;
                                }
                    o.Unlimited_Users__c = p.Unlimited__c;
                    o.PriceTotal = o.Quantity__c * o.PriceEach;
                }
                total += o.PriceTotal;
                netTotal += o.PriceTotal - o.BundleDiscount;
                if(o.Related_Product__r.Revenue_Type__c == 'Recurring'){
                    arr+=o.PriceTotal;
                }
            });
        });
        component.set('v.techQlis', qli3); 
        component.set('v.qliTotal',total);
        component.set('v.netTotal',netTotal);
        component.set('v.arr',arr);*/
    },    
    
    fetchPickListVal: function(component, fieldName, picklistOptsAttributeName) {   
        //console.log('TQ HLPR: fetchPicklistVal');
        var licenseDBUsers = component.get('v.dashboardUsers');
        
        var opts = [];
        var allValues = component.get("v.dashboardUserTiers");
       
        if(component.get('v.dbUsersLicense') !== undefined) {
            opts.push({
                class: "optionClass",
                label: licenseDBUsers,
                value: licenseDBUsers
            });
        }
        
        for (var i = 0; i < allValues.length; i++) {
            opts.push({
                class: "optionClass",
                label: allValues[i].Maximum__c,
                value: allValues[i].Maximum__c
            });
        }
        component.set("v." + picklistOptsAttributeName, opts);        
    },
    
    fetchPickListVal2: function(component, fieldName, picklistOptsAttributeName) {   
        //console.log('TQ HLPR: fetchPicklistVal2');
        var opts = [];
        var allValues = component.get("v.pvTiers");
        
        for (var i = 0; i < allValues.length; i++) {
            opts.push({
                class: "optionClass",
                label: allValues[i].Maximum__c,
                value: allValues[i].Maximum__c
            });
        }
        component.set("v." + picklistOptsAttributeName, opts);        
    },
    
    addServiceComponentsFromRules : function(component,event,helper,bundleId,productId, PartnerDiscount, Multiplicity_Key__c){
        console.log('TQ HLPR: addServiceComponentsFromRules');
        var allServiceProducts = component.get('v.allServiceProducts');  
        
        var serviceQlis = component.get('v.serviceQlis');
        var featureInQlis = _.findIndex(serviceQlis,{'Service_Bundle_Product__c': productId});
        var newProduct = _.filter(allServiceProducts,{'Id':productId});
        var fromRules = true;
       
        if(featureInQlis === -1 && allServiceProducts.length != 0){
            newProduct[0].PartnerDiscount = PartnerDiscount;
            serviceQlis.push(newProduct[0]);
        }
        
        //console.log('TQ HLPR: addServiceComponentsFromRules - CALL HLPR updateServicePrice2');
        helper.updateServicePrice2(component,event,helper,serviceQlis,bundleId,fromRules, Multiplicity_Key__c);  
    },
    
    removeServiceComponentsFromRules : function(component,event,helper,bundleId,productId, Multiplicity_Key__c){
        console.log('TQ HLPR: removeServiceComponents');
        //console.log(productId);
        var serviceQlis = component.get('v.serviceQlis');
        var featureInQlis = _.findIndex(serviceQlis,{'Id': productId});
        var fromRules = true;
        _.pullAt(serviceQlis,featureInQlis);
        component.set('v.serviceQlis',serviceQlis);
        //console.log('TQ HLPR: removeServiceComponents - CALL HLPR updateServicePrice2');
        helper.updateServicePrice2(component,event,helper,serviceQlis,bundleId,fromRules, Multiplicity_Key__c);  
    },
    
    updateServicePrice2 : function(component, event, helper,serviceQlis, bundleId,fromRules, Multiplicity_Key__c) {  
        console.log('TQ HLPR: updateServicePrice2');
        var priceList = component.get('v.selectedPriceList');
        var selectedTier = Number(component.get('v.selectedTier'));
        var price = component.get('v.allPricing');     
        price = _.flatten(price); 
        var serviceQliTotal = 0;
        var techQliTotal=0;
        var techNetTotal=0;
        var techArr=0;
        var qli = _.forEach(serviceQlis,function(o){  
            
            _.forEach(price,function(p){                
                if(o.PricingType__c == p.Pricing_Type__c && o.Quantity__c >= p.Minimum__c && o.Quantity__c <= p.Maximum__c ){
                    if(o.PriceUpdate != true){
                        if(priceList == undefined || priceList == 'Corporate') {
                            o.ServiceOriginalPrice = p.List_Price__c;
                            o.PriceEach = p.List_Price__c * o.PartnerDiscount;
                        }
                        else if (priceList == 'GSA') {
                            o.ServiceOriginalPrice = p.GSA__c;
                            o.PriceEach = p.GSA__c * o.PartnerDiscount;
                        }
                            else if (priceList == 'GSACarahsoft') {
                                o.ServiceOriginalPrice = p.GSACarahsoft__c;
                                o.PriceEach = p.GSACarahsoft__c * o.PartnerDiscount;
                            }
                                else if (priceList == 'FedRamp') {
                                    o.ServiceOriginalPrice = p.FedRamp__c;
                                    o.PriceEach = p.FedRamp__c * o.PartnerDiscount;
                                }
                                    else if (priceList == 'FedRampCarahsoft') {
                                        o.ServiceOriginalPrice = p.FedRampCarahsoft__c;
                                        o.PriceEach = p.FedRampCarahsoft__c * o.PartnerDiscount;
                                    }
                        o.PriceTotal = o.Quantity__c * o.PriceEach; 
                    }
                    if(o.Related_Service_Bundle__c === bundleId && (o.Multiplicity_Key__c === Multiplicity_Key__c || o.Multiplicity_Key__c === undefined)) {
                        serviceQliTotal += o.PriceTotal;
                    }
                    //bundleId = o.Related_Service_Bundle__c;
                }  
            });
        });
        component.set('v.serviceQlis',qli);
        var techLineId;
        var markupBundleProduct;
        _.forEach(component.get('v.selectedServiceBundles'),function(bundle){
            if(bundle.Id == bundleId){
                bundle.ServiceOriginalPrice = serviceQliTotal / bundle.PartnerDiscount;
                bundle.PriceEach = serviceQliTotal;  
                bundle.PriceTotal = serviceQliTotal;
                techLineId = bundle.Bundle_Product_Prerequisite__c;
                markupBundleProduct = bundle.Markup_Bundle_Product__c;
            }
        });
        
        var techQlis = component.get('v.techQlis');
        //var markupLine = _.findIndex(techQlis, {'Type__c' : 'Markup'}); This had to be updated 9/25 because new markup bundle products were added
        var markupLine = _.findIndex(techQlis, function(qli) {
            return qli.Related_Product__r.ProductCode.includes('ImpMarkup');
        });
        if(markupLine > -1) {
            if(techQlis[markupLine].Bundle_Product__c === markupBundleProduct) {
                techQlis[markupLine].PriceEach = serviceQliTotal * techQlis[markupLine].MarkupPercentage;
                techQlis[markupLine].PriceTotal = serviceQliTotal * techQlis[markupLine].MarkupPercentage;
                techQlis[markupLine].PriceUpdate = true;
            }
        }
        
        _.forEach(techQlis,function(value){
            if(value.Bundle_Product__c == techLineId){ 
                value.PriceEach = serviceQliTotal;
                value.PriceTotal = serviceQliTotal;
                value.PriceUpdate = true;
            }
            techQliTotal += value.PriceTotal;
            techNetTotal += value.PriceTotal - value.BundleDiscount;
            if(value.Related_Product__r.Revenue_Type__c == 'Recurring'){
                techArr+=value.PriceTotal;
            }
        });
        component.set('v.qliTotal',techQliTotal);
        component.set('v.netTotal',techNetTotal);
        component.set('v.arr',techArr);
        component.set('v.techQlis',techQlis);
        var fromRules = true;
        //  console.log('TQ HLPR: updateServicePrice2 - CALL HLPR calculateQliTotals');
        //  helper.calculateQliTotals(component,event,helper,techQlis,fromRules);
        //console.log('TQ HLPR: updateServicePrice2 - CALL HLPR serviceUpdateEvent');
        helper.serviceUpdateEvent(component,event,helper,fromRules);
    },
    
    updateServicePriceFromRules:function(component,event,helper,productId,price){
        console.log('TQ HLPR: updateServicePriceFromRules');
        var serviceQlis = component.get('v.serviceQlis');
        var bundleId;
        var fromRules = true;
        _.forEach(serviceQlis,function(s){
            if(s.Id === productId){
                s.ServiceOriginalPrice = price / s.PartnerDiscount;
                s.PriceEach = price;
                s.PriceTotal = price;
                s.PriceUpdate = true;
                bundleId = s.Related_Service_Bundle__c;
            }
        });
        component.set('v.serviceQlis',serviceQlis);
        //console.log('TQ HLPR: updateServicePriceFromRules - CALL HLPR updateServicePrice2');
        helper.updateServicePrice2(component,event,helper,serviceQlis, bundleId,fromRules);  
    },
    
    updateServiceTotalPriceFromRules:function(component,event,helper,productId,totalPrice){
        console.log('TQ HLPR: updateServiceTotalPriceFromRules');
        var techQlis = component.get('v.techQlis');
        //  var techQliTotal=0;
        //  var techNetTotal=0;
        //  var techArr=0;
        _.forEach(techQlis,function(s){
            if(s.Id === productId){
                s.PriceEach = 1;
                s.PriceTotal = totalPrice;
                s.PriceUpdate = true;
            }
            //   techQliTotal += s.PriceTotal;
            //   techNetTotal += s.PriceTotal - s.BundleDiscount;
            //   if(s.Related_Product__r.Revenue_Type__c == 'Recurring'){
            //       techArr+=s.PriceTotal;
            //   }
        });       
        //component.set('v.techQlis',techQlis);
        // component.set('v.qliTotal',techQliTotal);
        // component.set('v.netTotal',techNetTotal);
        // component.set('v.arr',techArr);
        var fromRules = true;
        //console.log('TQ HLPR: updateServiceTotalPriceFromRules - CALL HLPR calculateQliTotals');
        helper.calculateQliTotals(component,event,helper,techQlis,fromRules);
    },
    
    updateQuantityFromRules: function(component,event,helper,productId,quantity){
        console.log('TQ HLPR: updateQuantityFromRules')
        var serviceQlis = _.flattenDeep(component.get('v.serviceQlis'));
        var serviceQliTotal = 0;
        var bundleId;
        //  var techQliTotal=0;
        //  var techNetTotal=0;
        // var techArr=0;
        _.forEach(serviceQlis,function(s){
            if(s.Id === productId){
                s.Quantity__c = quantity;
                s.PriceTotal = s.PriceEach * quantity;
                s.PriceUpdate = true;
                bundleId = s.Related_Service_Bundle__c;
            }
            serviceQliTotal += s.PriceTotal;
            
        });
        
        var techLineId;
        _.forEach(component.get('v.selectedServiceBundles'),function(bundle){
            if(bundle.Id == bundleId){ 
                bundle.PriceTotal = serviceQliTotal;
                techLineId = bundle.Bundle_Product_Prerequisite__c;
            }
        });
        var techQlis = component.get('v.techQlis');
        
        _.forEach(techQlis,function(value){
            if(value.Id == techLineId){ 
                value.PriceEach = serviceQliTotal;
                value.PriceTotal = serviceQliTotal;
                value.PriceUpdate = true;
            }
            //   techQliTotal += value.PriceTotal;
            //  techNetTotal += value.PriceTotal - value.BundleDiscount;
            //   if(value.Related_Product__r.Revenue_Type__c == 'Recurring'){
            //       techArr+=value.PriceTotal;
            //   }
        });
        // component.set('v.qliTotal',techQliTotal);
        //  component.set('v.netTotal',techNetTotal);
        //  component.set('v.arr',techArr);
        //  component.set('v.techQlis',techQlis);
        
        //  component.set('v.serviceQlis', serviceQlis); 
        var fromRules = true;
        //console.log('TQ HLPR: updateQuantityFromRules - CALL HLPR calculateQliTotals')
        helper.calculateQliTotals(component,event,helper,techQlis,fromRules);
    },
    
    updateTechQuantityFromRules: function(component,event,helper,techLineId,quantity){
        console.log('TQ HLPR: updateTechQuantityFromRules')
        var techQlis = component.get('v.techQlis');
        
        _.forEach(techQlis,function(value){
            if(value.Id == techLineId && value.Multiplicity_Key__c == multiplicityKey){ 
                //console.log(quantity);
                value.Quantity__c = quantity;
                value.PriceTotal = value.PriceEach * quantity;
                value.PriceUpdate = true;
            }
        });
        var fromRules = true;
        //console.log('TQ HLPR: updateQuantityFromRules - CALL HLPR calculateQliTotals')
        helper.calculateQliTotals(component,event,helper,techQlis,fromRules);
    },
    
    addTechComponentsFromRules : function(component,event,helper,productId, PartnerDiscount, serviceBundle, Multiplicity_Key__c){
        console.log('TQ HLPR: addTechComponentsFromRules');
        var allBundleProducts = component.get('v.allBundleProducts');  
        var techQlis = component.get('v.techQlis');
        console.log('TQ HLPR: addTechComponentsFromRules: ', techQlis);
        var featureInQlis = _.findIndex(techQlis,{'Id': productId});
        var newProduct = _.filter(allBundleProducts,{'Id':productId});
        var selectedServiceBundles = component.get('v.selectedServiceBundles');
        //var sb = _.findIndex(selectedServiceBundles, {'Bundle_Product_Prerequisite__c' : serviceBundle});
        var sb;
        var bundleId;
        var fromRules = true;
        if(Multiplicity_Key__c != undefined) {
            sb = _.findIndex(selectedServiceBundles, function(bundle) {
                return bundle.Bundle_Product_Prerequisite__c === serviceBundle && bundle.Multiplicity_Key__c === Multiplicity_Key__c;
            });
        } else {
            sb = _.findIndex(selectedServiceBundles, {'Bundle_Product_Prerequisite__c' : serviceBundle});
        }
        if(sb > -1) {
            selectedServiceBundles[sb].Maintenance_Bundle_Product__c = newProduct[0].Id
            bundleId = selectedServiceBundles[sb].Id;
        }
        if(featureInQlis === -1 || newProduct[0].Add_Multiple__c){
            var qli = Object.assign({}, newProduct[0]);
            qli.PartnerDiscount = PartnerDiscount;
            qli.Availability__c = 'Included';
            qli.TierQty = 1;
            qli.Multiplicity_Key__c = Multiplicity_Key__c;
            qli.PriceEach = 0;
            qli.PriceTotal = 0;
            qli.PriceUpdate = true;
            qli.BundleDiscount = 0;
            techQlis.push(qli);
            /*newProduct[0].PartnerDiscount = PartnerDiscount;
            newProduct[0].Availability__c = 'Included';
            newProduct[0].Multiplicity_Key__c = Multiplicity_Key__c;
            newProduct[0].TierQty = 1;
            newProduct[0].PriceEach = 0;
            newProduct[0].PriceTotal = 0;
            newProduct[0].PriceUpdate = true;
            newProduct[0].BundleDiscount = 0;
            techQlis.push(newProduct[0]);*/
        }
        //helper.updatePrice2(component,event,helper,techQlis);
        //console.log('TQ HLPR: addTechComponentsFromRules - CALL HLPR updatePrice');
        helper.updatePrice(component,event,helper,techQlis,fromRules);
        helper.updateServicePrice2(component, event, helper, component.get('v.serviceQlis'), bundleId, fromRules, Multiplicity_Key__c);
        /*if(sb > -1) {
            selectedServiceBundles[sb].Maintenance_Bundle_Product__c = newProduct[0].Id;
            bundleId = selectedServiceBundles[sb].Id;
            //console.log('TQ HLPR: addTechComponentsFromRules - CALL HLPR updateServicePrice2');
            helper.updateServicePrice2(component, event, helper, component.get('v.serviceQlis'), bundleId,fromRules);
        }*/
    },
    
    removeTechComponentsFromRules : function(component,event,helper,productId, Multiplicity_Key__c){
        console.log('TQ HLPR: removeTechComponentsFromRules');
        var techQlis = component.get('v.techQlis');
        //var featureInQlis = _.findIndex(techQlis,{'Id': productId});
        var featureInQlis = _.findIndex(techQlis, function(qli) {
            return qli.Id === productId && (qli.Multiplicity_Key__c === component.get('v.Multiplicity_Key__c') || component.get('v.Multiplicity_Key__c') === undefined);
        });
        var fromRules = true;
        _.pullAt(techQlis,featureInQlis);
        component.set('v.techQlis',techQlis);
        //  helper.updatePrice2(component,event,helper,techQlis);  
        //console.log('TQ HLPR: removeTechComponentsFromRules - CALL HLPR updatePrice');
        helper.updatePrice(component,event,helper,techQlis,fromRules); 
    },
    
    updatePrice2 : function(component, event, helper, qli) {  
        console.log('TQ HLPR: updatePrice2'); 
        var priceList = component.get('v.selectedPriceList');       
        if(qli.length>0){
            var selectedTier = Number(component.get('v.selectedTier'));
            var price = component.get('v.allPricing');     
            price = _.flatten(price); 
            
            var group;
            group = _.groupBy(qli,'Price_Based_On__c');
            var priceGroup = [];
            _.forEach(group, function(value,key){
                _.filter(qli,function(o){
                    if(o.PricingType__c === key){
                        priceGroup[key] = o.Quantity__c;
                    }
                }); 
            });
            var qli2= _.flatten(qli);
            qli2 = _.forEach(qli2,function(o){
                _.forIn(priceGroup,function(value,key){
                    if(o.Price_Based_On__c == key){
                        o.TierQty = value;
                    }
                    if(o.Quantity_Multiplied_by_Core__c == true) {
                        o.Quantity__c = value;
                    }
                });
            });        
            var total=0;
            var netTotal=0;
            var arr=0;
            
            var qli3 = _.forEach(qli2,function(o){
                _.forEach(price,function(p){                
                    if(o.PricingType__c == p.Pricing_Type__c && o.TierQty >= p.Minimum__c && o.TierQty <= p.Maximum__c ){
                        if( o.PriceUpdate != true){
                            if(priceList == undefined || priceList == 'Corporate') {
                                o.PriceEach = p.List_Price__c;
                                o.BundleDiscount = o.Quantity__c * p.Bundle_Discount_Amount__c;
                            }
                            else if (priceList == 'GSA') {
                                o.PriceEach = p.GSA__c;
                                o.BundleDiscount = 0;
                            }
                                else if (priceList == 'GSACarahsoft') {
                                    o.PriceEach = p.GSACarahsoft__c;
                                    o.BundleDiscount = 0;
                                }
                                    else if (priceList == 'FedRamp') {
                                        o.PriceEach = p.FedRamp__c;
                                        o.BundleDiscount = 0;
                                    }
                                        else if (priceList == 'FedRampCarahsoft') {
                                            o.PriceEach = p.FedRampCarahsoft__c;
                                            o.BundleDiscount = 0;
                                        }
                            if(o.Related_Product__r.Eligible_for_Discount__c && o.PricingType__c==='Core') {
                                o.PriceEach = o.PriceEach * component.get('v.coreDiscountPercentage');
                            } else if (o.Related_Product__r.Eligible_for_Discount__c && o.PricingType__c != 'Core') {
                                o.PriceEach = o.PriceEach * component.get('v.nonCoreDiscountPercentage');
                            }
                            o.Unlimited_Users__c = p.Unlimited__c;
                            o.PriceTotal = o.Quantity__c * o.PriceEach;
                        }
                        total += o.PriceTotal;
                        netTotal += o.PriceTotal - o.BundleDiscount;
                        if(o.Related_Product__r.Revenue_Type__c == 'Recurring'){
                            arr+=o.PriceTotal;
                        }
                    }                
                });
            });
            component.set('v.qliTotal',total);
            component.set('v.netTotal',netTotal);
            component.set('v.arr',arr);
            component.set('v.techQlis',qli3);
            // helper.sortQlis(component, event, helper);
            
        }
    },
    
    createMaintenanceLines : function(component, event, helper) {
        console.log('Tech Helper: createMaintenanceLines');
        var techQlis = component.get('v.techQlis');
        //console.log(techQlis);
        var upgradeType = component.get('v.upgradeType');
        var fromRules = false;
        var maintenanceBundleProduct = _.filter(component.get('v.allBundleProducts'), function(bp) {
            if(bp.Related_Product__r.Name.toUpperCase().includes('SERVICES MAINTENANCE')) {
                return bp;
            }
        })[0];
        //var techQlis = component.get('v.techQlis');
        var licenses = component.get('v.licenses');
        //console.log(licenses);
        /*techQlis = _.compact(techQlis, function(qli) {
            return !qli.Related_Product__r.ProductCode.toUpperCase().includes('MAINT') || qli.License == undefined;
        });*/
        techQlis = _.filter(techQlis, function(qli) {
            return  qli.Related_Product__r.ProductCode != 'CreditES' && (!qli.Related_Product__r.ProductCode.toUpperCase().includes('MAINT') || qli.License == undefined);
        })
        _.forEach(licenses, function(l) {
            if(l.Product__r.ProductCode.toUpperCase().includes('MAINT')) {
                var qli = Object.assign({}, maintenanceBundleProduct);
                qli.PriceEach = l.Invoice_Amount__c / l.licenseCurrency * component.get('v.currency');
                qli.PriceTotal = l.Invoice_Amount__c / l.licenseCurrency * component.get('v.currency');
                qli.License = l.Id;
                qli.Multiplicity_Key__c = undefined;
                qli.PriceUpdate = true;
                qli.BundleDiscount = 0;
                //console.log('adding ES maintenance');
                techQlis.push(qli);
            }
        });
        if(upgradeType === 'change-dates' || upgradeType === 'merge'){
            var creditProduct = _.filter(component.get('v.allBundleProducts'), function(bp) {
                return bp.Related_Product__r.ProductCode === 'CreditES';
            })[0];
            var qli = Object.assign({}, creditProduct);
            var sum = -1 * _.sumBy(licenses, function(l) {
                if(l.Product__r.ProductCode.toUpperCase().includes('MAINT')) {
                    return l.Invoice_Amount__c / l.licenseCurrency * component.get('v.currency') * (l.MergeTerm / l.TermLength);
                }
            });
            qli.PriceEach = sum;
            qli.PriceTotal = sum;
            qli.PriceUpdate = true;
            qli.BundleDiscount = 0;
            qli.Availability__c = 'Included';
            techQlis.push(qli);
        }
        //console.log(techQlis);
        //helper.updatePrice(component, event, helper, techQlis);
        //component.set('v.techQlis', techQlis);
        helper.calculateQliTotals(component,event,helper,techQlis,fromRules);
    }
    
})