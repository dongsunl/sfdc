({
    doInit : function(component,event,helper){
        // console.log('CBC: doInit')        
        
    },
    
    reloadFab :function(component,event,helper){
        //  console.log('CBC: reloadFab')
        var cmpEvent = $A.get("e.c:fab_Reload");
        cmpEvent.fire();
    },
    
    send : function(component,event,helper){
        //  console.log('CBC: send')
        var target = event.getSource();
        var buttonName = target.get("v.value");
         // console.log(buttonName)
         // console.log(buttonName)
        if(buttonName.includes('Download')){
            var contractDocument = component.get('v.contractDocument');
            contractDocument.Contract_Downloaded__c = true;
            component.set('v.contractDocument',contractDocument)
            var cmpEvent = $A.get("e.c:fab_saveCdChanges");
            // console.log(contractDocument)
            cmpEvent.setParams({"contractDocument": contractDocument});
            cmpEvent.fire();
        }
        helper.getCongaMetaData(component,event,helper,buttonName);
        // sforce.one.back()
    },
})