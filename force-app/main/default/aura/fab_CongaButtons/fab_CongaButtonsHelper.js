({      
    getCongaMetaData : function(component,event,helper,buttonName){
        //console.log('CBH: getCongaMetaData');
        var action = component.get('c.getCongaButton');
        // console.log(buttonName);
        action.setParams({
            "buttonName": buttonName
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                // console.log(dataObj);
                helper.createURL(component,event,helper,dataObj,buttonName);                
            } 
            else if (state === 'ERROR') {
                var header = 'Conga MetaData Retrieval Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    createURL: function(component,event,helper,buttonData,buttonName){
        console.log('CBH: createURL')
        var congaButton = buttonData[0].Conga_Button__r;
         console.log('buttonData');
          console.log(buttonData);
        var congaCriteria = [];
        buttonData.forEach(function(bd){
            congaCriteria.push(bd.Conga_Button_Criteria__r);
        })
         console.log('congaCriteria');
         console.log(congaCriteria);
        var fabVariables = component.get('v.fabVariables');
          console.log('fabVariables');
         console.log(fabVariables);
        var buttonId;
        fabVariables.forEach(function(f){
            if(f.DeveloperName === congaButton.URL_Id__c){
                buttonId = component.get(f.Component_Code__c);                       
            }
        });
          console.log('buttonId')
          console.log(buttonId)
        var sessionId = component.get('v.sessionId');
        var partnerURL = component.get('v.partnerServerURL');
         console.log('partnerURL')
          console.log(partnerURL)
        var buttonURL = 'https://composer.congamerge.com/composer8/index.html?sessionId='+sessionId+'&serverUrl='+partnerURL+'&id='+buttonId;
        var buttonURL = '/apex/APXTConga4__Conga_Composer?serverUrl='+partnerURL+'&id='+buttonId;
        
        var queryData = _.filter(congaCriteria,{'Type__c':'Query'}); 
         console.log('queryData')
          console.log(queryData)
        if(queryData.length>0){
            var queryURL = this.getQueryURL(component,event,helper,queryData);
            console.log('queryURL');
            console.log(queryURL);
        }
        var qVariableData = _.filter(congaCriteria,{'Type__c':'QVariable'});  
        if(qVariableData.length>0){
            var qVariableURL = this.getQVariableURL(component,event,helper,qVariableData);  
              console.log('qVariableURL');
              console.log(qVariableURL);
        }
        var templateData = component.get('v.templateData'); 
        console.log('templateData');
        console.log(templateData);
        if(templateData.length>0){
            //  var templateURL = '&TemplateId='+'a3x220000002JvcAAE'
            var templateURL = this.getTemplateURL(component,event,helper,templateData,qVariableData,congaCriteria);
          //  console.log('templateURL');
          //  console.log(templateURL);
        }
        var parameterData = _.filter(congaCriteria,{'Type__c':'Parameter'});  
        if(parameterData.length>0){
            var parameterURL = this.getParameterURL(component,event,helper,parameterData);
            // console.log('parameterURL');
            // console.log(parameterURL);
        }   
        //console.log('queryURL')
        // console.log(queryURL)
        // buttonURL = buttonURL +
        if(queryURL!== undefined){
            buttonURL = buttonURL + queryURL 
        }
        if(qVariableURL!== undefined){
            buttonURL = buttonURL + qVariableURL 
        }
        if(templateURL!== undefined){
            buttonURL = buttonURL + templateURL 
        }
        if(parameterURL!== undefined){
            buttonURL = buttonURL + parameterURL
        }
        console.log('buttonURL')
        console.log(buttonURL)
        
        if(window.location.hostname.includes('visual.force.com')){//If in Visualforce domain
            //Navigation in the salesforce1/lightning app
            if( (typeof sforce != 'undefined') && sforce && (!!sforce.one) ) {
                sforce.one.navigateToURL(buttonURL);
            }
            else {
                //Navigation in classic.
                window.location.assign(buttonURL);
            }
        }
        else{//If not in Visualforce domain
            var event = $A.get("e.force:navigateToURL");
            event.setParams({"url": buttonURL});
            event.fire();
        }
        console.log('buttonName')
        console.log(buttonName)
        if(buttonName === 'Invoice_Email' || buttonName === 'Invoice_Email_RS'){
            helper.updateOppStage(component,event,helper);
        }
    },
    
    getQueryURL : function(component,event,helper,queryData){
        // console.log('CBH: getQueryURL');
        var congaQueries = component.get('v.congaQueries');
        var fabVariables = component.get('v.fabVariables');        
        var queryURL = '&QueryId=';    
        var qdSize = queryData.length;
        Object.keys(queryData).forEach(function(key, i) {
            var value = queryData[key];
            //  console.log('value')
            //  console.log(value)
            var alias = value.Alias__c;       
            //   console.log('alias')
            //   console.log(alias)
            var id;
            // console.log('congaQueries')
            //  console.log(congaQueries)
            congaQueries.forEach(function(cq){
                if(cq.APXTConga4__Name__c === value.Query_Name__c)
                {
                    id = cq.Id;
                }
            });
            var pv0;
            var pv1;
            fabVariables.forEach(function(f){
                if(f.DeveloperName === value.PV0__c && value.PV0__c !== undefined){
                    pv0 = '?pv0=' + component.get(f.Component_Code__c);                       
                }
            }); 
            if(value.PV1__c !== undefined){
                pv1 = '?pv1=' + value.PV1__c;
            }
            //  console.log('pv0')
            //  console.log(pv0)
            if(i+1<qdSize && id!= undefined && pv0!='?pv0=undefined' ){
                if(pv1 != undefined){
                    queryURL = queryURL + alias + id + pv0+ pv1 + ',';
                }else{
                    queryURL = queryURL + alias + id + pv0+',';
                }
                
            }else if(id!= undefined && pv0!='?pv0=undefined'){
                if(pv1 != undefined){
                    queryURL = queryURL + alias + id + pv0+ pv1;
                }else{
                    queryURL = queryURL + alias + id + pv0;
                }
                
            }
        });
        // console.log(queryURL);
        // console.log(queryURL);
        return queryURL;
    },
    
    getQVariableURL : function(component,event,helper,qVariableData){
        //  console.log('CBH: getQVariableURL');
        var congaQueries = component.get('v.congaQueries');
        var qVariableURL;        
        
        var qdSize = qVariableData.length;
        Object.keys(qVariableData).forEach(function(key, i) {
            var value = qVariableData[key];
            //console.log('value');   
            //console.log(value);   
            var id;
            congaQueries.forEach(function(cq){
                //console.log('cq')
                //console.log(cq)
                if(cq.APXTConga4__Name__c === value.QVar_Query_Name__c)
                {
                    id = cq.Id;
                }
            });
            //console.log('qVariableURL1');
            //console.log(qVariableURL);
            if(qVariableURL === undefined){
                qVariableURL =  "&QVar" + value.QVar_Number__c + "Id=" + id;
            }else{
                qVariableURL = qVariableURL + "&QVar" + value.QVar_Number__c;
            }
            
        });
        //console.log('qVariableURL2');
        //console.log(qVariableURL);
        return qVariableURL;
    },
    
    getTemplateURL : function(component,event,helper,templateData,qVariableData,congaCriteria){
        console.log('CBH: getTemplateURL');
        // console.log('templateData');
        // console.log(templateData);
        //  console.log('congaCriteria')
        //  console.log(congaCriteria)
        var templateURL;
        var tempValue;
        var tdSize = templateData.length;        
        congaCriteria.forEach(function(cc){
            if(cc.Type__c === 'Template'){
                if(cc.Template_Type__c === 'Name'){
                    templateData.forEach(function(td){
                        if(td.Name === cc.Template_Value__c){
                            templateURL = '&TemplateId=' + td.Template_Id__c;
                        }
                    })
                }else{
                    if(qVariableData.length > 0 ){
                        templateURL = '&TemplateId=' + '{QVar1}';
                    }
                }
            }              
        })
        // templateURL = templateURL + tempValue;
        // console.log(templateURL);
        return templateURL;
    },
    
    getParameterURL : function(component,event,helper,parameterData){    
        console.log('CBH: getParameterURL');
        var today = Date();
        var fabVariables = component.get('v.fabVariables');        
        var parameterURL; 
        var contractDocument = component.get('v.contractDocument');
        var opportunityId = component.get('v.opportunity').Id;
        var emailTemplates = component.get('v.congaEmailTemplates');
        //console.log('contractDocument')
        //console.log(contractDocument)
        Object.keys(parameterData).forEach(function(key, i) {
            var value = parameterData[key]; 
            // console.log('value');
            //         console.log(value);
            var variable;
            if(value.Parameter__c === 'OFN'){
                variable = contractDocument.Today__c + "_" + contractDocument.Subscriber__c + "_QSO_" + contractDocument.Name;
                //  console.log('variable')
                //  console.log(variable)
                if(parameterURL === undefined){
                    parameterURL = "&" + value.Parameter__c + "=" + variable;
                }else{
                    parameterURL = parameterURL + "&" + value.Parameter__c + "=" + variable;
                } 
            } 
            else if(value.Parameter__c === 'RetUrl'){
                variable = value.Parameter_Value__c + opportunityId;
                //  console.log('variable')
                //  console.log(variable)
                if(parameterURL === undefined){
                    parameterURL = "&" + value.Parameter__c + "=" + variable;
                }else{
                    parameterURL = parameterURL + "&" + value.Parameter__c + "=" + variable;
                } 
            } 
                else if(value.Parameter_Type__c === 'EmailTemplate'){
                    emailTemplates.forEach(function(et){
                        console.log('value')
                        console.log(value)
                        console.log('et')
                        console.log(et)
                        if(value.Parameter_Value__c === et.APXTConga4__Name__c){
                            variable = et.Id;
                            if(parameterURL === undefined){
                                parameterURL = "&" + value.Parameter__c + "=" + variable;
                            }else{
                                parameterURL = parameterURL + "&" + value.Parameter__c + "=" + variable;
                            } 
                        }
                    })
                   console.log('parameterURL');
                    console.log(parameterURL);  
                }
                    else if(value.Parameter_Type__c ==='Variable'){
                        fabVariables.forEach(function(f){
                            console.log('---Start---');
                            console.log(f.DeveloperName);
                             console.log('ParameterValue');
                            console.log(value.Parameter_Value__c);
                            
                            if(f.DeveloperName === value.Parameter_Value__c){
                                console.log('componentcode')
                                console.log(f.Component_Code__c)
                                variable = component.get(f.Component_Code__c); 
                                console.log('variable');
                                console.log(variable);
                                console.log('---End---');
                            }
                        }); 
                        if(parameterURL === undefined && variable !== undefined ){
                            parameterURL = "&" + value.Parameter__c + "=" + variable;
                        }else if(variable !== undefined) {
                            parameterURL = parameterURL + "&" + value.Parameter__c + "=" + variable;
                        } 
                    }else{
                        if(parameterURL === undefined){
                            parameterURL = "&" + value.Parameter__c + "=" + value.Parameter_Value__c;
                        }else {
                            parameterURL = parameterURL + "&" + value.Parameter__c + "=" + value.Parameter_Value__c;
                        } 
                    }
        });
        console.log('parameterURL');
        console.log(parameterURL);
        return parameterURL;
    },
    
    updateOppStage : function(component,event,helper){
        var recordType = component.get('v.opportunity').RecordType.Name;
        var opportunity = component.get('v.objInfo'); 
        var action = component.get('c.updateRecord');
        var newItem = {};
        newItem = JSON.parse(JSON.stringify(opportunity));
        newItem.Id = component.get('v.opportunity').Id;
        newItem.Sales_Process_Stage__c = 'Awaiting Client Payment';
        newItem.StageName = 'Invoice';
        newItem.ForecastCategoryName = 'Commit';       
        action.setParams({
            "record": newItem
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                console.log(dataObj);
               
                var cmpEvent = $A.get("e.c:fab_Reload");
                cmpEvent.fire();
               
            }  
            else if (state === 'ERROR') {
                var header = 'Contract Document Save Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    processErrors : function(component,response,header){
        console.log('MCH: processErrors');
        const errors = response.getError();
        let message = 'Unknown error'; 
        if (errors && Array.isArray(errors) && errors.length > 0) {
            const error = errors[0];
            if (typeof error.message != 'undefined') {
                message = error.message;
            } else if (typeof error.pageErrors != 'undefined' && Array.isArray(error.pageErrors) && error.pageErrors.length > 0) {
                const pageError = error.pageErrors[0];
                if (typeof pageError.message != 'undefined') {
                    message = pageError.message;
                }
            }
        }
        component.set('v.errorHeader',header);
        component.set('v.errorBody',message);
        component.set('v.isError',true);        
    },
})