({
    doInit: function(component, event, helper) {
        // Prepare a new record from template
        component.set('v.showSpinner',true)
        component.find("linkRecordCreator").getNewRecord(
            "Retainer_Link__c", // sObject type (objectApiName)
            null,      // recordTypeId
            false,     // skip cache?
            $A.getCallback(function() {
                var rec = component.get("v.newLink");
                var error = component.get("v.newLinkError");
                if(error || (rec === null)) {
                    console.log("Error initializing record template: " + error);
                    return;
                }
                console.log("Record template initialized: " + rec.apiName);
            })
        );
        helper.getCurrency(component,event,helper);
        helper.getLinkedRetainers(component,event,helper);
    },
    
    handleRetainerLink: function(component, event, helper) {
        console.log('handleRetainerLink') 
         component.set('v.showSpinner',true)
        component.set("v.simpleNewLink.Related_Opportunity__c", component.get("v.opportunity.Id"));
        component.set("v.simpleNewLink.Related_Retainer__c",component.get('v.selItem.val'))
        component.set("v.simpleNewLink.Project_name__c",component.get('v.opportunity.Name'))
        component.set("v.simpleNewLink.CurrencyIsoCode",component.get('v.selCurrency'))  
        
        component.find("linkRecordCreator").saveRecord(function(saveResult) {
              console.log(saveResult.state)
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                // record is saved successfully
              /*  var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Saved",
                    "message": "The record was saved."
                });
                resultsToast.fire();*/
                helper.reloadFab(component,event,helper);
                
            } else if (saveResult.state === "INCOMPLETE") {
                // handle the incomplete state
                console.log("User is offline, device doesn't support drafts.");
            } else if (saveResult.state === "ERROR") {
                // handle the error state
                console.log('Problem saving contact, error: ' + JSON.stringify(saveResult.error));
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
            }
        });
        
    },
    
    onChange : function(component,event,helper){
        component.set('v.selCurrency',component.find('select').get('v.value') ) 
        
    },
    
    validate: function(component,event,helper){
        console.log('validate')
        var reqAmount = component.find('valueField').get('v.value');
        console.log(reqAmount)
        var remainAmount = component.get('v.valueRemaining');
        console.log(remainAmount)
        if(reqAmount <= remainAmount){
            component.set('v.disabled',false);
        }else{
            component.set('v.disabled',true);
        }
    },
    
    handleEdit : function(component,event,helper){
        console.log('handleEdit')
        component.set('v.edit',true)
        console.log(component.find('editId').get('v.value'))
        component.set('v.editId',component.find('editId').get('v.value') )
    }
    
})