({
    doInit : function(component, event, helper) {
     //  console.log('MCC: doInit');
      //console.log(component.get('v.profileName'))
     
        component.set("v.showspinner", true);
        helper.createContractDocument(component,event,helper); 
        helper.getUserId(component,event,helper);
        helper.getProfile(component,event,helper);
    }, 
    
    handleMenuSelect: function (component,event,helper) {
      //  console.log('MCC: handleMenuSelect');
        var selectedMenuItemValue = event.getParam("value");
        component.set('v.contractType',selectedMenuItemValue);
        helper.saveCdChanges(component,event,helper);
    },
    
    openChoiceModal : function(component,event,helper){
       // console.log('MCC: openChoiceModal');
        component.set('v.choiceIsOpen',true);
    },
    
    onChoiceClose : function(component,event,helper){
      //  console.log('MCC: onChoiceClose');
        component.set('v.choiceIsOpen',false);
    },
    
    reloadFAB: function(component, event, helper) {
    //   console.log('-------------MCC: reloadFAB--------------');
        var opportunityId = component.get('v.opportunityId');
        helper.getOpportunity(component,event,helper,opportunityId); 
        component.set("v.showSpinner", true);
    },
    
    refreshOppId: function(component, event, helper){
       // console.log('MCC: refreshOppId');
        var params = event.getParam('arguments');
        if (params) {
        var opportunityId = params.oppId;
        helper.getOpportunity(component,event,helper,opportunityId);
            }
    },    
    
    saveCdChanges : function(component,event,helper){
        //console.log('MCC: saveCdChanges');
        var contractDocument = event.getSource().get("v.contractDocument");
        //console.log(contractDocument)
        component.set('v.contractDocument',contractDocument);
        helper.saveCdChanges(component,event,helper);
    },
  
})