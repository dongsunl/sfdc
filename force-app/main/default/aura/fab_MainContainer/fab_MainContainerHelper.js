({
    getUserId : function(component,event,helper){
        console.log('MCH: getUserId');
        var action = component.get('c.getUserId');
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                component.set('v.userId',dataObj)
            }  
            else if (state === 'ERROR') {
                var header = 'User Id Retrieval Error';
                //  helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    getProfile : function(component,event,helper){
        // console.log('MCH: getProfileId');
        var action = component.get('c.getProfileId');
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            // console.log(state)
            
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                component.set('v.profileId',dataObj)
                //   console.log('------profileid')
                //  console.log(dataObj)
            }  
            else if (state === 'ERROR') {
                const errors = response.getError();
                console.log(errors)
                var header = 'User Id Retrieval Error';
                //  helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    createContractDocument : function(component,event,helper) {
        //      console.log('MCH: createContractDocument');
        var opportunityId = component.get('v.opportunityId');
        var action = component.get('c.createContractDocument');
        action.setParams({
            "opportunityId": opportunityId
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                helper.getOpportunity(component,event,helper,opportunityId);
            }  
            else if (state === 'ERROR') {
                var header = 'Contract Document Creation Error';
                //   helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    getOpportunity : function(component,event,helper,opportunityId) {
        //        console.log('MCH: getOpportunity');
        var action = component.get('c.getOpportunity');
        action.setParams({
            "opportunityId": opportunityId
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                // console.log('opportunity')
                // console.log(dataObj)
                component.set('v.opportunity',dataObj);
                
                if(dataObj.Payment_Terms__c!=undefined){
                    component.set('v.payTermsSelectedValue',dataObj.Payment_Terms__c);
                }                
                
                // console.log(dataObj.Billed_Against_Retainer__c)
                component.set('v.retainer',dataObj.Billed_Against_Retainer__c);
                
                helper.getContractDocument(component,event,helper,opportunityId);	
                helper.getQuoteList(component,event,helper,opportunityId)
                helper.getServiceList(component,event,helper,opportunityId)
                //  console.log('dataObj.SyncedQuoteId')
                //  console.log(dataObj.SyncedQuoteId)
                if(dataObj.SyncedQuoteId !== undefined){
                    helper.getService(component,event,helper,dataObj.SyncedQuoteId);
                    helper.getQuoteLines(component,event,helper,dataObj.SyncedQuoteId);
                }
                if(dataObj.Client__c !== undefined){
                    helper.getClient(component,event,helper,dataObj.Client__c);
                }                
            } 
            else if (state === 'ERROR') {
                var header = 'Opportunity Retrieval Error';
                //  helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    getContractDocument : function(component,event,helper,opportunityId) {
        //         console.log('MCH: getContractDocument');
        var action = component.get('c.getContractDocument');
        action.setParams({
            "opportunityId": opportunityId
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                //  console.log(dataObj)
                if(dataObj !== undefined){
                    component.set('v.contractDocument',dataObj);
                    component.set('v.contractType', dataObj.Contract_Type__c);
                    component.set('v.contractMethodType', dataObj.Contract_Method_Type__c);
                    component.set('v.partnerServerURL',dataObj.PartnerURL__c);
                    if(dataObj.Data_Center_Location__c!=undefined){
                        component.set('v.dataCenterSelectedValue',dataObj.Data_Center_Location__c);
                    }
                    if(dataObj.Quote_Type__c!=undefined){
                        component.set('v.quoteTypeSelectedValue',dataObj.Quote_Type__c);
                    }
                    component.set('v.zeroLineItems',dataObj.Zero_Dollar_Line_Items__c);     
                    component.set('v.itemized',dataObj.Itemized__c);  
                    if(dataObj.Discounted__c === true){
                        component.set('v.discountVisible',dataObj.Discount_Visible__c);
                    }
                    else{
                        component.set('v.discountVisible',false);
                    }                     
                    component.set('v.discounted',dataObj.Discounted__c); 
                    component.set('v.premium',dataObj.Premium__c); 
                    component.set('v.servicesOnly',dataObj.Services_Only__c);
                    component.set('v.signatoryContact',dataObj.Legal_Contact__c);
                    component.set('v.multiYear',dataObj.MultiYear_Quote__c); 
                    component.set('v.myStartDate',dataObj.MultiYear_Start_Date__c); 
                    component.set('v.myEndDate',dataObj.MultiYear_End_Date__c); 
                    component.set('v.myTerm',dataObj.MultiYear_Term_in_months__c); 
                    component.set('v.myTotal',dataObj.MultiYear_Total__c);                                                  
                    helper.setContractMethodOptions(component,event,helper);
                    helper.getDocusignStatus(component,event,helper,dataObj.Id);
                    helper.getCongaTemplates(component,event,helper);
                    helper.getCongaEmailTemplates(component,event,helper);
                    helper.getLegalContact(component,event,helper);
                    helper.getSessionId(component,event,helper);
                    helper.getCongaQueries(component,event,helper);
                    helper.getFabVariables(component,event,helper);
                }
                component.set('v.saving',false);                
            } 
            else if (state === 'ERROR') {
                var header = 'Contract Document Retrieval Error';
                //  helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    getService : function(component,event,helper,quoteId){
        //     console.log('MCH: getService');
        if(quoteId!=undefined){
            var action = component.get('c.getService');
            action.setParams({
                "quoteId": quoteId
            });
            action.setCallback(this, $A.getCallback(function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var dataObj= response.getReturnValue();
                    //  console.log('dataObj')
                    //   console.log(dataObj)
                    component.set('v.service',dataObj);
                    console.log('dataObj');
                    if(dataObj.length>0){
                        console.log(dataObj[0].Qualtrics_Invoicing__c)
                        if(dataObj[0].Qualtrics_Invoicing__c == false){
                            console.log('qualtrics not invoicing')
                            var partnerAvailability = [];
                            dataObj.forEach(function(pa){
                                if(pa.Partner_Availability__r !== undefined){
                                    partnerAvailability.push(pa);
                                }
                            })
                            component.set('v.partnerAvailability',partnerAvailability);
                        }
                    }
                    
                    
                    helper.setContractOptions(component,event,helper);
                } 
                else if (state === 'ERROR') {
                    var header = 'Service Record Retrieval Error';
                    //  helper.processErrors(component,response,header);
                }
            }));
            $A.enqueueAction(action);
        }
    },
    
    getServiceList : function(component,event,helper,oppId){
        //      console.log('MCH: getServiceList');
        if(oppId!=undefined){
            var action = component.get('c.getServiceList');
            action.setParams({
                "oppId": oppId
            });
            action.setCallback(this, $A.getCallback(function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var dataObj= response.getReturnValue();
                    //     console.log('dataObj')
                    //     console.log(dataObj)
                    component.set('v.serviceList',dataObj);
                    
                } 
                else if (state === 'ERROR') {
                    var header = 'Service List Retrieval Error';
                    // helper.processErrors(component,response,header);
                }
            }));
            $A.enqueueAction(action);
        }
    },
    
    getQuoteLines : function(component,event,helper,quoteId){
        //      console.log('MCH: getQuoteLines');
        if(quoteId!=undefined){
            var action = component.get('c.getQuoteLines');
            action.setParams({
                "quoteId": quoteId
            });
            action.setCallback(this, $A.getCallback(function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var dataObj= response.getReturnValue();
                    component.set('v.quoteLines',dataObj);
                    var productFamily = this.groupBy(dataObj,'Product_Family_Display_Name__c');  
                    // console.log(productFamily)
                    component.set('v.quoteLinesGrouped',Object.getOwnPropertyNames(productFamily));
                } 
                else if (state === 'ERROR') {
                    var header = 'Quote Line Item Retrieval Error';
                    //  helper.processErrors(component,response,header);
                }
            }));
            
            $A.enqueueAction(action);
        }
        
    },
    
    getQuoteList : function(component,event,helper,opportunityId) {
        console.log('MCH: getQuoteList')		
        var action = component.get('c.getQuoteList');
        
        //console.log(opportunityId)
        action.setParams({
            "opportunityId": opportunityId
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            //console.log(state)
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue(); 
                console.log('quoteList');
                console.log(dataObj)
                
                if(component.get('v.multiYear')){
                    var multiYearYears = component.get('v.contractDocument').MultiYear_Number_of_Years__c;
                    //  console.log('multiYearYears')
                    //    console.log(multiYearYears)
                    if(multiYearYears !== undefined){
                        component.set('v.multiYearSelection',true);
                    }
                    var total = 0;
                    var yearCount = 0;
                    var startDate;
                    var endDate;
                    var term;
                    dataObj.forEach(function(ql){
                        
                        console.log(ql)
                        if(ql.Quote_Year_Multi_Year__c != undefined){  
                            console.log('ql.Quote_Year_Multi_Year__c');
                            console.log(ql.Quote_Year_Multi_Year__c);
                            console.log('ql.TotalPrice');
                            console.log(ql.TotalPrice);
                            total += parseFloat(ql.TotalPrice);
                            console.log('total')
                            console.log(total)
                            yearCount += 1;
                            if(ql.Quote_Year_Multi_Year__c === "1" || ql.IsSyncing === true){
                                startDate = ql.License_Start_Date__c;
                                // console.log('startDate')
                                // console.log(startDate)
                            }
                        } 
                        if(ql.Quote_Year_Multi_Year__c === multiYearYears) {
                            endDate = ql.License_End_Date__c;
                            // console.log('endDate')
                            // console.log(endDate)
                        }
                    })
                    component.set('v.multiYearSelectedQuotes',yearCount)
                    var multiYearCountMatch = false;
                    if(parseInt(multiYearYears) === yearCount){
                        //  console.log('this is true')
                        multiYearCountMatch=true
                    }
                    component.set('v.multiYearCountMatch',multiYearCountMatch)
                    //  console.log('yearCount');
                    //  console.log(yearCount)
                    
                    var date1 = new Date(startDate);//mm/dd/yyyy
                    // console.log(date1)
                    var date2 = new Date(endDate);//mm/dd/yyyy
                    // console.log(date2)
                    var timeDiffrence = Math.abs(date2.getTime() - date1.getTime());
                    // console.log(timeDiffrence)
                    var differDays = Math.ceil(timeDiffrence / (1000 * 3600 * 24)); 
                    // console.log(differDays)
                    var term = Math.round(differDays/30.41667)
                    // console.log(term)
                    console.log('total')
                            console.log(total)
                    component.set('v.myTotal',total);
                    component.set('v.myStartDate',startDate)
                    component.set('v.myEndDate',endDate)
                    component.set('v.myTerm',term)
                }
                component.set('v.quoteList',dataObj)
                //   console.log(startDate)
                //  console.log(dataObj)
                if(startDate !== undefined && dataObj !== undefined){
                    helper.updateQuote(component,event,helper,startDate,dataObj);
                    helper.updateMultiYear(component,event,helper,dataObj);
                }
            }  
            else if (state === 'ERROR') {
                var header = 'Quote List Retrieval Error';
                // helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    updateMultiYear: function(component, event, helper,quoteList) { 
        //  console.log('MCH: updateMultiYear')	
        var ContractDocument = component.get('v.cdObjInfo'); 
        var action = component.get('c.updateRecord');
        var cd = component.get('v.contractDocument')
        //  console.log(quoteList);
        var newItem = {};
        newItem = JSON.parse(JSON.stringify(ContractDocument));
        newItem.Id = cd.Id
        newItem.MultiYear_End_Date__c = component.get('v.myEndDate')
        newItem.MultiYear_Start_Date__c = component.get('v.myStartDate')
        newItem.MultiYear_Term_in_months__c = component.get('v.myTerm')
        newItem.MultiYear_Total__c = component.get('v.myTotal')
        quoteList.forEach(function(ql){
            if(ql.Quote_Year_Multi_Year__c === "1"){
                newItem.Year_1_Software_Amount__c = ql.TotalPrice - ql.Total_Services__c;
            }
            else if(ql.Quote_Year_Multi_Year__c === "2"){
                newItem.Year_2_Software_Amount__c = ql.TotalPrice - ql.Total_Services__c;
            }
                else if(ql.Quote_Year_Multi_Year__c === "3"){
                    newItem.Year_3_Software_Amount__c = ql.TotalPrice - ql.Total_Services__c;
                }
                    else if(ql.Quote_Year_Multi_Year__c === "4"){
                        newItem.Year_4_Software_Amount__c = ql.TotalPrice - ql.Total_Services__c;
                    }
                        else if(ql.Quote_Year_Multi_Year__c === "5"){
                            newItem.Year_5_Software_Amount__c = ql.TotalPrice - ql.Total_Services__c;
                        }else{
                            newItem.Year_5_Software_Amount__c = 0;
                        }           
        })       
        action.setParams({
            "record": newItem
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                //  console.log(dataObj);
                
            }  
            else if (state === 'ERROR') {
                var header = 'Contract Document Save Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },      
    
    updateQuote : function(component,event,helper,startDate,quoteList){  
        //      console.log('--------------QLH: updateQuote--------------')
        var action = component.get('c.updateQuoteList');
        // console.log(quoteList);
        //console.log(startDate)
        action.setParams({
            "quoteList": quoteList,
            "startDate" : startDate
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                // console.log('quoteList')
                // console.log(quoteList)
                // console.log('updatedQuoteList')
                // console.log(dataObj)
                
                component.set('v.quoteList',quoteList)
                //    console.log(component.get('v.quoteList'))
            }  
            else if (state === 'ERROR') {
                var header = 'Update Quote Error';
                //   helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action);
    },
    
    getCongaTemplates: function(component,event,helper){
        //         console.log('CSH: getCongaTemplates');
        var action = component.get('c.getCongaTemplates');  
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                component.set('v.congaTemplates',dataObj);
                //  console.log('congaTemplates')
                // console.log(dataObj)
                //helper.deleteContractDocumentTemplates(component,event,helper);
                helper.createContractDocumentTemplates(component,event,helper);
            } 
            else if (state === 'ERROR') {
                var header = 'Conga Templates Retrieval Error';
                //  helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    getCongaEmailTemplates: function(component,event,helper){
        //    console.log('CSH: getCongaEmailTemplates');
        var action = component.get('c.getCongaEmailTemplates');  
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                component.set('v.congaEmailTemplates',dataObj);
                //    console.log('congaTemplates')
                //  console.log(dataObj)
                
            } 
            else if (state === 'ERROR') {
                var header = 'Conga Email Templates Retrieval Error';
                // helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    createContractDocumentTemplates: function(component,event,helper){
        // console.log('CSH: createContractDocumentTemplates');   
        var action = component.get('c.insertRecords');
        var cdId = component.get('v.contractDocument').Id;
        var cd = component.get('v.contractDocument');    
        var opportunity = component.get('v.opportunity');
        //  console.log(cd)
        // console.log(opportunity)
        var partnerAvailability = component.get('v.partnerAvailability'); 
        var imp = '';
        if(cd.Implementation__c !== undefined){
            imp = _.trim(cd.Implementation__c);
        }        
        var itemList = [];
        if(opportunity.RecordType.Name === 'Panels'){
            if(component.get('v.contractType') === 'Service Order'){
                if(cd.MSA__c === true){
                    var field = 'APXTConga4__Name__c';
                    var name = 'Service Order MSA RS';
                    var type = 'Service Order';
                    var newItem = this.createTemplateRecord(component,event,helper,field,name,type);
                    itemList.push(newItem);
                }
                else if(component.get('v.contractMethodType')=== 'Custom Service Order' || component.get('v.contractMethodType')=== 'Paper Service Order'){
                    var field = 'APXTConga4__Name__c';
                    var name = 'Service Order RS Full';
                    var type = 'Service Order';
                    var newItem = this.createTemplateRecord(component,event,helper,field,name,type);
                    itemList.push(newItem);
                }
                    else{
                        var field = 'APXTConga4__Name__c';
                        var name = 'Service Order RS';
                        var type = 'Service Order';
                        var newItem = this.createTemplateRecord(component,event,helper,field,name,type);
                        itemList.push(newItem);  
                    }                
            } 
            var field = 'APXTConga4__Name__c';
            var name = 'Quote RS';
            var type = 'Quote';
            var newItem = this.createTemplateRecord(component,event,helper,field,name,type);
            itemList.push(newItem);
        }else{
            
            if(opportunity.SyncedQuoteId !== undefined && opportunity.SyncedQuote.XM_Products__c!== undefined){
                if(opportunity.SyncedQuote.XM_Products__c.includes('Delighted')){
                    if(cd.MSA__c === true){
                        var field = 'APXTConga4__Name__c';
                        var name = 'Service Order MSA Delighted';
                        var type = 'Service Order';
                        var newItem = this.createTemplateRecord(component,event,helper,field,name,type);
                        itemList.push(newItem);
                    }else{
                        var field = 'APXTConga4__Name__c';
                        var name = 'Service Order Delighted';
                        var type = 'Service Order';
                        var newItem = this.createTemplateRecord(component,event,helper,field,name,type);
                        itemList.push(newItem);
                    }   
                }
                else{
                    var field = 'APXTConga4__Name__c';
                    var name = 'Subscription Services Exhibit';
                    var type = 'Service Order';
                    var newItem = this.createTemplateRecord(component,event,helper,field,name,type);
                    itemList.push(newItem);   
                    if(cd.MSA__c === true){
                        var field = 'APXTConga4__Name__c';
                        var name = 'Service Order MSA';
                        var type = 'Service Order';
                        var newItem = this.createTemplateRecord(component,event,helper,field,name,type);
                        itemList.push(newItem);
                    }else{
                        var field = 'APXTConga4__Name__c';
                        var name = 'Service Order';
                        var type = 'Service Order';
                        var newItem = this.createTemplateRecord(component,event,helper,field,name,type);
                        itemList.push(newItem);
                    }    
                }     
            }
            
            if(imp != undefined && imp !=null && imp !='' ){
                var field = 'APXTConga4__Name__c';
                var name = imp;
                var type = 'Implementation';
                
                var service = component.get('v.service')
                //  console.log(service)
                var newItem = this.createTemplateRecord(component,event,helper,field,name,type);
                itemList.push(newItem);
                //   if(service.Qualtrics_Invoicing__c != true){
                if(partnerAvailability != undefined){
                    console.log('thisisworking')
                    if(partnerAvailability.length > 0){
                        var field = 'Id';
                        var name = partnerAvailability[0].Partner_Availability__r.Conga_Template_P1__c;
                        var type = 'Partner';
                        if(name!==undefined){
                            var newItem = this.createTemplateRecord(component,event,helper,field,name,type);
                            itemList.push(newItem);
                        }
                        var name2 = partnerAvailability[0].Partner_Availability__r.Conga_Template_P2__c;
                        if(name2!==undefined){
                            var newItem2 = this.createTemplateRecord(component,event,helper,field,name2,type);
                            itemList.push(newItem2);
                        }
                    }
                }
                // }
                
            }
            
            if(cd.MultiYear_Quote__c === true){
                var field = 'APXTConga4__Name__c';
                var name = 'Fees Exhibit';
                var type = 'Service Order';
                var newItem = this.createTemplateRecord(component,event,helper,field,name,type);
                itemList.push(newItem);
                
                var field = 'APXTConga4__Name__c';
                var name = 'License Configuration';
                var type = 'Service Order';
                var newItem = this.createTemplateRecord(component,event,helper,field,name,type);
                itemList.push(newItem);
            }else{
                var field = 'APXTConga4__Name__c';
                var name = 'Quote';
                var type = 'Quote';
                var newItem = this.createTemplateRecord(component,event,helper,field,name,type);
                itemList.push(newItem);
                
                var field = 'APXTConga4__Name__c';
                var name = 'Quote';
                var type = 'Service Order';
                var newItem = this.createTemplateRecord(component,event,helper,field,name,type);
                itemList.push(newItem); 
            } 
            var field = 'APXTConga4__Name__c';
            var name = 'Quote Multiple Options';
            var type = 'Quote Multiple';
            var newItem = this.createTemplateRecord(component,event,helper,field,name,type);
            itemList.push(newItem);
        }
        //console.log(itemList)
        action.setParams({
            "records": itemList,
            "cdId": cdId
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            //console.log(state)
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();  
                //console.log(dataObj)
                helper.getTemplateIds(component,event,helper);
            }  
            else if (state === 'ERROR') {
                var header = 'Contract Document Template Create Error';
                //  helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    createTemplateRecord : function(component,event,helper,field,name,type){
        //      console.log('CSH: createTemplateRecord');
        //  console.log(name);
        var ContractDocumentTemplate = component.get('v.cdtObjInfo'); 
        var cd = component.get('v.contractDocument');
        var congaTemplates = component.get('v.congaTemplates');
        var ct =  _.find(congaTemplates,[String(field),name])
        var newItem = {};
        
        newItem = JSON.parse(JSON.stringify(ContractDocumentTemplate));
        newItem.Name = ct.APXTConga4__Name__c;
        newItem.Template_Type__c = type;
        newItem.Contract_Document__c = cd.Id;        
        newItem.Sort_Order__c = ct.Order__c;
        
        newItem.Template_Id__c = ct.Id.substr(0,15); 
        //  console.log('newItem');
        //  console.log(newItem)
        return newItem;
        
    },
    
    groupBy : function(xs, key) {
        return xs.reduce(function(rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
        }, {});
    },
    
    getClient : function(component,event,helper,clientId){
        //      console.log('MCH: getClient');
        if(clientId!=undefined){
            var action = component.get('c.getClient');
            action.setParams({
                "clientId": clientId
            });
            action.setCallback(this, $A.getCallback(function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var dataObj= response.getReturnValue();
                    var contractDocument = component.get('v.contractDocument');
                    component.set('v.client',dataObj);
                    if(dataObj.MSA_Effective_Date__c === undefined && contractDocument.MSA__c === true){
                        contractDocument.MSA__c = false;
                        helper.saveCdChanges(component,event,helper);
                    }else if(dataObj.MSA_Effective_Date__c !== undefined && contractDocument.MSA__c === false){
                        contractDocument.MSA__c = true;
                        helper.saveCdChanges(component,event,helper);
                    }
                } 
                else if (state === 'ERROR') {
                    var header = 'Client Record Retrieval Error';
                    //   helper.processErrors(component,response,header);
                }
            }));
            $A.enqueueAction(action);
        }
    },
    
    getDocusignStatus : function(component,event,helper,cdId){
        //     console.log('MCH: getDocusignStatus');
        var action = component.get('c.getDocusignStatus');
        action.setParams({
            "cdId": cdId
        });        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                // console.log(dataObj)
                if(dataObj.length>0){
                    component.set('v.docusignHasRecords',true);
                    helper.getDocusignRecipients(component,event,helper,dataObj);
                }
                component.set('v.docusignStatusRecords',dataObj);
                dataObj.forEach(function(d){
                    if(d.dsfs__Envelope_Status__c === 'Sent'){
                        component.set('v.docusignStatusId',d.Id);  
                    } 
                }); 
                
            } 
            else if (state === 'ERROR') {
                var header = 'Docusign Status Record Retrieval Error';
                //  helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    getDocusignRecipients : function(component,event,helper,statusRecords){
        var statIds=[];
        statusRecords.forEach(function(sr){
            statIds.push(sr.Id);
        });
        
        var action = component.get('c.getDocusignRecipientStatus');
        action.setParams({
            "statIds": statIds
        });        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                //console.log(dataObj)
                if(dataObj.length>0){
                    component.set('v.docusignRecipientStatusRecords',dataObj)
                    
                }
            } 
            else if (state === 'ERROR') {
                var header = 'Docusign Status Record Retrieval Error';
                //  helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    saveCdChanges: function(component, event, helper) { 
        //     console.log('MCH: saveCdChanges');
        var recordType = component.get('v.opportunity').RecordType.Name;
        var ContractDocument = component.get('v.cdObjInfo'); 
        var action = component.get('c.updateRecord');
        var cd = component.get('v.contractDocument');
        //   console.log(cd)
        var msa = cd.MSA__c;
        if(recordType !== 'Panels'){
            var multiyear = cd.MultiYear_Quote__c;
        }
        var newItem = {};
        newItem = JSON.parse(JSON.stringify(ContractDocument));
        newItem.Id = cd.Id
        newItem.MSA__c = msa;
        newItem.Contract_Downloaded__c = cd.Contract_Downloaded__c;
        newItem.Legal_Case_Created__c = cd.Legal_Case_Created__c;
        
        newItem.Contract_Type__c = component.get('v.contractType');
        if(recordType !== 'Panels'){
            newItem.MultiYear_Quote__c = multiyear;
            newItem.Data_Center_Location__c = cd.Data_Center_Location__c
        }
        newItem.Quote_Type__c = cd.Quote_Type__c;
        newItem.Contract_Method_Type__c = cd.Contract_Method_Type__c;
        newItem.Legal_Contact__c = cd.Legal_Contact__c;
        newItem.Subscriber__c = cd.Subscriber__c
        action.setParams({
            "record": newItem
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                //console.log(dataObj);
                var opportunityId = component.get('v.opportunityId');
                var cmpEvent = $A.get("e.c:fab_Reload");
                cmpEvent.fire();
                // helper.getOpportunity(component,event,helper,opportunityId);
            }  
            else if (state === 'ERROR') {
                var header = 'Contract Document Save Error';
                //   helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },      
    
    setContractOptions: function(component,event,helper){
        //    console.log('MCH: setContractOptions');
        var items = [];
        var oppAmount = component.get('v.opportunity').Amount; 
        var service = component.get('v.service');        
        var servicesCount = 0;
        var contractType = component.get('v.contractType');
        // console.log('**service:',service);
        //console.log('**contractType:',contractType);
        if(service != undefined && service != null){
            service.forEach(function(s){
                // console.log('s')
                // console.log(s)
                if(s.Package__r.Category__c != 'Self' && s.Status__c !== 'Request Cancelled'){
                    servicesCount += 1
                }
            })
            // console.log(servicesCount	) 
        }        
        var recordType = component.get('v.opportunity').RecordType.Name;
        //  console.log(servicesCount)
        // console.log('**recordtype: ',recordType);
        // console.log(oppAmount)
        if(oppAmount >=75000){
            var qso = {"label": "Service Order", "value": "Service Order"};
            items.push(qso);
            if(contractType === 'Credit Card' || contractType === 'Purchase Order'){
                contractType = '';                
            }
        }
        else if((oppAmount >= 10000 && oppAmount < 75000) || (oppAmount < 10000 && servicesCount > 0 && recordType !== 'Panels')){
            var purchaseOrder = {"label": "Purchase Order", "value": "Purchase Order"};
            items.push(purchaseOrder);
            var qso = {"label": "Service Order", "value": "Service Order"};
            items.push(qso);  
            if(contractType === 'Credit Card'){
                contractType = '';    
            }           
            
        }   else{
            var creditCard = {"label": "Credit Card", "value": "Credit Card"};
            items.push(creditCard);
            var purchaseOrder = {"label": "Purchase Order", "value": "Purchase Order"};
            items.push(purchaseOrder);
            var qso = {"label": "Service Order", "value": "Service Order"};
            items.push(qso); 
        } 
        //  console.log(items)
        component.set("v.contractOptions", items);
        component.set('v.contractType',contractType);
    },
    
    setContractMethodOptions : function(component,event,helper) {
        console.log('MCH: setContractMethodOptions');
        var soStdDisabled = false;
        var soPapDisabled = false;
        var soCusDisabled = false;
        var items = [];
        var paymentTerms = component.get('v.payTermsSelectedValue');
        var contractMethodType = component.get('v.contractMethodType');
        var xmProducts = component.get('v.opportunity.SyncedQuote.XM_Products__c')
        
        if(/*paymentTerms === 'Custom' || */(xmProducts !== undefined && xmProducts.includes('Delighted') && xmProducts.length > 11)){
            //soStdDisabled = true;
            var custom = {"label": "Custom Service Order", "value": "Custom Service Order"}
            items.push(custom);
            var paper = {"label": "Paper Service Order", "value": "Paper Service Order"}
            items.push(paper);
            if(contractMethodType === 'Standard Service Order'){
                contractMethodType = 'Custom Service Order';
            }
        }else{
            var standard = {"label": "Standard Service Order", 
                            "value": "Standard Service Order" }
            items.push(standard);
            var custom = {"label": "Custom Service Order", "value": "Custom Service Order"}
            items.push(custom);
            var paper = {"label": "Paper Service Order", "value": "Paper Service Order"}
            items.push(paper);  
        }
        component.set("v.contractMethodOptions", items);
        component.set('v.contractMethodType',contractMethodType)
    },
    
    getLegalContact : function(component,event,helper){
        console.log('MCH: getLegalContact')
        var action = component.get('c.getLegalContact');  
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            console.log(state)
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                console.log('======legal contact=======')
                console.log(dataObj[0].Id)
                component.set('v.legalContactId',dataObj[0].Id)
            } 
            else if (state === 'ERROR') {
                var header = 'Legal Contact Retrieval Error';
                //   helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },   
    
    getSessionId : function(component,event,helper){
        //       console.log('MCH: getSessionId')
        var action = component.get('c.getSessionId');
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                //  console.log(dataObj);
                component.set('v.sessionId',dataObj);
            } 
            else if (state === 'ERROR') {
                var header = 'Session Id Retrieval Error';
                //  helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    getCongaQueries : function(component,event,helper){
        //       console.log('MCH: getCongaQueries');
        var action = component.get('c.getCongaQueries');
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                component.set('v.congaQueries',dataObj);
            } 
            else if (state === 'ERROR') {
                var header = 'Conga Query Retrieval Error';
                //   helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    getFabVariables : function(component,event,helper){
        //        console.log('MCH: getFabVariables');
        var action = component.get('c.getFabVariables');
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                component.set('v.fabVariables',dataObj);
                //  console.log('dataObj')
                // console.log(dataObj)
            } 
            else if (state === 'ERROR') {
                var header = 'FAB Variable Retrieval Error';
                // helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    getTemplateIds : function(component,event,helper){
        //    console.log('MCH: getTemplateIds')
        //  console.log(component.get('v.contractDocument'))
        var cdId = component.get('v.contractDocument').Id;
        //    console.log('cdId')
        //     console.log(cdId)
        var action = component.get('c.getTemplateIds');
        action.setParams({
            "cdId": cdId
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var templateData= response.getReturnValue();
                //  console.log('templateData')
                //  console.log(templateData)
                component.set('v.templateData',templateData);
                component.set("v.showspinner", false);
            } 
            else if (state === 'ERROR') {
                var header = 'Conga Template Retrieval Error';
                //helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },    
    
    processErrors : function(component,response,header){
        //  console.log('MCH: processErrors');
        component.set("v.showspinner", false);
        const errors = response.getError();
        let message = 'Unknown error'; 
        if (errors && Array.isArray(errors) && errors.length > 0) {
            const error = errors[0];
            if (typeof error.message != 'undefined') {
                message = error.message;
            } else if (typeof error.pageErrors != 'undefined' && Array.isArray(error.pageErrors) && error.pageErrors.length > 0) {
                const pageError = error.pageErrors[0];
                if (typeof pageError.message != 'undefined') {
                    message = pageError.message;
                }
            }
        }
        component.set('v.errorHeader',header);
        component.set('v.errorBody',message);
        component.set('v.isError',true);        
    },
    
    sendToVF : function(component, event, helper) {
        //   console.log('MCH: sendToVF');
        var message = 'updated';
        var vfOrigin = "https://" + component.get("v.vfHost");
        var vfWindow = component.find("vfFrame").getElement().contentWindow;
        vfWindow.postMessage(message, vfOrigin);
    },
    
})