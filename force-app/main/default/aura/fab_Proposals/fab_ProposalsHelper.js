({
    urlDirection : function(component,event,helper,url){
       // console.log('PH: urlDirection')
        var context = component.get("v.UserContext");
        //console.log(context);
        if(context != undefined) {
            if(context == 'Theme4t' || context == 'Theme4d') {
                //   console.log('VF in S1 or LEX');
                sforce.one.navigateToURL(url);
            } else {
                //  console.log('VF in Classic'); 
                window.location.assign(url);
            }
        } else {
            //   console.log('standalone Lightning Component');
            var event = $A.get("e.force:navigateToURL");
            event.setParams({"url": url});
            event.fire();
        }
    },
    
    getCongaQueries : function(component,event,helper){
      //  console.log('PH: getCongaQueries');
        var action = component.get('c.getCongaQueries');
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                component.set('v.congaQueries',dataObj);
            } 
            else if (state === 'ERROR') {
                helper.processErrors(component,response);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    getFabVariables : function(component,event,helper){
       // console.log('PH: getFabVariables');
        var action = component.get('c.getFabVariables');
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                component.set('v.fabVariables',dataObj);
                // console.log('dataObj')
                // console.log(dataObj)
            } 
            else if (state === 'ERROR') {
                helper.processErrors(component,response);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    getTemplateIds : function(component,event,helper){
      //  console.log('PH: getTemplateIds')
        var cdId = component.get('v.contractDocument').Id;
        var action = component.get('c.getTemplateIds');
        action.setParams({
            "cdId": cdId
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var templateData= response.getReturnValue();
                component.set('v.templateData',templateData);
            } 
            else if (state === 'ERROR') {
                helper.processErrors(component,response);
            }
        }));
        $A.enqueueAction(action); 
    },    
    
    setProposalOptions : function(component,event,helper){
       // console.log('PH: setProposalOptions')
        var congaTemplates = component.get('v.congaTemplates')
        var items = [];
        var contractDocument = component.get('v.contractDocument')
        var values = [];
        if(contractDocument.Contract_Document_Lines__r !== undefined){
            contractDocument.Contract_Document_Lines__r.forEach(function(cdl){
                if(cdl.Template_Type__c === "Proposal"){
                    var newValue = cdl.Name                
                    values.push(newValue)
                }
            })
        }
       // console.log(values)
        component.set('v.values',values)
      //  console.log(component.get('v.values'))
        congaTemplates.sort(function (a, b) {
            return Number(a.APXTConga4__Description__c) - Number(b.APXTConga4__Description__c);
        });
        
        congaTemplates.forEach(function(ct){
            if(ct.APXTConga4__Template_Group__c==='Proposal'){
                var item = {
                    "label": ct.APXTConga4__Name__c,
                    "value": ct.APXTConga4__Name__c
                };
                items.push(item);
            }
        })        
        component.set("v.options", items);      
        //  console.log('contractDocument')
        // console.log(contractDocument)
        
        
    },
    
    getSessionId : function(component,event,helper){
     //   console.log('RBH: getSessionId')
        var action = component.get('c.getSessionId');
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                //  console.log(dataObj);
                component.set('v.sessionId',dataObj);
            } 
            else if (state === 'ERROR') {
                helper.processErrors(component,response);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    getCongaMetaData : function(component,event,helper,buttonName){
      //  console.log('RBH: getCongaMetaData');
        var action = component.get('c.getCongaButton');
        //  console.log(buttonName);
        action.setParams({
            "buttonName": buttonName
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                //  console.log(dataObj);
                helper.createURL(component,event,helper,dataObj);
            } 
            else if (state === 'ERROR') {
                helper.processErrors(component,response);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    createURL: function(component,event,helper,buttonData){
      //  console.log('RBH: createURL')
        var congaButton = buttonData[0].Conga_Button__r;
        //  console.log(congaButton);
        var congaCriteria = [];
        buttonData.forEach(function(bd){
            congaCriteria.push(bd.Conga_Button_Criteria__r);
        })
        // console.log('congaCriteria');
        //  console.log(congaCriteria);
        var fabVariables = component.get('v.fabVariables');
        //  console.log('fabVariables');
        //  console.log(fabVariables);
        var buttonId;
        fabVariables.forEach(function(f){
            if(f.DeveloperName === congaButton.URL_Id__c){
                buttonId = component.get(f.Component_Code__c);                       
            }
        });
        var sessionId = component.get('v.sessionId');
        var partnerURL = component.get('v.partnerServerURL');
        
        var buttonURL = 'https://composer.congamerge.com/composer8/index.html?sessionId='+sessionId+'&serverUrl='+partnerURL+'&id='+buttonId;
        
        var queryData = _.filter(congaCriteria,{'Type__c':'Query'}); 
        //  console.log('queryData')
        //   console.log(queryData)
        if(queryData.length>0){
            var queryURL = this.getQueryURL(component,event,helper,queryData);
            //       console.log('queryURL');
            //       console.log(queryURL);
        }
        var qVariableData = _.filter(congaCriteria,{'Type__c':'QVariable'});  
        if(qVariableData.length>0){
            var qVariableURL = this.getQVariableURL(component,event,helper,qVariableData);  
            //   console.log('qVariableURL');
            //   console.log(qVariableURL);
        }
        var templateData = component.get('v.templateData'); 
        // console.log('templateData');
        // console.log(templateData);
        if(templateData.length>0){
            var templateURL = this.getTemplateURL(component,event,helper,templateData,qVariableData);
            // console.log('templateURL');
            //   console.log(templateURL);
        }
        var parameterData = _.filter(congaCriteria,{'Type__c':'Parameter'});  
        if(parameterData.length>0){
            var parameterURL = this.getParameterURL(component,event,helper,parameterData);
            //       console.log('parameterURL');
            //               console.log(parameterURL);
        }    
        buttonURL = buttonURL + queryURL + qVariableURL + templateURL + parameterURL;
      //  console.log('buttonURL')
     //   console.log(buttonURL)
        
        var context = component.get("v.UserContext");
        
        if(context != undefined) {
            if(context == 'Theme4t' || context == 'Theme4d') {
                //   console.log('VF in S1 or LEX');
                sforce.one.navigateToURL(buttonURL);
            } else {
                //  console.log('VF in Classic'); 
                window.location.assign(buttonURL);
            }
        } else {
            //   console.log('standalone Lightning Component');
            var event = $A.get("e.force:navigateToURL");
            event.setParams({"url": buttonURL});
            event.fire();
        }
        
    },
    
    getQueryURL : function(component,event,helper,queryData){
      //  console.log('RBH: getQueryURL');
        var congaQueries = component.get('v.congaQueries');
        var fabVariables = component.get('v.fabVariables');
        var queryURL = '&QueryId=';        
        
        var qdSize = queryData.length;
        Object.keys(queryData).forEach(function(key, i) {
            var value = queryData[key];
            var alias = value.Alias__c;               
            var id;
            congaQueries.forEach(function(cq){
                if(cq.APXTConga4__Name__c === value.Query_Name__c)
                {
                    id = cq.Id;
                }
            });
            var pv0;
            fabVariables.forEach(function(f){
                if(f.DeveloperName === value.PV0__c){
                    pv0 = component.get(f.Component_Code__c);                       
                }
            });                
            if(i+1<qdSize && id!= undefined && pv0!=undefined ){
                queryURL = queryURL + alias + id + '?pv0=' + pv0+',';
            }else if(id!= undefined && pv0!=undefined ){
                queryURL = queryURL + alias + id + '?pv0=' + pv0;
            }
            
        });
        // console.log(queryURL);
        return queryURL;
    },
    
    getQVariableURL : function(component,event,helper,qVariableData){
     //   console.log('RBH: getQVariableURL');
        var congaQueries = component.get('v.congaQueries');
        var qVariableURL;        
        
        var qdSize = qVariableData.length;
        Object.keys(qVariableData).forEach(function(key, i) {
            var value = qVariableData[key];
            //    console.log('value');   
            // console.log(value);   
            var id;
            congaQueries.forEach(function(cq){
                //   console.log('cq')
                //   console.log(cq)
                if(cq.APXTConga4__Name__c === value.QVar_Query_Name__c)
                {
                    id = cq.Id;
                }
            });
            // console.log('qVariableURL1');
            //  console.log(qVariableURL);
            if(qVariableURL === undefined){
                qVariableURL =  "&QVar" + value.QVar_Number__c + "Id=" + id;
            }else{
                qVariableURL = qVariableURL + "&QVar" + value.QVar_Number__c;
            }
            
        });
        // console.log('qVariableURL2');
        // console.log(qVariableURL);
        return qVariableURL;
    },
    
    getTemplateURL : function(component,event,helper,templateData,qVariableData){
      //  console.log('RBH: getTemplateURL');
        var templateURL = '&TemplateId='; 
        var tdSize = templateData.length;
        if(qVariableData.length > 0 ){
            templateURL = templateURL + '{QVar1}';
        }else{
            Object.keys(templateData).forEach(function(key, i) {
                var value = templateData[key];
                //console.log(value)
                var tempValue = value.Template_Id__c; 
                if(i+1<tdSize && tempValue!= undefined){
                    templateURL = templateURL + tempValue +',';
                }else if(tempValue!= undefined){
                    templateURL = templateURL + tempValue;
                }
            });
        }
        
        //console.log(templateURL);
        return templateURL;
    },
    
    getParameterURL : function(component,event,helper,parameterData){    
       // console.log('RBH: getParameterURL');
        var today = Date();
        var fabVariables = component.get('v.fabVariables');        
        var parameterURL; 
        Object.keys(parameterData).forEach(function(key, i) {
            var value = parameterData[key]; 
            // console.log(value);
            var variable;
            if(value.Parameter_Type__c ==='Variable'){
                fabVariables.forEach(function(f){
                    // console.log(f.DeveloperName);
                    //  console.log(value.Parameter_Value__c);
                    if(f.DeveloperName === value.Parameter_Value__c){
                        //   console.log('componentcode')
                        //   console.log(f.Component_Code__c)
                        variable = component.get(f.Component_Code__c); 
                        //   console.log('variable');
                        //   console.log(variable);
                    }
                }); 
                if(parameterURL === undefined){
                    parameterURL = "&" + value.Parameter__c + "=" + variable;
                }else{
                    parameterURL = parameterURL + "&" + value.Parameter__c + "=" + variable;
                } 
            }else{
                if(parameterURL === undefined){
                    parameterURL = "&" + value.Parameter__c + "=" + value.Parameter_Value__c;
                }else{
                    parameterURL = parameterURL + "&" + value.Parameter__c + "=" + value.Parameter_Value__c;
                } 
            }
        });
        //   console.log(parameterURL);
        return parameterURL;
    },
    
    createContractDocumentTemplates: function(component,event,helper,templates){
      //  console.log('PH: createContractDocumentTemplates');   
        var action = component.get('c.insertRecords');
        var cd = component.get('v.contractDocument'); 
        var itemList = [];        
        templates.forEach(function(t){                    
            var field = 'APXTConga4__Name__c';
            var name = t;
            var type = 'Proposal';
            var newItem = helper.createTemplateRecord(component,event,helper,field,name,type);
            itemList.push(newItem);
        })        
        action.setParams({
            "records": itemList
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
            }  
            else if (state === 'ERROR') {
                helper.processErrors(component,response);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    createTemplateRecord : function(component,event,helper,field,name,type){
      //  console.log('CSH: createTemplateRecord');
        var ContractDocumentTemplate = component.get('v.cdtObjInfo'); 
        var cd = component.get('v.contractDocument');
        var congaTemplates = component.get('v.congaTemplates');
        var ct =  _.find(congaTemplates,[String(field),name])
        var newItem = {};
        newItem = JSON.parse(JSON.stringify(ContractDocumentTemplate));
        newItem.Name = ct.APXTConga4__Name__c;
        newItem.Template_Type__c = type;
        newItem.Contract_Document__c = cd.Id;
        if(ct.APXTConga4__Description__c != undefined){
            newItem.Sort_Order__c = Number(ct.APXTConga4__Description__c);
        }
        newItem.Template_Id__c = ct.Id.substr(0,15); 
        return newItem;
    },
    
    processErrors : function(component,response){
        // Retrieve the error message sent by the server
        const errors = response.getError();
        let message = 'Unknown error'; // Default error message
        if (errors && Array.isArray(errors) && errors.length > 0) {
            const error = errors[0];
            if (typeof error.message != 'undefined') {
                message = error.message;
            } else if (typeof error.pageErrors != 'undefined' && Array.isArray(error.pageErrors) && error.pageErrors.length > 0) {
                const pageError = error.pageErrors[0];
                if (typeof pageError.message != 'undefined') {
                    message = pageError.message;
                }
            }
        }
        // Display error in console
        console.error('Error: '+ message);
        console.error(JSON.stringify(errors));
        
        
    },
    
    handleErrors : function(component,errors){
      //  console.log('RSH: handleErrors');
        if (errors) {
            if (errors[0] && errors[0].message) {
                component.find('notifLib').showNotice({
                    "variant": "error",
                    "header": "Something has gone wrong!",
                    "message": "Error message: " + errors[0].message,
                    closeCallback: function() {
                        return;
                    }
                });
            }
        } 
        else {
            component.find('notifLib').showNotice({
                "variant": "error",
                "header": "Something has gone wrong!",
                "message": "Unknown error",
                closeCallback: function() {
                    return;
                }
            });
        }
    }
})