({
    groupQlis : function(component,event,helper) {
        var quote = component.get('v.quote').QuoteLineItems
        console.log('quoteLines')
        console.log(quote)
        var productFamily = this.groupBy(quote,'Product_Family_Display_Name__c');  
       // console.log(Object.getOwnPropertyNames(productFamily))
        component.set('v.quoteLinesGrouped',Object.getOwnPropertyNames(productFamily));
    },
    
    groupBy : function(xs, key) {
        return xs.reduce(function(rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
        }, {});
    },
})