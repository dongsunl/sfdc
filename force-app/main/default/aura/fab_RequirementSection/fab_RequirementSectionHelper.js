({
    upload: function(component, file, fileContents, description,helper) {
        var fromPos = 0;
        var toPos = Math.min(fileContents.length, fromPos + 950000);
        
        // start with the initial chunk
        this.uploadChunk(component, file, fileContents, description, helper, fromPos, toPos, '');   
    },
    
    uploadChunk: function(component, file, fileContents, description,helper, fromPos, toPos, attachId) {
        //  console.log('RSH: upload');
        //console.log(' uploading chunk fromPos ' + fromPos + ' toPos ' + toPos);
        var oppId = component.get('v.opportunity').Id
        var action = component.get('c.saveTheChunk'); 
        //  console.log(file.type)
        //  console.log(encodeURIComponent(fileContents))
        
        //Get the current chunk from the file contents.
        var chunk = fileContents.substring(fromPos, toPos);
        
        action.setParams({          
            "fileName": file.name,
            "base64Data": encodeURIComponent(chunk), 
            "contentType": file.type,
            "oppId" : oppId,
            "description" : description,
            "fileId" : attachId
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();   
            if (state === "SUCCESS") {
                //console.log('successful load of chunk fromPos ' + fromPos + ' toPos ' + toPos);
                //Get the uploaded file id
                var fileId = response.getReturnValue();  
                //Set up the next chunk file position
                fromPos = toPos;
                toPos = Math.min(fileContents.length, fromPos + 950000);
                //console.log('next chunk fromPos ' + fromPos + ' toPos ' + toPos);
                //If there is still another chunk left, upload it
                if (fromPos < toPos) {
                    this.uploadChunk(component, file, fileContents, description, helper, fromPos, toPos, fileId);  
                }
                else{
                    var dataObj= response.getReturnValue();
                    component.set('v.saving',false);                    
                    component.set('v.showSpinner', false);
                    //console.log('should be final hide spinner');
                    console.timeEnd("fileupload");
                    helper.getFiles(component,helper,oppId,description);
                    helper.reloadFab(component,event,helper);
                }
                
            }  
            else if (state === 'ERROR') {
                console.timeEnd("fileupload");
                helper.handleErrors(component,response);
                component.set('v.showSpinner', false);
            }
        }));
        $A.enqueueAction(action); 
        
    },
    
    getFiles : function(component,helper,arecordId,description){
        //  console.log('RSH: getFiles')        
        var action = component.get('c.getContentDocs');   
        var requirementName;
        // console.log('----------------------description')
        // console.log(description)
        if(description === 'Purchase Order'){
            requirementName = 'Upload Purchase Order'
        }else if(description === 'Service Order'){
            requirementName = 'Upload Signed Contract'
        }
        action.setParams({          
            "arecordId": arecordId
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();   
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                component.set('v.files',dataObj);
                component.set('v.showSpinner', false);
                var requirement = component.get('v.requirement');
                // console.log('dataObj');
                //  console.log(dataObj);
                //  console.log('requirementName')
                //  console.log(requirementName)
                //   console.log('requirement')
                //   console.log(requirement)
                if(requirementName===requirement ){
                    var countOfAttachments = 0;
                    if(dataObj.length >0){
                        countOfAttachments = this.countRecords(dataObj,description);  
                    } 
                    //  console.log(countOfAttachments);
                    // console.log(requirementName)
                    helper.attachmentEvent(component,event,countOfAttachments,requirementName);
                }                
            }  
            else if (state === 'ERROR') {
                helper.handleErrors(component,response);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    deletePOAttachment:function(component,event,helper,recordId,oppId,description){
        // console.log('RSH: deletePOAttachment')        
        var action = component.get('c.deletePOAttachment');   
        var countOfAttachments = 0;
        var files = component.get('v.files');
        
        countOfAttachments = this.countRecords(files,description);
        //console.log(countOfAttachments);
        action.setParams({          
            "recordId": recordId,
            "oppId" : oppId,
            "countAtt" : countOfAttachments,
            "description" : description
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();   
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();               
                helper.getFiles(component,helper,oppId,description);
                
            }  
            else if (state === 'ERROR') {
                helper.handleErrors(component,response);
            }
        }));
        $A.enqueueAction(action); 
    },  
    
    countRecords: function(files,description){
        // console.log('RSH: countRecords')        
        var count = 0;
        if(files.length>0){
            files.forEach(function(f){
                if(f.Description === description){
                    count++;
                }
            });
        }
        return count;
    },
    
    attachmentEvent : function(component, event,countOfAttachments,requirementName) { 
        // console.log('RSH: attachmentEvent')        
        var cmpEvent = component.getEvent("attachmentEvent");
        //    console.log(cmpEvent);
        cmpEvent.setParams({ 
            "countOfAttachments" : countOfAttachments,
            "requirement" : requirementName
        }); 
        
        cmpEvent.fire(); 
        
    }, 
    
    urlDirection : function(component,event,helper,url){
        // console.log('RSH: urlDirection')
        var context = component.get("v.UserContext");
        //console.log(context);
        if(context != undefined) {
            if(context == 'Theme4t' || context == 'Theme4d') {
                //   console.log('VF in S1 or LEX');
                sforce.one.navigateToURL(url);
            } else {
                //  console.log('VF in Classic'); 
                window.location.assign(url);
            }
        } else {
            //   console.log('standalone Lightning Component');
            var event = $A.get("e.force:navigateToURL");
            event.setParams({"url": url});
            event.fire();
        }
    },
    
    validateService : function(component,event,helper){
        var service = component.get('v.service');
        //  console.log(service)
    },
    
    handleErrors : function(component,response){
        // Retrieve the error message sent by the server
        const errors = response.getError();
        let message = 'Unknown error'; // Default error message
        if (errors && Array.isArray(errors) && errors.length > 0) {
            const error = errors[0];
            if (typeof error.message != 'undefined') {
                message = error.message;
            } else if (typeof error.pageErrors != 'undefined' && Array.isArray(error.pageErrors) && error.pageErrors.length > 0) {
                const pageError = error.pageErrors[0];
                if (typeof pageError.message != 'undefined') {
                    message = pageError.message;
                }
            }
        }
        // Display error in console
        console.error('Error: '+ message);
        console.error(JSON.stringify(errors));
        
        
    },
    
    getAttachments : function(component,event,helper,cId){
        //  console.log('RSH: getAttachments')
        var action = component.get('c.getFileId');   
        // console.log(cId)
        action.setParams({          
            "cId": cId
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();   
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                component.set('v.customAttachments',dataObj);
                //   console.log('attachments')      
                // console.log(dataObj)            
            }  
            else if (state === 'ERROR') {
                helper.handleErrors(component,response);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    reloadFab :function(component,event,helper){
        //  console.log('RSC: reloadFab')
        var cmpEvent = $A.get("e.c:fab_Reload");
        cmpEvent.fire();
    },
})