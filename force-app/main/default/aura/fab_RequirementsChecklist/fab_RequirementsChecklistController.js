({
    doInit: function (component,event,helper){
       // console.log('RCC: doInit')
        var contractType = component.get('v.contractType');
        var contractMethodType = component.get('v.contractMethodType');
        var requirementType; 
        if(component.get('v.retainer')===true){
            requirementType = 'Retainer'
           
        }
        else if(contractType === 'Service Order'){
            if(contractMethodType !== undefined){
                requirementType = contractMethodType;
            }else{
                requirementType = 'No Contract Method';
            }
        }
            else{
                requirementType = contractType;
            }
        component.set('v.requirementType',requirementType);    
        var service = component.get('v.service');        
        var totalServices = 0;        
        var servicesCompleted = 0;
        if(service != null){
            service.forEach(function(s){
                totalServices += 1;
                if(s.Status__c === 'SOW Signed' || s.Status__c === 'Request Cancelled' || s.Status__c === 'SOW in QSO'){
                    servicesCompleted += 1;
                }
            })
        }    
        component.set('v.totalServices',totalServices);
        component.set('v.servicesCompleted',servicesCompleted);
        helper.setRequirementsList(component,event,helper,requirementType);        
    },
    
    handleShowActiveSectionName: function (component, event, helper) {
     //   console.log('RCC: handleShowActiveSectionName')
        alert(component.find("accordion").get('v.activeSectionName'));
    },
    
    handleSetActiveSectionC: function (component) {
      //  console.log('RCC: handleSetActiveSectionC')
        component.find("accordion").set('v.activeSectionName', 'C');
    },
    
    updateRequirement:function(component,event,helper){
        var countOfAttachments = event.getParam("countOfAttachments"); 
        var requirement = event.getParam("requirement");
        var requirements = component.get('v.requirements');
        requirements.forEach(function(r){
            if(r.Requirement === requirement){
                if(countOfAttachments >0){
                    r.Completed = true
                }else{
                    r.Completed = false
                }
            }
            component.set('v.requirements',requirements);
        })
        
    },
    
 /*   updateSubmitForInvoice : function(component,event,helper){
        helper.setReadyToSubmit(component,event,helper,'Paper Service Order');
    }*/
    
    
    
})