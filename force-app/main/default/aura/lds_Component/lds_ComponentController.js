({
    afterScriptsLoaded : function (cmp, event, helper) {
        console.log('**afterScript Loading');
        var compEvent = cmp.getEvent("loading");
        compEvent.fire();
        
        helper.callServer(
            cmp,
            "c.getXmQuoting",
            function(response){
                console.log('**getXmQuoting get response' , response)

                cmp.set('v.quotingPermissions', response);
                //console.log('CMP CTRL: afterScriptsLoaded - CALL HLPR setAvailableExperiences');
                helper.setAvailableExperiences(cmp, event, helper, response);
            },{
                
            },true
        );
        console.log('***getXmQuoting process done')
    }
})