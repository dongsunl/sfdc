({
    getCompletionCriteriaRecords : function(component, fieldValue) {
        component.set("v.isProcessing", true);
        component.set("v.displayedStageName", fieldValue);
		var action = component.get("c.getCompletionCriteriaRecords");
        action.setParams({
            scope: component.get("v.scope"),
            recordId: component.get("v.recordId"),
            fieldName: "Guided_Sales_Path__c",
            fieldValue: fieldValue
        });
        action.setCallback(this, function(response) {
			var state = response.getState();
            if (state === "SUCCESS") {
				component.set("v.completionCriteriaRecords", response.getReturnValue());
		        component.set("v.isProcessing", false);
            } else if (state === "INCOMPLETE") {
                console.log("Not done yet...");
            } else if (state === "ERROR") {
                console.log(response.getError());
            }
        });
        $A.enqueueAction(action);
	},
    setFieldValue: function(component, event, helper) {
        var selectedCompletionCriteria = event.getParam("selectedCompletionCriteria");
        var fieldName = selectedCompletionCriteria.resultsFieldName;
        var fieldValue = event.getParam("selectedValue");
        var action = component.get("c.updateFieldValue");
        action.setParams({
            recordId: component.get("v.record.Id"),
            fieldName: fieldName,
            fieldValue: fieldValue
        });
        component.set("v.isProcessing", true);
        action.setCallback(this, function(response) {
			var state = response.getState();
            if (state === "SUCCESS") {
                var currentDisplayedStageName = component.get("v.displayedStageName");
                helper.getCompletionCriteriaRecords(component, currentDisplayedStageName);
                helper.getGlobalDynamicActions(component, currentDisplayedStageName);
                helper.getStageProgressRecords(component);
            } else if (state === "INCOMPLETE") {
                console.log("Not saved yet...");
            } else if (state === "ERROR") {
                console.log(response.getError());
            }
        });
        $A.enqueueAction(action);
    },
	getGlobalDynamicActions : function(component, fieldValue) {
		var action = component.get("c.getGlobalDynamicActions");
        action.setParams({
            scope: component.get("v.scope"),
            recordId: component.get("v.recordId"),
            fieldName: "Guided_Sales_Path__c",
            fieldValue: fieldValue
        });
        action.setCallback(this, function(response) {
			var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.globalDynamicActions", response.getReturnValue());
            } else if (state === "ERROR") {
                console.log(response.getError());
            }
        });
        $A.enqueueAction(action);
	},
	getStageProgressRecords : function(component) {
		var action = component.get("c.getStageProgress");
        action.setParams({
            scope: component.get("v.scope"),
            recordId: component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
			var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.stageProgressRecords", response.getReturnValue());
		        component.set("v.isProcessing", false);
            } else if (state === "ERROR") {
                console.log(response.getError());
            }
        });
        $A.enqueueAction(action);
	},
    displayModal: function(component, event) {
        var dynamicAction = event.getParam("dynamicAction");
        var attributes = {};
        if (dynamicAction.recordIdOutputParameterName != undefined) {
            attributes[dynamicAction.recordIdOutputParameterName] = component.get("v.recordId")
        }
        if (dynamicAction.userIdOutputParameterName != undefined) {
            attributes[dynamicAction.userIdOutputParameterName] = $A.get("$SObjectType.CurrentUser.Id")
        }
        $A.createComponent(
            dynamicAction.goToDestination,
            attributes,
            function(newComponent, status, errorMessage) {
                if (status === 'SUCCESS') {
                    var body = component.get("v.body");
                    body = [];
                    body.push(newComponent);
                    component.set("v.body", body);
                }
            }
        );
		component.set("v.modalTitle", dynamicAction.modalTitle);
        component.set("v.displayModal", true);
	}
})