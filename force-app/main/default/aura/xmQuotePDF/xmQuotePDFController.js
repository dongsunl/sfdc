({
	doInit : function(component, event, helper) {
        var queryCall = component.get("c.getQuotingRequirements");
        queryCall.setParams({
            "id": component.get("v.recordId")
        })
        queryCall.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				var quotingRequirements = response.getReturnValue();
                console.log(quotingRequirements);
                if (quotingRequirements.length === 0)  {
                    helper.redirectUser(component);
                } else if(quotingRequirements[0].includes("Service")) {
                    component.set("v.requiresOrPending", true);
                }
                component.set("v.doneProcessing", true);
            } else if (state === "ERROR") {
                console.log("Error getting quoting requirements");
            } else {
                console.log("Unknown problem getting quoting requirements");
            }
        });
        $A.enqueueAction(queryCall);
	},
    generateQuotePDF : function(component, event, helper) {
        helper.redirectUser(component);
    }
})