@istest
public class AccountStatusUpdateTest{
public static testmethod void AccountStatusUpdateTest(){
    account a = new account(name='test', Account_Status__c='Not Contacted');
    a.BillingState = 'Utah';
    a.BillingCountry = 'USA';
    a.BillingPostalCode = '84604';
    insert a; 
    task t = new task(WhatId=a.id, Status='new', Priority='low');
    insert t;
    account a2 = [select Account_Status__c from account where id = :a.id];
    system.assertequals(a2.Account_Status__c, 'No Open Opportunity');
/*
    account a1 = new account(name='test', Account_Status__c='Active');
    a1.BillingState = 'Utah';
    a1.BillingCountry = 'USA';
    a1.BillingPostalCode = '84604';
    insert a1; 
    task t1 = new task(WhatId=a1.id, Status='new', Priority='low');
    insert t1;
    account a3 = [select Account_Status__c from account where id = :a1.id];
    system.assertequals(a3.Account_Status__c, 'Active'); */
}
}