@istest
public class AccountStatusUpdateTest2{
public static testmethod void AccountStatusUpdateTest2(){
    account a = new account(name='test', Account_Status__c='Not Contacted');
    a.BillingState = 'Utah';
    a.BillingCountry = 'USA';
    a.BillingPostalCode = '84602';
    insert a; 
    event e = new event(WhatId=a.id, ActivityDateTime=datetime.now(), DurationInMinutes=60);
    insert e;
    account a2 = [select Account_Status__c from account where id = :a.id];
    system.assertequals(a2.Account_Status__c, 'No Open Opportunity');
/*
    account a1 = new account(name='test', Account_Status__c='Active');
    a1.BillingState = 'Utah';
    a1.BillingCountry = 'USA';
    a1.BillingPostalCode = '84602';
    insert a1; 
    event e1 = new event(WhatId=a1.id, ActivityDateTime=datetime.now(), DurationInMinutes=60);
    insert e1;
    account a3 = [select Account_Status__c from account where id = :a1.id];
    system.assertequals(a3.Account_Status__c, 'Active'); */
}
}