public with sharing class Account_AddUserHandler {
	
	public Account_AddUserHandler(){}
	public static final String STR_PROFILETEAMLEAD = Label.Account_AddUser_360Team; 				//'Q-Sales 360 Team Lead w/ LinkedIn' ;
	public static final String STR_PROFILE360 = Label.Account_AddUser_Sales360; 					//'Q-Sales 360';
	public static final String STR_PROFILE_PANEL = Label.Account_AddUser_Panels;					//'Q-Panels';
	public static final String STR_PROFILE_OPTDEV = Label.Account_AddUser_OptDev;					//'Q-OptDev';
	public static final String STR_PROFILE_OPTDEVLINKED = Label.Account_AddUser_OptDevLinkedIn;		//'Q-OptDev - LinkedIn';
	public static final String STR_PROFILE_SALES = Label.Account_AddUser_SalesRS;					//'Q-Sales RS';
	public static final String STR_PROFILE_SALESLINKED = Label.Account_AddUser_SalesRSLinkedIn;		//'Q-Sales RS - LinkedIn';
	public static final String STR_SALES_OPS = Label.Account_AddUser_SalesOps;						//'Sales Ops';
	public static final String STR_PROFILE_CLIENTSUCCESS = Label.Account_AddUser_ClientSuccessRep;	//'Client Success Rep';
	private static boolean flagvalue = false;
	
	public static boolean hasAlreadyfired() {
         return flagvalue;
    }

    public static void setAlreadyfired(){
        flagvalue = true;
    }
	
	public void onInsertAccount(List<Account> lstAccount){
		
		Map<Id,Id> userMap = new Map<Id,Id>();
		String strUserProfile;
		
		for(Account objAcc : lstAccount){
			userMap.put(objAcc.Id,objAcc.OwnerId);
		}
		
		User objUser =[Select Profile.Name,
							  ProfileId 
						From User
						Where Id IN: userMap.values()];

		strUserProfile = objUser.Profile.Name;
		User objUserSales = [Select Name,
									Profile.Name,
							  		ProfileId 
							From User 
							Where Name =: STR_SALES_OPS];
		
		if(objUserSales != Null){
			for(Account objAcc : lstAccount){
				System.debug('============in insert=========='+objAcc);
				if(strUserProfile.equalsIgnoreCase(STR_PROFILETEAMLEAD) || strUserProfile.equalsIgnoreCase(STR_PROFILE360)){
					
					objAcc.X360_EE_Rep__c = objUser.Id; 
					objAcc.Panels_Rep__c = objUserSales.Id;
					objAcc.Site_Intercept_Rep__c = objUserSales.Id;
					objAcc.Target_Audience_Rep__c = objUserSales.Id;
					objAcc.OpDev_Rep__c = objUserSales.Id;
					objAcc.OwnerId = objUserSales.Id;
					objAcc.Client_Services_Rep__c = objUserSales.Id;
					objAcc.Professional_Services_Rep__c = objUserSales.Id;
				}
				if(strUserProfile.equalsIgnoreCase(STR_PROFILE_PANEL)){
					
					objAcc.Panels_Rep__c = objUser.Id; 
					objAcc.X360_EE_Rep__c = objUserSales.Id; 
					objAcc.Site_Intercept_Rep__c = objUserSales.Id;
					objAcc.Target_Audience_Rep__c = objUserSales.Id;
					objAcc.OpDev_Rep__c = objUserSales.Id;
					objAcc.OwnerId = objUserSales.Id;
					objAcc.Client_Services_Rep__c = objUserSales.Id;
					objAcc.Professional_Services_Rep__c = objUserSales.Id;
				}
				if(strUserProfile.equalsIgnoreCase(STR_PROFILE_OPTDEV) || strUserProfile.equalsIgnoreCase(STR_PROFILE_OPTDEVLINKED)){
					
					objAcc.OpDev_Rep__c = objUser.Id; 
					objAcc.Panels_Rep__c = objUserSales.Id; 
					objAcc.X360_EE_Rep__c = objUserSales.Id; 
					objAcc.Site_Intercept_Rep__c = objUserSales.Id;
					objAcc.Target_Audience_Rep__c = objUserSales.Id;
					objAcc.OwnerId = objUserSales.Id;
					objAcc.Client_Services_Rep__c = objUserSales.Id;
					objAcc.Professional_Services_Rep__c = objUserSales.Id;
				}
				if(strUserProfile.equalsIgnoreCase(STR_PROFILE_SALES) || strUserProfile.equalsIgnoreCase(STR_PROFILE_SALESLINKED)){
					objAcc.Site_Intercept_Rep__c = objUser.Id; 
					objAcc.OwnerId = objUser.Id; 
					objAcc.Target_Audience_Rep__c = objUser.Id; 
				}
				if(strUserProfile.equalsIgnoreCase(STR_PROFILE_CLIENTSUCCESS)){
					
					objAcc.Client_Services_Rep__c = objUser.Id;
					objAcc.OpDev_Rep__c = objUserSales.Id; 
					objAcc.Panels_Rep__c = objUserSales.Id; 
					objAcc.X360_EE_Rep__c = objUserSales.Id; 
					objAcc.Site_Intercept_Rep__c = objUserSales.Id;
					objAcc.Target_Audience_Rep__c = objUserSales.Id;
					objAcc.OwnerId = objUserSales.Id;
					objAcc.Professional_Services_Rep__c = objUserSales.Id;
				}
			}//for
		}//if
	}
}