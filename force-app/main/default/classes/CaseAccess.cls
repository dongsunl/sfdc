public class CaseAccess {
    
    // Public static variable used to protect against recursion.
    public static Boolean preparationForAssignmentRulesExecuted = false;
    public static Boolean assignmentRulesExecuted = false;

    /**
     * Sets the proper field value(s) to ensure assignment rules
     * execute on the list of case ids provided.
     */
    @InvocableMethod(label='Execute Assignment Rules' description='Executes case assignment rules for the case id(s) provided.')
    public static void prepareToExecuteAssignmentRules(List<Id> caseIds) {
        
        // Exit immediately if this process has already executed during this invocation.
        if (preparationForAssignmentRulesExecuted) {
            return;
        } else {
            preparationForAssignmentRulesExecuted = true;
        }

        // Throw an exception if the current user does not have proper permissions to update a case.
        if (!Schema.SObjectType.Case.isUpdateable()) {
            throw new InvalidPermissionsException(InvalidPermissionsException.CASE_UPDATE_MESSAGE);
        }
        
        // Add all records to a list to be updated in bulk.
        List<Case> records = new List<Case>();
        Case record;
        for (Id caseId: caseIds) {
            record = new Case(Id=caseId);
            record.Execute_Assignment_Rules__c = true;
            records.add(record);
        }
        system.debug('records');
        system.debug(records);
		List<Database.SaveResult> results = Database.update(records, false);
    }
    
    /**
     * Executes the assignment rules on any case included
     * in the list of case ids provided.
     * 
     * Note: The assignment rules are only executed if the
     * proper field value is set to state the assignment
     * rules should execute.
     */
    public static void executeAssignmentRules(List<Id> caseIds) {
        
        // Exit immediately if this process has already executed during this invocation.
        /*if (assignmentRulesExecuted) {
            return;
        } else {
            assignmentRulesExecuted = true;
        }*/

        // Throw an exception if the current user does not have proper permissions to update a case.
        if (!Schema.SObjectType.Case.isUpdateable()) {
            throw new InvalidPermissionsException(InvalidPermissionsException.CASE_UPDATE_MESSAGE);
        }
        
        // Exit immediately if an active assignment rule is not found.
        AssignmentRule assignmentRule = getActiveAssignmentRule();
        if (assignmentRule == null) {
            return;
        }
        
        // Add all records to a list to be updated in bulk.
        List<Case> records =
            [SELECT Id, Execute_Assignment_Rules__c
             FROM Case
             WHERE Id IN : caseIds
             AND Execute_Assignment_Rules__c = true
             LIMIT 200];
        system.debug('records2');
        system.debug(records);
        for (Case record : records) {
            record.Execute_Assignment_Rules__c = false;
        }
        Database.DMLOptions dmlOptions = new Database.DMLOptions();
        dmlOptions.AssignmentRuleHeader.assignmentRuleId = assignmentRule.Id;
		List<Database.SaveResult> results = Database.update(records, dmlOptions);
    }
    
    private static AssignmentRule getActiveAssignmentRule() {
        
        // Throw an exception if the current user does not have the proper permissions
        if (!Schema.SObjectType.AssignmentRule.isQueryable()
	            || !Schema.SObjectType.AssignmentRule.fields.Id.isAccessible()
	            || !Schema.SObjectType.AssignmentRule.fields.SObjectType.isAccessible()
	            || !Schema.SObjectType.AssignmentRule.fields.Active.isAccessible()) {
            throw new InvalidPermissionsException(InvalidPermissionsException.RECORD_TYPE_ACCESS_MESSAGE);
        }
        
        // Query for a single active assignment rule related to cases.
        List<AssignmentRule> assignmentRules =
            [SELECT Id
             FROM AssignmentRule
             WHERE SObjectType = 'Case'
             AND Active = true
             LIMIT 1];
        if (assignmentRules.size() == 1) {
            return assignmentRules[0];
        }
        return null;
    }
}