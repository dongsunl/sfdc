@isTest
public with sharing class ClientAddressUpdateTest {

	static Account acct {get;set;}
    static Opportunity opp {get;set;}
    static Client__c clientObj {get;set;}
    static Contract_Document__c contractDocObj {get;set;}

    @testsetup 
    static void test_setup(){
    	TestFactory.disableTriggers();
    	acct = TestFactory.createAccount();
    	insert acct;
    	opp = TestFactory.createOpportunity(acct.Id, null);
    	insert opp;
    	clientObj = TestFactory.createClient(acct.Id);
    	insert clientObj;
		contractDocObj = TestFactory.createContractDoc(clientObj.Id);
		insert contractDocObj;
    }

    static void QueryStartingItems(){
        acct = database.Query('select '+BuildQuery('Account')+' where name = \'test Account\'');
        opp = database.Query('select '+BuildQuery('Opportunity'));
        clientObj = database.Query('select '+BuildQuery('Client__c'));
        Id clientId = clientObj.Id;
		contractDocObj = database.Query('select '+BuildQuery('Contract_Document__c')+' where Client__c = :clientId');
    }

    static string BuildQuery(String obj){
        Map<String, Schema.SObjectField> SobjtField = Schema.getGlobalDescribe().get(obj).getDescribe().fields.getMap();
        string qry = '';
        for (Schema.SObjectField s : SobjtField.values()) {
            qry += s + ',';
        }
        qry = qry.removeend(',');
        qry = qry+' from '+ obj;
        return qry;
    }

	@isTest 
	static void Test_updateClientRecords(){
		QueryStartingItems();
		Map<Id,Contract_Document__c> contractDocMap = new Map<Id,Contract_Document__c>();
		Map<Id,Client__c> clientMap = new Map<Id,Client__c>();
		contractDocObj.Billing_City__c = 'myCity';
		contractDocObj.Billing_Country__c = 'USA';
		contractDocObj.Billing_State__c = 'NY';
		contractDocObj.Billing_Street__c = '123 MyStreet';
		contractDocObj.Billing_Zip_Postal__c = '00001';
		update contractDocObj;
		contractDocMap.put(contractDocObj.Id, contractDocObj);
		clientMap.put(clientObj.Id, clientObj);
		Test.startTest();
			ClientAddressUpdate.updateClientRecords(contractDocMap, clientMap);
		Test.stopTest();
		clientObj = database.Query('select '+BuildQuery('Client__c'));
		system.assertEquals('myCity', clientObj.Billing_City__c);
		system.assertEquals('USA', clientObj.Billing_Country__c);
		system.assertEquals('NY', clientObj.Billing_State_Province__c);
		system.assertEquals('123 MyStreet', clientObj.Billing_Street_Address__c);
		system.assertEquals('00001', clientObj.Billing_Zip_Postal_Code__c);
	}

	@isTest 
	static void Test_queryClientsRelatedToContractDocuments(){
		QueryStartingItems();
		Map<Id,Contract_Document__c> contractDocMap = new Map<Id,Contract_Document__c>();
		Map<Id,Client__c> clientMap = new Map<Id,Client__c>();
		contractDocMap.put(contractDocObj.Id, contractDocObj);
		Test.startTest();
			clientMap = ClientAddressUpdate.queryClientsRelatedToContractDocuments(contractDocMap);
		Test.stopTest();
		system.assertEquals(1,clientMap.size());
	}

	@isTest 
	static void Test_findWorkingmapMapFound(){
		QueryStartingItems();
		Map<Id,Contract_Document__c> contractDocNewMap = new Map<Id,Contract_Document__c>();
		Map<Id,Contract_Document__c> contractDocOldMap = new Map<Id,Contract_Document__c>();
		Map<Id,Contract_Document__c> workingMap = new Map<Id,Contract_Document__c>();
		Map<Id,Client__c> clientMap = new Map<Id,Client__c>();
		Contract_Document__c contractDocObjOld = contractDocObj.clone(true,true,true,true);
		contractDocObj.Billing_City__c = 'myCity';
		contractDocObj.Billing_Country__c = 'USA';
		contractDocObj.Billing_State__c = 'NY';
		contractDocObj.Billing_Street__c = '123 MyStreet';
		contractDocObj.Billing_Zip_Postal__c = '00001';
		update contractDocObj;
		contractDocNewMap.put(contractDocObj.Id, contractDocObj);
		contractDocOldMap.put(contractDocObjOld.Id, contractDocObjOld);
		clientMap.put(clientObj.Id, clientObj);
		Test.startTest();
			workingMap = ClientAddressUpdate.findWorkingmap(contractDocNewMap, contractDocOldMap);
		Test.stopTest();
		system.assertEquals(1, workingMap.size());
	}

	@isTest 
	static void Test_findWorkingmapNoMap(){
		QueryStartingItems();
		Map<Id,Contract_Document__c> contractDocNewMap = new Map<Id,Contract_Document__c>();
		Map<Id,Contract_Document__c> contractDocOldMap = new Map<Id,Contract_Document__c>();
		Map<Id,Contract_Document__c> workingMap = new Map<Id,Contract_Document__c>();
		Map<Id,Client__c> clientMap = new Map<Id,Client__c>();
		Contract_Document__c contractDocObjOld = contractDocObj.clone(true,true,true,true);
		contractDocNewMap.put(contractDocObj.Id, contractDocObj);
		contractDocOldMap.put(contractDocObjOld.Id, contractDocObjOld);
		clientMap.put(clientObj.Id, clientObj);
		Test.startTest();
			workingMap = ClientAddressUpdate.findWorkingmap(contractDocNewMap, contractDocOldMap);
		Test.stopTest();
	//system.assertEquals(0, workingMap.size());
	}

	@isTest 
	static void Test_startAddressUpdateProcess(){
		QueryStartingItems();
		Map<Id,Contract_Document__c> contractDocNewMap = new Map<Id,Contract_Document__c>();
		Map<Id,Contract_Document__c> contractDocOldMap = new Map<Id,Contract_Document__c>();
		Contract_Document__c contractDocObjOld = contractDocObj.clone(true,true,true,true);
		contractDocObj.Billing_City__c = 'myCity';
		contractDocObj.Billing_Country__c = 'USA';
		contractDocObj.Billing_State__c = 'NY';
		contractDocObj.Billing_Street__c = '123 MyStreet';
		contractDocObj.Billing_Zip_Postal__c = '00001';
		update contractDocObj;
		contractDocNewMap.put(contractDocObj.Id, contractDocObj);
		contractDocOldMap.put(contractDocObjOld.Id, contractDocObjOld);
		Test.startTest();
			ClientAddressUpdate.startAddressUpdateProcess(contractDocNewMap, contractDocOldMap);
		Test.stopTest();
		clientObj = database.Query('select '+BuildQuery('Client__c'));
		system.assertEquals('myCity', clientObj.Billing_City__c);
		system.assertEquals('USA', clientObj.Billing_Country__c);
		system.assertEquals('NY', clientObj.Billing_State_Province__c);
		system.assertEquals('123 MyStreet', clientObj.Billing_Street_Address__c);
		system.assertEquals('00001', clientObj.Billing_Zip_Postal_Code__c);
	}

	@isTest 
	static void Test_startUpdateShippingFromBilling(){
		QueryStartingItems();
		clientObj.Billing_City__c = 'Provo';
		clientObj.Billing_Country__c = 'USA';
		clientObj.BillingCountry2__c = 'United States';
		clientObj.Billing_State_Province__c = 'UT';
		clientObj.Billing_Street_Address__c = '333 W River Park Dr';
		clientObj.Billing_Zip_Postal_Code__c = '84604';
		Test.startTest();
			ClientAddressUpdate.startUpdateShippingFromBilling(new List<Client__c>{clientObj});
		Test.stopTest();
		system.assertEquals(clientObj.Shipping_City__c,clientObj.Billing_City__c);
		system.assertEquals(clientObj.Shipping_Country__c,clientObj.Billing_Country__c);
		system.assertEquals(clientObj.ShippingCountry2__c,clientObj.BillingCountry2__c);
		system.assertEquals(clientObj.Shipping_State__c,clientObj.Billing_State_Province__c);
		system.assertEquals(clientObj.Shipping_Street__c,clientObj.Billing_Street_Address__c);
		system.assertEquals(clientObj.Shipping_Zip_Postal_Code__c,clientObj.Billing_Zip_Postal_Code__c);
	}
}