@isTest
public class ClientLicensesTest {
    static Account a;
    static Bundle__c b;
    static Client__c c;
    static Client_License__c cl1;
    static Client_License__c cl2;
    private static void dataSetup() {
        a = new Account(
            Name = 'Test Account'
        );
        insert a;
        b = new Bundle__c(
            Name = 'Test Bundle',
            Experience__c = 'CX',
            Bundle__c = '1'
        );
        insert b;
        c = new Client__c(
            Name = 'Test Client',
            Account__c = a.Id
        );
        insert c;
        cl1 = new Client_License__c(
            Name = 'Test Client License 1',
            Client__c = c.Id,
            License_Start_Date__c = system.today(),
            License_End_Date__c = system.today() + 364,
            Status__c = 'Active',
            CurrencyIsoCode = 'USD',
            Original_Amount__c = 10000,
            Invoice_Amount__c = 10000,
            Quantitiy__c = 250,
            Bundle__c = b.Id
        );
        insert cl1;
        cl2 = new Client_License__c(
            Name = 'Test Client License 2',
            Client__c = c.Id,
            License_Start_Date__c = system.today(),
            License_End_Date__c = system.today() + 364,
            Status__c = 'Active',
            CurrencyIsoCode = 'USD',
            Original_Amount__c = 2500,
            Invoice_Amount__c = 2000,
            Quantitiy__c = 10,
            Bundle__c = b.Id
        );
        insert cl2;
    }
    @isTest static void retrieveLicenseGroupings() {
        dataSetup();
        Test.startTest();
        List<String> groupings = ClientLicenses.getLicenseGroupings(c.Id);
        Test.stopTest();
    }
    @isTest static void retrieveClientLicenses() {
        dataSetup();
        Test.startTest();
        List<Client_License__c> licenses = ClientLicenses.getClientLicenses(c.Id);
        Test.stopTest();
    }
    @isTest static void retrieveAccess() {
        User objUser = [SELECT ID
                        FROM User
                        WHERE Profile.Name = 'System Administrator'
                        AND isActive = true LIMIT 1];
        
        System.runAs(objUser) {
            Test.startTest();
            List<Boolean> access = ClientLicenses.getAccess();
            Test.stopTest();
        }
    }
    @isTest static void deleteLicense() {
        dataSetup();
        Test.startTest();
        ClientLicenses.deleteRecord(cl2.Id);
        Test.stopTest();
    }
}