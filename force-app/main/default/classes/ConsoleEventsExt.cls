public with sharing class ConsoleEventsExt {

	public Id i {get;set;}
	public integer panel {get;set;}
	public String s {get;set;}
	public event e {get;set;}


	public ConsoleEventsExt() {
		i = ApexPages.CurrentPage().getParameters().get('id');
		panel = 1;
	}

	public PageReference CreateEvent(){
		if(s == null){
			return null;
		}
		if(i == null){
			return null;
		}
		e = new event(subject=s,type=s,WhoId=i,OwnerId=userinfo.getUserId());
		panel = 2;
		return null;
	}

	public void MyCancelMethod(){
		e = null;
		s = null;
		panel = 1;
	}

	public void MySaveMethod(){
		Try{
			system.debug(logginglevel.info, '*****e='+e);
			insert e;
			e = null;
			s = null;
			panel = 1;
		}
		Catch(exception e1){
			system.debug(logginglevel.info, '*****'+e1);
		}
	}

}