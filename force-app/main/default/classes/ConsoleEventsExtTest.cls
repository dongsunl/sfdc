@isTest
private class ConsoleEventsExtTest {
	
	@isTest static void test_method_one() {
		Contact c = new Contact(firstname='Fname',lastname='Lname',email='test@test.com');
		insert c;
		PageReference Page = new PageReference('/apex/ConsoleEvents?id='+c.id);
		test.setcurrentpage(Page);
		ConsoleEventsExt cee = new ConsoleEventsExt();
		cee.s = 'test';
		cee.CreateEvent();
		cee.e.durationinminutes = 30;
		cee.MySaveMethod();
		cee.MyCancelMethod();
	}
	
}