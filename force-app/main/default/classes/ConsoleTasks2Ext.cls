public with sharing class ConsoleTasks2Ext {
	public Contact c {get;set;}
	public Id i {get;set;}
	public Task t {get;set;}
	public String s {get;set;}
	public integer panel {get;set;}


	public ConsoleTasks2Ext() {
		i = ApexPages.CurrentPage().getParameters().get('id');
		panel = 1;
	}

	public PageReference CreateTask(){
		system.debug(logginglevel.info, '*****s='+s);
		system.debug(logginglevel.info, '*****i='+i);
		if(s == null){
			return null;
		}
		if(i == null){
			return null;
		}
		t = new task(subject=s,type=s,WhoId=i,OwnerId=userinfo.getUserId());
		system.debug(logginglevel.info, '*****t='+t);
		system.debug(logginglevel.info, '*****panel='+panel);
		panel = 2;
		system.debug(logginglevel.info, '*****panel='+panel);
		return null;
	}

	public void MyCancelMethod(){
		t = null;
		s = null;
		panel = 1;
	}

	public void MySaveMethod(){
		Try{
			insert t;
			t = null;
			s = null;
			panel = 1;
		}
		Catch(exception e){
			system.debug(logginglevel.info, '*****'+e);
		}
	}
}