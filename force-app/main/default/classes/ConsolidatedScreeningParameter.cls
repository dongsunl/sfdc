public class ConsolidatedScreeningParameter {
    
    private ConsolidatedScreeningManager manager;
    private ConsolidatedScreeningRequest request;
   	private Client__c client;
    private Account account;
    
    private String CLIENT_QUERY = 'SELECT Id, Name FROM Client__c WHERE Id =: searchClient';
    private String ACCOUNT_QUERY = 'SELECT Id, Name FROM Account WHERE Id =: searchAccount';
    
    public ConsolidatedScreeningParameter(ConsolidatedScreeningRequest request,ConsolidatedScreeningManager manager){
        this.manager = manager;
        this.request = request;
    }
    
    //Get Account and Client names and package them into a list of strings to be passed into the request
    public List<String> createSearchStringList(Id searchClient, Id searchAccount){
        List<String> searchStrings = new List<String>();
        if(manager.validationCheck(searchClient)){
            client = database.query(CLIENT_QUERY);
            searchStrings.add(encodeString(client.Name));
        }
        if(manager.validationCheck(searchAccount)){
            account = database.query(ACCOUNT_QUERY);
            searchStrings.add(encodeString(account.Name));
        }
        return searchStrings;
    }
    
    public List<String> encodeSearchStringList(List<String> searchStrings){
        List<String> encodedSearchStrings = new List<String>();
        for(String searchString : searchStrings){
            encodedSearchStrings.add(encodeString(searchString));
        }
        return encodedSearchStrings;
    }
    
    //Encode the search strings for use as parameters in the API call 
    private String encodeString(String s){
        String searchString = s.replaceAll('\\s+','');
        searchString = EncodingUtil.urlEncode(searchString, 'UTF-8');
        system.debug(searchString);
        return searchString;
    }
    
    public Client__c getClient(){
        return client;
    }
	
    public Account getAccount(){
        return account;
    }
}