public with sharing class ContactTriggerHandler {
    
    public static boolean firstRunOnBeforeInsert = true;
    public static boolean firstRunOnAfterInsert = true;
    public static boolean firstRunOnBeforeUpdate = true;
    public static boolean firstRunOnAfterUpdate = true;
    public static boolean firstRunOnBeforeDelete = true;
    public static boolean firstRunOnAfterDelete = true;
    public static boolean firstRunOnUndelete = true;
    
    
    public void OnBeforeInsert(Contact[] newObjects){
        if(firstRunOnBeforeInsert){
            firstRunOnBeforeInsert = false;
        }
    }
    
    public void OnAfterInsert(Contact[] newObjects, map<id,Contact> MapNewMap){
        if(firstRunOnAfterInsert){
            firstRunOnAfterInsert = false;
            CountContacts.afterInsertTriggerUpdates(newObjects, null);
        }
    }
    
    public void OnBeforeUpdate(Contact[] oldObjects, Contact[] updatedObjects, map<id,Contact> MapNewMap, map<id,Contact> MapOldMap){
        if(firstRunOnBeforeUpdate){
            firstRunOnBeforeUpdate = false;
            CountContacts.beforeUpdateTriggerUpdates(updatedObjects, MapOldMap);
        }
    }
    
    public void OnAfterUpdate(Contact[] oldObjects, Contact[] updatedObjects, map<id,Contact> MapNewMap, map<id,Contact> MapOldMap){
        if(firstRunOnAfterUpdate){
            firstRunOnAfterUpdate = false;
            //Query accounts to be used in all of the future methods.
            Map<Id,Contact> oppMap = new Map<Id,Contact>([
                SELECT Id 
                FROM Contact
                WHERE Id in :MapNewMap.keyset()
            ]);
        }
    }
    
    public void OnBeforeDelete(Contact[] ObjectsToDelete, map<id,Contact> MapNewMap, map<id,Contact> MapOldMap){
        if(firstRunOnBeforeDelete){
            firstRunOnBeforeDelete = false;
            
        }
    }
    
    public void OnAfterDelete(Contact[] deletedObjects, map<id,Contact> MapOldMap){
        if(firstRunOnAfterDelete){
            firstRunOnAfterDelete = false;
            CountContacts.afterDeleteTriggerUpdates(deletedObjects, MapOldMap);
        }
    }
    
    public void OnUndelete(Contact[] restoredObjects){
        if(firstRunOnUndelete){
            firstRunOnUndelete = false;
            
        }
    }
    
}