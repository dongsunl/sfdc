@isTest
public class ContactTriggerHandlerTest {
    public static testmethod void ContactTriggerHandlerTest(){
        User admin = [SELECT Id from User where FirstName = 'Admin'];
        Account a = new account(name='test');
        a.Name = 'TestAccount';
        a.SDR_RS__c = admin.Id;
        a.Number_of_Contacts__c = 0;
        Id newAccountId = a.Id;
        insert a;
        Contact c = new contact(
            FirstName ='fname1o2984u5h',
            LastName ='lname',
            Account = a,
            SDR_RC_CX_Rep__c = admin.Id
        );
        
        insert c;
        Contact cont = [SELECT Id, FirstName from Contact where Id = :c.Id];
        cont.FirstName = 'fname1o2984u5h2';
        update cont; 
       
        delete cont;
    }
}