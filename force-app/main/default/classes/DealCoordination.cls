global with sharing class DealCoordination {

    public Quote quoteObj {get;set;}
    public List<Deal_Coordination__mdt> metaData {get;set;}
    public Map<String,String> dateRequestedMap {get;set;} //This will be for the Date Requested Lookup.  It will be the field name as the key and the value from the Quote as the Value
    public Map<String,String> statusMap {get;set;} //This will be the field name as the key and the value from the Quote as the Value.
    public String actionToTake {get;set;}
    public User userObj {get;set;}
    
    public DealCoordination(ApexPages.StandardController stdController) {
        if(ApexPages.CurrentPage().getParameters().get('Id') == null){
            return;
        }
        metaData = [SELECT DeveloperName,Javascript_Button__c,MasterLabel,Requested_Date_Lookup__c,SLA__c,Status_Lookup__c,Request_Button_Name__c,Display_Order__c,Completed_Status__c
                    FROM Deal_Coordination__mdt
                    WHERE Availability__c = 'Available' OR Availability__c = 'Required' ORDER BY Display_Order__c];
        quoteObj = queryQuote();
        userObj = queryUser();
        populateMaps();
    }

    @testVisible
    private User queryUser(){
        Map<String, Schema.SObjectField> SobjtField = Schema.getGlobalDescribe().get('User').getDescribe().fields.getMap();
        string qry = 'Profile.Name,';
        for (Schema.SObjectField s : SobjtField.values()) {
            qry += s + ',';
        }
        qry = qry.removeend(',');
        Id userId = UserInfo.getUserId();
        qry = 'SELECT '+qry+' FROM '+ 'User' + ' WHERE Id = :userId';
        return Database.query(qry);
    }

    @testVisible
    private void populateMaps(){
        dateRequestedMap = new Map<String,String>{null => '-'};
        statusMap = new Map<String,String>{null => '-'};
        Map<String, Schema.SObjectField> SobjtField = Schema.getGlobalDescribe().get('Quote').getDescribe().fields.getMap();
        for(Deal_Coordination__mdt dealCoordinationObj : metaData){
            if(dealCoordinationObj.Requested_Date_Lookup__c != null){
                if(!SobjtField.containsKey(dealCoordinationObj.Requested_Date_Lookup__c)){
                    dateRequestedMap.put(dealCoordinationObj.Requested_Date_Lookup__c,'Field does not exist.');
                } else if(quoteObj.get(dealCoordinationObj.Requested_Date_Lookup__c) != null){
                    Date dateFieldValue;
                    Try{
                        dateFieldValue = (Date)quoteObj.get(dealCoordinationObj.Requested_Date_Lookup__c);
                    }
                    Catch(exception e){
                        system.debug(logginglevel.error, '*****'+e);
                        dateFieldValue = Date.valueOf((Datetime)quoteObj.get(dealCoordinationObj.Requested_Date_Lookup__c));
                    }
                    dateRequestedMap.put(dealCoordinationObj.Requested_Date_Lookup__c,dateFieldValue.format());
                } else {
                    dateRequestedMap.put(dealCoordinationObj.Requested_Date_Lookup__c,'-');
                }                
            }
            if(dealCoordinationObj.Status_Lookup__c != null){
                if(!SobjtField.containsKey(dealCoordinationObj.Status_Lookup__c)){
                    statusMap.put(dealCoordinationObj.Status_Lookup__c,'Field does not exist.');
                } else if(quoteObj.get(dealCoordinationObj.Status_Lookup__c) != null){
                    statusMap.put(dealCoordinationObj.Status_Lookup__c,(String)quoteObj.get(dealCoordinationObj.Status_Lookup__c));
                } else {
                    statusMap.put(dealCoordinationObj.Status_Lookup__c,'-');
                } 
            }
        }
    }

    @testVisible
    private Quote queryQuote(){
        Map<String, Schema.SObjectField> SobjtField = Schema.getGlobalDescribe().get('Quote').getDescribe().fields.getMap();
        string qry = '';
        for (Schema.SObjectField s : SobjtField.values()) {
            qry += s + ',';
        }
        qry = qry.removeend(',');
        qry = 'SELECT '+qry+' FROM '+ 'Quote' + ' WHERE Id = \'' + ApexPages.CurrentPage().getParameters().get('Id') + '\'';
        system.debug(logginglevel.error, '***** qry='+qry);
        return Database.query(qry);
    }

}