global class EB_ApprovalPlugin implements Process.Plugin {

    // This method runs when called by a flow's Apex plug-in element.
    global Process.PluginResult invoke(
        Process.PluginRequest request) {
        // Set up variables to store input parameters from
        // the flow.
        ID targetObjectId = (ID) request.inputParameters.get('TargetRecord');
        String status = (String) request.inputParameters.get('status');
        String comment = (String) request.inputParameters.get('comment');
        if (comment == null) comment = '';
        Boolean passed;
        if (status == 'Approve') {
            passed=EB_Utility_Approval.Approve(targetObjectId, comment);
        } else if (status == 'Reject') {
            passed=EB_Utility_Approval.Reject(targetObjectId, comment);
        } else {
            throw new ApprovalException('Status is incorrect. Status must be either Approve or Reject.');
        }

        Map<String, Object> result = new Map<String, Object>();

        result.put('success', passed);
        return new Process.PluginResult(result);
    }


    global Process.PluginDescribeResult describe() {
        // Set up plugin metadata
        Process.PluginDescribeResult result = new
        Process.PluginDescribeResult();
        result.description =
            'This plugin allows approval or rejection or records.';
        result.tag = 'EB Plugins';


        result.inputParameters = new
        List<Process.PluginDescribeResult.InputParameter> {
            new Process.PluginDescribeResult.InputParameter(
                'TargetRecord',
                'Record for approval',
                Process.PluginDescribeResult.ParameterType.ID,
                true),
            new Process.PluginDescribeResult.InputParameter(
                'status',
                'Valid options are Approve or Reject',
                Process.PluginDescribeResult.ParameterType.STRING,
                true),
            new Process.PluginDescribeResult.InputParameter(
                'comment',
                'Comment for approval process.',
                Process.PluginDescribeResult.ParameterType.STRING,
                FALSE)
        };

        // Create a list that stores output parameters sent
        // to the flow.
        result.outputParameters = new List <
        Process.PluginDescribeResult.OutputParameter > {
            // String
            new Process.PluginDescribeResult.OutputParameter(
                'success',
                Process.PluginDescribeResult.ParameterType.BOOLEAN)
        };

        return result;
    }

    public class ApprovalException extends Exception {}
}