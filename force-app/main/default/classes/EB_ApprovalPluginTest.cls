@isTest
private class EB_ApprovalPluginTest {
	@istest static void test_Invoke() {
		Opportunity opp = (Opportunity)EB_Utility_CreateObjectForTesting.CreateObject('Opportunity');
		Opp.LID__LinkedIn_Company_Id__c = '5';
		opp.Forecast_PercentageV2__c = 0;
		opp.Opp_from_Lead_Conversion__c = false;
		insert opp;
        
        opp.StageName = 'Won';
        update opp;

		// Insert an account
		Survey__c survey = (Survey__c)EB_Utility_CreateObjectForTesting.CreateObject('Survey__c');
		survey.Status__c = 'Quantitative Pending';
		survey.Opportunity__c = opp.id;
		insert survey;

	/*	try {
			EB_Utility_Approval.submitForApproval(survey.id, 'Test Approval');
		} catch (DmlException e) {
			System.debug(e.getMessage());
		}*/

		EB_ApprovalPlugin plugin = new EB_ApprovalPlugin();
		Map<String, Object> inputValues = new Map<String, Object>();
		inputValues.put('TargetRecord', survey.Id);
		inputValues.put('status', 'Approve');
		inputValues.put('comment', 'Comments');

		Process.PluginRequest request = new Process.PluginRequest(inputValues);

		Test.startTest();
		plugin.invoke(request);
		Test.stopTest();

		//No errors, we are good.
	}

	/*
	 * This tests the describe() method
	 */
	@isTest
	static void describeTest() {
		EB_ApprovalPlugin plugin = new EB_ApprovalPlugin();
		Process.PluginDescribeResult result = plugin.describe();

		System.AssertEquals(3, result.inputParameters.size());
		System.AssertEquals(1, result.OutputParameters.size());
	}
}