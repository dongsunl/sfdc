public with sharing class EB_OppCreateRandomNumberHandle Implements EB_TriggerHandler.HandlerInterface {
	public EB_OppCreateRandomNumberHandle() {

	}

	public void handle() {
		if (Trigger_Settings__c.getInstance().OppCreateRandomNumber__c == false) return;

		for (SObject opp : Trigger.new) {
			Decimal randNumber = Decimal.valueOf(Math.roundToLong(Math.Random() * 9 + 1));
			opp.put('Random_Number__c', randNumber);
		}
	}
}