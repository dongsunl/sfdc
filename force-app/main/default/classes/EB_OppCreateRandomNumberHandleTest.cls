@isTest
private class EB_OppCreateRandomNumberHandleTest {

    @testSetup static void setupMethod() {
        Trigger_Settings__c setting = new Trigger_Settings__c();
        setting.OppCreateRandomNumber__c = true;
        insert setting;
        
        CreateSobjectForTesting csft = new CreateSobjectForTesting();
        Contact contact = (Contact) csft.CreateObject('Contact');
        contact.LID__LinkedIn_Company_Id__c = '';
        insert contact;
    }

    @isTest static void test_RandomNumber() {
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        
        Opportunity opp = (Opportunity) EB_Utility_CreateObjectForTesting.CreateObject('Opportunity');
        Opp.LID__LinkedIn_Company_Id__c = '0';
        Opp.Forecast_PercentageV2__c = 0;
        Opp.Opp_from_Lead_Conversion__c = false;
        Opp.Random_Number__c = 0;
        Opp.Qualification_Contact__c = contact.Id;
        System.debug(Opp);
        Test.StartTest();
        insert opp;
        Test.StopTest();

        opp=[SELECT Random_Number__c FROM Opportunity WHERE ID=:opp.Id];
        system.debug(opp.Random_Number__c);
       // System.assertNotEquals(0, opp.Random_Number__c);
      //  System.assert(opp.Random_Number__c > 0, 'Random number is less than 1');
      //  System.assert(opp.Random_Number__c < 11, 'Random number is greater than 10');
    }

    /**
     PanelsLoadUpdate Trigger causes bulk inserts on opps to fail.
     */

    //@isTest static void test_RandomBulk() {
    //    List<Opportunity> oppList = new List<Opportunity>();
    //    for (Integer i = 0; i < 101; i++) {
    //        Opportunity opp = (Opportunity)EB_Utility_CreateObjectForTesting.CreateObject('Opportunity');
    //        Opp.LID__LinkedIn_Company_Id__c = '0';
    //        Opp.Forecast_PercentageV2__c = 0;
    //        Opp.Opp_from_Lead_Conversion__c = false;
    //        oppList.add(opp);
    //    }
    //    Test.StartTest();
    //    insert oppList;
    //    Test.StopTest();
    //    oppList = [SELECT Random_Number__c
    //               FROM Opportunity];
    //    System.assertEquals(150, oppList.Size(), 'Returned wrong number of records.');

    //    Decimal Highest = -10;
    //    Decimal Lowest = 100;
    //    for (Opportunity opp : oppList) {
    //        if (opp.Random_Number__c < Lowest) Lowest = opp.Random_Number__c;
    //        if (opp.Random_Number__c > Highest) Highest = opp.Random_Number__c;
    //    }
    //    System.assertNotEquals(Lowest, Highest, 'Number is not Random');
    //    System.assertEquals(1, Lowest);
    //    System.assertEquals(10, Highest);
    //}
}