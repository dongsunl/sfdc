public class EmailHunterCalloutMock implements HttpCalloutMock {

    public HttpResponse respond(HttpRequest request) {
        
        EmailHunterResponse emailHunterResponse = new EmailHunterResponse();
		emailHunterResponse.data.sources.add(new EmailHunterResponse.ResponseDataSource());
        
        HttpResponse response = new HttpResponse();
        response.setBody(JSON.serialize(emailHunterResponse));
        response.setStatusCode(200);
        return response;
    }
}