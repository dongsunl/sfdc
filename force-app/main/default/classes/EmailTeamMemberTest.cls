@isTest
private class EmailTeamMemberTest {

	@testSetup static void testSetup() {
		User u;
		Account acc;
		User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
		String orgId = UserInfo.getOrganizationId();
		System.runAs(currentUser) {
			Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
			UserRole role = new UserRole(DeveloperName='MyCustomRole', Name='My Role');
			insert role;
			u = new User(
				FirstName='Test', 
				LastName='User', 
				Alias='tuser', 
				Email='test@test.com', 
				UserName='test' + orgId + '@qualtrics.com',
				UserRoleId=role.Id,
				ProfileId = prof.Id,
				TimeZoneSidKey = 'America/Denver',
				LocaleSidKey = 'en_US',
				LanguageLocaleKey = 'en_US',
				EmailEncodingKey = 'ISO-8859-1');
			insert u;
		}
		acc = TestFactory.createAccount();
		insert acc;
		PriceBook2 pb = TestFactory.createPriceBook();
		insert pb;
		Opportunity opp = TestFactory.createOpportunity(acc.Id, pb.Id);
		opp.Turn_Off_Initial_CX_EX_RC_Amounts__c = true;
		opp.OwnerId = u.Id;
		insert opp;
	}
	
	@isTest static void addSME() {
		OpportunityTeamMember sme = new OpportunityTeamMember(OpportunityId = [SELECT Id FROM Opportunity LIMIT 1].Id, UserId = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()].Id, TeamMemberRole = 'SME', OpportunityAccessLevel = 'Read');
		insert sme;
	}
	
}