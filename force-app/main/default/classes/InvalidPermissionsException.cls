public class InvalidPermissionsException extends Exception {
    public void testingMethod(){
    }
    public final static String STANDARD_MESSAGE = 'You do not have permissions to perform this action.';
    
    public final static String CASE_UPDATE_MESSAGE = 'You do not have permissions to update the case object or required fields.';
    public final static String CONTACT_ACCESS_MESSAGE = 'You do not have permissions to query the contact object or required fields.';
    public final static String CONTRACT_DOCUMENT_ACCESS_MESSAGE = 'You do not have permissions to query the contract document object or required fields.';
    
    public final static String OPPORTUNITY_ACCESS_MESSAGE = 'You do not have permissions to query the opportunity object or required fields.';
    public final static String OPPORTUNITY_OR_CONTRACT_DOCUMENT_UPDATE_MESSAGE = 'You do not have permissions to update the case object, contract document object, or required fields.';
    
    public final static String QUOTE_ACCESS_MESSAGE = 'You do not have permissions to query the quote object or required fields.';
    public final static String QUOTE_OR_OPPORTUNITY_ACCESS_MESSAGE = 'You do not have permissions to query the quote object, opportunity object, or required fields.';
    
    public final static String RECORD_TYPE_ACCESS_MESSAGE = 'You do not have permissions to query the record type object or required fields.';
    public final static String REDIRECTION_ACCESS_MESSAGE = 'You do not have permissions to query the redirection settings object, redirection fields object or required fields.';
}