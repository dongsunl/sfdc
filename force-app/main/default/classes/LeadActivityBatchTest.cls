@isTest
private class LeadActivityBatchTest {

    static testMethod void LeadActivity_Test() {
      
		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
  		User objUser = new User(Alias = 'standt', Email='standarduser@testorg.com', 
	  	EmailEncodingKey='UTF-8', FirstName='Test', LastName=' ContactFranchise', LanguageLocaleKey='en_US', 
	  	LocaleSidKey='en_US', ProfileId = p.Id, 
	  	TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1234@testorg.com');
       	database.insert(objUser);
       	
       	Group objGroup = [Select g.Name, g.Id, g.Email From Group g Where Name ='Nurture'];
       	
   		Lead objLead = new Lead();
       	objLead.LastName = 'test Lead';
       	objLead.Company = 'test company';
       	objLead.Status = 'stauts';
       	objLead.LeadSource = 'test source';
       	objLead.IsConverted = false;
       	objLead.OwnerId = objUser.Id;
       	database.insert(objLead);
       	
       	Task objTask = new Task();
      	objTask.OwnerId = objUser.Id;
       	objTask.Subject = 'test subject';
       	objTask.Priority = 'low';
       	objTask.Sales_Stage__c = 'test stage';
       	objTask.Status = 'test status';
       	objTask.ActivityDate = system.today().addDays(-56);
       	objTask.WhoId = objLead.Id;
       	database.insert(objTask);
       	
       	Task objTask1 = new Task();
      	objTask1.OwnerId = objUser.Id;
       	objTask1.Subject = 'test subject';
       	objTask1.Priority = 'low';
       	objTask1.Sales_Stage__c = 'test stage';
       	objTask1.Status = 'test status';
       	objTask1.ActivityDate = system.today().addDays(1);
       	objTask1.WhoId = objLead.Id;
       	database.insert(objTask1);
       	
       	test.startTest();
       		
       		LeadActivityBatch objBatch = new LeadActivityBatch();
       		ID batchprocessid = Database.executeBatch(objBatch);
       		Lead objLead1 = [Select Id, OwnerId From Lead Where Id =: objLead.Id];
       		
       		//system.assertEquals(objGroup.Id, objLead1.OwnerId);
       	test.stopTest();
       
    }
    
}