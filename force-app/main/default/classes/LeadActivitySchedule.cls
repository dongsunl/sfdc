global with sharing class LeadActivitySchedule implements Schedulable{
	global LeadActivitySchedule(){}
	global void execute(SchedulableContext sc) {
	      LeadActivityBatch objBatch = new LeadActivityBatch(); 
	      database.executebatch(objBatch);
	}
}