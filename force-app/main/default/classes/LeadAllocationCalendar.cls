public with sharing class LeadAllocationCalendar {

    Public String CUrl;
    Public String LeadID;
    Public Lead L = new Lead();
    Public String Tier;
    Public String Region;
    Public List<User> TierUsers = new List<User>();
    Public Set<ID> SetUsers = new Set<ID>();
    Public List<User> finalUsers {get; set;}

    public LeadAllocationCalendar(ApexPages.StandardController controller) {
        
        LeadID = controller.getId();
        System.debug('This is the lead ID for the test'  + LeadID);
        L = [Select ID, FirstName, LastName, Tier__c, Region__c from Lead where ID = :LeadID];
        Region = L.Region__c;
        System.debug('This is the Lead ID  ' + LeadID);
        
        
    }

    public void UserAllocation(){
    
        Tier = L.Tier__c;
        
        TierUsers = [Select ID, FirstName, LastName, Rep_Tier__c, Region__c, Tier_A__c, Tier_B__c, Tier_C__c,
               Tier_E__c, Tier_D__c from User where Exclude_from_Lead_Allocation__c = False AND Region__c = :Region];
               
       For(User U: TierUsers){
            if(U.Tier_A__c == True && Tier == 'A'){
                SetUsers.add(U.ID);
            }
            if(U.Tier_B__c == True && Tier == 'B'){
                SetUsers.add(U.ID);
            }
            if(U.Tier_C__c == True && Tier == 'C'){
                SetUsers.add(U.ID);
            }
            if(U.Tier_D__c == True && Tier == 'D'){
                SetUsers.add(U.ID);
           }
            if(U.Tier_E__c == True && Tier == 'E'){
                SetUsers.add(U.ID);
            }
        } 
        
        finalUsers = [Select ID, Name, Last_Lead_Assigned__c from User where Id in :SetUsers Order by Last_Lead_Assigned__c Asc Limit 3];
        
        
        If(Tier == 'A' && Region == 'APAC'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V8zs';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bI72';
        }
        If(Tier == 'A' && Region == 'Canada'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V8zj';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bI7M';
        }
        If(Tier == 'A' && Region == 'Central'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V90W';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bI7g';
        }
        If(Tier == 'A' && Region == 'EMEA'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V90b';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIFB';
        }
        If(Tier == 'A' && Region == 'Great Lakes'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V91A';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIFV';
        }
        If(Tier == 'A' && Region == 'Northeast'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V89r';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIFp';
        }
        If(Tier == 'A' && Region == 'Southeast'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V91F';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIG9';
        }
        If(Tier == 'A' && Region == 'West'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V91j';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bI6d';
        }
        If(Tier == 'B' && Region == 'APAC'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V8zn';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bI77';
        }
        If(Tier == 'B' && Region == 'Canada'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V8zx';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bI7R';
        }
        If(Tier == 'B' && Region == 'Central'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V90R';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bI7l';
        }
        If(Tier == 'B' && Region == 'EMEA'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V90g';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIFG';
        }
        If(Tier == 'B' && Region == 'Great Lakes'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V915';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIFa';
        }
        If(Tier == 'B' && Region == 'Northeast'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V8zY';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIFu';
        }
        If(Tier == 'B' && Region == 'Southeast'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V91K';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIGE';
        }
        If(Tier == 'B' && Region == 'West'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V91e';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006b4DU';
        }
        If(Tier == 'C' && Region == 'APAC'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V8zi';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bI7C';
        }
        If(Tier == 'C' && Region == 'Canada'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V902';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bI7W';
        }
        If(Tier == 'C' && Region == 'Central'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V90H';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bI7N';
        }
        If(Tier == 'C' && Region == 'EMEA'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V90q';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIFL';
        }
        If(Tier == 'C' && Region == 'Great Lakes'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V910';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIFf';
        }
        If(Tier == 'C' && Region == 'Northeast'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V8zV';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIFz';
        }
        If(Tier == 'C' && Region == 'Southeast'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V90m';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIGJ';
        }
        If(Tier == 'C' && Region == 'West'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V91Z';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bI6i';
        }
        If(Tier == 'D' && Region == 'APAC'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V8zd';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bI7H';
        }
        If(Tier == 'D' && Region == 'Canada'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V8zW';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bI7b';
        }
        If(Tier == 'D' && Region == 'Central'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V907';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bI7q';
        }
        If(Tier == 'D' && Region == 'EMEA'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V8zo';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIFQ';
        }
        If(Tier == 'D' && Region == 'Great Lakes'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V90v';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIFk';
        }
        If(Tier == 'D' && Region == 'Northeast'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V8zK';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIG4';
        }
        If(Tier == 'D' && Region == 'Southeast'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V91P';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIGO';
        }
        If(Tier == 'D' && Region == 'West'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V91U';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bI6s';
        }
        If(Region == 'US Academic'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V91V';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIGT';
        }            
        If(Region == 'Canada Academic'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V91o';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIGY';
        } 
        If(Region == 'Not for Profit Academic'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V91k';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIGd';
        } 
        If(Region == 'EMEA Academic'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V91t';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIGi';
        } 
        If(Region == 'Healthcare'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V91y';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIGn';
        } 
        If(Region == 'Federal Government'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V923';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIGs';            
        } 
        If(Region == 'Retail West'){
            //CUrl = 'https://cs4.salesforce.com/00U/c?isdtp=mn&cType=2&fcf=00BP0000000V928';
            CUrl = 'https://na3.salesforce.com/00U/c?isdtp=mn&cType=2&md3=241&md0=2013&fcf=00B50000006bIGx';
        } 
        
    


        
        
       
    System.debug('This is the Lead ID  ' + LeadID);
    System.debug('This is the Lead Tier  ' + Tier);
    System.debug('This is the TierUsers   ' +TierUsers);
    System.debug('This is the Users set  ' +SetUsers);
    System.debug('This is the Final User List  ' + FinalUsers);
    //System.debug('This is the First User in  List  ' + FinalUsers[0]);
    
    }
    
   Public String getCUrl(){
     return CUrl;
     
   }
  
   
    
}