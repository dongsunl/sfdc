/*
 * Class Name: aura.OnboardingClient
 * 
 * @author David
 * 
 * @date 8/24/2018
 * 
 * @description:
 */
public class OnboardingLightningController {

	@AuraEnabled
	public static String getClients(String opportunityId) {
		//Compile list of Contacts at the Account.
		List<Opportunity> oppList  = [Select AccountId from Opportunity where Id = :OpportunityId];
		String accountId = '';
		if (oppList.isEmpty()) {
			return  '';
		} else {
			accountId = oppList[0].AccountId;
		}
		List<Client__c> acctclients = [SELECT Name, ID, Client_Status__c FROM Client__c WHERE Account__c = :accountId];

		//contactcount = acctcontacts.size();
		String returnVals = '';
		for (Client__c c : acctclients) {
			returnVals += '{"label": "' + c.Name + '", "value" : "' + c.Id + '"},' ;
		}
		return '[' + returnVals.substring(0, returnVals.length() - 1) + ']';
	}
	@AuraEnabled
	public static Map<String, String > getBrandLabels(){
		Map<String, Schema.SobjectField> fields = Brand__c.sObjectType.getDescribe().fields.getMap();
		Map<String, String> fieldList = new Map<String, String> ();
		for (String key : fields.keySet()) {
			fieldList.put(fields.get(key).getDescribe().getName() , fields.get(key).getDescribe().getLabel() ) ;
		}
		return fieldList;
	}
	/*
	@AuraEnabled
	public static Map<String, String> getOpportunityLabels(){
		Map<String, Schema.SobjectField> fields = Opportunity.sObjectType.getDescribe().fields.getMap();
		Map<String, String> fieldList = new Map<String, String> ();
		for (String key : fields.keySet()) {
			fieldList.put(fields.get(key).getDescribe().getName() , fields.get(key).getDescribe().getLabel() ) ;
		}
		return fieldList;
	}
	
	@AuraEnabled
	public static Map<String, String > getContactLabels(){
		Map<String, Schema.SobjectField> fields = Contact.sObjectType.getDescribe().fields.getMap();
		Map<String, String> fieldList = new Map<String, String> ();
		for (String key : fields.keySet()) {
			fieldList.put(fields.get(key).getDescribe().getName() , fields.get(key).getDescribe().getLabel() ) ;
		}
		return fieldList;
	}
	*/
	@AuraEnabled
	public static List<Contact> getContacts(String opportunityId , String searchTerm){
		List<Opportunity> oppList  = [Select AccountId from Opportunity where Id = :OpportunityId];
		String accountId = '';
		if (oppList.isEmpty()) {
			return  new List<Contact> () ;
		} else {
			accountId = oppList[0].AccountId;
		}
		System.debug('*** searchTerm: ' + searchTerm);
		if (String.isBlank(searchTerm)) {
			return [SELECT Id, Name , Email , Phone FROM Contact WHERE AccountId = :accountId Order By Name ];
		} else  {
			searchTerm = '%' + searchTerm + '%';
			System.debug('*** searchTerm: ' + searchTerm);
			return [SELECT Id, Name , Email , Phone FROM Contact WHERE AccountId = :accountId and Name like :searchTerm Order By Name ];
		}
	}

	@AuraEnabled
	public static List<Contact> getContactsFromAccount(String accountId) {
		return accountId != null ? [SELECT Id, Name, Email, Phone FROM Contact WHERE AccountId = :accountId ORDER BY LastName] : new List<Contact>();
	}

	@AuraEnabled
	public static List<OpportunityLineItem> getOppLineItems (String opportunityId) {
		List<OpportunityLineItem> lineItems = [select Id ,Product2.Name , Quantity , UnitPrice ,  TotalPrice   from OpportunityLineItem where OpportunityId = :opportunityId ];
		return  lineItems ;

	}
	@AuraEnabled
	public static List<Brand__c> getBrandItems (String opportunityId) {
		List<Brand__c> lineItems = [select Id, Type__c , Name , Brand_Admin__r.Name , Brand_Admins__c, Existing_Brand_Admin_Username__c, Existing_Brand_Admin_Brand_ID__c, Users_To_Move__c , Users_To_Create__c , Brands_To_Merge__c   from Brand__c where Opportunity__c = :opportunityId ];
		return  lineItems ;

	}
	@AuraEnabled
	public static ClientObject getBillingInfo (String opportunityId) {
		List<Opportunity> oppList  = [Select AccountId from Opportunity where Id = :OpportunityId];
		String accountId = '';
		if (oppList.isEmpty()) {
			return  new ClientObject ();
		} else {
			accountId = oppList[0].AccountId;
		}
		List<Account> acctList = [Select BillingStreet , BillingCity , BillingState , BillingPostalCode from Account where Id = :accountId  ] ;
		ClientObject clientObj = new ClientObject() ;
		clientObj.HeadquarterBillingToStreet = acctList[0].BillingStreet ;
		clientObj.HeadquarterBillingToCity = acctList[0].BillingCity ;
		clientObj.HeadquarterBillingToState = acctList[0].BillingState;
		clientObj.HeadguarterBillingToPostalCode = acctList[0].BillingPostalCode;
		return clientObj ;

	}
	/*
	@AuraEnabled
	public static Client__c getOpportunityClient (String oppId) {
		Opportunity opp = [Select Id, Client__c from Opportunity where Id = :oppId] ;
		if (String.isBlank(opp.Client__c )) {
			return new Client__c ();
		} else {
			return Database.Query( getFieldsQuery('Client__c' , false) + ' FROM Client__c WHERE ID = \'' + opp.Client__c  + '\'');
		}

	}
	
	@AuraEnabled
	public static Brand__c getBrand (String Id) {

			return Database.Query( getFieldsQuery('Brand__c' , false) + ' , Brand_Admin__r.Name  FROM Brand__c WHERE ID = \'' + Id  + '\'');


	}
	*/
	@AuraEnabled
	public static ClientObject getSelectedClient (String clientId) {

		Client__c client = [SELECT Name, BillToName__c, Billing_Street_Address__c, Billing_City__c, Billing_State_Province__c, Billing_Zip_Postal_Code__c, BillingCountry2__c, Shipping_Street__c, Shipping_City__c, Shipping_State__c, Shipping_Zip_Postal_Code__c, ShippingCountry2__c
		FROM Client__c WHERE ID = :clientId];
		ClientObject clientObj = new ClientObject() ;
		clientObj.ClientName = client.Name;
		clientObj.BilltoName =client.BillToName__c ;
		clientObj.BillingStreet = client.Billing_Street_Address__c ;
		clientObj.BillingCity = client.Billing_City__c ;
		clientObj.BillingState = client.Billing_State_Province__c;
		clientObj.BillingPostalCode = client.Billing_Zip_Postal_Code__c ;
		clientObj.EndUserStreet = client.Shipping_Street__c ;
		clientObj.EndUserCity = client.Shipping_City__c ;
		clientObj.EndUserState = client.Shipping_State__c ;
		clientObj.EndUserPostalCode = client.Shipping_Zip_Postal_Code__c;
		return clientObj;

	}
	/*
    @AuraEnabled
	public static Client__c getClientAddressInfo (String clientId) {
        if (String.isBlank(clientId)) {
            return null;
        }
		Client__c client = [SELECT Name, BillToName__c, Billing_Street_Address__c, Billing_City__c, Billing_State_Province__c, Billing_Zip_Postal_Code__c, BillingCountry2__c, Shipping_Street__c, Shipping_City__c, Shipping_State__c, Shipping_Zip_Postal_Code__c, ShippingCountry2__c
							FROM Client__c WHERE ID = :clientId];
		return client;
	}
	
	@AuraEnabled
	public static Contact getSelectedContact (String contactId , String dataString , String oppId   ) {
		Contact clientObj = (Contact)Json.deserialize(dataString , Contact.class );
		clientObj.Id = contactId;
		update clientObj;
		Opportunity opp = new Opportunity() ;
		opp.Id = oppId ;
		opp.Billing_Contact__c = contactId;
		System.Debug( '******* Opp ' + opp );
		update opp;

		System.Debug(contactId);
		return [SELECT ID, Name, FirstName, LastName, Email, Phone, Contact_Role__c, Status__c, LeadSource FROM Contact WHERE ID = :contactId];


	}
	
	@AuraEnabled
	public static Contact getSelectedContact2 (String contactId  ) {
		System.Debug(contactId);
		return Database.Query( getFieldsQuery('Contact' , false) + ' FROM Contact WHERE ID = :contactId');

	}
	*/
	@AuraEnabled
	public static Opportunity getOpportunity (String oppId  ) {

		Opportunity returnOppId = Database.Query( getFieldsQuery('Opportunity' , false) + ',RecordType.Name , Account.Name , (Select Id from OpportunityLineItems where Bundle_Name__c = \'RC1\' ) FROM Opportunity WHERE ID = :oppId' ) ;

		if (String.isEmpty(returnOppId.Client__c)) {
			return returnOppId ;
		} else {
			return Database.Query( getFieldsQuery('Opportunity' , false) + ', Account.Name, RecordType.Name , (Select Id from OpportunityLineItems where Bundle_Name__c = \'RC1\'   ) ,Billing_Contact__r.Name , Client__r.Ship_To_Name__c ,Client__r.BillToName__c , Client__r.Billing_Street_Address__c , Client__r.Billing_City__c , Client__r.Billing_State_Province__c , Client__r.Billing_Zip_Postal_Code__c , Client__r.Billing_Country__c , Client__r.Shipping_Street__c , Client__r.Shipping_City__c , Client__r.Shipping_State__c , Client__r.Shipping_Zip_Postal_Code__c , Client__r.Shipping_Country__c FROM Opportunity WHERE ID = :oppId');
		}

	}

	@AuraEnabled
	public static Boolean hasRC1Product(String oppId) {
		if (oppId == null) {
			return false;
		} else {
			return [SELECT Id FROM OpportunityLineItem WHERE OpportunityId = :oppId AND Bundle_Name__c = 'RC1' LIMIT 1].size() > 0;
		}
	}

	/*
	@AuraEnabled
	public static string getDataCenter () {

		String returnVals = '{"label": "Enter Data Center Location", "value" : "" },';
		for (Schema.PicklistEntry ple : GetObjectsPickListValues('Contract_Document__c', 'Data_Center_Location__c'))  {
			returnVals += '{"label": "' + ple.getLabel() + '", "value" : "' + ple.getValue() + '"},' ;
		}
		return '[' + returnVals.substring(0, returnVals.length() - 1) + ']';
	}
	
	@AuraEnabled
	public static string getOnboardingEmailLanguage () {
		String returnVals = '';
		for (Schema.PicklistEntry ple : GetObjectsPickListValues('Opportunity', 'Onboarding_Email_Language__c'))  {
			returnVals += '{"label": "' + ple.getLabel() + '", "value" : "' + ple.getValue() + '"},' ;
		}
		return '[' + returnVals.substring(0, returnVals.length() - 1) + ']';
	}
	
	@AuraEnabled
	public static boolean isOnboardingComplete (String oppId) {
		boolean Status = true ;
		Opportunity opp = [Select Sales_Order_Bill_To_Name__c , Sales_Order_Ship_To_Name__c from Opportunity where id = :oppId ];
		for ( Brand__c brand : [Select Brand_Admin__c from Brand__c where Opportunity__c = :oppId] ) {
			if (Status == true && String.isEmpty(brand.Brand_Admin__c) ) {
				Status = false ;
			}
		}

		if (Status == true && String.isEmpty(opp.Sales_Order_Bill_To_Name__c) || String.isEmpty(opp.Sales_Order_Ship_To_Name__c  ) ) {
			Status = false ;
		}
		return Status;

	}
	
	@AuraEnabled
	public static string getTrainingLanguageLanguage () {
		String returnVals = '';
		for (Schema.PicklistEntry ple : GetObjectsPickListValues('Opportunity', 'Training_Language__c'))  {
			returnVals += '{"label": "' + ple.getLabel() + '", "value" : "' + ple.getValue() + '"},' ;
		}
		return '[' + returnVals.substring(0, returnVals.length() - 1) + ']';
	}
	
	@AuraEnabled
	public static string getEmailLanguage () {
		String returnVals = '';
		for (Schema.PicklistEntry ple : GetObjectsPickListValues('Opportunity', 'Invoice_Email_Language__c'))  {
			returnVals += '{"label": "' + ple.getLabel() + '", "value" : "' + ple.getValue() + '"},' ;
		}
		return '[' + returnVals.substring(0, returnVals.length() - 1) + ']';
	}
	
	@AuraEnabled
	public static string getPaymentTerms () {
		String returnVals = '';
		for (Schema.PicklistEntry ple : GetObjectsPickListValues('Opportunity', 'Payment_Terms__c'))  {
			returnVals += '{"label": "' + ple.getLabel() + '", "value" : "' + ple.getValue() + '"},' ;
		}
		return '[' + returnVals.substring(0, returnVals.length() - 1) + ']';
	}
	
	@AuraEnabled
	public static String getBrandTypes() {
		String returnVals = '';
		for (Schema.PicklistEntry ple : GetObjectsPickListValues('Brand__c', 'Brand_Type__c'))  {
			returnVals += '{"label": "' + ple.getLabel() + '", "value" : "' + ple.getValue() + '"},' ;
		}
		return '[' + returnVals.substring(0, returnVals.length() - 1) + ']';
	}
	
	@AuraEnabled
	public static String getNewContactFieldOptions() {
		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeStartObject();
		gen.writeFieldName('Contact_Role__c');
		gen.writeStartArray();
		for (Schema.PicklistEntry ple : GetObjectsPickListValues('Contact', 'Contact_Role__c'))  {
			gen.writeStartObject();
			gen.writeStringField('label', ple.getLabel());
			gen.writeStringField('value', ple.getValue());
			gen.writeEndObject();
		}	
		gen.writeEndArray();
		gen.writeFieldName('Status__c');
		gen.writeStartArray();
		for (Schema.PicklistEntry ple : GetObjectsPickListValues('Contact', 'Status__c'))  {
			gen.writeStartObject();
			gen.writeStringField('label', ple.getLabel());
			gen.writeStringField('value', ple.getValue());
			gen.writeEndObject();
		}	
		gen.writeEndArray();
		gen.writeFieldName('LeadSource');
		gen.writeStartArray();
		for (Schema.PicklistEntry ple : GetObjectsPickListValues('Contact', 'LeadSource'))  {
			gen.writeStartObject();
			gen.writeStringField('label', ple.getLabel());
			gen.writeStringField('value', ple.getValue());
			gen.writeEndObject();
		}	
		gen.writeEndArray();
		gen.writeEndObject();
		return gen.getAsString();
	}
	*/
	@AuraEnabled
	public static String generatePicklistJSON(String sObjectType, List<String> fieldApiNames) {
		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeStartObject();
		for(String fieldApiName : fieldApiNames) {
			gen.writeFieldName(fieldApiName);
			gen.writeStartArray();
			for (Schema.PicklistEntry ple : GetObjectsPickListValues(sObjectType, fieldApiName))  {
				if (ple.isActive()) {
					gen.writeStartObject();
					gen.writeStringField('label', ple.getLabel());
					gen.writeStringField('value', ple.getValue());
					gen.writeEndObject();
				}
			}	
			gen.writeEndArray();
		}
		gen.writeEndObject();
		return gen.getAsString();
	}

	/*
	@AuraEnabled
	public static String getContactRole() {
		String returnVals = '';
		for (Schema.PicklistEntry ple : GetObjectsPickListValues('Contact', 'Contact_Role__c'))  {
			returnVals += '{"label": "' + ple.getLabel() + '", "value" : "' + ple.getValue() + '"},' ;
		}
		return '[' + returnVals.substring(0, returnVals.length() - 1) + ']';
	}
	@AuraEnabled
	public static String getContactStatus() {
		String returnVals = '';
		for (Schema.PicklistEntry ple : GetObjectsPickListValues('Contact', 'Status__c'))  {
			returnVals += '{"label": "' + ple.getLabel() + '", "value" : "' + ple.getValue() + '"},' ;
		}
		return '[' + returnVals.substring(0, returnVals.length() - 1) + ']';
	}
	@AuraEnabled
	public static String getContactLeadSource() {
		String returnVals = '';
		for (Schema.PicklistEntry ple : GetObjectsPickListValues('Contact', 'LeadSource'))  {
			returnVals += '{"label": "' + ple.getLabel() + '", "value" : "' + ple.getValue() + '"},' ;
		}
		return '[' + returnVals.substring(0, returnVals.length() - 1) + ']';
	}
	*/
	@AuraEnabled
	public static String getBillingCountries() {
		String returnVals = '';
		for (Schema.PicklistEntry ple : GetObjectsPickListValues('Client__c', 'BillingCountry2__c'))  {
			returnVals += '{"label": "' + ple.getLabel() + '", "value" : "' + ple.getValue() + '"},' ;
		}
		return '[' + returnVals.substring(0, returnVals.length() - 1) + ']';
	}
	@AuraEnabled
	public static String getShippingCountries() {
		String returnVals = '';
		for (Schema.PicklistEntry ple : GetObjectsPickListValues('Client__c', 'ShippingCountry2__c'))  {
			returnVals += '{"label": "' + ple.getLabel() + '", "value" : "' + ple.getValue() + '"},' ;
		}
		return '[' + returnVals.substring(0, returnVals.length() - 1) + ']';
	}

	/*
	@AuraEnabled
	public static boolean SaveClientRecord (String dataString) {
		ClientObject clientObj = (ClientObject)Json.deserialize(dataString , ClientObject.class );
		System.Debug(clientObj );
		Client__c newclient = new Client__c();
		newClient.Name = clientObj.ClientName;
		newclient.Billing_Street_Address__c = clientObj.BillingStreet;
		newclient.Billing_City__c = clientObj.BillingCity;
		newclient.Billing_State_Province__c = clientObj.BillingState;
		newclient.Billing_Zip_Postal_Code__c = clientObj.BillingPostalCode;

		if (clientObj.hasEndUser != null && clientObj.hasEndUser) {
			newclient.Shipping_Street__c = clientObj.EndUserStreet;
			newclient.Shipping_City__c = clientObj.EndUserCity;
			newclient.Shipping_State__c = clientObj.EndUserState;
			newclient.Shipping_Zip_Postal_Code__c = clientObj.BillingPostalCode; //newclient.ShippingCountry2__c = newclient.BillingCountry2__c;
		} else  {
			newclient.Shipping_Street__c = clientObj.BillingStreet;
			newclient.Shipping_City__c = clientObj.BillingCity;
			newclient.Shipping_State__c = clientObj.BillingState;
			newclient.Shipping_Zip_Postal_Code__c = clientObj.BillingPostalCode;
		}
		insert newclient ;
		return true;
	}
	*/
	@AuraEnabled
	public static String SaveContactAdminRecord (String dataString , String OppId) {
		Contact contactObj = (Contact)Json.deserialize(dataString , Contact.class );
		Opportunity opp = [Select Id, Client__c from Opportunity where Id = :OppId ];
		Contact ct = new Contact () ;
		System.Debug('  ******* contactObj ********** ' + contactObj) ;
		ct.FirstName = contactObj.FirstName ;
		ct.LastName = contactObj.LastName ;
		ct.Email = contactObj.Email;
		ct.Contact_Role__c = contactObj.Contact_Role__c ;
		ct.Status__c = contactObj.Status__c;
		ct.LeadSource = contactObj.LeadSource ;
		ct.Client__c = opp.Client__c;



		ct.AccountId = [Select Account__c from Client__c where Id = :opp.Client__c ].Account__c ;
		ct.RecordTypeId = '012500000009pRcAAI';
		insert ct;

		return ct.Id;

	}
	/*
	@AuraEnabled
	public static String SaveContactRecord (String dataString , String clientId , String OppId) {
		ContactObject contactObj = (ContactObject)Json.deserialize(dataString , ContactObject.class );
		Contact ct = new Contact () ;
		ct.FirstName = contactObj.FirstName ;
		ct.LastName = contactObj.LastName ;
		ct.Email = contactObj.Email;
		ct.Contact_Role__c = contactObj.ContactLevel;
		ct.Status__c = contactObj.Status;
		ct.LeadSource = contactObj.LeadSource ;
		ct.Client__c = clientId;
		ct.MailingCountry = contactObj.Country;
		ct.AccountId = [Select Account__c from Client__c where Id = :clientId].Account__c ;
		insert ct;
		Opportunity opp = new Opportunity() ;
		opp.Id = OppId ;
		opp.Billing_Contact__c = ct.Id;
		update opp;
		return ct.Id;

	}
	
	@AuraEnabled
	public static Contact SaveContactRecord2 (String dataString , String clientId , String OppId) {
		ContactObject contactObj = (ContactObject)Json.deserialize(dataString , ContactObject.class );
		Contact ct = new Contact () ;
		ct.FirstName = contactObj.FirstName ;
		ct.LastName = contactObj.LastName ;
		ct.Email = contactObj.Email;
		ct.Contact_Role__c = contactObj.ContactLevel;
		ct.Status__c = contactObj.Status;
		ct.LeadSource = contactObj.LeadSource ;
		ct.Client__c = clientId;
		ct.MailingCountry = contactObj.Country;
		ct.AccountId = [Select Account__c from Client__c where Id = :clientId].Account__c ;
		insert ct;
		Opportunity opp = new Opportunity() ;
		opp.Id = OppId ;
		opp.Billing_Contact__c = ct.Id;
		update opp;
		return Database.Query( getFieldsQuery('Contact' , false) + '  FROM Contact WHERE ID = \'' + ct.Id + '\'');

	}
	*/
	@AuraEnabled
	public static Contact saveBillingContact(String contactJSON) {
		Contact billingContact = (Contact) JSON.deserialize(contactJSON, Contact.class);
		try {
			upsert billingContact;
			//Opportunity opp = new Opportunity(Id = oppId, Billing_Contact__c = billingContact.Id);
			//update opp;
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
		return billingContact;
	}
	@AuraEnabled
	public static Boolean DeleteBrandRecord (String BrandId   ) {
		Delete [Select Id from Brand__c where Id = :BrandId] ;
		return true;
	}
	@AuraEnabled
	public static void deleteLinkedBrands (String oppId) {
		if (oppId != null) {
			List<Brand__c> brandsToDelete = [SELECT Id FROM Brand__c WHERE Opportunity__c = :oppId];
			if (!brandsToDelete.isEmpty()) {
				try {
					delete brandsToDelete;
				} catch (Exception e) {
					throw new AuraHandledException('Error deleting brand records: ' + e.getMessage());
				}
			}
		} else {
			throw new AuraHandledException('Error deleting brand records: No Opportunity ID provided.');
		}
	}
	@AuraEnabled
	public static Brand__c RemoveBrandAdmin  ( String brandId   ) {
		List<Brand__c> brandList = Database.Query( getFieldsQuery('Brand__c' , false) + ' , Brand_Admin__r.Name  FROM Brand__c WHERE ID = \'' + brandId  + '\'');
		brandList[0].Requested_Username__c = null;
		brandList[0].Brand_Admin__c = null;
		update brandList;
		return brandList[0] ;
	}
	@AuraEnabled
	public static Brand__c SaveBrandRecord (String dataString , String OppId   ) {

		Boolean existingbranduser = false;
		String singleusernew = 'single';
		String singleuserexisting = 'single';

		Brand__c newbrand = (Brand__c) Json.deserialize(dataString , Brand__c.class );
		System.Debug('***** newbrand ' + newbrand);
		Opportunity o = [Select RecordType.Name , Opportunity_Type__c from Opportunity where Id = :OppId] ;
		if(o.RecordType.Name == 'XM'){
			newbrand.Brand_Type__c = 'XM';
			newbrand.Type__c = 'XM';
		}
		newbrand.Brand_Admin_Exists_As_User__c = existingbranduser;
		if(o.Opportunity_Type__c == 'New License' && singleusernew == 'single' || o.Opportunity_Type__c != 'New License' && singleuserexisting == 'single'){
			newbrand.SingleUserorEntryLicense__c = TRUE;
		}
		newbrand.Opportunity__c = OppId;
		System.debug ('************ newbrand: ' + newbrand );


		Database.UpsertResult res = Database.upsert(newbrand);
		System.debug( '***** UpsertResult ' + res);
		System.debug ('************ newbrand after upsert: ' + newbrand );
		String brandId = res.getId();
		return  Database.Query( getFieldsQuery('Brand__c' , false) +  ' , Brand_Admin__r.Name FROM Brand__c WHERE ID = :brandId');
		/*
		 *
        newbrand.Brand_Admin_Exists_As_User__c = existingbranduser;
        if(o.Opportunity_Type__c == 'New License' && singleusernew == 'single' || o.Opportunity_Type__c != 'New License' && singleuserexisting == 'single'){
            newbrand.SingleUserorEntryLicense__c = TRUE;
        }
        insert newbrand;
        newbrand = new Brand__c(Opportunity__c = o.Id, Brands_To_Merge__c = '', Users_To_Move__c = '', Users_To_Create__c = '');
        existingbranduser = FALSE;
        tabview = 'brand';
        querybrands();
        if(getbrandspurchased() == brandsadded){
            brandinfocomplete = TRUE;
        }
        else{brandinfocomplete = FALSE;}

        return null;
		 */
	}
	/*
	@AuraEnabled
	public static Boolean SaveOpportunityRecord (String dataString , String OppId   ) {

		Opportunity opp = (Opportunity) Json.deserialize(dataString , Opportunity.class );
		opp.Id = OppId;
		System.Debug(opp);
		update opp;

		return  true;

	}
	*/

	/*
	@AuraEnabled
	public static  Boolean AddAttachment(String OppId , String fileName ,  String base64Data , String contentType) {
		System.Debug(base64Data) ;
		base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
		Attachment a = new Attachment(ParentId = OppId , Name = fileName , ContentType = contentType , Body = EncodingUtil.base64Decode(base64Data) );
		insert a;

		return true;
	}
	@AuraEnabled
	public static List<Attachment> getAttachmentList (String OppId) {
		return [SELECT Id, Name, ContentType, Description, BodyLength FROM Attachment WHERE ParentId = :OppId AND IsPrivate = FALSE];
	}
	*/




	/*
	 public pageReference insertclient(){
        if(!newclientaddr){
            newclient.Billing_Street_Address__c = o.Account.BillingStreet;
            newclient.Billing_City__c = o.Account.BillingCity;
            newclient.Billing_State_Province__c = o.Account.BillingState;
            newclient.Billing_Zip_Postal_Code__c = o.Account.BillingPostalCode;
        }
        if(!newclientenduseraddr){
            newclient.Shipping_Street__c = newclient.Billing_Street_Address__c;
            newclient.Shipping_City__c = newclient.Billing_City__c;
            newclient.Shipping_State__c = newclient.Billing_State_Province__c;
            newclient.Shipping_Zip_Postal_Code__c = newclient.Billing_Zip_Postal_Code__c;
            newclient.ShippingCountry2__c = newclient.BillingCountry2__c;
        }
        newclient.BillToName__c = clientbillto;
        insert newclient;
        o.Client__c = newclient.id;
        clientcomplete = TRUE;
        unsavedchanges = TRUE;
        tabview = 'client';
        return null;
    }
	 *
	 */

	public static List<Schema.PicklistEntry> GetObjectsPickListValues(string ObjName, string objField) {
		Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
		Map <String, Schema.SObjectField> field_Map = schemaMap.get(objName).getDescribe().fields.getMap();
		List<Schema.PicklistEntry> ple = field_map.get(objField).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
		return ple;
	}

	public class ClientObject {
		@AuraEnabled public String ClientName {get;set;}

		@AuraEnabled public String HeadquarterBillingToStreet {get;set;}
		@AuraEnabled public String HeadquarterBillingToCity {get;set;}
		@AuraEnabled public String HeadquarterBillingToState {get;set;}
		@AuraEnabled public String HeadguarterBillingToPostalCode {get;set;}
		@AuraEnabled public String BilltoName {get;set;}
		@AuraEnabled public String BillingStreet {get;set;}
		@AuraEnabled public String BillingCity {get;set;}
		@AuraEnabled public String BillingState {get;set;}
		@AuraEnabled public String BillingPostalCode {get;set;}
		@AuraEnabled public String EndUserStreet {get;set;}
		@AuraEnabled public String EndUserCity {get;set;}
		@AuraEnabled public String EndUserState {get;set;}
		@AuraEnabled public String EndUserPostalCode {get;set;}
		@AuraEnabled public String EndUserCountry {get;set;}
		@AuraEnabled public boolean hasEndUser {get;set;}
	}
	public class ContactObject {
		@AuraEnabled public String ContactName {get;set;}
		@AuraEnabled public String FirstName {get;set;}
		@AuraEnabled public String LastName {get;set;}
		@AuraEnabled public String Email {get;set;}
		@AuraEnabled public String BusinessPhone {get;set;}
		@AuraEnabled public String Country {get;set;}
		@AuraEnabled public String ContactLevel {get;set;}
		@AuraEnabled public String Status {get;set;}
		@AuraEnabled public String LeadSource {get;set;}

	}
	/*
	public class NamePair {
		@AuraEnabled public String Name {
			get;set;
		}
		@AuraEnabled public String Value {
			get;set;
		}

		public NamePair(String v, String n) {
			Name = n;
			Value = v;
		}
	}
	*/

	/***** Utility methods ***************/
	public static String getFieldsQuery(String objName, Boolean getFormulas) {
		Map<String, Schema.SObjectField> fldObjMap;
		Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
		fldObjMap = gd.get(objName).getDescribe().fields.getMap();
		List<Schema.SObjectField> fldObjMapValues = new List<Schema.SObjectField>();
		for (Schema.SObjectField fld : fldObjMap.values()) {
			if (fld.getDescribe().isAccessible()) {
				fldObjMapValues.add(fld);
			}
		}
		String theQuery = 'SELECT ';
		for(Schema.SObjectField s : fldObjMapValues) {
			if (s.getDescribe().isAccessible() && (getFormulas ? true : !s.getDescribe().isCalculated())) {
				String theName = s.getDescribe().getName();
				theQuery += theName + ',';
			}
		}

		// Trim last comma
		theQuery = theQuery.subString(0, theQuery.length() - 1);
		//theQuery += ' FROM ' + objName;
		return theQuery;
	}


}