@isTest
private class OnboardingLightningControllerTest {
    @testSetup static void setupData() {
        
         AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
    	insert avaCustomSetting;
        
        Account testAccount = new Account(Name='Test', Industry='High Tech', AnnualRevenue=100, NumberOfEmployees=100, BillingStreet='111 Test St.', BillingCity='Indianapolis', BillingState='IN', BillingPostalCode='12345');
        insert testAccount;

        List<Contact> testContacts = new List<Contact>();
        for (Integer i=0; i < 5; i++) {
            testContacts.add(new Contact(FirstName='Test', LastName='Contact ' + i, AccountId=testAccount.Id, Contact_Role__c='C-Level', Status__c='Prospect', LeadSource='Outbound - Sales'));
        }
        insert testContacts;

        List<Client__c> testClients = new List<Client__c>();
        for (Integer i=0; i < 5; i++) {
            testClients.add(new Client__c(Name='Test Client ' + i, Client_Status__c='Active', Account__c=testAccount.Id));
        }
        testClients[0].BillToName__c = 'Test Client 0';
        testClients[0].Billing_Street_Address__c = '111 Test St.';
        testClients[0].Billing_City__c = 'Indianapolis';
        testClients[0].Billing_State_Province__c = 'IN';
        testClients[0].Billing_Zip_Postal_Code__c = '12345';
        testClients[0].Shipping_Street__c = '123 Test Ave.';
        testClients[0].Shipping_City__c = 'Carmel';
        testClients[0].Shipping_State__c = 'IN';
        testClients[0].Shipping_Zip_Postal_Code__c = '67890';
        testClients[1].BillToName__c = 'Test Client 1';
        testClients[1].Billing_Street_Address__c = '111 Test St.';
        testClients[1].Billing_City__c = 'Indianapolis';
        testClients[1].Billing_State_Province__c = 'IN';
        testClients[1].Billing_Zip_Postal_Code__c = '12345';
        testClients[1].Shipping_Street__c = '123 Test Ave.';
        testClients[1].Shipping_City__c = 'Carmel';
        testClients[1].Shipping_State__c = 'IN';
        testClients[1].Shipping_Zip_Postal_Code__c = '67890';
        insert testClients;

        Id xmRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('XM').getRecordTypeId();
        Opportunity testOpportunity = new Opportunity(Name='Test Opp', AccountId=testAccount.Id, RecordTypeId=xmRecordTypeId, Qualification_Contact__c=testContacts[0].Id, CloseDate=Date.today(), StageName='Invoice', Client__c=testClients[0].Id);
        insert testOpportunity;

        Id pricebookId = Test.getStandardPricebookId();
        List<Product2> testProducts = new List<Product2>();
        for (Integer i=0; i < 5; i++) {
            testProducts.add(new Product2(Name='Test Product ' + 1, Display_Order__c=i));
        }
        insert testProducts;

        List<PricebookEntry> testEntries = new List<PricebookEntry>();
        for (Product2 p : testProducts) {
            testEntries.add(new PricebookEntry(Pricebook2Id=pricebookId, Product2Id=p.Id, UnitPrice=1000, IsActive=true));
        }
        insert testEntries;

        List<OpportunityLineItem> testLineItems = new List<OpportunityLineItem>();
        for (PricebookEntry e : testEntries) {
            testLineItems.add(new OpportunityLineItem(PricebookEntryId=e.Id, OpportunityId=testOpportunity.Id, UnitPrice=200, Quantity=5));
        }
        testLineItems[0].Bundle_Name__c = 'RC1';
        testLineItems[1].Bundle_Name__c = 'RC1';
        insert testLineItems;

        List<Brand__c> testBrands = new List<Brand__c>();
        for (Integer i=0; i < 5; i++) {
            testBrands.add(new Brand__c(Name='Test Brand ' + i, Opportunity__c=testOpportunity.Id));
        }
        insert testBrands;
    }

    @isTest static void testGetClients() {
        Id testOppId = [SELECT Id FROM Opportunity WHERE Name='Test Opp'].Id;
        String result = OnboardingLightningController.getClients(testOppId);
        System.assertEquals(5, result.split('},').size());

        String blankString = OnboardingLightningController.getClients('faketestid');
        System.assertEquals('', blankString);
    }

    @isTest static void testGetBrandLabels() {
        Map<String, String> brandLabels = OnboardingLightningController.getBrandLabels();
        System.assertEquals('Opportunity', brandLabels.get('Opportunity__c'));
        System.assertEquals('Account ID', brandLabels.get('Account_ID__c'));
    }

    @isTest static void testGetContacts() {
        Id testOppId = [SELECT Id FROM Opportunity WHERE Name='Test Opp'].Id;
        List<Contact> contacts = OnboardingLightningController.getContacts(testOppId, 'Contact');
        System.assertEquals(5, contacts.size());

        contacts = OnboardingLightningController.getContacts(testOppId, '2');
        System.assertEquals(1, contacts.size());
        System.assertEquals('Test Contact 2', contacts[0].Name);

        contacts = OnboardingLightningController.getContacts('faketestid', 'Contact');
        System.assertEquals(0, contacts.size());

        contacts = OnboardingLightningController.getContacts(testOppId, 'fakename');
        System.assertEquals(0, contacts.size());

        contacts = OnboardingLightningController.getContacts(testOppId, '');
        System.assertEquals(5, contacts.size());
    }

    @isTest static void testGetContactsFromAccount() {
        Id testAcctId = [SELECT Id FROM Account WHERE Name='Test'].Id;
        List<Contact> contacts = OnboardingLightningController.getContactsFromAccount(testAcctId);
        System.assertEquals(5, contacts.size());

        contacts = OnboardingLightningController.getContactsFromAccount('faketestid');
        System.assertEquals(0, contacts.size());

        contacts = OnboardingLightningController.getContactsFromAccount(null);
        System.assertEquals(0, contacts.size());
    }

    @isTest static void testGetOppLineItems() {
        Id testOppId = [SELECT Id FROM Opportunity WHERE Name='Test Opp'].Id;
        List<OpportunityLineItem> lineItems = OnboardingLightningController.getOppLineItems(testOppId);
        System.assertEquals(5, lineItems.size());

        lineItems = OnboardingLightningController.getOppLineItems('faketestid');
        System.assertEquals(0, lineItems.size());
    }

    @isTest static void testGetBrandItems() {
        Id testOppId = [SELECT Id FROM Opportunity WHERE Name='Test Opp'].Id;
        List<Brand__c> brands = OnboardingLightningController.getBrandItems(testOppId);
        System.assertEquals(5, brands.size());

        brands = OnboardingLightningController.getBrandItems('faketestid');
        System.assertEquals(0, brands.size());
    }

    @isTest static void testGetBillingInfo() {
        Id testOppId = [SELECT Id FROM Opportunity WHERE Name='Test Opp'].Id;
        OnboardingLightningController.ClientObject clientObject = OnboardingLightningController.getBillingInfo(testOppId);
        System.assertEquals('111 Test St.', clientObject.HeadquarterBillingToStreet);
        System.assertEquals('Indianapolis', clientObject.HeadquarterBillingToCity);
        System.assertEquals('IN', clientObject.HeadquarterBillingToState);
        System.assertEquals('12345', clientObject.HeadguarterBillingToPostalCode);

        clientObject = OnboardingLightningController.getBillingInfo('faketestid');
        System.assertEquals(null, clientObject.HeadquarterBillingToStreet);
    }

    @isTest static void testGetSelectedClient() {
        Id testClientId = [SELECT Id FROM Client__c WHERE Name = 'Test Client 1'].Id;
        OnboardingLightningController.ClientObject clientObject = OnboardingLightningController.getSelectedClient(testClientId);
        System.assertEquals('Test Client 1', clientObject.ClientName);
        System.assertEquals('Test Client 1', clientObject.BilltoName);
        System.assertEquals('111 Test St.', clientObject.BillingStreet);
        System.assertEquals('Indianapolis', clientObject.BillingCity);
        System.assertEquals('IN', clientObject.BillingState);
        System.assertEquals('12345', clientObject.BillingPostalCode);
        System.assertEquals('123 Test Ave.', clientObject.EndUserStreet);
        System.assertEquals('Carmel', clientObject.EndUserCity);
        System.assertEquals('IN', clientObject.EndUserState);
        System.assertEquals('67890', clientObject.EndUserPostalCode);
    }

    @isTest static void testGetOpportunity() {
        Id testOppId = [SELECT Id FROM Opportunity WHERE Name='Test Opp'].Id;
        Opportunity opp = OnboardingLightningController.getOpportunity(testOppId);
        System.assertEquals(2, opp.OpportunityLineItems.size());
    }

    @isTest static void testHasRC1Product() {
        Id testOppId = [SELECT Id FROM Opportunity WHERE Name='Test Opp'].Id;
        System.assert(OnboardingLightningController.hasRC1Product(testOppId));
        System.assert(!OnboardingLightningController.hasRC1Product('faketestid'));
    }

    @isTest static void testGeneratePicklistJSON() {
        String picklistJSONString = OnboardingLightningController.generatePicklistJSON('Opportunity', new List<String>{'Invoice_Email_Language__c', 'Payment_Terms__c', 'ARR_NRR__c', 'Auto_Renewal__c', 'Renewal_Price_Increase__c','Study_Type__c'});
        Map<String, List<Map<String, String>>> picklistJSON = (Map<String, List<Map<String, String>>>) JSON.deserialize(picklistJSONString, Map<String, List<Map<String, String>>>.class);
        System.assert(picklistJSON.containsKey('Invoice_Email_Language__c'));
        System.assert(picklistJSON.containsKey('Auto_Renewal__c'));
        System.assert(picklistJSON.containsKey('Study_Type__c'));
    }

    @isTest static void testGetBillingCountries() {
        String countryJSONString = OnboardingLightningController.getBillingCountries();
        List<Map<String, String>> countryJSON = (List<Map<String, String>>) JSON.deserialize(countryJSONString, List<Map<String, String>>.class);
    }

    @isTest static void testGetShippingCountries() {
        String countryJSONString = OnboardingLightningController.getShippingCountries();
        List<Map<String, String>> countryJSON = (List<Map<String, String>>) JSON.deserialize(countryJSONString, List<Map<String, String>>.class);
    }

    @isTest static void testSaveContactAdminRecord() {
        Opportunity testOpp = [SELECT Id, Client__c, Client__r.Account__c FROM Opportunity WHERE Name='Test Opp'];
        String adminJSONString = '{"FirstName": "New", "LastName": "Admin", "Email": "newadmin@newadmin.test", "Contact_Role__c": "VP", "Status__c": "Active Customer", "LeadSource": "Outbound Sales"}';
        String adminRecordId = OnboardingLightningController.SaveContactAdminRecord(adminJSONString, testOpp.Id);
        Contact newContact = [SELECT Id, Name, Contact_Role__c, Client__c, AccountId FROM Contact WHERE Name = 'New Admin'];
        System.assertEquals(adminRecordId, newContact.Id);
        System.assertEquals('VP', newContact.Contact_Role__c);
        System.assertEquals(testOpp.Client__c, newContact.Client__c);
        System.assertEquals(testOpp.Client__r.Account__c, newContact.AccountId);
    }

    @isTest static void testSaveBillingContact() {
        Id testAcctId = [SELECT Id FROM Account WHERE Name='Test'].Id;
        String contJSONString = '{"FirstName": "New", "LastName": "Contact", "Email": "newcontact@newcontact.test", "Contact_Role__c": "VP", "Status__c": "Active Customer", "LeadSource": "Outbound Sales", "AccountId": "' + testAcctId + '"}';
        Contact newContact = OnboardingLightningController.saveBillingContact(contJSONString);
        System.assertEquals('New', newContact.FirstName);
        System.assertEquals('newcontact@newcontact.test', newContact.Email);
        System.assertEquals('VP', newContact.Contact_Role__c);

        contJSONString = '{"Id": "' + newContact.Id + '", "Email": "newcontact2@newcontact2.test"}';
        newContact = OnboardingLightningController.saveBillingContact(contJSONString);
        System.assertEquals('newcontact2@newcontact2.test', newContact.Email);

        contJSONString = '{"Id": "' + newContact.Id + '", "Email": "newcontact3"}';
        try {
            newContact = OnboardingLightningController.saveBillingContact(contJSONString);
        } catch(Exception e) {
            System.assertEquals('System.AuraHandledException', e.getTypeName());
        }
    }

    @isTest static void testDeleteBrands() {
        Id testBrandId = [SELECT Id FROM Brand__c WHERE Name='Test Brand 0'].Id;
        Id testOppId = [SELECT Id FROM Opportunity WHERE Name='Test Opp'].Id;

        Boolean deleteResult = OnboardingLightningController.DeleteBrandRecord(testBrandId);
      //  System.assert(deleteResult);
      //  System.assertEquals(4, [SELECT Id FROM Brand__c WHERE Opportunity__c = :testOppId].size());

        OnboardingLightningController.deleteLinkedBrands(testOppId);
      //  System.assertEquals(0, [SELECT Id FROM Brand__c WHERE Opportunity__c = :testOppId].size());

        try {
            OnboardingLightningController.deleteLinkedBrands(null);
        } catch (Exception e) {
          //  System.assert(e.getMessage().contains('No Opportunity ID provided'));
        }
    }

    @isTest static void testRemoveBrandAdmin() {
        Brand__c testBrand = [SELECT Id FROM Brand__c WHERE Name='Test Brand 0'];
        Contact testContact = [SELECT Id FROM Contact WHERE Name='Test Contact 0'];

        testBrand.Brand_Admin__c = testContact.Id;
        testBrand.Requested_Username__c = 'testtesttest';
        update testBrand;

        Brand__c updatedBrand = OnboardingLightningController.RemoveBrandAdmin(testBrand.Id);
        System.assertEquals(null, updatedBrand.Requested_Username__c);
        System.assertEquals(null, updatedBrand.Brand_Admin__c);
    }
}