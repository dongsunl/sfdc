public class OpportunitiestoSubmitSelector {
	//Gets Opps for display on the CS homepage 
    public List<Opportunity> getOpportunities() {
        List<Opportunity> Opportunities =        
        [select id, Name, Account.Name, Amount, CloseDate, StageName from Opportunity WHERE OwnerId = :UserInfo.getUserId() AND Submitted_for_Sales_Approval_Date__c = null AND StageName = 'Sales Qualification'];
        return Opportunities;
    }

}