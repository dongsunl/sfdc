public with sharing class OpportunityAccess {

    @AuraEnabled
    public static Opportunity revertOppHandover(Id id) {

        final String REVERT_OPP_HANDOVER_RECORD_TYPE = '012500000009noC';
        final String APPROVAL_STATUS_REJECTED = 'Rejected';
        final String STAGE_NAME_REJECTED = 'Rejected';
        final String SALES_PROCESS_STAGE_REJECTED = 'Rejected';
        
        if (!Schema.SObjectType.Opportunity.isUpdateable()
	            || !Schema.SObjectType.Opportunity.fields.RecordTypeId.isUpdateable()
	            || !Schema.SObjectType.Opportunity.fields.Approval_Status__c.isUpdateable()
	            || !Schema.SObjectType.Opportunity.fields.StageName.isUpdateable()
	            || !Schema.SObjectType.Opportunity.fields.Sales_Process_Stage__c.isUpdateable()
	            || !Schema.SObjectType.Opportunity.fields.OwnerId.isUpdateable()) {
            throw new InvalidPermissionsException(InvalidPermissionsException.OPPORTUNITY_ACCESS_MESSAGE);
        }
        
        List<Opportunity> records =
            [SELECT Id, Prior_Owner_Id__c
              FROM Opportunity
              WHERE Id = : id
              LIMIT 1];
        if (records.size() == 1) {
            Opportunity record = records[0];
            record.RecordTypeId = REVERT_OPP_HANDOVER_RECORD_TYPE;
            record.Approval_Status__c = APPROVAL_STATUS_REJECTED;
            record.StageName = STAGE_NAME_REJECTED;
            record.Sales_Process_Stage__c = SALES_PROCESS_STAGE_REJECTED;
            if (record.Prior_Owner_Id__c != null) {
	            record.OwnerId = record.Prior_Owner_Id__c;
            }
			update record;
            return record;
        }
		throw new DmlException('Record not found');
    }

    @InvocableMethod(label='Enforce Distinct Memberships' description='Limit opportunity members to a single representative of each title.')
    public static void enforceDistinctMemberships(List<Id> newOpportunityTeamMemberIds) {
        
        // Create a collection of relevant opportunity team member properties.
        Map<Id, OpportunityTeamMembership> mapOfOpportunityTeamMemberships = new Map<Id, OpportunityTeamMembership>();
		for (OpportunityTeamMember opportunityTeamMember :
	            [SELECT Id, OpportunityId, UserId, User.Title
                 FROM OpportunityTeamMember
                 WHERE Id IN : newOpportunityTeamMemberIds
                 LIMIT 1000]) {
			mapOfOpportunityTeamMemberships.put(opportunityTeamMember.OpportunityId, new OpportunityTeamMembership(opportunityTeamMember));
		}
        
        // Locate any related opportunity team members to be evaluated.
        List<OpportunityTeamMember> opportunityTeamMembersToBeDeleted = new List<OpportunityTeamMember>();
        OpportunityTeamMembership opportunityTeamMembership;
		for (OpportunityTeamMember opportunityTeamMember :
	            [SELECT Id, OpportunityId, UserId, User.Title
    	         FROM OpportunityTeamMember
        	     WHERE OpportunityId IN : mapOfOpportunityTeamMemberships.keySet()
                 LIMIT 1000]) {
			opportunityTeamMembership = mapOfOpportunityTeamMemberships.get(opportunityTeamMember.OpportunityId);
			if (opportunityTeamMember.UserId != opportunityTeamMembership.getUserId()
					&& opportunityTeamMember.User.Title == opportunityTeamMembership.getTitle()) {
				opportunityTeamMembersToBeDeleted.add(opportunityTeamMember);
			}
		}
        
		RunAsSystem.deleteOpportunityTeamMembers(opportunityTeamMembersToBeDeleted);
    }
}