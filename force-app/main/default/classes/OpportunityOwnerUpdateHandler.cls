@isTest //This has been moved to the OpportunityFieldUpdates class and has been commented out but 
//Salesforce still wanted code coverage so marking it as test prevents the need for that.

/*
	Revision History:
    Version     Version Author    Date          Comments
	1.0			Prajakta Sanap	 21/05/2014   1. Opportunity field Approval Status (Approval_Status__c) is changed to 'Accepted' or 'Accepted-Auto'
												then Change Opportunity Owner to the user listed in Opportunity Field Assign To (Assign_To__c )
*/

public class OpportunityOwnerUpdateHandler {
	//private static final String ACCEPTED = Label.Accepted;
	//private static final String ACCEPTED_AUTO = Label.Accepted_Auto;

	//public static void updateOwner(List<Opportunity> newList, map<Id, Opportunity> OldMap) {

	//	for (Opportunity obj : newList) {
	//		if (
	//		    OldMap != null
	//		    && obj.Approval_Status__c != OldMap.get(obj.Id).Approval_Status__c
	//		    && obj.Assign_To__c != null
	//		    && (
	//		        obj.Approval_Status__c == ACCEPTED
	//		        || obj.Approval_Status__c == ACCEPTED_AUTO
	//		    )
	//		) {
	//			obj.OwnerId = obj.Assign_To__c;
	//		} else if (
	//		    OldMap == null
	//		    && obj.Assign_To__c != null
	//		    && (
	//		        obj.Approval_Status__c == ACCEPTED
	//		        || obj.Approval_Status__c == ACCEPTED_AUTO
	//		    )
	//		) {
	//			obj.OwnerId = obj.Assign_To__c;
	//		}
	//	}
	//}

	//public void updateOwner(List<Opportunity> newList, map<Id, Opportunity> OldMap) {
	//	List<Opportunity> lstOpportunity = new List<Opportunity>();

	//	system.debug('-------------newList-------------' + newList);
	//	for (Opportunity obj : newList) {
	//		if (OldMap != null && (obj.Approval_Status__c != OldMap.get(obj.Id).Approval_Status__c)) {
	//			lstOpportunity.add(obj);
	//		} else if (OldMap == null) {
	//			lstOpportunity.add(obj);
	//		}
	//	}
	//	system.debug('-------------lstOpportunity-------------' + lstOpportunity);

	//	if (lstOpportunity != null && lstOpportunity.size() > 0) {
	//		for (Opportunity objOpp : lstOpportunity) {
	//			if (objOpp.Approval_Status__c == ACCEPTED || objOpp.Approval_Status__c == ACCEPTED_AUTO) {
	//				if (objOpp.Assign_To__c != null) {
	//					objOpp.OwnerId = objOpp.Assign_To__c;
	//					system.debug('-------------objOpp.OwnerId-----------' + objOpp.OwnerId);
	//				}
	//			}
	//		}
	//	}
	//}
}