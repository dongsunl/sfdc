public class OpportunityTeamMembership {
    
    private Id opportunityId;
    private Id userId;
    private String title;
    
    public OpportunityTeamMembership(OpportunityTeamMember opportunityTeamMember) {
        this.opportunityId = opportunityTeamMember.OpportunityId;
        this.userId = opportunityTeamMember.UserId;
        this.title = opportunityTeamMember.User.Title;
    }
    
    public Id getOpportunityId() {
        return this.opportunityId;
    }
    
    public Id getUserId() {
        return this.userId;
    }
    
    public String getTitle() {
        return this.title;
    }
}