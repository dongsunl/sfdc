@IsTest
public class OpportunityTeamMembershipTest {
    testMethod static void opportuntiyTeamMembership(){
        Opportunity opp = RedirectionTestUtilities.createOpportunity();
        OpportunityTeamMember teamMember = new OpportunityTeamMember(
            OpportunityId = opp.Id,
            UserId = system.UserInfo.getUserId()
        );
        OpportunityTeamMembership opportunityTeamMembership = new OpportunityTeamMembership(teamMember);
        Id oppId = opportunityTeamMembership.getOpportunityId();
        system.assertEquals(opp.Id, oppId);
        Id userId = opportunityTeamMembership.getUserId();
        system.assertEquals(system.UserInfo.getUserId(), userId);
    }
}