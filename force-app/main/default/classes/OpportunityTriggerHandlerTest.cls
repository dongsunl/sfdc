@IsTest
    //This test class covers code contained in and refrenced by the OpportunityTriggerHandler class
	//Add test methods as needed   
public class OpportunityTriggerHandlerTest {

    testMethod static void OpportunityTriggerHandlerTest(){
        //setup test
         AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
    Insert avaCustomSetting;
		Account acct = new Account(Name='TEST');
        insert acct;
        
        Contact cont = new Contact(FirstName = 'TEST', 
                                   LastName = 'TEST', 
                                   AccountId = acct.Id, 
                                   Contact_Role__c = 'Director', 
                                   LeadSource = 'Referral', 
                                   Lead_Source_Detail__c = 'Partnerships',
                                   Currently_engaged_in_data_collection__c = 'TRUE');
        
        Opportunity newOpp = new Opportunity(AccountId=acct.Id,
                                          Name='TEST',
                                          StageName = 'Pending',
                                          CloseDate = System.today(),
                                          Turn_Off_Initial_CX_EX_RC_Amounts__c = true,
                                          Qualification_Contact__c = cont.Id,
                                          SAP_Referral_Info__c = '');
        insert newOpp;  
        
        //Test SAP Info Updates
        Test.startTest();
        newOpp.SAP_Referral_Info__c = 'TEST_BR_ENCODED_TEST';
        update newOpp;
       	List<Opportunity> updatedOpps = [SELECT Id, SAP_Referral_Info__c from Opportunity where Id =: newOpp.Id];
        OpportunityFieldUpdates.reformatSAPInformation(updatedOpps);
        String sapInfo = updatedOpps[0].SAP_Referral_Info__c;
        system.assert(!sapInfo.contains('_BR_ENCODED_'), sapInfo);
        
        //Test Update Opp from contact
        OpportunityFieldUpdates.updateOppFromContact(newOpp,cont);
        String updatedField = newOpp.Currently_engaged_in_data_collection__c;
        system.assert(updatedField == 'TRUE', updatedField);
        delete newOpp;
        Test.stopTest();
    }
}