@IsTest
public class PanelDemographicsCountryScreenTest {
	
    @IsTest
    public static void testCountriesScreen(){
        //Set up test
        List<Database.SaveResult> saveResults = new List<Database.SaveResult>();
        Account account = new Account(
            name = 'Caribe Sol'
        );
        saveResults.add(Database.insert(account));
        List<Opportunity> screeningOpps = new List<Opportunity>();
        Opportunity opportunity = new Opportunity(
            AccountId = account.Id,
            Name = 'Test',
            StageName = 'Pending',
            CloseDate = System.today(),
            Turn_Off_Initial_CX_EX_RC_Amounts__c = true,
            Countries__c = 'Cuba'
        );
		saveResults.add(Database.insert(opportunity));
        screeningOpps.add(opportunity);
        Test.startTest();
        List<ProcessInstance> approvals = [SELECT Id FROM ProcessInstance WHERE TargetObjectId =: opportunity.Id];
        system.assert(approvals.size() > 0);
        Test.stopTest();
    }
    
    @IsTest
	public static void testPanelDemographicScreen(){
        //Set up test
        List<Database.SaveResult> saveResults = new List<Database.SaveResult>();
        Account account = new Account(
            name = 'Caribe Sol'
        );
        saveResults.add(Database.insert(account));
        List<Opportunity> screeningOpps = new List<Opportunity>();
        Opportunity opportunity = new Opportunity(
            AccountId = account.Id,
            Name = 'Test',
            StageName = 'Pending',
            CloseDate = System.today(),
            Turn_Off_Initial_CX_EX_RC_Amounts__c = true,
            Panel_Demographics__c = 'Cuba, Syria'
        );
		saveResults.add(Database.insert(opportunity));
        screeningOpps.add(opportunity);
        Test.startTest();
        List<ProcessInstance> approvals = [SELECT Id FROM ProcessInstance WHERE TargetObjectId =: opportunity.Id];
        system.assert(approvals.size() > 0);
        Test.stopTest();
    }
}