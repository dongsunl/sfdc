/***********
Description: This class is helper for PanelProjectVendorTrigger.
This class is used for calculating the number of bid made by a vendor(i.e no. of panels project vendors per vendor)
and the number of bid own by a vendor(i.e no. of panels project vendor slected for use per vendor).
This helper class is created for bulkify the trigger for more records
***********/
public class PanelProjectVendorHelper{
    //Description: Excute method is to calculate the no bid made by a vendor and the no. of bid own by a vendor and update the count in the vendor record.
    public static void deleteEventHandler(Map<Id,Panels_Project_Vendor__c> oldMap){
        Set<Id> vendorIds = new Set<Id>();
        for(Panels_Project_Vendor__c each : oldMap.values()){
            vendorIds.add(each.VendorName__c);
        }
        //selecting the vendors invoked for this transaction
        List<Vendor__c> vendorList = [SELECT ID,NAME,Number_of_times_Bided__c, Number_of_times_selected__c,(SELECT ID,VendorName__c,Selected_for_Use__c,VendorID__c FROM Panels_Project_Vendors__r) FROM Vendor__c where Id In:vendorIds];
        for(Vendor__c eachVendor : vendorList){
            Integer bidSubmitted = 0;
            Integer bidSelected = 0;
            System.debug('eachVendor '+eachVendor);
            System.debug('eachVendor.Project_Line_Items__r '+eachVendor.Panels_Project_Vendors__r);
            for(Panels_Project_Vendor__c eachProjectLine : eachVendor.Panels_Project_Vendors__r){
                if(!oldMap.containsKey(eachProjectLine.Id)){
                    bidSubmitted += 1;
                    if(eachProjectLine.Selected_for_Use__c){
                        bidSelected += 1;
                    }
                }
                
            }
            //assigning the value of the number of bid made by a vendor and the number of bid own by a vendor
            eachVendor.Number_of_times_Bided__c = bidSubmitted;
            eachVendor.Number_of_times_selected__c = bidSelected;
        }
        //updating the Vendorist
        database.update(vendorList,false);
        
    }
    
    public static Map<string,decimal> fetchExchangeRates(){
         List<DatedConversionRate> rates = [SELECT Id, isoCode, Conversionrate, nextStartDate, startDate FROM DatedConversionRate WHERE StartDate <= TODAY AND NextStartDate > TODAY];
        Map<string,decimal> conversionRateMap=  new Map<string,decimal>();
        if(rates.size() >0){
            for(DatedConversionRate exchangerate : rates)
            {
                conversionRateMap.put(exchangerate.isoCode,exchangerate.Conversionrate);
            }
        }
        return conversionRateMap;
    }
    public static Map<Id,Opportunity> fetchOpportunities(List<Panels_Project_Vendor__c> panelProjectsLst){
        Set<Id> oppIds = new Set<Id>();
        for(Panels_Project_Vendor__c PrjVendor : panelProjectsLst){
             oppIds.add(PrjVendor.Opportunity__c);
        }

        Map<Id,Opportunity> opportunityEntries = new Map<Id,Opportunity>(
                                                   [Select id,currencyIsoCode from Opportunity  where Id IN: oppIds]);
                                                   
        return opportunityEntries;
    }
    
    public static void upsertBeforeEventHandler(List<Panels_Project_Vendor__c> PanelProjectsLst){
    
        Map<string,decimal> conversionRateMap = fetchExchangeRates();
        Map<Id,Opportunity> opportunityEntries = fetchOpportunities(PanelProjectsLst);
        
         for(Panels_Project_Vendor__c PrjVendor : panelProjectsLst){ 
             
            if(PrjVendor.Approved_Cost__c != null){
                PrjVendor.Approved_Amount__c = (PrjVendor.Approved_Cost__c/conversionRateMap.get(PrjVendor.CurrencyIsoCode))*conversionRateMap.get(opportunityEntries.get(PrjVendor.Opportunity__C).CurrencyIsoCode);
            }
            else if(PrjVendor.Approved_Cost__c == null){
                PrjVendor.Approved_Amount__c = null;
            }
        }
    }
}