public class PanelsProjectManagerRecommendationClass {
    
    public static void managerRecommendation(List<Opportunity> newOpps, Map<Id,Opportunity> oldMap){
        
        RecordType panelsList = [Select Id From RecordType WHERE Name='Panels' AND SobjectType='Opportunity'];
        Id panelsId = panelsList.Id;
        list<OpportunityTeamMember> otms = new List<OpportunityTeamMember>();
        User[] potentialProjectManagers = [SELECT Id, Project_Manager_Load__c FROM User WHERE Receive_Panels_Projects__c = true];
        for(Opportunity o : newOpps) {
           if (o.RecordTypeID == panelsId && o.Ready_for_PM__c == true && oldMap.get(o.id).Ready_for_PM__c == false && o.Project_Manager__c == null && oldMap.get(o.id).Project_Manager__c == null){
          
            Id projectManagerId = null;
                Double lowestLoad = -1;
                for(User u : potentialProjectManagers) {
                    if(u.Project_Manager_Load__c < lowestLoad || lowestLoad == -1){
                        projectManagerId = u.Id;
                        lowestLoad = u.Project_Manager_Load__c;
                    }
                }
                o.Project_Manager__c = projectManagerId;
                if(projectManagerId!=null){
                    OpportunityTeamMember otm = new OpportunityTeamMember(
                        TeamMemberRole = 'Panels Rep',
                        OpportunityId = o.Id,
                        UserId = projectManagerId
                    );
                    otms.add(otm);
                }
            }
        }
        
        if(!otms.isEmpty()){
            List<Database.SaveResult> results = Database.insert(otms, false);
        }
    }
    
}