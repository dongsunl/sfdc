@isTest
private class PartnerServiceAccountBatchTest {
  @isTest static void testBatch() {
    Account acctObj1 = TestFactory.createAccount();
    //acctObj1.IsPartner = true;
    insert acctObj1;
      
    Opportunity oppObj1 = TestFactory.createOpportunity(acctObj1.Id, Test.getStandardPricebookId());
    insert oppObj1;
      
    Quote q = TestFactory.createQuote(oppObj1.Id,Test.getStandardPricebookId());
    insert q;

    List<Q_Partner__c> testPartners = new List<Q_Partner__c>();
    Q_Partner__c partnerObj1 = TestFactory.createPartner(acctObj1.Id);
    Q_Partner__c partnerObj2 = TestFactory.createPartner(acctObj1.Id);
    Q_Partner__c partnerObj3 = TestFactory.createPartner(acctObj1.Id);
    Q_Partner__c partnerObj4 = TestFactory.createPartner(acctObj1.Id);
    testPartners.add(partnerObj1);
    testPartners.add(partnerObj2);
    testPartners.add(partnerObj3);
    testPartners.add(partnerObj4);
    insert testPartners;
      
    QService__c qs = new QService__c(Name='Test',Service_Type__c='Build Services',Q_Partner__c=partnerObj1.Id,Opportunity__c=oppObj1.Id,Quote__c=q.Id);
    insert qs;
    
    List<Partner_Service__c> testPartnerServices = new List<Partner_Service__c>([SELECT Id, Partner_Account__c, Q_Partner__r.RelatedAccount__c FROM Partner_Service__c]);
      System.debug('TEST PARTNER SERVICES ' + testPartnerServices);
      for (Partner_Service__c ps : testPartnerServices) {
          ps.Partner_Account__c = null;
      }
      update testPartnerServices;
      
    Test.startTest();
    Database.executeBatch(new PartnerServiceAccountBatch());
    Test.stopTest();
      
    System.assert([SELECT Id, Partner_Account__c, Q_Partner__r.RelatedAccount__c FROM Partner_Service__c WHERE Partner_Account__c = null AND Q_Partner__r.RelatedAccount__c != null].size() == 0);
  }
}