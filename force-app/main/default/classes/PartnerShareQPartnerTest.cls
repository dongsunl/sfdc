@isTest
private class PartnerShareQPartnerTest {

	@istest static void test1() {

        Account acctObj = TestFactory.createAccount();
        insert acctObj;

		acctObj.IsPartner = true;
		update acctObj;

		Contact cObj = new Contact(LastName = 'Test', AccountId = acctObj.Id);
		insert cObj;

        Q_Partner__c partnerObj = TestFactory.createPartner(acctObj.Id);
        insert partnerObj;



		Id sysAdminpId = [select id from profile where name='System Administrator'].id;

		User tempUser = [SELECT Id FROM User WHERE ProfileId =: sysAdminpId AND IsActive = TRUE LIMIT 1];

		Id p = [select id from profile where name='Partner Community User'].id;
	
        String orgId = UserInfo.getOrganizationId();
	    String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
    	Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
	    String uniqueName = orgId + dateString + randomInt;
        
		System.runAs(tempUser){
			User u1 = new User( email = uniqueName + '@test' + orgId + '.org',
	                profileid = p,
	                Username = uniqueName + '@test' + orgId + '.org',
	                Alias = 'GDS',
	                TimeZoneSidKey='America/New_York',
	                EmailEncodingKey='ISO-8859-1',
	                LocaleSidKey='en_US',
	                LanguageLocaleKey='en_US',
	                ContactId = cObj.Id,
	                PortalRole = 'Manager',
	                FirstName = 'Test',
	                LastName = 'User');
			insert u1;
		}

		Pricebook2 pb2Obj = TestFactory.createPriceBook();
        insert pb2Obj;

        Product2 prodObj = TestFactory.createProduct();
        insert prodObj;

        PricebookEntry standardPbe = TestFactory.createPricebookEntry(Test.getStandardPricebookId(), prodObj.Id);
        insert standardPbe;

        PricebookEntry customPbe = TestFactory.createPricebookEntry(pb2Obj.Id, prodObj.Id);
        insert customPbe;

		Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales Qualification').getRecordTypeId();

		// Opportunity oppObj = TestFactory.createOpportunity(acctObj.Id, pb2Obj.Id);
        // oppObj.Partner__c = partnerObj.Id;
		// oppObj.RecordTypeId = recordTypeId;
		// Test.startTest();
        //     insert oppObj;
        // Test.stopTest();

		Test.startTest();
		partnerObj.RelatedAccount__c = acctObj.Id;
		update partnerObj;
		Test.stopTest();

    }

	@isTest static void test_method_two() {
		// Implement test code
	}

}