public class Q2C_AttachmentsExtension {
    public String selectedType {get;set;}    
    public String description {get;set;}
    public Opportunity opp {get;set;} 
    public String fileName {get;set;}
    public Blob fileBody {get;set;}
    public String var {get;set;}
    public boolean alert;
    public User oppOwner {get;set;}
    
    public Q2C_AttachmentsExtension(ApexPages.StandardController controller) {
        opp = [SELECT Opportunity_Type__c, Client__c, AccountId, Amount, QSO_Attached__c, QSO_Requirement_Override__c, Owner_Global_Region__c, Owner.Contract_Automation_Pilot__c, Type
               FROM Opportunity 
               WHERE Id = :ApexPages.CurrentPage().getParameters().get('id')];
        oppOwner = new User(Id=opp.ownerId, Contract_Automation_Pilot__c=opp.Owner.Contract_Automation_Pilot__c);
    }
    
    // create an actual Attachment record with the opportunity as parent
    private Database.SaveResult saveStandardAttachment(Id parentId) {
        Database.SaveResult result;
        
        Attachment attachment = new Attachment();
        attachment.body = this.fileBody;
        attachment.name = selectedType;
        attachment.parentId = parentId;
        attachment.description = this.filename;
        // insert the attahcment
        result = Database.insert(attachment);
        // reset the file for the view state
        fileBody = Blob.valueOf(' ');
        return result;
    }
    
    public PageReference processUpload() {
        try {           
            
            Database.SaveResult attachmentResult = saveStandardAttachment(opp.Id);
            
            if (attachmentResult == null || !attachmentResult.isSuccess()) {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                                                           'Could not save attachment.'));            
                return null;
            } 
            
        } catch (Exception e) {
            ApexPages.AddMessages(e);
            return null;
        }
        
        PageReference page = ApexPages.currentPage();
        page.setRedirect(true);
        return page;
    }   
    
    public PageReference closeAtt() {
        consolidatedScreeningCheck(opp.Client__c,opp.AccountId,opp.Id);  
        if(
            ApexPages.CurrentPage().getParameters().get('returnToOpp') == 'false'
            && oppOwner.Contract_Automation_Pilot__c == true
        ){
            PageReference ReturnPage = new PageReference('/apex/cdr_GenerateDocs?id=' + opp.id); 
            ReturnPage.setRedirect(true); 
            return ReturnPage;
        } else {
            PageReference ReturnPage = new PageReference('/' + opp.id); 
            ReturnPage.setRedirect(true); 
            return ReturnPage;
        }        
    }
    
    public pageReference deleteAttachment(){
        
        try{
            Attachment att=new Attachment(id=var);
            delete att;
        }
        catch(Exception e){
            ApexPages.AddMessages(e);
        }
        
        PageReference page = ApexPages.currentPage();
        page.setRedirect(true);
        return page;
    }
    
    public boolean getAlert(){
        
        //Opportunity o = [SELECT Id, Amount, QSO_Attached__c, QSO_Requirement_Override__c, Owner_Global_Region__c, Owner.Contract_Automation_Pilot__c FROM Opportunity WHERE Id =: opp.Id];
        if(opp.Owner.Contract_Automation_Pilot__c = TRUE && opp.Amount >= 5000 && opp.QSO_Attached__c == FALSE && opp.QSO_Requirement_Override__c == FALSE){
            alert = TRUE;
        }
        else if(opp.Owner_Global_Region__c!='EMEA' && opp.Amount >= 75000 && opp.QSO_Attached__c == FALSE && opp.QSO_Requirement_Override__c == FALSE){
            alert = TRUE;
        }
        else if(opp.Owner_Global_Region__c=='EMEA' && opp.QSO_Attached__c == FALSE && opp.QSO_Requirement_Override__c == FALSE){
            alert = TRUE;
        }
        
        Return alert;
    }
    
    public void consolidatedScreeningCheck(Id clientId, Id accountId, Id opportunityId){
        if(opp.Opportunity_Type__c.contains('New License') || opp.Type.contains('New Deal')){
            ConsolidatedScreeningManager screeningCheck = new ConsolidatedScreeningManager(clientId , accountId, opportunityId);
        } 
    }
    
}