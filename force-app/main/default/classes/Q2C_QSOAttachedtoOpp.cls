public class Q2C_QSOAttachedtoOpp {
    public static void attachQSO(Attachment[] att){
        Set<Id> attSet = new Set<Id>();        
        for (Attachment a: att)
        {     
            if(a.Name.contains('QSO')){
                attSet.add(a.ParentId);
            }            
        }
        
        if(!attSet.isEmpty()){
            Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>([SELECT Id, QSO_Attached__c FROM Opportunity WHERE ID in:attSet]);
            Set<Id> oppSet = oppMap.keySet();
            
            List<Opportunity> opps = new List<Opportunity>();
            if(!oppSet.isEmpty()){
                for(Id o:oppSet){
                    Opportunity opp = new Opportunity();
                    opp.Id = o;
                    opp.QSO_Attached__c = TRUE;
                    opps.add(opp);
                }
                update opps;
            } 
        }        
    }
    
    public static void deleteQSO(Attachment[] oldatt){
        Set<Id> attSet = new Set<Id>();        
        for (Attachment a: oldatt)
        {     
            if(a.Name.contains('QSO')){
                attSet.add(a.ParentId);
            }            
        }
        
        if(!attSet.isEmpty()){
            Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>([SELECT Id, QSO_Attached__c FROM Opportunity WHERE ID in:attSet]);
            Set<Id> oppSet = oppMap.keySet();
            
            List<Opportunity> opps = new List<Opportunity>();
            if(!oppSet.isEmpty()){
                for(Id o:oppSet){
                    Opportunity opp = new Opportunity();
                    opp.Id = o;
                    opp.QSO_Attached__c = FALSE;
                    opps.add(opp);
                }
                update opps;
            } 
        }       
    }
    public static void attachPO(Attachment[] att){
        Set<Id> attSet = new Set<Id>();        
        for (Attachment a: att)
        {     
            if(a.Name.contains('PO')){
                attSet.add(a.ParentId);
            }            
        }
        
        if(!attSet.isEmpty()){
            Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>([SELECT Id, PO_Attached__c FROM Opportunity WHERE ID in:attSet]);
            Set<Id> oppSet = oppMap.keySet();
            
            List<Opportunity> opps = new List<Opportunity>();
            if(!oppSet.isEmpty()){
                for(Id o:oppSet){
                    Opportunity opp = new Opportunity();
                    opp.Id = o;
                    opp.PO_Attached__c = TRUE;
                    opps.add(opp);
                }
                update opps;
            } 
        }        
    }
    
    public static void deletePO(Attachment[] oldatt){
        Set<Id> attSet = new Set<Id>();        
        for (Attachment a: oldatt)
        {     
            if(a.Name.contains('PO')){
                attSet.add(a.ParentId);
            }            
        }
        
        if(!attSet.isEmpty()){
            Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>([SELECT Id, PO_Attached__c FROM Opportunity WHERE ID in:attSet]);
            Set<Id> oppSet = oppMap.keySet();
            
            List<Opportunity> opps = new List<Opportunity>();
            if(!oppSet.isEmpty()){
                for(Id o:oppSet){
                    Opportunity opp = new Opportunity();
                    opp.Id = o;
                    opp.PO_Attached__c = FALSE;
                    opps.add(opp);
                }
                update opps;
            } 
        }       
    }
}