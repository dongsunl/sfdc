public class Q2C_RedirectOnboardingController {
    
    // Instanciate the Flow for use by the Controller - linked by VF "interview" attribute
    public Flow.Interview.Q2C_Billing_Information flDemo {get;set;}
     
    // Factor your PageReference as a full GET/SET
    public PageReference prFinishLocation {
        get {
            if(ApexPages.CurrentPage().getParameters().get('returnToOpp') != null){
                PageReference prRef = new PageReference('/apex/Q2C_AttachmentsPage?id=' + strOutputVariable + 
                    '&returnToOpp=' + ApexPages.CurrentPage().getParameters().get('returnToOpp'));
                prRef.setRedirect(true);
                return prRef;
            } else {
                PageReference prRef = new PageReference('/apex/Q2C_AttachmentsPage?id=' + strOutputVariable);
                prRef.setRedirect(true);
                return prRef;
            }
        }
        set { prFinishLocation = value; }
    }
    
    // Factor your Flow output variable pull as a full GET / SET
    public String strOutputVariable {
        get {
            String strTemp = '';
            
            if(flDemo != null) {
                strTemp = string.valueOf(flDemo.getVariableValue('varOppId'));
            }
            return strTemp;
        }
        set { strOutputVariable = value; }
    } 
    
   
}