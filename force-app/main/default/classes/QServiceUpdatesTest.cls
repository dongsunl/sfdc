@IsTest
public class QServiceUpdatesTest {
    
    static Opportunity opportunityObj {get;set;}
    static Quote quoteObj {get;set;}
    static QService__c serviceObj {get;set;}
     
    
    @isTest
    static void Test_testServiceCancelled(){
        TestFactory.disableTriggers();
        AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
        Insert avaCustomSetting;
        Account accountObj = TestFactory.createAccount();
        insert accountObj;
        Pricebook2 pb = TestFactory.createPriceBook();
        insert pb;
        Product2 prod = TestFactory.createProduct();
        insert prod;
        opportunityObj = TestFactory.createOpportunity(accountObj.Id, pb.Id);
        insert opportunityObj;
        quoteObj = TestFactory.createQuote(opportunityObj.Id, pb.Id);
        insert quoteObj;
        opportunityObj.SyncedQuoteId = quoteObj.Id;
        update opportunityObj;
        Contract_Document__c contractDocObj = TestFactory.createContractDoc2(opportunityObj.Id);
        contractDocObj.Implementation__c = 'imp_cx1_sow';
        insert contractDocObj;
        Bundle__c bundleObj = TestFactory.createBundle();
        insert bundleObj;
        Product_Bundle_Junction__c bundleProductObj = TestFactory.createBundleProduct(bundleObj.Id);
        insert bundleProductObj;
        Service_Bundle__c serviceBundleObj = TestFactory.createServiceBundle(bundleProductObj.Id);
        insert serviceBundleObj;
        serviceObj = TestFactory.createService(opportunityObj.Id, quoteObj.Id);
        
        insert serviceObj;
        serviceObj.Status__c = 'Request Cancelled';
        serviceObj.Cancel_Allowed__c = true;
        
       // system.debug('serviceObj:'+serviceObj);
        update serviceObj;
        
    }
    
    
    
}