@istest
public class QualifiedContactFieldUpdateTest{
public static testmethod void QualifiedContactFieldUpdateTest(){
    account a = new account(name='test');
    a.BillingState = 'Utah';
    a.BillingCountry = 'USA';
    a.BillingPostalCode = '84602';
    insert a;
    opportunity o = new opportunity(name='test',stagename='Additional Presentations',closedate=date.today(), Research_Suite_Value__c = 1.00 );
    contact c = new contact(
        firstname='fname',
        lastname='lname',
        Currently_engaged_in_data_collection__c = 'Yes',
        Method_of_data_collection__c = 'In House',
        Current_Solution__c = 'Monetate',
        Research_Target__c = 'Consumer',
        Current_method_details__c = 'cmd',
        Responses_per_Year__c = 5,
        Not_involved_in_data_collection__c = 'No Interest',
        Anticipated_Use_Case__c = 'Employee Feedback',
        Contract_Expiration__c = date.today().adddays(5),
        Timeframe__c = '2-4 months',
        Project_Status__c = 'Informal, probable vendor selection',
        Qualification_Notes__c = 'aaaaa'
        );
    insert c;
        
    o.Qualification_Contact__c = c.id;
    insert o;
    
    OpportunityContactRole CR = new OpportunityContactRole();
    CR.ContactID = c.ID;
    CR.OpportunityID = o.ID;
    insert CR;
    
    opportunity o1 = [select Currently_engaged_in_data_collection__c,Anticipated_Use_Case__c,Qualification_Notes__c from opportunity where id = :o.id];
    system.assertequals(o1.Currently_engaged_in_data_collection__c, 'Yes');
    system.assertequals(o1.Anticipated_Use_Case__c, 'Employee Feedback');
    system.assertequals(o1.Qualification_Notes__c, 'aaaaa');
    
    /*
    c.Currently_engaged_in_data_collection__c = 'No';
    c.Anticipated_Use_Case__c = 'Student Feedback';
    c.Qualification_Notes__c = 'bbbbb';
    update c;
    opportunity o3 = new opportunity(name='test',stagename='Additional Presentations',closedate=date.today(), Research_Suite_Value__c = 1.00 );
    insert o3;
    o3.Qualification_Contact__c = c.id;
    update o3;
    opportunity o2 = [select Currently_engaged_in_data_collection__c,Anticipated_Use_Case__c,Qualification_Notes__c from opportunity where id = :o3.id];
    system.assertequals(o2.Currently_engaged_in_data_collection__c, 'No');
    system.assertequals(o2.Anticipated_Use_Case__c, 'Student Feedback');
    system.assertequals(o2.Qualification_Notes__c, 'bbbbb');
*/
}
}