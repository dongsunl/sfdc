global class QualtricsMavenlinkWSAMasterBatch implements Database.Batchable<String>, Database.AllowsCallouts, Database.Stateful{
	private static string c = 'QualtricsMavenlinkWSAMasterBatch';
	private String updatedAfter {get;set;}
	
	
	
	
	public QualtricsMavenlinkWSAMasterBatch(String updatedAfter){
		this.updatedAfter = updatedAfter;
	}
	
	global List<String> start(Database.BatchableContext context) {
        String m = 'start';
        try{
        		mavenlink.MavenlinkLogExternal.Save();
        return new String[]{updatedAfter};
        } catch(Exception e){mavenlink.MavenlinkLogExternal.Write(c,m,'Error = ('+e.getStackTraceString()+') '+e.getMessage()); mavenlink.MavenlinkLogExternal.Save(); return null;}
    }
	
	global void execute(Database.BatchableContext context, List<String> strings){
    	String m = 'execute';
    	try{
    		   mavenlink.MavenlinkLogExternal.Save();
        } catch(Exception e){mavenlink.MavenlinkLogExternal.Write(c,m,'Error = ('+e.getStackTraceString()+') '+e.getMessage()); mavenlink.MavenlinkLogExternal.Save();}
    }
    
    global void finish(Database.BatchableContext ctx) {
        String m = 'finish';
        String testWSA = '{'+
    							'"count": 1,'+
    							'"results": ['+
    								'{"key": "workspace_allocations","id": "16783325"}'+
    							'],'+
    							'"workspace_allocations": {'+
    								'"16783325": {'+
    									'"start_date": "2018-06-26",'+
    									'"workspace_resource_id": "119281205",'+
    									'"created_at": "2018-07-24T13:19:05-07:00",'+
            							'"updated_at": "2018-08-10T18:50:25-07:00",'+
    									'"id": "16783325"'+
								'}'+
							'},'+
							'"workspace_resources": {'+
								'"119281205": {'+
									'"role_name": "PS.Something"'+
								'}'+
							'}'+
						'}';
        try{
        		
        		Map<String,Object> workspaceAllocations = null;
        		Map<String,Object> workspaceResources = null;
			Map<String,String> resourceWorkspaceMap = new Map<String,String>();
    			Map<String,String> allocationResourceMap = new Map<String,String>();
    			Set<String> workspaceIds = new Set<String>();
        		mavenlink.MavenlinkLogExternal.Write(c,m,'updatedAfter = '+updatedAfter);           
			
			Integer totalCount=0;
        		Integer totalPages=0;
            Map<String,Object> r = null;
            if (!Test.isRunningTest()){
            		r = mavenlink.MavenlinkExternalAPI.callAPI('/workspace_allocations.json?updated_after='+updatedAfter+'&include=workspace_resource&per_page=1', 'GET', null);
            }else{
            		r = (Map<String,Object>) JSON.deserializeUntyped(testWSA);
            }
            totalCount=Integer.valueOf(r.get('count')==null? 0 : r.get('count'));
	        	mavenlink.MavenlinkLogExternal.Write(c,m,'('+updatedAfter+')'+'Total count='+totalCount);
	        	totalPages=(totalCount/200)+1;
	        	mavenlink.MavenlinkLogExternal.Write(c,m,'('+updatedAfter+')'+'Total Pages='+totalPages);
            
            	for(Integer j=1;j<=totalPages;j++){ //1
            		if (!Test.isRunningTest()){
            			r = mavenlink.MavenlinkExternalAPI.callAPI('/workspace_allocations.json?updated_after='+updatedAfter+'&include=workspace_resource&per_page=200', 'GET', null);
            		}else{
            			r = (Map<String,Object>) JSON.deserializeUntyped(testWSA);
            		}
	            //mavenlink.MavenlinkLogExternal.Write(c,m,'r = '+r);           
	            workspaceAllocations = (Map<String,Object>)r.get('workspace_allocations');
	            mavenlink.MavenlinkLogExternal.Write(c,m,'workspaceAllocations = '+workspaceAllocations.size()); 
	            workspaceResources = (Map<String,Object>)r.get('workspace_resources');
	            mavenlink.MavenlinkLogExternal.Write(c,m,'workspaceResources = '+workspaceResources.size()); 
	
	            //Set<String> workspaceIds = new Set<String>();
	            //Map<String,String> resourceWorkspaceMap = new Map<String,String>();
	            //Map<String,String> allocationResourceMap = new Map<String,String>();
	            for (String key:workspaceResources.keySet()){
	                String workspaceId = String.valueOf((((Map<String,Object>)workspaceResources.get(key)).get('workspace_id')));
	                workspaceIds.add(workspaceId);
	                resourceWorkspaceMap.put(key,workspaceId);
	            }
	            for (String key:workspaceAllocations.keySet()){
	                String workspaceResourceId = String.valueOf((((Map<String,Object>)workspaceAllocations.get(key)).get('workspace_resource_id')));
	                allocationResourceMap.put(key,workspaceResourceId);
	            }
            	}
			
        		if(!Test.isRunningTest()) 
        			if(workspaceIds.size()>0 || (workspaceAllocations!=null && workspaceAllocations.size()>0))
        			Database.executeBatch( new QualtricsMavenlinkWSAllocBatch(new Set<String>(),workspaceIds.size(),1,workspaceIds,
                    workspaceAllocations,workspaceResources,resourceWorkspaceMap,allocationResourceMap), 1);
    			mavenlink.MavenlinkLogExternal.Write(c,m,'Completed all Workspaces Allocations;'+workspaceAllocations.size()+'for '+workspaceIds.size()+ ' worskpaces');
        
        mavenlink.MavenlinkLogExternal.Save();
        } catch(Exception e){mavenlink.MavenlinkLogExternal.Write(c,m,'Error = ('+e.getStackTraceString()+') '+e.getMessage()); mavenlink.MavenlinkLogExternal.Save();}
    }
    
}