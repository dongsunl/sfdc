public class QualtricsMavenlinkWSAllocSchedulable implements Schedulable, Database.AllowsCallouts{
    private static string c = 'QualtricsMavenlinkWSAllocSchedulable';
    public static string jobName = 'qualtrics_mavenlink_workspace_allocations';
    
    public void execute(SchedulableContext sc) {
        String m = 'execute';
        try{
        mavenlink.MavenlinkLogExternal.Write(c,m,'Start SyncSchedulable');
        DateTime backDate=getBackDate();
        String updatedAfter= (string.valueOfGmt(backDate)).replace(':', '-').replace(' ', 'T').replace('Z', ''); 
        mavenlink.MavenlinkLogExternal.Write(c,m,'updatedAfter = '+updatedAfter);
        
        if(!Test.isRunningTest()) {
			Database.executeBatch( new QualtricsMavenlinkWSAMasterBatch(updatedAfter), 1);
        }

		
        mavenlink.MavenlinkLogExternal.Write(c,m,'End SyncSchedulable');
        mavenlink.MavenlinkLogExternal.Save();
        } catch(Exception e){mavenlink.MavenlinkLogExternal.Write(c,m,'Error = ('+e.getStackTraceString()+') '+e.getMessage()); mavenlink.MavenlinkLogExternal.Save();}
    }

    private static DateTime getBackDate(){
        String m='getBackDate';
        DateTime backDate=datetime.newInstanceGmt(2000, 01, 01);
        mavenlink.MavenlinkLogExternal.Write(c,m,'Entering method');
        try{  
            CronJobDetail CJD =[select Id, Name from CronJobDetail Where Name=:jobName Limit 1]; 
            CronTrigger CT = [select Id, NextFireTime, PreviousFireTime, StartTime from CronTrigger Where CronJobDetailId=:CJD.Id Limit 1];  
            if(CT.PreviousFireTime!=null){  
                datetime prevTime = CT.PreviousFireTime;datetime nextTime = CT.NextFireTime;   
                Long milliseconds = nextTime.getTime() - prevTime.getTime();Long duration = milliseconds / 1000 / 60;duration = ((duration * 1) + 30)*(-1); //Go back an additional 30 minutes                    
                backDate=dateTime.now().addMinutes((Integer)duration);   
            }
        }  catch(Exception e){mavenlink.MavenlinkLogExternal.Write(c,m,'Error = ('+e.getStackTraceString()+') '+e.getMessage()); return null;}      
        mavenlink.MavenlinkLogExternal.Write(c,m,'Exiting method');
        return backDate;                
    }
    
    
    
}