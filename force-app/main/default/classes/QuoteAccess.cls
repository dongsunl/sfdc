public with sharing class QuoteAccess {

	@AuraEnabled
    public static List<String> getQuotingRequirements(Id id) {
        
        if (!Schema.SObjectType.Quote.isQueryable()
	            || !Schema.SObjectType.Quote.fields.Id.isAccessible()
	            || !Schema.SObjectType.Quote.fields.Quoting_Requirements__c.isAccessible()) {
            throw new InvalidPermissionsException(InvalidPermissionsException.QUOTE_ACCESS_MESSAGE);
        }
        
        List<Quote> records =
            [SELECT Quoting_Requirements__c
             FROM Quote
             WHERE Id = : id
             LIMIT 1];
		if (records.size() == 1) {
			Quote record = records[0];
            List<String> quotingRequirements = new List<String>();
            if (record.Quoting_Requirements__c != null) {
				quotingRequirements = records[0].Quoting_Requirements__c.split(';');
            }
            return quotingRequirements;
        }
        throw new QueryException('Record not found');
    }
}