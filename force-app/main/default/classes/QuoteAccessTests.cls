@IsTest
public with sharing class QuoteAccessTests {

    testMethod static void testGetQuotingRequirements() {
        
        // Prepare for tests
        Account testAccount = new Account();
        testAccount.Name='Test Account';
        insert testAccount;
        
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name='Test Opportunity';
        testOpportunity.AccountId=testAccount.Id;
        testOpportunity.StageName='Pending';
        testOpportunity.CloseDate=System.today();
        testOpportunity.Turn_Off_Initial_CX_EX_RC_Amounts__c=true;
        insert testOpportunity;
        
        Quote testQuote = new Quote();
        testQuote.Name='Test Quote';
        testQuote.OpportunityId=testOpportunity.Id;
        insert testQuote;
        
        List<String> quotingRequirements;
        
        Test.startTest();
        
        // Successful test with null Quoting_Requirements__c
        quotingRequirements = QuoteAccess.getQuotingRequirements(testQuote.Id);
        
        // Successful test with Quoting_Requirements__c
        testQuote.QuotingRequirementsOverride__c  = false;
        testQuote.ExpirationDate = System.Today().addDays(-1);
        update testQuote;
        quotingRequirements = QuoteAccess.getQuotingRequirements(testQuote.Id);
        
        // Failed test
        try {
	        quotingRequirements = QuoteAccess.getQuotingRequirements(null);
        } catch (QueryException e) {
            System.assertEquals(e.getMessage(), 'Record not found');
        }

        Test.stopTest();
    }
}