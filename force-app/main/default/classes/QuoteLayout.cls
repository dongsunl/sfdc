public with sharing class QuoteLayout {

	public Quote quoteObj {get;set;}
	public Map<String, List<Quote_Layout__mdt>> metadataMap {get;set;}
	public List<String> sectionList {get;set;}
	@testVisible private List<Quote_Layout__mdt> quoteLayoutMdtList {get;set;}
	public String syncStatus {get;set;}
	public Opportunity oppObj {get;set;}
	public List<QService__c> serviceList {get;set;}
    public list<Service_Component__c> serviceComponentList {get;set;} 
	public List<Partner_Related_Products__c> partnerList {get;set;}
	public List<Case> caseList {get;set;}
	public List<QuoteLineItem> qliList {get;set;}
	public List<Deal_Coordination__mdt> metaData {get;set;}
    public Map<String,String> dateRequestedMap {get;set;} //This will be for the Date Requested Lookup.  It will be the field name as the key and the value from the Quote as the Value
    public Map<String,String> statusMap {get;set;} //This will be the field name as the key and the value from the Quote as the Value.
    public String actionToTake {get;set;}
    public User userObj {get;set;}
    public Boolean useCustomLayout {get;set;}
    
    
    
    public PageReference runWhenPageLoads(){
    
    	if(ApexPages.CurrentPage().getParameters().get('Id') == null){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No Id was loaded with the page.  Please try opening the Quote again.');
			ApexPages.addMessage(myMsg);
			return null;
		}
		Profile userProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
		List<Quote_Layout_Assignment__mdt> quoteLayoutAssignmentList = [SELECT Profile_Name__c, RecordType_name__c, Use_Custom_Layout__c
																		FROM Quote_Layout_Assignment__mdt
																		WHERE Profile_Name__c = :userProfile.Name];
		system.debug(logginglevel.error, '***** quoteLayoutAssignmentList='+quoteLayoutAssignmentList);
		if(quoteLayoutAssignmentList == null || quoteLayoutAssignmentList.size() == 0){
			return redirectToWithoutOverride();
		}
        
		quoteObj = queryQuote();
		List<RecordType> quoteRecordType = [SELECT Name FROM RecordType WHERE Id = :quoteObj.RecordTypeId Limit 1];
		system.debug(logginglevel.error, '***** quoteRecordType='+quoteRecordType);
		useCustomLayout = false;
		for(Quote_Layout_Assignment__mdt quoteLayoutMdt : quoteLayoutAssignmentList){
			if(
				(quoteLayoutMdt.RecordType_name__c == null 
				|| quoteLayoutMdt.RecordType_name__c == quoteRecordType[0].Name)
				&& quoteLayoutMdt.Use_Custom_Layout__c != true
			){
				return redirectToWithoutOverride();
			} else if(
				(quoteLayoutMdt.RecordType_name__c == null 
				|| quoteLayoutMdt.RecordType_name__c == quoteRecordType[0].Name)
				&& quoteLayoutMdt.Use_Custom_Layout__c == true
			){
				useCustomLayout = true;
				break;
			}
		}
		if(useCustomLayout == false){
			return redirectToWithoutOverride();
		}
		quoteLayoutMdtList = [SELECT MasterLabel,Display_Order__c,Field_API_Name__c,Is_Editable__c,Quote_Layout_Section__c,Quote_Layout_Section__r.MasterLabel
								FROM Quote_Layout__mdt
								ORDER BY Quote_Layout_Section__c,Display_Order__c];
		oppObj = queryOpportunity();
		if(oppObj.SyncedQuoteId == null){
			syncStatus = 'None';
		} else if(oppObj.SyncedQuoteId == quoteObj.Id){
			syncStatus = 'This';
		} else {
			syncStatus = 'Other';
		}
		createMetadataMap();
		populateMetadataMap();
		queryService();
        queryServiceComponent();
		queryPartner();
		queryCases();
		queryQli();
		metaData = [SELECT DeveloperName,Javascript_Button__c,MasterLabel,Requested_Date_Lookup__c,SLA__c,Status_Lookup__c,Request_Button_Name__c,Display_Order__c,Completed_Status__c
                    FROM Deal_Coordination__mdt
                    WHERE Availability__c = 'Available' OR Availability__c = 'Required' ORDER BY Display_Order__c];
		userObj = queryUser();
        populateMaps();
        return null;
    }

	public QuoteLayout(ApexPages.StandardController controller) {
		
	}

	public PageReference redirectToWithoutOverride(){
        string BaseURL = ApexPages.currentPage().getURL();
        if(BaseURL.contains('stopQuoteSync=1')){
             Try{
			Quote quoteObj = [SELECT OpportunityId, IsSyncing FROM Quote WHERE Id = :ApexPages.CurrentPage().getParameters().get('Id')];
			System.debug('>>>quoteObj :' + quoteObj);
			Opportunity oppObj = [SELECT SyncedQuoteId FROM Opportunity WHERE Id = :quoteObj.OpportunityId];
			System.debug('>>>oppObj :' + oppObj);
			oppObj.SyncedQuoteId = null;
			System.debug(logginglevel.INFO,'==oppObj===' + oppObj);
			update oppObj;
		}
		Catch(exception e){
			system.debug(logginglevel.error, '*****'+e);
		}
            
        }
       
        
		system.debug(logginglevel.error, '***** Start redirectToWithoutOverride *****');
		Pagereference MyPage = new Pagereference(Url.getSalesforceBaseUrl().toExternalForm() + '/'+ApexPages.CurrentPage().getParameters().get('Id')+'?nooverride=1');
        system.debug('mypage:'+MyPage);
		MyPage.setRedirect(true);
		return MyPage;
	}

	@testVisible
	private void queryQli(){
		String qry = 'SELECT ';
		for(Schema.FieldSetMember memberField : SObjectType.QuoteLineItem.FieldSets.QuoteLayoutRelatedList.getFields()){
			qry += memberField.getFieldPath() + ',';
		}
		qry = qry.removeEnd(',');
		Id quoteId = quoteObj.Id;
		qry += ' FROM QuoteLineItem WHERE QuoteId = :quoteId';
		system.debug(logginglevel.error, '***** qry='+qry);
		qliList = Database.Query(qry);
	}

	@testVisible
	private void queryCases(){
		String qry = 'SELECT ';
		for(Schema.FieldSetMember memberField : SObjectType.Case.FieldSets.QuoteLayoutRelatedList.getFields()){
			qry += memberField.getFieldPath() + ',';
		}
		qry = qry.removeEnd(',');
		Id quoteId = quoteObj.Id;
		qry += ' FROM Case WHERE Related_Quote__c = :quoteId';
		system.debug(logginglevel.error, '***** qry='+qry);
		caseList = Database.Query(qry);
	}

	@testVisible
	private void queryService(){
		String qry = 'SELECT ';
		for(Schema.FieldSetMember memberField : SObjectType.QService__c.FieldSets.QuoteLayoutRelatedList.getFields()){
			qry += memberField.getFieldPath() + ',';
		}
		qry = qry.removeEnd(',');
		Id quoteId = quoteObj.Id;
        qry += ' FROM QService__c WHERE Quote__c = :quoteId';
        system.debug(logginglevel.error, '***** qry='+qry);
        serviceList = Database.Query(qry);
    }
    
    @testVisible
    private void queryServiceComponent(){
        String qry = 'SELECT ';
        for(Schema.FieldSetMember memberField : SObjectType.Service_Component__c.FieldSets.QuoteLayoutRelatedList.getFields()){
            qry += memberField.getFieldPath() + ',';
        }
        qry = qry.removeEnd(',');
        Id quoteId = quoteObj.Id;
        qry += ' FROM Service_Component__c WHERE Related_Quote__c = :quoteId';
        system.debug(logginglevel.error, '***** qry='+qry);
        serviceComponentList = Database.Query(qry);
    }
    
    @testVisible
    private void queryPartner(){
        String qry = 'SELECT ';
        for(Schema.FieldSetMember memberField : SObjectType.Partner_Related_Products__c.FieldSets.QuoteLayoutRelatedList.getFields()){
			qry += memberField.getFieldPath() + ',';
		}
		qry = qry.removeEnd(',');
		Id quoteId = quoteObj.Id;
		qry += ' FROM Partner_Related_Products__c WHERE Quote__c = :quoteId';
		system.debug(logginglevel.error, '***** qry='+qry);
		partnerList = Database.Query(qry);
	}

	@testVisible
	private Opportunity queryOpportunity(){
		Id oppId = quoteObj.OpportunityId;
		return [SELECT SyncedQuoteId FROM Opportunity WHERE Id = :oppId];
	}

	@testVisible
	private void populateMetadataMap(){
		for(Quote_Layout__mdt quoteLayoutMdt : quoteLayoutMdtList){
			metadataMap.get(quoteLayoutMdt.Quote_Layout_Section__r.MasterLabel).add(quoteLayoutMdt);
		}
	}

	@testVisible
	private void createMetadataMap(){
		metadataMap = new Map<String, List<Quote_Layout__mdt>>();
		sectionList = new List<String>();
		for(Quote_Layout_Section__mdt section : [SELECT Display_Order__c, MasterLabel FROM Quote_Layout_Section__mdt ORDER BY Display_Order__c]){
			metadataMap.put(section.MasterLabel, new List<Quote_Layout__mdt>());
			sectionList.add(section.MasterLabel);
		}
	}

	@testVisible
	private Quote queryQuote(){
		Map<String, Schema.SObjectField> SobjtField = Schema.getGlobalDescribe().get('Quote').getDescribe().fields.getMap();
        string qry = '';
        for (Schema.SObjectField s : SobjtField.values()) {
            qry += s + ',';
        }
        qry = qry.removeend(',');
        qry = 'SELECT '+qry+' FROM '+ 'Quote' + ' WHERE Id = \'' + ApexPages.CurrentPage().getParameters().get('Id') + '\'';
        system.debug(logginglevel.error, '***** qry='+qry);
        return Database.query(qry);
	}

	public PageReference saveIt(){
		Try{
			update quoteObj;
			Pagereference MyPage = new Pagereference('/'+quoteObj.Id);
			MyPage.setRedirect(true);
			return MyPage;
		}
		Catch(exception e){
			system.debug(logginglevel.error, '*****'+e);
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0));
			ApexPages.addMessage(myMsg);
			return null;
		}
	}

    @remoteAction
	public static void syncIt(String quoteId){
		Try{
			Quote quoteObj = [SELECT OpportunityId, IsSyncing, Service_Template_Codes__c, Partner_Email__c FROM Quote WHERE Id = :quoteId];
			Opportunity oppObj = [SELECT Id, SyncedQuoteId FROM Opportunity WHERE Id = :quoteObj.OpportunityId];
            Contract_Document__c cd = [SELECT Implementation__c FROM Contract_Document__c WHERE Opportunity__c = :oppObj.Id];
            cd.Implementation__c = quoteObj.Service_Template_Codes__c;
            cd.Partner_Email__c = quoteObj.Partner_Email__c;
			oppObj.SyncedQuoteId = quoteObj.Id;
            oppObj.Service_Edited__c = true;
            update cd;
			update oppObj;
		}
		Catch(exception e){
			system.debug(logginglevel.error, '*****'+e);
		}
	}

	@remoteAction
	public static void stopSync(String quoteId){
		System.debug('>>>stopSync');
		System.debug('>>>quoteId :' + quoteId);
		Try{
			Quote quoteObj = [SELECT OpportunityId, IsSyncing FROM Quote WHERE Id = :quoteId];
			System.debug('>>>quoteObj :' + quoteObj);
			Opportunity oppObj = [SELECT SyncedQuoteId FROM Opportunity WHERE Id = :quoteObj.OpportunityId];
			System.debug('>>>oppObj :' + oppObj);
			oppObj.SyncedQuoteId = null;
			System.debug(logginglevel.INFO,'==oppObj===' + oppObj);
			update oppObj;
		}
		Catch(exception e){
			system.debug(logginglevel.error, '*****'+e);
		}
	}

    @testVisible
    private User queryUser(){
        Map<String, Schema.SObjectField> SobjtField = Schema.getGlobalDescribe().get('User').getDescribe().fields.getMap();
        string qry = 'Profile.Name,';
        for (Schema.SObjectField s : SobjtField.values()) {
            qry += s + ',';
        }
        qry = qry.removeend(',');
        Id userId = UserInfo.getUserId();
        qry = 'SELECT '+qry+' FROM '+ 'User' + ' WHERE Id = :userId';
        return Database.query(qry);
    }

    @testVisible
    private void populateMaps(){
        dateRequestedMap = new Map<String,String>{null => '-'};
        statusMap = new Map<String,String>{null => '-'};
        Map<String, Schema.SObjectField> SobjtField = Schema.getGlobalDescribe().get('Quote').getDescribe().fields.getMap();
        for(Deal_Coordination__mdt dealCoordinationObj : metaData){
            if(dealCoordinationObj.Requested_Date_Lookup__c != null){
                if(!SobjtField.containsKey(dealCoordinationObj.Requested_Date_Lookup__c)){
                    dateRequestedMap.put(dealCoordinationObj.Requested_Date_Lookup__c,'Field does not exist.');
                } else if(quoteObj.get(dealCoordinationObj.Requested_Date_Lookup__c) != null){
                    Date dateFieldValue;
                    Try{
                        dateFieldValue = (Date)quoteObj.get(dealCoordinationObj.Requested_Date_Lookup__c);
                    }
                    Catch(exception e){
                        system.debug(logginglevel.error, '*****'+e);
                        dateFieldValue = Date.valueOf((Datetime)quoteObj.get(dealCoordinationObj.Requested_Date_Lookup__c));
                    }
                    dateRequestedMap.put(dealCoordinationObj.Requested_Date_Lookup__c,dateFieldValue.format());
                } else {
                    dateRequestedMap.put(dealCoordinationObj.Requested_Date_Lookup__c,'-');
                }
            }
            if(dealCoordinationObj.Status_Lookup__c != null){
                if(!SobjtField.containsKey(dealCoordinationObj.Status_Lookup__c)){
                    statusMap.put(dealCoordinationObj.Status_Lookup__c,'Field does not exist.');
                } else if(quoteObj.get(dealCoordinationObj.Status_Lookup__c) != null){
                    statusMap.put(dealCoordinationObj.Status_Lookup__c,(String)quoteObj.get(dealCoordinationObj.Status_Lookup__c));
                } else {
                    statusMap.put(dealCoordinationObj.Status_Lookup__c,'-');
                }
            }
        }
    }

    public PageReference pricingAdjustmentClass(){
    	system.debug(logginglevel.error, '***** Start pricingAdjustmentClass *****');
    	Pagereference MyPage = new Pagereference('/apex/XM_Discounts?Id='+quoteObj.Id);
    	MyPage.setRedirect(true);
    	return MyPage;
    }
}