@isTest
public with sharing class QuoteLayoutTest {
	/**
		for this test class to work correctly there needs to be a Quote_Layout_Assignment__mdt with custom layout and without. 
			1. for the custom layout there needs to be a Quote_Layout_Assignment__mdt with Use_Custom_Layout__c set to true and an active user with the profile found in Profile_Name__c
			2. for without the custom layout there needs to be a Quote_Layout_Assignment__mdt with Use_Custom_Layout__c set to false and an active user with the profile found in Profile_Name__c
		also if the coverage is not right make sure that the record types are correct.
	*/

	//the static variables here will be created in the test setup for each test method and then will be accessable in the method
	static Account acct {get;set;}
    static Opportunity opp {get;set;}
	static Quote quoteTrueObj {get;set;}
	static Quote quoteFalseObj {get;set;}
	static list<QuotelineItem> lstQLI {get;set;}
	static Quote_Layout_Assignment__mdt quoteLayoutTrue {get;set;}
	static Quote_Layout_Assignment__mdt quoteLayoutFalse {get;set;}
	static RecordType recordTypeTrueObj {get;set;}
	static RecordType recordTypeFalseObj {get;set;}
	static User userTrueObj {get;set;}
	static User userFalseObj {get;set;}
    //the test setup runs when every test method starts.
    //Any objects you need to create that will be used by more than one method should be created here.
    @testsetup
    static void test_setup(){
        AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
    Insert avaCustomSetting;
		lstQLI = new list<QuotelineItem>();
    	TestFactory.disableTriggers();
    	acct = TestFactory.createAccount();
    	insert acct;

    	opp = TestFactory.createOpportunity(acct.Id, null);
    	insert opp;
    	Set<String> availabeProfiles = new Set<String>();
    	List<AggregateResult> userActiveList = [SELECT Profile.Name pn FROM User WHERE IsActive = true GROUP BY Profile.Name];
    	for(AggregateResult userActiveObj : userActiveList){
    		availabeProfiles.add((String)userActiveObj.get('pn'));
    	}
		Try{
			quoteLayoutTrue = [SELECT Profile_Name__c, RecordType_name__c, Use_Custom_Layout__c
								FROM Quote_Layout_Assignment__mdt
								WHERE Use_Custom_Layout__c = true AND RecordType_name__c != null AND Profile_Name__c IN :availabeProfiles LIMIT 1];
		}
		Catch(exception e){
			Try{
				quoteLayoutTrue = [SELECT Profile_Name__c, RecordType_name__c, Use_Custom_Layout__c
									FROM Quote_Layout_Assignment__mdt
									WHERE Use_Custom_Layout__c = true AND Profile_Name__c IN :availabeProfiles LIMIT 1];
			}
			Catch(exception e1){
				system.assert(false, 'There are no Quote Layout Assignments with Use Custom Layout set to true and an active user for that profile.  This test class cannot continue.');
			}
				
		}		
    	quoteTrueObj = TestFactory.createQuote(opp.Id, Test.getStandardPricebookId());
		quoteTrueObj.Name = 'Quote True';
    	if(quoteLayoutTrue.RecordType_name__c != null){
    		recordTypeTrueObj = [SELECT Id FROM RecordType WHERE sobjectType = 'Quote' AND Name = :quoteLayoutTrue.RecordType_name__c];
			quoteTrueObj.recordtypeId = recordTypeTrueObj.Id;
    	}
    	insert quoteTrueObj;
    	Try{
    		quoteLayoutFalse = [SELECT Profile_Name__c, RecordType_name__c, Use_Custom_Layout__c
								FROM Quote_Layout_Assignment__mdt
								WHERE Use_Custom_Layout__c = false AND RecordType_name__c != null AND Profile_Name__c IN :availabeProfiles LIMIT 1];
    	}
    	Catch(exception e){
    		Try{
    			quoteLayoutFalse = [SELECT Profile_Name__c, RecordType_name__c, Use_Custom_Layout__c
									FROM Quote_Layout_Assignment__mdt
									WHERE Use_Custom_Layout__c = false AND Profile_Name__c IN :availabeProfiles LIMIT 1];
    		}
    		Catch(exception e1){
    			system.assert(false, 'There are no Quote Layout Assignments with Use Custom Layout set to false and an active user for that profile.  This test class cannot continue.');
    		}
	    		
    	}
    	quoteFalseObj = TestFactory.createQuote(opp.Id, Test.getStandardPricebookId());
		quoteFalseObj.Name = 'Quote False';
    	if(quoteLayoutFalse.RecordType_name__c != null){
    		recordTypeFalseObj = [SELECT Id FROM RecordType WHERE sobjectType = 'Quote' AND Name = :quoteLayoutFalse.RecordType_name__c];
			quoteFalseObj.recordtypeId = recordTypeFalseObj.Id;
    	}
		insert quoteFalseObj;
		Product2 prod = new Product2(Name = 'Laptop X200',
            Family = 'Hardware',Display_Order__c=78, IsActive=true);
        insert prod;
		PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
		lstQLI.add(TestFactory.createQuoteLineItem(quoteTrueObj.Id, standardPrice.Id));
		lstQLI.add(TestFactory.createQuoteLineItem(quoteTrueObj.Id, standardPrice.Id));
		lstQLI.add(TestFactory.createQuoteLineItem(quoteFalseObj.Id, standardPrice.Id));
		lstQLI.add(TestFactory.createQuoteLineItem(quoteFalseObj.Id, standardPrice.Id));
		insert lstQLI;
    }

    //this will query the static objects at the top and make them availabe in the test methods.
    static void QueryStartingItems(){

        acct = database.Query('select '+BuildQuery('Account')+' where name = \'test Account\'');
        opp = [SELECT Id from Opportunity where AccountId =: acct.Id LIMIT 1];
        Set<String> availabeProfiles = new Set<String>();
    	List<AggregateResult> userActiveList = [SELECT Profile.Name pn FROM User WHERE IsActive = true GROUP BY Profile.Name];
    	for(AggregateResult userActiveObj : userActiveList){
    		availabeProfiles.add((String)userActiveObj.get('pn'));
    	}
        quoteTrueObj = [SELECT RecordTypeId FROM Quote WHERE Name = 'Quote True'];
        recordTypeTrueObj = [SELECT Name FROM RecordType WHERE Id = :quoteTrueObj.RecordTypeId];
       	Try{
        	quoteLayoutTrue = [SELECT Profile_Name__c, RecordType_name__c, Use_Custom_Layout__c
								FROM Quote_Layout_Assignment__mdt
								WHERE  Use_Custom_Layout__c = true AND RecordType_name__c = :recordTypeTrueObj.Name AND Profile_Name__c IN :availabeProfiles LIMIT 1];
        }
        Catch(exception e){
        	quoteLayoutTrue = [SELECT Profile_Name__c, RecordType_name__c, Use_Custom_Layout__c
								FROM Quote_Layout_Assignment__mdt
								WHERE  Use_Custom_Layout__c = true AND RecordType_name__c = null AND Profile_Name__c IN :availabeProfiles LIMIT 1];
        }
        quoteFalseObj = [SELECT RecordTypeId FROM Quote WHERE Name = 'Quote False'];
		recordTypeFalseObj = [SELECT Name FROM RecordType WHERE Id = :quoteFalseObj.RecordTypeId];
		Try{
			quoteLayoutFalse = [SELECT Profile_Name__c, RecordType_name__c, Use_Custom_Layout__c
								FROM Quote_Layout_Assignment__mdt
								WHERE Use_Custom_Layout__c = false AND RecordType_name__c = :recordTypeFalseObj.Name AND Profile_Name__c IN :availabeProfiles LIMIT 1];
		}
		Catch(exception e){
			quoteLayoutFalse = [SELECT Profile_Name__c, RecordType_name__c, Use_Custom_Layout__c
								FROM Quote_Layout_Assignment__mdt
								WHERE Use_Custom_Layout__c = false AND RecordType_name__c = null AND Profile_Name__c IN :availabeProfiles LIMIT 1];
		}
		userTrueObj = [SELECT Id FROM User WHERE IsActive = true AND Profile.Name = :quoteLayoutTrue.Profile_Name__c LIMIT 1];
		userFalseObj = [SELECT Id FROM User WHERE IsActive = true AND Profile.Name = :quoteLayoutFalse.Profile_Name__c LIMIT 1];
    }

    static string BuildQuery(String obj){
        Map<String, Schema.SObjectField> SobjtField = Schema.getGlobalDescribe().get(obj).getDescribe().fields.getMap();
        string qry = '';
        for (Schema.SObjectField s : SobjtField.values()) {
            qry += s + ',';
        }
        qry = qry.removeEnd(',');
        qry = qry+' from '+ obj;
        return qry;
    }

    @isTest static void test_customLayout() {
         AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
    Insert avaCustomSetting;
		QueryStartingItems();

		System.runAs(userTrueObj) {
			ApexPages.StandardController sc = new ApexPages.StandardController(quoteTrueObj);

			PageReference pageRef = Page.QuoteLayout;
			Test.setCurrentPage(pageRef);
			pageRef.getParameters().put('id',quoteTrueObj.Id);
			QuoteLayout controller = new QuoteLayout(sc);

			Test.startTest();
				controller.runWhenPageLoads();
			Test.stopTest();
		}
	}

    @isTest static void test_save() {
         AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
    Insert avaCustomSetting;
		QueryStartingItems();

		System.runAs(userTrueObj) {
			ApexPages.StandardController sc = new ApexPages.StandardController(quoteTrueObj);

			PageReference pageRef = Page.QuoteLayout;
			Test.setCurrentPage(pageRef);
			pageRef.getParameters().put('id',quoteTrueObj.Id);
			QuoteLayout controller = new QuoteLayout(sc);
				controller.runWhenPageLoads();

			Test.startTest();
				controller.saveIt();
			Test.stopTest();
		}
	}

    @isTest static void test_pricingAdjustmentClass() {
         AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
    Insert avaCustomSetting;
		QueryStartingItems();

		System.runAs(userTrueObj) {
			ApexPages.StandardController sc = new ApexPages.StandardController(quoteTrueObj);

			PageReference pageRef = Page.QuoteLayout;
			Test.setCurrentPage(pageRef);
			pageRef.getParameters().put('id',quoteTrueObj.Id);
			QuoteLayout controller = new QuoteLayout(sc);
				controller.runWhenPageLoads();

			Test.startTest();
				controller.pricingAdjustmentClass();
			Test.stopTest();
		}
	}

	@isTest static void test_standardLayout() {
         AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
    Insert avaCustomSetting;
		QueryStartingItems();

		System.runAs(userFalseObj) {
			ApexPages.StandardController sc = new ApexPages.StandardController(quoteFalseObj);

			PageReference pageRef = Page.QuoteLayout;
			Test.setCurrentPage(pageRef);
			pageRef.getParameters().put('id',quoteFalseObj.Id);
			QuoteLayout controller = new QuoteLayout(sc);

			Test.startTest();
				controller.runWhenPageLoads();
			Test.stopTest();
		}
	}

	@isTest static void test_stopQuoteSync() {
         AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
    Insert avaCustomSetting;
		QueryStartingItems();
		System.runAs(userTrueObj) {
			ApexPages.StandardController sc = new ApexPages.StandardController(quoteTrueObj);

			PageReference pageRef = Page.QuoteLayout;
			System.Test.setCurrentPage(pageRef);
			pageRef.getParameters().put('id',quoteTrueObj.Id);
			QuoteLayout controller = new QuoteLayout(sc);
			controller.runWhenPageLoads();

			System.Test.startTest();
			QuoteLayout.syncIt(quoteTrueObj.Id);
			QuoteLayout.stopSync(quoteTrueObj.Id);
			System.Test.stopTest();
		}
	} 
}