public class QuoteSyncButton {
    public Quote quoteObj {get;set;}
    public String syncStatus {get;set;}
    public Opportunity oppObj {get;set;}
    
   /* public  PageReference runWhenPageLoads(){
        quoteObj = queryQuote();
        oppObj = queryOpportunity();
        if(oppObj.SyncedQuoteId == null){
            syncStatus = 'None';
        } else if(oppObj.SyncedQuoteId == quoteObj.Id){
            syncStatus = 'This';
        } else {
            syncStatus = 'Other';
        }
        return null;
    }*/
    
    public QuoteSyncButton(ApexPages.StandardController controller) {
        quoteObj = queryQuote();
        oppObj = queryOpportunity();
     /*   if(oppObj.SyncedQuoteId == null){
            syncStatus = 'None';
        } else if(oppObj.SyncedQuoteId == quoteObj.Id){
            syncStatus = 'This';
        } else {
            syncStatus = 'Other';
        }*/
    }
    
    
    @testVisible
    public Quote queryQuote(){
        Map<String, Schema.SObjectField> SobjtField = Schema.getGlobalDescribe().get('Quote').getDescribe().fields.getMap();
        string qry = '';
        for (Schema.SObjectField s : SobjtField.values()) {
            qry += s + ',';
        }
        qry = qry.removeend(',');
        qry = 'SELECT '+qry+' FROM '+ 'Quote' + ' WHERE Id = \'' + ApexPages.CurrentPage().getParameters().get('Id') + '\'';
        system.debug(logginglevel.error, '***** qry='+qry);
        return Database.query(qry);
    }
    
    @testVisible
    public Opportunity queryOpportunity(){
        Id oppId = quoteObj.OpportunityId;
        return [SELECT SyncedQuoteId FROM Opportunity WHERE Id = :oppId];
    }
    
    
@testVisible
    public static PageReference syncIt(){
        Quote quoteObj = [SELECT OpportunityId, IsSyncing, Service_Template_Codes__c, Partner_Email__c FROM Quote WHERE Id = :ApexPages.currentPage().GetParameters().Get('Id')];
			Opportunity oppObj = [SELECT Id, SyncedQuoteId FROM Opportunity WHERE Id = :quoteObj.OpportunityId];
            Contract_Document__c cd = [SELECT Implementation__c FROM Contract_Document__c WHERE Opportunity__c = :oppObj.Id];
            cd.Implementation__c = quoteObj.Service_Template_Codes__c;
        cd.Partner_Email__c = quoteObj.Partner_Email__c;
			oppObj.SyncedQuoteId = quoteObj.Id;
            oppObj.Service_Edited__c = true;
            update cd;
			update oppObj;
        return null;
    }
  
    
}