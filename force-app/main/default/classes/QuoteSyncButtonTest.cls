@isTest
public class QuoteSyncButtonTest {
    static Account acct {get;set;}
    static Opportunity opp {get;set;}
    static Quote quoteObj;
    static Contract_Document__c conDoc;
    static Client__c client {get;set;}
    
    @isTest static void test_user() {
        AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
    Insert avaCustomSetting;
        User objUser = [SELECT ID
                        FROM User
                        WHERE Profile.Name = 'System Administrator'
                        AND isActive = true LIMIT 1];
        
        System.runAs(objUser) {
            TestFactory.disableTriggers();
            
            acct = TestFactory.createAccount();
            insert acct;
            
            opp = new Opportunity();
            opp = TestFactory.createOpportunity(acct.Id, null);
            insert opp;
            
            quoteObj = new Quote();
            quoteObj = TestFactory.createQuote(opp.Id, Test.getStandardPricebookId());
            insert quoteObj;
            
             client = new Client__c();
            client = TestFactory.createClient(acct.Id);
            insert client;
            
             conDoc = new Contract_Document__c();
            conDoc = TestFactory.createContractDoc2(opp.Id);
            insert conDoc;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(quoteObj);
            PageReference pageRef = Page.QuoteSyncButton;
            System.Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('id',quoteObj.Id);  
            
            
            System.Test.startTest();
            QuoteSyncButton controller = new QuoteSyncButton(sc);
            QuoteSyncButton.syncIt();
            System.Test.stopTest();
            
        }
    }
}