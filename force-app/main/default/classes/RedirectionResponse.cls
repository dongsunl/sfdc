public with sharing class RedirectionResponse {

    /**
     * Variables
     */
    private String redirectionUrl {get;set;}
    private Boolean redirectEntireWindow {get;set;}
    private RedirectionException err {get;set;}
    private List<String> logs {get;set;}
    
    /**
     * Class Constructor
     * 
     * Accepts all possible parameters, ensures all variables are
     * populated based on the values provided.
     */
    public RedirectionResponse(String redirectionUrl, Boolean redirectEntireWindow, RedirectionException err, List<String> logs) {
        this.redirectionUrl = redirectionUrl;
        this.redirectEntireWindow = redirectEntireWindow;
        this.err = err;
        this.logs = logs;
    }

    /**
     * Overloaded Constructor
     * 
     * This constructor is called when the the request is valid.
     * In this scenario all we care about is the redirection URL
     * and if the redirection should occur in a new window.  The
     * other variables are hardcoded to be null (ignored).
     */
    public RedirectionResponse(String redirectionUrl, Boolean redirectEntireWindow) {
        this(redirectionUrl, redirectEntireWindow, null, null);
    }
    
    /**
     * Overloaded Constructor
     * 
     * This constructor is called when the request is invalid.
     * All we care about in this scenario is the error messages.
     * The other variables are hardcoded to be null (ignored).
     */
    public RedirectionResponse(RedirectionException err) {
        this(null, false, err, null);
    }
    
    /**
     * Standard Getters
     */
    public String getRedirectionUrl() {
        return redirectionUrl;
    }
    
    public Boolean getRedirectEntireWindow() {
        return redirectEntireWindow;
    }
    
    public RedirectionException getErr() {
        return err;
    }
    
    public List<String> getLogs() {
        return logs;
    }
    
    /**
     * Get Redirection Mode
     * 
     * - If the request is in debug mode or errors exist
	 *   no redirection should be performed.
	 * - If the request is valid and the settings state
	 *   the redirection should include the entire window
	 *   a JavaScript redirection is required.
	 * - If the request is valid and the settings state
	 *   the redirection should NOT include the entire window
	 *   a Page Reference redirection is required.
	 */
    public String getRedirectionMode() {
        if (logs == null && err == null) {
            if (redirectEntireWindow) {
	            return RedirectionConstants.REDIRECTION_MODE_JAVASCRIPT;
            } else {
	            return RedirectionConstants.REDIRECTION_MODE_PAGEREFERENCE;
            }
        }
        return RedirectionConstants.REDIRECTION_MODE_NONE;
    }
}