@RestResource(urlMapping='/Survey/*')
global class RestAPI {
    
    @HttpGet
    global static List<sObject> getRecords() {
        RestRequest request = RestContext.request;
        system.debug(request);
        system.debug(request.params.keySet());
        //Grab the sObject from the url
        Integer sObjectStartIndex = request.requestURI.indexOfIgnoreCase('/Survey/')+8;
        //system.debug(sObjectStartIndex);
        Integer sObjectEndIndex = request.requestURI.lastIndexOf('/');
        //system.debug(sObjectEndIndex);
        String requestName = request.requestURI.substring(sObjectStartIndex, sObjectEndIndex);
        //system.debug(objectType);
        String objectId = request.params.get('Id');
        system.debug(objectId);
        String objectName = request.params.get('Name');
        system.debug(objectName);
        String queryVariable;
        if(objectName != 'null') {
            queryVariable = '%'+objectName+'%';
        } else if(objectId != 'null') {
            queryVariable = objectId;
        } else if(requestName == 'RecordTypes'){
            queryVariable = 'Case';
        } else if(requestName == 'Entitlements') {
            queryVariable = 'Active';
        }
        system.debug(queryVariable);
        Map<String, String> objectMap = new Map<String, String>{'Opportunity' => 'Opportunity', 'Opportunities' => 'Opportunity', 'Account' => 'Account', 'Accounts' => 'Account', 'RecordTypes' => 'RecordType', 'Entitlements' => 'Entitlement', 'User' => 'User', 'Quote' => 'Quote', 'Quotes' => 'Quote', 'ContentVersion' => 'ContentVersion', 'Client__c' => 'Client__c', 'Clients' => 'Client__c'};
            Map<String, String> fieldMap = new Map<String, String>();
        Map<String, String> clauseMap = new Map<String, String>{'Opportunity' => 'Id = ', 'Opportunities' => 'Name LIKE ', 'Account' => 'Id = ', 'Accounts' => 'Name LIKE ', 'RecordTypes' => 'SobjectType = ', 'Entitlements' => 'Status = ', 'User' => 'Id = ', 'Quote' => 'Id = ', 'Quotes' => 'Name LIKE ', 'ContentVersion' => 'Id = ', 'Client__c' => 'Id = ', 'Clients' => 'Name LIKE '};
        fieldMap.put('Opportunity', 'Id, AccountId, Account.Name, Account.AnnualRevenue, Account.NumberOfEmployees, Account.BillingStreet, Account.BillingCountry, Amount, Amount_USD__c, Assigned_SME__c, Assigned_SME__r.Full_Name__c, BX_Use_Case__c, Forecast_Percentage_NEW__c, Client__c, Client__r.Name, CloseDate, Competitors__c, Competitor_we_lost_to__c, Countries__c, CreatedDate, CurrencyConversionRate__c, CX_Quota_Relief_New__c, datahugapps__NextMeeting__c, DA_Decision_Process__c, Estimated_Project_End_Date__c, ExpectedRevenue,Field_Time__c, EX_Quota_Relief_New__c, Final_LOI__c, Final_N__c, Incident_Rate_Actual__c, Incumbent__c, IndustryFormula__c, Initial_Opportunity_Amount__c, Invoice_Amount__c, LeadSource, Lead_Type__c, Lighthouse_Use_Case__c, LOI__c, Medallia_Incumbent__c, MEDDICC_Champions__c, MEDDICC_Comments__c, MEDDICC_Compelling_Event__c, MEDDICC_Decision_Process__c, MEDDICC_Economic_Buyer__c, MEDDICC_Executive_Sponsor__c, MEDDICC_Identify_Pain__c, MEDDICC_Metric__c, N__c, Name, Needs_Committee_Review__c, Notes__c, Notes_for_SME__c, Opportunity_Owner_Email__c, Ordered_Fiscal_Period__c,Owner.Sales_Level__c, Owner.Global_Region__c, Owner_Name__c, OwnerId, Panels_Buyer__c, POC__c, Price_Per_Response__c, Primary_Competitor__c, Probability, Product__c, Products__c, Project_Deadline__c, Project_Manager__c, Project_Manager_Email__c, Project_Start_Date__c, Qualification_Contact__c, Qualification_Contact__r.Name, Qualification_Contact__r.Email, Quota_Relief_Amount__c, RC_Quota_Relief_New__c, Reason_for_Decision__c, RecordType.Name, Request_TS_Support__c, Research_Expert__c, Sales_Notes_to_TS__c, Sales_Process_Stage__c, SME_Request_Date__c, SME_Requested__c, SME_Status__c, Solution_Architect_User__c, Spec_Dev__c, Spec_Dev_Email__c, StageName,SyncedQuoteId, SyncedQuote.Name, SyncedQuote.Pre_SOW_Services__c, Tech_Sales_Rep__c, Tech_Sales_Rep_Name__c, Tech_Sales_Request_Date__c, Tech_Sales_Status__c, Type');
        fieldMap.put('Opportunities', 'Id, Name');
        fieldMap.put('Contact', 'Id, Name');        
        fieldMap.put('Account', 'Id, BillingAddress, Domestic_Ultimate_DUNS_Number__c, Linkedin_Company_Website__c, Name, Phone');
        fieldMap.put('Accounts', 'Id, Name');
        fieldMap.put('RecordTypes', 'Id, Name');
        fieldMap.put('Entitlements', 'Id, Name');
        fieldMap.put('User', 'Id, Email, EmployeeID__c, Full_Name__c, Global_Region__c, Rep_Tier__c, Role_Name__c, Sales_Team__c');
        fieldMap.put('Quote', 'Id, AccountId, Account.Name, Name, OpportunityId, Opportunity.Name, Opportunity.Solution_Architect_User__c, Pre_SOW_Services__c, Products__c, Related_Client__c, Related_Client__r.Name, TotalPrice, Total_Price_USD__c');
        fieldMap.put('Quotes', 'Id, Name');
        fieldMap.put('ContentVersion', 'ContentDocumentId');
        fieldMap.put('Client__c', 'Account__c, Account__r.Name, Name');
        fieldMap.put('Clients', 'Id, Name');
        String queryFields = fieldMap.get(requestName);
        String queryObject = objectMap.get(requestName);
        String queryClause = clauseMap.get(requestName);
        system.debug(queryFields);
        system.debug(queryObject);
        system.debug(queryClause);
        List<sObject> records = Database.query('SELECT ' + queryFields + ' FROM ' + queryObject + ' WHERE ' + queryClause + ':queryVariable');
        return records;
    }
    
    @HttpPost
    global static List<ID> createRecord() {
        RestRequest request = RestContext.request;
        //Grab the sObject from the url
        Integer sObjectStartIndex = request.requestURI.indexOfIgnoreCase('/Survey/')+8;
        //system.debug(sObjectStartIndex);
        Integer sObjectEndIndex = request.requestURI.lastIndexOf('/');
        //system.debug(sObjectEndIndex);
        String requestName = request.requestURI.substring(sObjectStartIndex, sObjectEndIndex);
        system.debug(requestName);
        system.debug(request.params.keySet());
        sObject sObj = Schema.getGlobalDescribe().get(requestName).newSObject();
        for (String key : request.params.keySet()) {
            Schema.DisplayType fieldType = Schema.getGlobalDescribe().get(requestName).getDescribe().fields.getMap().get(key).getDescribe().getType();
            system.debug(key);
            system.debug(request.params.get(key));
            system.debug(fieldType);
            if(request.params.get(key) != '') {
                if(fieldType == Schema.DisplayType.DATE) {
                    sObj.put(key, Date.valueOf(request.params.get(key)));
                } else if(fieldType == Schema.DisplayType.BOOLEAN) {
                    sObj.put(key, Boolean.valueOf(request.params.get(key)));
                }else if(fieldType == Schema.DisplayType.CURRENCY) {
                    sObj.put(key, Decimal.valueOf(request.params.get(key)));
                } else if(fieldType == Schema.DisplayType.DATETIME) {
                    sObj.put(key, DateTime.valueOf(request.params.get(key)));   
                }else if(fieldType == Schema.DisplayType.BASE64) {
                    sObj.put(key, EncodingUtil.base64Decode(request.params.get(key)));
                }else if(fieldType == Schema.DisplayType.ID) {
                    if(request.params.get(key) != '') {
                        sObj.put(key, ID.valueOf(request.params.get(key)));
                    }
                }else {
                    sObj.put(key, request.params.get(key));
                }
            }
        }
        system.debug(sObj);
        upsert sObj;
        system.debug(sObj.Id);
        List<ID> ids = new List<ID>();
        ids.add(sObj.Id);
        return ids;
    }
}