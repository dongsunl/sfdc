@IsTest
public class RestAPITest {
    
    @isTest static void testGetOpportunityById() {
         AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
        Insert avaCustomSetting;
        Account acc = TestFactory.createAccount();
        insert acc;
        system.debug(acc);
        Opportunity opp = TestFactory.createOpportunity(acc.Id, Test.getStandardPriceBookId());
        insert opp;
        system.debug(opp);
        RestRequest request = new RestRequest();
        request.requestURI = '/services/apexrest/Survey/Opportunity/';
        request.httpMethod = 'GET';
        request.addParameter('Id', opp.Id);
        request.addParameter('Name', 'null');
        RestContext.request = request;
        List<Opportunity> opps = RestAPI.getRecords();
        system.debug(opps);
        System.assertEquals(1, opps.size());
    }
    
    @isTest static void testGetAccountsByName() {
         AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
        Insert avaCustomSetting;
		List<Account> accounts = new List<Account>();
        Account acc1 = new Account();
        acc1.Name = 'Test Account 1';
        accounts.add(acc1);
        Account acc2 = new Account();
        acc2.Name = 'Test Account 2';
        accounts.add(acc2);
        Account acc3 = new Account();
        acc3.Name = 'Account 3';
        accounts.add(acc3);
        insert accounts;
        system.debug(accounts);
        RestRequest request = new RestRequest();
        request.requestURI = '/services/apexrest/Survey/Accounts/';
        request.httpMethod = 'GET';
        request.addParameter('Id', 'null');
        request.addParameter('Name', 'test');
        RestContext.request = request;
        List<Account> results = RestAPI.getRecords();
        system.debug(results);
        system.assertEquals(2, results.size());
    }
    
    @isTest static void testGetRecordTypes() {
         AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
        Insert avaCustomSetting;
        RestRequest request = new RestRequest();
        request.requestURI = '/services/apexrest/Survey/RecordTypes/';
        request.httpMethod = 'GET';
        request.addParameter('Id', 'null');
        request.addParameter('Name', 'null');
        RestContext.request = request;
        List<RecordType> results = RestAPI.getRecords();
        system.debug(results);
        system.assertEquals(true, results.size() > 0);
    }
    
    @isTest static void testGetEntitlements() {
         AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
        Insert avaCustomSetting;
        Account acc = TestFactory.createAccount();
        insert acc;
        List<Entitlement> entitlements = new List<Entitlement>();
        Entitlement e1 = new Entitlement();
        e1.Name = 'Test Entitlement 1';
        e1.StartDate = System.today().addDays(-1);
        e1.AccountId = acc.Id;
        entitlements.add(e1);
        Entitlement e2 = new Entitlement();
        e2.Name = 'Test Entitlement 2';
        e2.StartDate = System.today().addDays(1);
        e2.AccountId = acc.Id;
        entitlements.add(e2);
        insert entitlements;
        RestRequest request = new RestRequest();
        request.requestURI = '/services/apexrest/Survey/Entitlements/';
        request.addParameter('Id', 'null');
        request.addParameter('Name', 'null');
        RestContext.request = request;
        List<Entitlement> results = RestAPI.getRecords();
        system.debug(results);
        system.assertEquals(1, results.size());
    }
    
    @isTest static void testCreateCase() {
         AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
        Insert avaCustomSetting;
        Account acc = TestFactory.createAccount();
        insert acc;
        RestRequest request = new RestRequest();
        request.requestURI = '/services/apexrest/Survey/Case/';
        request.httpMethod = 'POST';
        request.addParameter('AccountId', acc.Id);
        request.addParameter('Execute_Assignment_Rules__c', 'true');
        request.addParameter('Id', '');
        request.addParameter('Subject', 'Test Subject');
        request.addParameter('Description', 'Testing');
        RestContext.request = request;
        List<ID> results = RestAPI.createRecord();
        system.debug(results);
        system.assertEquals(1, results.size());
    }

}