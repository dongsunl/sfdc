@IsTest
public class SOW_AttachedTest
{
public static testMethod void SOW_AttachedTest()
    {
        
       /****Account acct = new Account(Name = 'Test Account');
       insert acct;
       
       Contact cont = new Contact(LastName = 'Contact', FirstName = 'Test', AccountID = acct.id, Contact_Role__c = 'Manager', Status__c = 'Active Customer', LeadSource = 'Referral');
       insert cont;*****/
       
       Opportunity opp = new Opportunity(Pricebook2Id = '01s50000000dG3v', Check_Contact_Roles__c =true, Name = 'Test Opp', LeadSource = 'Event', Lead_Source_Detail__c = 'Webinar', CloseDate = Date.today(), StageName = 'Commit', Forecast_Percentage_NEW__c = 100);
       insert opp;
       
       Quote quote = new Quote(Name = 'Test Quote', Quote_Type__c = 'New License', OpportunityID = opp.ID, Quote_Origination_Date__c = Date.today(), ExpirationDate = Date.today(), License_Start_Date__c = Date.today());
       insert quote;
       
       Service__c serv = new Service__c(Name = 'Test Project Edition', Status__c ='Inactive', Opportunity__c = opp.ID, quote__c = quote.ID);
       insert serv; 
           
     
      
        Attachment att = new Attachment(Name='textAtt', ParentId = serv.Id, body = Blob.valueOf('Some Text'), Description='TestAttachment');
        insert att;
        
       system.assert(serv.SOW_Attached__c = TRUE);
    }
}