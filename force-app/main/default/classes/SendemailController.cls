public class SendemailController {
 /*   public static void sendInvoice(Opportunity[] opp, map<Id,Opportunity> OldMap){
        
        User u = [SELECT ID from User WHERE Name = 'Qualtrics Onboarding' LIMIT 1];
        Id onboardingId = u.Id;
        List<Opportunity> opps = new List<Opportunity>();
        
        for(Opportunity obj:opp){
            if(OldMap.get(obj.Id).Sales_Process_Stage__c !='Invoice' && obj.Sales_Process_Stage__c == 'Invoice')
            {
                opps.add(obj);               
            }
        }
        if(opps!=Null && opps.size()>0){
            string template = 'Invoice Email Template';
            Id templateId;
            
            try{
                  templateId = [SELECT ID FROM EmailTemplate where name =:template].id;
               
            }
            catch (QueryException e){
                system.debug('error: '+ e);
            }
            
            List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
            for(Opportunity o:opps){
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.setTemplateId(templateId);               
                message.setWhatId(o.Id);
                message.setTargetObjectId(onboardingId);
                String[] toAddresses = new String[] {};
                    if(o.Owner_Global_Region__c == 'EMEA'){
                        toAddresses.add('ar-emea@qualtrics.com');
                    }
                else{
                    toAddresses.add('ar@qualtrics.com');
                }         
                message.setToAddresses(toAddresses);
                message.setCCAddresses(new String[]{'ginab@qualtrics.com'});
                message.setSaveAsActivity(false);
                messages.add(message);
                
                List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
                for (Attachment a : [select Name, Body, BodyLength from Attachment where ParentId = :o.Id]){
                    Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                    efa.setFileName(a.Name);
                    efa.setBody(a.Body);
                    fileAttachments.add(efa);
                }
                message.setFileAttachments(fileAttachments);
            }
            if(!test.isrunningtest()){
                Messaging.sendEmail(messages);
            }
            
        }
    }
 /*   public String oppId {get;set;}
    
    Public SendemailController(){
        oppId = ApexPages.currentPage().getParameters().get('Id');
        system.debug('opp id->'+oppId );
    }
    
    public Opportunity opp;
    
    public Opportunity getOpp(){
        opp = [
            SELECT Id, Qualification_Contact__r.Email, Client__r.Name, CloseDate, Amount, Quota_Relief_Amount__c, Owner.Name, Opportunity_Type__c, 
            Payment_Terms__c, PO_Required__c, Purchase_Order_Number__c, Installments__c, Billing_Contact__r.Name, Biling_Address__c, Billing_Instructions__c, 
            EIN__c, Client__r.Shipping_Street__c, List_Price__c, Discount__c, Quota_Relief_Adjustment__c, License_Start_Date__c, License_End_Date__c, 
            License_Referral_Employee__c, Initial_Opportunity_Amount__c, Name, N__c, LOI__c, Price_per_response__c, Incidence_Rate__c, Panel_Demographics__c,
            Products__c, Research_Suite_Revenue__c, Research_Suite_Summary__c, ResearchSuiteAcademicSummary__c, Target_Audience_Revenue__c, 
            Target_Audience_Summary__c, Site_Intercept_Revenue__c, Site_Intercept_Summary__c, Vocalize_Revenue__c, Vocalize_Summary__c, X360_Revenue__c,
            X360_Summary__c, Employee_Engagement_Revenue__c, Employee_Engagement_Summary__c, Service_Revenue__c, Service_Summary__c, ProfessionalServicesRep__c,
            CoTermProfServProduct__c, ProfessionalServicesAmount__c, Custom_Payment_Terms__c, Invoice_Email_Language__c, ARR_NRR__c            
            FROM Opportunity WHERE id=:oppId];
        return opp;
    }
    
    Public Pagereference sendEmailFunction(){
        Opportunity myEmail = [
            SELECT Id, Owner.Email, Client__r.Name, Spec_Dev__r.Name, Opportunity_Type__c, Owner.Name, Owner_Global_Region__c      
            FROM Opportunity WHERE id=:oppId];
        
        try {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {};
                toAddresses.add('onboarding@qualtrics.com');
            if(myEmail.Owner_Global_Region__c == 'Worldwide'){
                toAddresses.add('ar@qualtrics.com');
            }
            else if(myEmail.Owner_Global_Region__c == 'North America'){
                toAddresses.add('ar@qualtrics.com');
            }
            else if(myEmail.Owner_Global_Region__c == 'APAC'){
                toAddresses.add('ar@qualtrics.com');
            }
            else if(myEmail.Owner_Global_Region__c == 'LATAM'){
                toAddresses.add('ar@qualtrics.com');
            }
            else if(myEmail.Owner_Global_Region__c == 'EMEA'){
                toAddresses.add('ar-emea@qualtrics.com');
            }else{
                toAddresses.add('ar@qualtrics.com');
            }
            
            String[] ccAddresses = new String[] {'ginab@qualtrics.com'};
                mail.setToAddresses(toAddresses);
            mail.setCcAddresses(ccAddresses);
            mail.setSubject('Pending Sales Order for '+ myEmail.Client__r.Name+' | '+ myEmail.Opportunity_Type__c);
            blob body;
            PageReference htmlPage = page.InvoiceEmailPage;
            body = htmlPage.getContent();
            string htmlBody = body.toString();
            mail.setHtmlBody(htmlBody);
            
            List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
            for (Attachment a : [select Name, Body, BodyLength from Attachment where ParentId = :oppId]){
                Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                efa.setFileName(a.Name);
                efa.setBody(a.Body);
                fileAttachments.add(efa);
                
            }
            
            
            mail.setFileAttachments(fileAttachments);
            
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            
        } catch(Exception e) {
            system.debug(e);
            
            Messaging.SingleEmailMessage mail=new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {'ginab@qualtrics.com'};
                mail.setToAddresses(toAddresses);
            mail.setReplyTo('ginab@qualtrics.com');
            mail.setSenderDisplayName('Apex error message');
            mail.setSubject('Invoice Email Error');
            mail.setPlainTextBody(e.getMessage());
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        
        
        PageReference reference = new PageReference('/'+oppId);
        reference.setRedirect(true);
        return reference;
    }*/
    
}