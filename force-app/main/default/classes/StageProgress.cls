public with sharing class StageProgress implements Comparable {

    /**
     * Private variables tightly coupled with the fields
     * on the stage details custom metadata object.
     */
	private String fieldValue;
	private Decimal displayOrder;
    
    /**
     * Calculated private variables.
     */
	private Decimal completionPercentage;

    /**
     * Class Constructor
     * 
     * The custom metadata record is used to populate
     * a majority of the properties within this class.
     * The SObject provided is used to determine what
     * the calculated completion percentage should be.
     */
    public StageProgress(Stage_Details__mdt stageDetails, SObject sObj) {
        this.fieldValue = stageDetails.Field_Value__c;
        this.displayOrder = stageDetails.Display_Order__c;
		this.completionPercentage = calculateCompletionPercentage(stageDetails.Completion_Criteria__r, sObj);
    }
    
    /**
     * Calculates the completion percentage based on the
     * SObject provided and list of completion criteria
     * checkbox values.
     */
    private Decimal calculateCompletionPercentage(List<Completion_Criteria__mdt> completionCriteriaRecords, SObject sObj) {
       
        // By default the numerator and denominator are
        // set to 0.  These will increment as progress
        // is made.
        Decimal numerator = 0;
        Decimal denominator = 0;
        
        // Loop through each completion criteria (checkbox).
        for (Completion_Criteria__mdt record : completionCriteriaRecords) {
            
            // Increment the number of fields evaluated by 1.
            denominator += 1;
            
            // It is possible the field name specified in the
            // completion criteria record does not exist on
            // the SObject provided.  We protect ourselves from
            // these errors by wrapping this in a try/catch block.
            try {
                
                // If the related checkbox is checked we increment
                // the number of completed items by 1.
	            if (Boolean.valueOf(sObj.get(record.Results_Field_Name__c))) {
    	            numerator += 1;
        	    }
            } catch (Exception e) {
                System.debug('Error evaluating field name: ' + e.getMessage());
            }
        }
        
        // We cannot divide by 0.  If the denominator is 0, which
        // means no criteria have been evaluated, then this stage
        // is completed.  Return 100%.
        if (denominator == 0) {
	        return 100;
            
        // Otherwise we return the calculated percentage and multiply
        // it by 100.  (ie: .25 x 100 = 25 or 25%)
        } else {
            return (numerator / denominator) * 100;
        }
    }
    
    /**
     * Required because the current class implements the
     * comparable interface.  This allows instances of
     * this class to be ordered using the sort command.
     */
    public Integer compareTo(Object otherRecord) {
        
        StageProgress otherStageProgress = (StageProgress)otherRecord;
		Decimal thisDisplayOrder = this.getDisplayOrder();
        Decimal otherDisplayOrder = otherStageProgress.getDisplayOrder();
        
        if (thisDisplayOrder < otherDisplayOrder) {
            return -1;
        } else if (otherDisplayOrder < thisDisplayOrder) {
            return 1;
        }
        return 0;
    }
    
    /**
     * Properties exposed to lightning components.
     */
    @AuraEnabled
    public String getFieldValue() {
        return fieldValue;
    }
    
    @AuraEnabled
    public Decimal getCompletionPercentage() {
        return completionPercentage;
    }
    
    @AuraEnabled
    public Decimal getDisplayOrder() {
        
        // If the display order is not populated we
        // return a 0.  Numeric values are required
        // to properly sort instances of this class.
        return this.displayOrder == null ? 0 : this.displayOrder;
    }
}