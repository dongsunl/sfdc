public class SyncQuoteExtension {
    
    private final String ACTION_PARAMETER_NAME = 'action';
    private final String ACTION_SYNC = 'sync';
    private final String ACTION_UNSYNC = 'unsync';
    private final String DEFAULT_ACTION = ACTION_SYNC;

    private id quoteRecordId;
    private Quote quoteRecord;
    private Opportunity opportunityRecord;
    private Contract_Document__c contractDocumentRecord;
    
    private String action;
    
    public SyncQuoteExtension(ApexPages.StandardController controller) {
        this.quoteRecordId = controller.getId();
        init();
    }
    
    private void init() {
        initQuoteAndOpportunity();
        initContractDocument();
        initAction();
    }
    
    private void initQuoteAndOpportunity() {
        
        if (!Schema.SObjectType.Quote.isQueryable()
	            || !Schema.SObjectType.Quote.fields.Id.isAccessible()
	            || !Schema.SObjectType.Quote.fields.Service_Template_Codes__c.isAccessible()
	            || !Schema.SObjectType.Quote.fields.Partner_Email__c.isAccessible()
            	|| !Schema.SObjectType.Opportunity.isQueryable()
	            || !Schema.SObjectType.Opportunity.fields.Id.isAccessible()
	            || !Schema.SObjectType.Opportunity.fields.SyncedQuoteId.isAccessible()
	            || !Schema.SObjectType.Opportunity.fields.Service_Edited__c.isAccessible()) {
            throw new InvalidPermissionsException(InvalidPermissionsException.QUOTE_OR_OPPORTUNITY_ACCESS_MESSAGE);
        }
        
        List<Quote> quotes =
			[SELECT Id, Service_Template_Codes__c, Partner_Email__c,
             	Opportunity.Id, Opportunity.SyncedQuoteId, Opportunity.Service_Edited__c
             FROM Quote
             WHERE Id = : quoteRecordId
             LIMIT 1];
        if (quotes.size() == 1) {
            this.quoteRecord = quotes[0];
            this.opportunityRecord = quotes[0].Opportunity;
        }
    }
    
    private void initContractDocument() {
        
        if (opportunityRecord == null) {
            return;
        }
        
        if (!Schema.SObjectType.Contract_Document__c.isQueryable()
	            || !Schema.SObjectType.Contract_Document__c.fields.Id.isAccessible()
	            || !Schema.SObjectType.Contract_Document__c.fields.Implementation__c.isAccessible()
	            || !Schema.SObjectType.Contract_Document__c.fields.Partner_Email__c.isAccessible()
	            || !Schema.SObjectType.Contract_Document__c.fields.Opportunity__c.isAccessible()) {
            throw new InvalidPermissionsException(InvalidPermissionsException.CONTRACT_DOCUMENT_ACCESS_MESSAGE);
        }
        
        List<Contract_Document__c> contractDocuments =
            [SELECT Id, Implementation__c, Partner_Email__c
             FROM Contract_Document__c
             WHERE Opportunity__c = : opportunityRecord.Id
             LIMIT 1];
        if (contractDocuments.size() == 1) {
            this.contractDocumentRecord = contractDocuments[0];
        }
    }
    
    private void initAction() {
        action = ApexPages.currentPage().getParameters().get(ACTION_PARAMETER_NAME);
        if (action == null) {
            action = DEFAULT_ACTION;
        }
        action = String.escapeSingleQuotes(action);
    }
    
    public PageReference execute() {
        if (action == ACTION_SYNC) {
            doSync();
        } else if (action == ACTION_UNSYNC) {
            doUnsync();
        }
        saveRecords();
        return getQuoteUrl();
    }
    
    private void doSync() {
        
        if (quoteRecord == null || opportunityRecord == null || contractDocumentRecord == null) {
            return;
        }
        
        if (!Schema.SObjectType.Opportunity.fields.SyncedQuoteId.isUpdateable()
	            || !Schema.SObjectType.Opportunity.fields.Service_Edited__c.isUpdateable()
	            || !Schema.SObjectType.Contract_Document__c.fields.Implementation__c.isUpdateable()
	            || !Schema.SObjectType.Contract_Document__c.fields.Partner_Email__c.isUpdateable()) {
            throw new InvalidPermissionsException(InvalidPermissionsException.OPPORTUNITY_OR_CONTRACT_DOCUMENT_UPDATE_MESSAGE);
        }
        
        opportunityRecord.SyncedQuoteId = quoteRecord.Id;
		opportunityRecord.Service_Edited__c = true;
        contractDocumentRecord.Implementation__c = quoteRecord.Service_Template_Codes__c;
        contractDocumentRecord.Partner_Email__c = quoteRecord.Partner_Email__c;
    }
    
    private void doUnsync() {
        
        if (quoteRecord == null || opportunityRecord == null || contractDocumentRecord == null) {
            return;
        }
        
        if (!Schema.SObjectType.Opportunity.fields.SyncedQuoteId.isUpdateable()
	            || !Schema.SObjectType.Opportunity.fields.Service_Edited__c.isUpdateable()
	            || !Schema.SObjectType.Contract_Document__c.fields.Implementation__c.isUpdateable()
	            || !Schema.SObjectType.Contract_Document__c.fields.Partner_Email__c.isUpdateable()) {
            throw new InvalidPermissionsException(InvalidPermissionsException.OPPORTUNITY_OR_CONTRACT_DOCUMENT_UPDATE_MESSAGE);
        }

        opportunityRecord.SyncedQuoteId = null;
		opportunityRecord.Service_Edited__c = false;
        contractDocumentRecord.Implementation__c = null;
        contractDocumentRecord.Partner_Email__c = null;
    }
    
    private void saveRecords() {
        
        if (opportunityRecord == null || contractDocumentRecord == null) {
            return;
        }
        
        if (!Schema.SObjectType.Opportunity.isUpdateable()
	            || !Schema.SObjectType.Contract_Document__c.isUpdateable()) {
            throw new InvalidPermissionsException(InvalidPermissionsException.OPPORTUNITY_OR_CONTRACT_DOCUMENT_UPDATE_MESSAGE);
        }
        
        update opportunityRecord;
        update contractDocumentRecord;
    }
    
    private PageReference getQuoteUrl() {
        return new PageReference('/' + quoteRecordId);
    }
}