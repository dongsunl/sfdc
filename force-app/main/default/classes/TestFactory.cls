@isTest
public without sharing class TestFactory {

	public static void disableTriggers(){
		Trigger_Settings__c triggerSettingsObj = Trigger_Settings__c.getOrgDefaults();
		triggerSettingsObj.Disable_All_Triggers__c = true;
		insert triggerSettingsObj;
	}

	public static Attachment createAttachment(Id parentId){
		Attachment objAtt = new Attachment();
        objAtt.Name = 'QSO';
        objAtt.body = Blob.valueof('string');
        objAtt.ParentId = parentId;
        return objAtt;
	}
	
	public static Account createAccount(){
		Account acctObj = new Account();
		acctObj.Name = 'test Account';
		return acctObj;
	}

	public static Q_Partner__c createPartner(Id acctId){
		Q_Partner__c partnerObj = new Q_Partner__c();
		partnerObj.RelatedAccount__c = acctId;
		return partnerObj;
	}

	public static Pricebook2 createPriceBook(){
		return new Pricebook2(IsActive=true,Name='test');
	}

	public static Product2 createProduct(){
		Product2 prodObj = new Product2();
		prodObj.Name = 'Test Product';
		prodObj.Display_Order__c = 1;
		return prodObj;
	}

	public static PricebookEntry createPricebookEntry(Id pricebkId, Id prodId){
		PricebookEntry pbObj = new PricebookEntry();
		pbObj.Pricebook2Id = pricebkId;
		pbObj.product2id = prodId;
		pbObj.UnitPrice = 1000;
		pbObj.IsActive = true;
        pbObj.CurrencyIsoCode = 'USD';
		return pbObj;
	}

	public static Opportunity createOpportunity(Id acctId, Id priceBookId){
		Opportunity oppObj = new Opportunity();
		oppObj.Name = 'Test Opp';
		oppObj.StageName = 'test Stage';
		oppObj.CloseDate = date.today();
		oppObj.Turn_Off_Initial_CX_EX_RC_Amounts__c = true;
        //oppObj.LID__LinkedIn_Company_Id__c = '5';
        //oppObj.Forecast_PercentageV2__c = 0;
        //oppObj.Opp_from_Lead_Conversion__c = false;
        //oppObj.CurrencyIsoCode = 'USD';
        oppObj.Pricebook2Id = priceBookId;
        oppObj.AccountId = acctId;
		return oppObj;
	}

	public static Quote createQuote(Id oppId, Id pricebookId){
		Quote quoteObj = new Quote();
		quoteObj.OpportunityId = oppId;
		quoteObj.Name = 'Test Quote';
        quoteObj.Pricebook2Id = pricebookId;
		return quoteObj;
	}

	public static QuotelineItem createQuoteLineItem(Id quoteId, Id priceBookId){
		QuotelineItem qliObj = new QuoteLineItem();
		qliObj.QuoteId = quoteId;
		qliObj.Quantity = 1;
		qliObj.UnitPrice = 100;
		qliObj.pricebookentryId = priceBookId;
		return qliObj;
	}


	private static sObject CreateObject(string sObjectName) {
        sObject sObj = Schema.getGlobalDescribe().get(sObjectName).newSObject() ;
        Map<String, Schema.SObjectField> SobjtField = Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap();
        for (string s : SobjtField.keySet()) {
            Schema.DescribeFieldResult field = SobjtField.get(s).getDescribe();
            if (field.isAccessible() == true && field.isCreateable() == true) {
                if (field.getSoapType() == Schema.SoapType.String) {
                    if (field.getType() == Schema.DisplayType.Picklist || field.getType() == Schema.DisplayType.multipicklist) {
                        sObj.put(s, SobjtField.get(s).getDescribe().getPickListValues().get(0).getValue());
                    } else if (field.getType() == Schema.DisplayType.Email) {
                        sObj.put(s, 'test@test.com');
                    } else {
                    	if(field.getLength() < 4){
                    		sObj.put(s, '');
                    	} else {
                    		sObj.put(s, 'abcd');
                    	}                        
                    }
                } else if (field.getSoapType() == Schema.SoapType.Boolean) {
                    sObj.put(s, True);
                } else if (field.getSoapType() == Schema.SoapType.Date) {
                    sObj.put(s, Date.today());
                } else if (field.getSoapType() == Schema.SoapType.DateTime) {
                    sObj.put(s, DateTime.now());
                } else if (field.getSoapType() == Schema.SoapType.Double) {
                    sObj.put(s, 1.2);
                } else if (field.getSoapType() == Schema.SoapType.Integer) {
                    sObj.put(s, 3);
                }
            }
        }
        return sObj;
    }
    
    public static Client__c createClient(Id acctId){
		Client__c clientObj = new Client__c();
		clientObj.Account__c = acctId;
		clientObj.Name = 'Test Client';
		return clientObj;
	}
	
	public static Contract_Document__c createContractDoc(Id clientId){
		Contract_Document__c contractDocObj = new Contract_Document__c();
		contractDocObj.Client__c = clientId;
		return contractDocObj;
	}
    
    public static Contract_Document__c createContractDoc2(Id opportunityId){
		Contract_Document__c contractDocObj2 = new Contract_Document__c();
		contractDocObj2.Opportunity__c = opportunityId;
		return contractDocObj2;
	}

	public static QService__c createService(Id opportunityId, Id quoteId){
		QService__c serviceObj = new QService__c();
		serviceObj.Opportunity__c = opportunityId;
        serviceObj.Name = 'test service for qupdates';
        serviceObj.Quote__c = quoteId;
        serviceObj.Status__c = 'SOW in QSO';
        serviceObj.Name = 'Test Service 1';
        serviceObj.Custom__c = false;
        serviceObj.Service_Type__c = 'Implementation';        
		return serviceObj;
	}
    
    public static Bundle__c createBundle(){
		Bundle__c bundleObj = new Bundle__c();		
        bundleObj.Name = 'Test bundle';
        bundleObj.Experience__c = 'CX';
        bundleObj.Bundle__c = '0';
        bundleObj.Active__c = TRUE;
		return bundleObj;
	}
    
   public static Product_Bundle_Junction__c createBundleProduct(Id bundleId){
		Product_Bundle_Junction__c bundleProductObj = new Product_Bundle_Junction__c();		
        bundleProductObj.Name = 'Test Bundle Product';
        bundleProductObj.Availability__c = 'Available';        
        bundleProductObj.Related_Bundle__c = bundleId;
		return bundleProductObj;
	}
    
    public static Service_Bundle__c createServiceBundle(Id bundleProductId){
		Service_Bundle__c serviceBundleObj = new Service_Bundle__c();		
        serviceBundleObj.Name = 'CX 1 Standard Implementation';
        serviceBundleObj.Bundle_Type__c = 'Implementation';
        serviceBundleObj.Conga_Template_Code__c = 'imp_cx1_sow';
        serviceBundleObj.Bundle_Product_Prerequisite__c = bundleProductId;
		return serviceBundleObj;
	}

}