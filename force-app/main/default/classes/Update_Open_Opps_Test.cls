@isTest
public class Update_Open_Opps_Test {
    @isTest
    public static void updateopenopportunitycounttest(){
        List<Id> testList = new List<Id>();
        //Contact base = [select Id from Contact Limit 1];
        testList.add('0035000003GsaNSAAZ');
        Integer actual_num = [select Id from Opportunity where (isClosed != true and Qualification_Contact__c =:testList)].size();
        
        Update_Open_Opps.updateopenopportunitycount(testList);
        
        Contact updated = [select Id, Number_of_Open_Opps__c from Contact where Id =:testList[0]];
        System.assertEquals(updated.Number_of_Open_Opps__c, actual_num);
    }
}