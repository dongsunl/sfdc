public with sharing class XM_DiscountsController {
    public QuoteDetails wrapObj {get; set;} //To display relevent info on page
    public String ISOCode {get; set;} //For currency ISO code
    Id quoteId;//Class level variable to store quote Id
    public List<Discount_Approver__mdt> lstDiscountApprover {get; set;}//Display data from custom metadata
    public List<QuoteLineItem> lstQuoteLineItems {get; set;}
    public List<Help_Note__mdt> lstHelpNote;
    public String helpText {get; set;}
    public Map<String, String> fieldLabelToHelpNoteMap {get; set;}
    //
    private static List<Field> lstField = new List<Field> {
        new Field(1, 'Total Eligible ARR'),
            new Field(2, 'Available Bundle Discount'),
            new Field(3, 'Desired Final ARR'),
            new Field(4, 'Reason'),
            new Field(5, 'free months'),
            new Field(6, 'Prorated Value of Tech'),
            new Field(7, 'Services'),
            new Field(8, 'Summit Section'),
            new Field(9, 'Quote Subtotal'),
            new Field(10, 'Discount Calculations')
            };
                
                
                private static Map<Decimal, String> MapFields = new Map<Decimal, String> {
                    1 => 'Total Eligible ARR',
                        2 => 'Available Bundle Discount',
                        3 => 'Desired Final ARR',
                        4 => 'Reason',
                        5 => 'free months',
                        6 => 'Prorated Value of Tech',
                        7 => 'Services',
                        8 => 'Summit Section',
                        9 => 'Quote Subtotal',
                        10 => 'Discount Calculations'
                        };
                            
                            /*
@ Description : Constructor to initialize
@ Parameters  : Standard controller
*/
                            public XM_DiscountsController(ApexPages.StandardController sc) {
                                ISOCode = UserInfo.getDefaultCurrency();//Get User currecy ISO code
                                Quote quoteObj = (Quote)sc.getRecord();
                                fieldLabelToHelpNoteMap = new Map<String, String>();
                                quoteId = sc.getId();//Get quote id from URL
                                lstHelpNote = new List<Help_Note__mdt>();
                                lstDiscountApprover = new List<Discount_Approver__mdt>();
                                getQuoteData();
                            }
    
    /*
@ Description : Method to calaculate various fields and display on page
*/
    public void getQuoteData() {
        // Help Text
        for (Field field : lstField) {
            fieldLabelToHelpNoteMap.put(field.label, '');
        }
        for (Help_Note__mdt help : [SELECT  Help_Number__c, Help_Text__c
                                    FROM    Help_Note__mdt]) {
                                        if (help.Help_Number__c != NULL && MapFields.containsKey(help.Help_Number__c)) {if (help.Help_Text__c != Null) {fieldLabelToHelpNoteMap.put(MapFields.get(help.Help_Number__c), help.Help_Text__c);
                                            }
                                        }
                                    }
        wrapObj = new QuoteDetails();
        Quote quoteObj = [SELECT License_Term_in_months__c,Name,
                          Free_Months_Included__c,Requested_Discount_Price__c,
                          DiscountRequestNotes_New__c,License_Length_Input__c,
                          Discount_Amount__c,Summit_Discount_Amount__c,
                          Discount_Approval_Status__c,Discretionary_Discount__c,Self_Approval_Threshold__c,
                          Opportunity.CurrencyIsoCode,
                          Cross_Sell__c,Quote_Type__c,Historical_Discount__c,OpportunityId,QuoteNumber,Opportunity.Name
                          FROM Quote
                          WHERE Id = : quoteId LIMIT 1];
        wrapObj.quoteObj = quoteObj;
        ISOCode = quoteObj.Opportunity.CurrencyIsoCode;//Get records's currency ISO code
        lstQuoteLineItems = [SELECT Item_Original_Price__c,Credit_Type__c,TotalPrice,Product_Family__c,
                             Product_Sub_Family__c, quoteId, Max_Bundle_Discount__c, Eligible_for_Discount__c, quote.DiscountRequestNotes_New__c,
                             quote.Free_Months_Included__c, quote.License_Length_Input__c, Bundle_Discount_Ineligibility__c, quote.Requested_Discount_Price__c, Product2.Name,
                             Subtotal__c, Product_Code__c, Quantity, ARR__c, External_Display_Name__c
                             FROM QuoteLineItem
                             WHERE quoteId = : quoteId
                             ORDER BY Product_Family__c, Product_Sub_Family__c];
        lstDiscountApprover = [SELECT MasterLabel,Discount_Percent__c,Display_Order__c,Is_Premium__c
                               FROM Discount_Approver__mdt
                               ORDER BY Display_Order__c];
        //calculations that only run onload
        if(quoteObj.Free_Months_Included__c != null){
            wrapObj.freeMonths = quoteObj.Free_Months_Included__c;
        } else {
            wrapObj.freeMonths = 0;
        }
        if(quoteObj.Quote_Type__c != null){
            wrapObj.quoteType = quoteObj.Quote_Type__c;
        } else {
            wrapObj.quoteType = '';
        }
        //this is just used to set the scale to 2 decimal places.
        if (quoteObj.Requested_Discount_Price__c != null) {
            wrapObj.quoteObj.Requested_Discount_Price__c = quoteObj.Requested_Discount_Price__c.setScale(2);
        }
        if (quoteObj.Summit_Discount_Amount__c != null) {
            wrapObj.quoteObj.Summit_Discount_Amount__c = quoteObj.Summit_Discount_Amount__c.setScale(2);
        }
        
        //calculations that run repeatedly.
        calculateFormValues();
    }
    
    public void calculateFormValues() {
        wrapObj.totalEligibleARR = 0;
        wrapObj.availableBundleDiscount = 0;
        wrapObj.serviceSubtotal = 0;
        wrapObj.prorated_Value_of_Tech = 0;
        wrapObj.discretionary_Discount = 0;
        wrapObj.discretionary_DiscountMod = 0;
        wrapObj.summitPass = 0;
        wrapObj.summitSubtotal = 0;
        wrapObj.summitAfterDiscount = 0;
        wrapObj.premiumAmt = 0;
        wrapObj.premiumPercent = 0;
        wrapObj.licenseLength = 0;
        wrapObj.lstServices = new List<QuoteLineItem>();
        wrapObj.lstCredits = new List<QuoteLineItem>();
        wrapObj.lstSummits = new List<QuoteLineItem>();
        wrapObj.ineligibleItemsTotal = 0;
        wrapObj.postBundleDiscountAmount = 0;
        
        for (QuoteLineItem qli : lstQuoteLineItems) {
            //if(qli.Product_Code__c != null && qli.Product_Code__c.containsIgnoreCase('SUMPass')){
                //wrapObj.lstSummits.add(qli);
            //}
            if(qli.Product_Sub_Family__c == 'Credit'){
                qli.External_Display_Name__c = 'Credit';
                //    wrapObj.lstCredits.add(qli);
                //    if(qli.Subtotal__c != null){
                //        wrapObj.serviceSubtotal += qli.Subtotal__c;
                //         wrapObj.ineligibleItemsTotal += qli.Subtotal__c;
                //   }
            } else if(qli.Product_Family__c == 'Services'){
                qli.External_Display_Name__c = 'Service';
                //   wrapObj.lstServices.add(qli);
                //   if(qli.Subtotal__c != null){
                //       wrapObj.serviceSubtotal += qli.Subtotal__c;
                //       wrapObj.ineligibleItemsTotal += qli.Subtotal__c;
                //   }
            } else if(qli.Product_Code__c != null && qli.Product_Code__c =='SUMPassGen'){wrapObj.lstSummits.add(qli); wrapObj.summitPass += qli.Quantity; wrapObj.summitSubtotal += qli.Item_Original_Price__c;
                 if(qli.Subtotal__c != null){
                     wrapObj.serviceSubtotal += qli.Subtotal__c;
                     //wrapObj.ineligibleItemsTotal += qli.Subtotal__c;
                }
              }
            else if(qli.Bundle_Discount_Ineligibility__c == true){qli.External_Display_Name__c = 'Tech';}
            else{
                qli.External_Display_Name__c = 'Other';
            }
            if (qli.Eligible_for_Discount__c == true && qli.Bundle_Discount_Ineligibility__c == false) {
                if(qli.ARR__c != null){
                    wrapObj.totalEligibleARR += qli.ARR__c.setScale(2);
                }
                if (qli.Max_Bundle_Discount__c != NULL && qli.Max_Bundle_Discount__c > 0) {
                    wrapObj.availableBundleDiscount += (qli.Max_Bundle_Discount__c * qli.Quantity).setScale(2);
                }
                
            }else{
                if(qli.TotalPrice != 0 && qli.Product_Code__c !='SUMPass'){
                    wrapObj.lstCredits.add(qli);
                }
                
                if(qli.Subtotal__c != null){
                    wrapObj.serviceSubtotal += qli.Subtotal__c;
                    wrapObj.ineligibleItemsTotal += qli.Subtotal__c;
                }
                
            }
        }
        if (wrapObj.quoteObj.License_Length_Input__c == null && wrapObj.quoteObj.License_Term_in_months__c != null) {wrapObj.quoteObj.License_Length_Input__c = wrapObj.quoteObj.License_Term_in_months__c;} else if (wrapObj.quoteObj.License_Length_Input__c == null) {wrapObj.quoteObj.License_Length_Input__c = 0;}        
        wrapObj.licenseLength = wrapObj.freeMonths + wrapObj.quoteObj.License_Length_Input__c;
        if(wrapObj.quoteObj.Requested_Discount_Price__c != null){
            wrapObj.premiumAmt = (wrapObj.quoteObj.Requested_Discount_Price__c - wrapObj.totalEligibleARR).setScale(2);
        } else {
            wrapObj.premiumAmt = (0 - wrapObj.totalEligibleARR).setScale(2);
        }
        if (wrapObj.premiumAmt > 0 && wrapObj.totalEligibleARR > 0) {wrapObj.premiumPercent = (wrapObj.premiumAmt / wrapObj.totalEligibleARR).setScale(2);}
        if (wrapObj.totalEligibleARR != null &&
            wrapObj.availableBundleDiscount != null &&
            wrapObj.quoteObj.Requested_Discount_Price__c != null
           ) {
               wrapObj.discretionary_Discount = (wrapObj.totalEligibleARR - wrapObj.availableBundleDiscount - wrapObj.quoteObj.Requested_Discount_Price__c).setScale(2);
               if(wrapObj.discretionary_Discount < 0){
                   wrapObj.discretionary_Discount = 0;
               }
           }
        if (
            wrapObj.discretionary_Discount != null 
            && wrapObj.totalEligibleARR != null 
            && wrapObj.totalEligibleARR != 0 
            && (
                wrapObj.availableBundleDiscount == null
                || wrapObj.availableBundleDiscount == 0
            )
        ) {//without bundle discount
            wrapObj.discretionary_DiscountMod = (wrapObj.discretionary_Discount / wrapObj.totalEligibleARR).setScale(2);
        } else if (
            wrapObj.discretionary_Discount != null 
            && wrapObj.totalEligibleARR != null 
            && wrapObj.totalEligibleARR != 0 && wrapObj.availableBundleDiscount != null && wrapObj.availableBundleDiscount != 0) {wrapObj.discretionary_DiscountMod = (wrapObj.discretionary_Discount / (wrapObj.totalEligibleARR - wrapObj.availableBundleDiscount)).setScale(2);}
        if(
            wrapObj.totalEligibleARR != null 
            && wrapObj.totalEligibleARR != 0 && wrapObj.availableBundleDiscount != null && wrapObj.availableBundleDiscount != 0){wrapObj.postBundleDiscountAmount = wrapObj.totalEligibleARR - wrapObj.availableBundleDiscount;} 
        else if(
            wrapObj.totalEligibleARR != null
        ){
            wrapObj.postBundleDiscountAmount = wrapObj.totalEligibleARR;
        }
        checkDiscountApprovalStatus();
        if(wrapObj.lstSummits.size() > 0){calcSummitAfterDiscount();
        }
        calculateProratedValueOfTech();
    }
    
    public void calculateProratedValueOfTech(){
        Decimal requestedARR = wrapObj.quoteObj.Requested_Discount_Price__c;
        Decimal proratedCredits = 0;
        Decimal discretionaryDiscount = wrapObj.discretionary_Discount;
        Decimal bundleDiscount;
        Decimal originalARR = 0;
        Decimal otherIneligibleLineItems = 0;
        if(requestedARR == null && wrapObj.totalEligibleARR != null){requestedARR = wrapObj.totalEligibleARR;}
        if(wrapObj.availableBundleDiscount != null){
            bundleDiscount = wrapObj.availableBundleDiscount;
        }
        for(QuoteLineItem qli : lstQuoteLineItems){
            if(qli.Credit_Type__c == 'Proration Credit'){proratedCredits += qli.TotalPrice;}
            if(
                qli.Product_Sub_Family__c != 'Credit'
                && qli.Product_Family__c != 'Services'
                && qli.Product2.Name != null
                && !qli.Product_Code__c.containsIgnoreCase('SUMPass')
            ){
                if(qli.Item_Original_Price__c != null){
                    originalARR += qli.Item_Original_Price__c;
                }                
            } else if(qli.Credit_Type__c != 'Proration Credit'){
                otherIneligibleLineItems += qli.TotalPrice;
            }
        }
        system.debug(logginglevel.error, '***** requestedARR='+requestedARR);
        system.debug(logginglevel.error, '***** proratedCredits='+proratedCredits);
        system.debug(logginglevel.error, '***** discretionaryDiscount='+discretionaryDiscount);
        system.debug(logginglevel.error, '***** bundleDiscount='+bundleDiscount);
        system.debug(logginglevel.error, '***** originalARR='+originalARR);
        system.debug(logginglevel.error, '***** otherIneligibleLineItems='+otherIneligibleLineItems);
        Decimal step1;
        if(bundleDiscount != null){
            step1 = originalARR - bundleDiscount;
        } else {
            step1 = originalARR;
        }
        system.debug(logginglevel.error, '***** originalARR - bundleDiscount = '+step1);
        Decimal step2;
        if(step1 != 0){
            step2 = 1 - (discretionaryDiscount/step1);
        } else {
            system.debug(logginglevel.error, '***** (step1) originalARR - bundleDiscount was = 0 *****');
            return;
        }
        system.debug(logginglevel.error, '***** (step2) 1 - (discretionaryDiscount/step1) = '+step2);
        Decimal step3 = proratedCredits * step2;
        system.debug(logginglevel.error, '***** (step3) proratedCredits * step2 = '+step3);
        
        wrapObj.prorated_Value_of_Tech = requestedARR + step3 + otherIneligibleLineItems; //- wrapObj.quoteObj.Summit_Discount_Amount__c
        if(wrapObj.quoteObj.Summit_Discount_Amount__c != null && wrapObj.quoteObj.Summit_Discount_Amount__c != 0){
            wrapObj.prorated_Value_of_Tech = wrapObj.prorated_Value_of_Tech - wrapObj.quoteObj.Summit_Discount_Amount__c;
        }
        system.debug(logginglevel.error, '***** prorated_Value_of_Tech = requestedARR + step3 + otherIneligibleLineItems ='+wrapObj.prorated_Value_of_Tech);
        /*
Requested ARR +
[Sum of all Proration Credits (indicated by Credit Type) x (1 - [(Discretionary Discount)/(Original ARR - Bundle Discount)]) +
Other ineligible line items (exclude proration credit)
*/
    }
    
    public PageReference submitForm() {
        system.debug(logginglevel.error, '***** wrapObj.quoteObj.Requested_Discount_Price__c='+wrapObj.quoteObj.Requested_Discount_Price__c);
        system.debug(logginglevel.error, '***** wrapObj.quoteObj.DiscountRequestNotes_New__c='+wrapObj.quoteObj.DiscountRequestNotes_New__c);
        system.debug(logginglevel.error, '***** wrapObj.quoteObj.Discount_Approval_Status__c='+wrapObj.quoteObj.Discount_Approval_Status__c);
        if(wrapObj.quoteObj.Discount_Approval_Status__c != 'Historical Discount'){
            //   system.debug('this is not working');
            if (
                wrapObj.quoteObj.Requested_Discount_Price__c > 0 
                && wrapObj.quoteObj.DiscountRequestNotes_New__c == null
                || wrapObj.quoteObj.DiscountRequestNotes_New__c == ''
                || (String.isNotBlank(wrapObj.quoteObj.DiscountRequestNotes_New__c) 
                    && wrapObj.quoteObj.DiscountRequestNotes_New__c.length() < 10
                   )
            ) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'If Desired Final ARR is populated, Reason must be greater than 10 characters.');
                ApexPages.addMessage(myMsg);
                return null;
            } 
            
        }
        
        wrapObj.summitPass = 0;
        wrapObj.summitSubtotal = 0;
        for (QuoteLineItem qli : lstQuoteLineItems) {
            if (String.isNotBlank(qli.Product_Code__c) &&
                qli.Product_Code__c.containsIgnoreCase('SUMPassGen')
               ) {
                   wrapObj.summitPass += qli.Quantity;
               }
            if (String.isNotBlank(qli.Product_Code__c) &&
                qli.Product_Code__c.containsIgnoreCase('SUMPassGen')
               ) {
                   wrapObj.summitSubtotal += qli.Subtotal__c;
               }
        }
        //convert to record Currency and save
        Opportunity oppObj = [SELECT CurrencyIsoCode
                              from Opportunity
                              WHERE Id = : wrapObj.quoteObj.OpportunityId LIMIT 1];
        //Get conversion rate
        CurrencyType objCurrencyType = [SELECT ConversionRate
                                        FROM CurrencyType
                                        WHERE IsoCode = : oppObj.CurrencyIsoCode LIMIT 1];
        //Saving currency values in record currency
        wrapObj.quoteObj.Free_Months_Included__c = wrapObj.freeMonths;
        wrapObj.quoteObj.License_Term_in_months__c = wrapObj.freeMonths + wrapObj.quoteObj.License_Length_Input__c;
        wrapObj.quoteObj.Discretionary_Discount__c = wrapObj.discretionary_DiscountMod * 100;
        wrapObj.quoteObj.Summit_Number_of_Passes__c = wrapObj.summitPass;
        checkDiscountApprovalStatus();
        update wrapObj.quoteObj;
        return new PageReference('/apex/XM_Discount_Approvals?Id=' + wrapObj.quoteObj.Id);
    }
    
    public void checkDiscountApprovalStatus(){
        // If Discount Calculations
        if (
            wrapObj.quoteObj.Requested_Discount_Price__c != null &&
            wrapObj.quoteObj.Requested_Discount_Price__c <= wrapObj.totalEligibleARR
        ) {
            wrapObj.quoteObj.Requested_Discount_Amount__c = wrapObj.discretionary_Discount;
            wrapObj.quoteObj.Requested_Discount__c = 100 * wrapObj.discretionary_DiscountMod;
            if (wrapObj.discretionary_DiscountMod <= (wrapObj.quoteObj.Self_Approval_Threshold__c/100) &&
                wrapObj.quoteObj.Cross_Sell__c == false &&
                wrapObj.quoteObj.Quote_Type__c != 'Non-Core Add-on'
               ) {
                   wrapObj.quoteObj.Discount_Approval_Status__c = 'Historical Discount';
               } else if (wrapObj.quoteObj.Discount_Approval_Status__c == 'Historical Discount') {
                   wrapObj.quoteObj.Discount_Approval_Status__c = null;
               }
        }
        // If Premium Calculations
        else if (
            wrapObj.quoteObj.Requested_Discount_Price__c != null &&
            wrapObj.quoteObj.Requested_Discount_Price__c > wrapObj.totalEligibleARR
        ) {
            wrapObj.quoteObj.Requested_Discount_Amount__c = wrapObj.premiumAmt;
            
            wrapObj.quoteObj.Requested_Discount__c = -100 * wrapObj.premiumPercent;
            if (wrapObj.quoteObj.Historical_Discount__c <= -1 ||
                (wrapObj.quoteObj.Historical_Discount__c < 0 && wrapObj.quoteObj.Historical_Discount__c > -1 && wrapObj.premiumPercent > 0)
               ) {
                   wrapObj.quoteObj.Discount_Approval_Status__c = 'Historical Discount';
               } else if(wrapObj.quoteObj.Discount_Approval_Status__c == 'Historical Discount'){wrapObj.quoteObj.Discount_Approval_Status__c = null;}
        }
    }
    
    public PageReference calcSummitAfterDiscount() {
        wrapObj.summitAfterDiscount = 0;
        if (wrapObj.quoteObj.Summit_Discount_Amount__c != null) {
            wrapObj.quoteObj.Summit_Discount_Amount__c = wrapObj.quoteObj.Summit_Discount_Amount__c.setScale(2);
        }
        if (wrapObj.summitSubtotal > 0 &&
            wrapObj.summitSubtotal > wrapObj.quoteObj.Summit_Discount_Amount__c
           ) {
               wrapObj.summitAfterDiscount = (wrapObj.summitSubtotal - wrapObj.quoteObj.Summit_Discount_Amount__c).setScale(2);
           }
        //calculateFormValues();
        return null;
    }
    
    //Wrapper class to save the details about Quote and related Quote line items
    public class QuoteDetails {
        public Decimal totalEligibleARR {get; set;}
        public Decimal availableBundleDiscount {get; set;}
        public Decimal freeMonths {get; set;}
        public String quoteType {get; set;}
        public Decimal summitAfterDiscount {get; set;}
        //Used extra variable for inputText(doesn't support field)
        public Quote quoteObj {get; set;}
        public Decimal licenseLength {get; set;}
        public Decimal summitPass {get; set;}
        public Decimal summitSubtotal {get; set;}
        public Decimal premiumAmt {get; set;}
        public Decimal premiumPercent {get; set;}
        public Decimal prorated_Value_of_Tech {get; set;}
        public Decimal postBundleDiscountAmount {get;set;}
        public list<QuoteLineItem> lstServices {get; set;}
        public list<QuoteLineItem> lstCredits {get; set;}
        public list<QuoteLineItem> lstSummits {get; set;}
        public Decimal ineligibleItemsTotal {get;set;}
        public Decimal serviceSubtotal {get; set;}
        public Decimal discretionary_Discount {get; set;}
        public Decimal discretionary_DiscountMod {get; set;}
        
    }
    
    private class Field {
        public Decimal helpnumber;
        public String label;
        
        public Field(Decimal helpnumber, String label) {
            this.helpnumber = helpnumber;
            this.label = label;
        }
    }
}
/*
Quote -> Requested_Discount_Price__c : Desired Final ARR
*/