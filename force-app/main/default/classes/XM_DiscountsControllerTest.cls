/*-------------------------------------------------------------------------------------------------
Description: Test class for XM_DiscountsController apex controller of the page XM_Discounts
    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 03-10-2017       | 1. Added test methods to test the functionality
   =============================================================================================================================
*/

@isTest
private class XM_DiscountsControllerTest {
	static Account acct {get;set;}
    static Opportunity opp {get;set;}
	static Quote quoteObj;
	static list<QuotelineItem> lstQLI;

	/*
     @ Description : The test setup runs when every test method starts.
	     			 Any objects you need to create that will be used by more than one method should be created here.
	*/
    @testsetup
    static void test_setup(){
		lstQLI = new list<QuotelineItem>();
    	TestFactory.disableTriggers();
    	acct = TestFactory.createAccount();
    	insert acct;
		opp = new Opportunity();
    	opp = TestFactory.createOpportunity(acct.Id, null);
    	insert opp;

		quoteObj = new Quote();
		quoteObj = TestFactory.createQuote(opp.Id, Test.getStandardPricebookId());
		quoteObj.Requested_Discount_Price__c = 100;
		quoteObj.Free_Months_Included__c = 2;
		quoteObj.License_Length_Input__c = 12;
		quoteObj.Summit_Discount_Amount__c = 12;
		insert quoteObj;
		Product2 prod1 = new Product2(Name = 'Laptop X200',
            Family = 'Hardware',Display_Order__c=78, IsActive=true, Product_Sub_Family__c ='Credit', Eligible_for_Discount__c = true);
        insert prod1;
		Product2 prod2 = new Product2(Name = 'Test Summit',
            Family = 'Hardware',ProductCode = 'SUMPass', Display_Order__c=78, IsActive=true, Eligible_for_Discount__c = true);
        insert prod2;
		Product2 prod3 = new Product2(Name = 'Test Summit',
            Family = 'Services',Display_Order__c=78, IsActive=true, Eligible_for_Discount__c = true);
        insert prod3;
		PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prod1.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
		for(Integer i=0; i<20; i++) {

			QuoteLineItem objQLI = TestFactory.createQuoteLineItem(quoteObj.Id, standardPrice.Id);
			objQLI.List_Price__c = 10;
			// objQLI.Subtotal__c = 50*(i+1);
			// objQLI.Revenue_Type__c = 'Recurring';
			objQLI.Max_Bundle_Discount__c = (i+1)*2;
			if(i<6) {
				objQLI.Product2Id = prod1.Id;
			}
			else if(i>=6 && i<12) {
				objQLI.Product2Id = prod3.Id;
			}
			else {//Test data for Summits
				objQLI.Product2Id = prod2.Id;
			}
			lstQLI.add(objQLI);
		}
		insert lstQLI;
    }

	/*
     @ Description : this will query the static objects at the top and make them availabe in the test methods.
	*/
    static void QueryStartingItems(){
        acct = database.Query('select '+BuildQuery('Account')+' where name = \'test Account\'');
        opp = [SELECT Id from Opportunity where AccountId =: acct.Id LIMIT 1];
		// lstQLI = [SELECT Id FROM QuotelineItem WHERE QuoteId =: quoteObj.ID];
		// quoteObj = database.Query('select '+BuildQuery('Quote')+' where OpportunityId =: opp.Id LIMIT 1');
		quoteObj = [SELECT Id from Quote where OpportunityId =: opp.Id LIMIT 1];
    }

	/*
     @ Description : Method to form query
	 @Return:	   : Returns Query string
	*/
    static string BuildQuery(String obj){
        Map<String, Schema.SObjectField> SobjtField = Schema.getGlobalDescribe().get(obj).getDescribe().fields.getMap();
        string qry = '';
        for (Schema.SObjectField s : SobjtField.values()) {
            qry += s + ',';
        }
        qry = qry.removeEnd(',');
        qry = qry+' from '+ obj;
        return qry;
    }

	/*
     @ Description : Test method to test Quote objct functionality
	*/
	@isTest static void test_QuoteCalculations() {
		QueryStartingItems();
		User objUser = [SELECT ID
						FROM User
						WHERE Id =: UserInfo.getUserId()
						AND isActive = true LIMIT 1];
		System.runAs(objUser) {
			System.Test.startTest();
			ApexPages.StandardController sc = new ApexPages.StandardController(quoteObj);
			PageReference pageRef = Page.QuoteLayout;
			System.Test.setCurrentPage(pageRef);
			pageRef.getParameters().put('id',quoteObj.Id);
			XM_DiscountsController controller = new XM_DiscountsController(sc);
			System.Test.stopTest();
			Quote quoteObj = [SELECT Free_Months_Included__c,Requested_Discount_Price__c
							from Quote
							where Id =: quoteObj.Id LIMIT 1];
			if(quoteObj != NULL) {
				System.assertEquals(controller.wrapObj.freeMonths, (quoteObj.Free_Months_Included__c).setScale(2));
				System.assertEquals(controller.wrapObj.premiumAmt, (quoteObj.Requested_Discount_Price__c -
									controller.wrapObj.totalEligibleARR).setScale(2));
			}

		}
	}
    
	/*
     @ Description : Method to test service, credit and summits' list on page
	*/
	@isTest static void test_QLILists() {
		QueryStartingItems();
		User objUser = [SELECT ID
						FROM User
						WHERE Id =: UserInfo.getUserId()
						AND isActive = true LIMIT 1];
		System.runAs(objUser) {
			System.Test.startTest();
			ApexPages.StandardController sc = new ApexPages.StandardController(quoteObj);
			PageReference pageRef = Page.QuoteLayout;
			System.Test.setCurrentPage(pageRef);
			pageRef.getParameters().put('id',quoteObj.Id);
			XM_DiscountsController controller = new XM_DiscountsController(sc);
			System.Test.stopTest();
			//System.assertEquals(6,controller.wrapObj.lstCredits.size());
			//System.assertEquals(6,controller.wrapObj.lstServices.size());
			//System.assertEquals(8,controller.wrapObj.lstSummits.size());
		}
	}

	/*
     @ Description : Method to test submit functionality
	*/
	@isTest static void test_submitForm() {
		QueryStartingItems();
		User objUser = [SELECT ID
						FROM User
						WHERE Id =: UserInfo.getUserId()
						AND isActive = true LIMIT 1];
		System.runAs(objUser) {
			System.Test.startTest();
			ApexPages.StandardController sc = new ApexPages.StandardController(quoteObj);
			PageReference pageRef = Page.QuoteLayout;
			System.Test.setCurrentPage(pageRef);
			pageRef.getParameters().put('id',quoteObj.Id);
			XM_DiscountsController controller = new XM_DiscountsController(sc);
			controller.wrapObj.freeMonths = 2;
			PageReference retpageRef = controller.submitForm();
			System.Test.stopTest();
			Quote quoteObjActual = [SELECT License_Length_Input__c,License_Term_in_months__c,Free_Months_Included__c
									from Quote
									where Id=: controller.wrapObj.quoteObj.Id];
			Quote quoteObjExpected = [SELECT License_Length_Input__c,License_Term_in_months__c from Quote where Id=: quoteObj.Id];
			System.assertEquals(2, quoteObjActual.Free_Months_Included__c);
		//	System.assertEquals(quoteObjActual.License_Term_in_months__c, 2 + quoteObjExpected.License_Length_Input__c);
		//	System.assertEquals(true, retpageRef.getURL().contains('/apex/XM_Discount_Approvals?Id='+quoteObj.Id));
		}        
	}

	/*
     @ Description : Method to test Reason field length
	*/
	@isTest static void test_reasonLenghthLessthan10chars() {
		QueryStartingItems();
		User objUser = [SELECT ID
						FROM User
						WHERE Id =: UserInfo.getUserId()
						AND isActive = true LIMIT 1];
		System.runAs(objUser) {
			System.Test.startTest();
			ApexPages.StandardController sc = new ApexPages.StandardController(quoteObj);
			PageReference pageRef = Page.QuoteLayout;
			System.Test.setCurrentPage(pageRef);
			pageRef.getParameters().put('id',quoteObj.Id);
			XM_DiscountsController controller = new XM_DiscountsController(sc);
			controller.wrapObj.quoteObj.Requested_Discount_Price__c = 2;
			controller.wrapObj.quoteObj.DiscountRequestNotes_New__c = 'test';
			controller.submitForm();
			System.Test.stopTest();
			System.assertEquals(1, ApexPages.getMessages().size());
		}
	}
}