@isTest 
public class ava_OverrideAddressValidationTest {
    
    static Account acct {get;set;}
    static Client__c client {get;set;}
    static Opportunity opp {get;set;}
    static Quote quote {get;set;}
    
    @testsetup
    static void test_setup(){
        AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
    	insert avaCustomSetting;
        
    	TestFactory.disableTriggers();
    	        
        acct = TestFactory.createAccount();
        insert acct;      
        
        client = TestFactory.createClient(acct.Id);
        client.Shipping_Street__c = '333 W River Park Dr';
        client.Shipping_City__c = 'Provo';
        client.Shipping_Country__c = 'United States';
        client.Shipping_State__c = 'Utah';
        client.Shipping_Zip_Postal_Code__c = '84604';
        insert client; 
        
        opp = TestFactory.createOpportunity(acct.Id, null);
        opp.Client__c = client.Id;
        insert opp;
        
        quote = TestFactory.createQuote(opp.Id, null);
        quote.Related_Client__c  = client.Id;
        insert quote; 
    }

    static void QueryStartingItems(){
        acct = database.Query('select '+BuildQuery('Account')+' where name = \'test Account\'');
        client = [SELECT Id, Shipping_Street__c,  Shipping_City__c, Shipping_Country__c, Shipping_State__c, Shipping_Zip_Postal_Code__c 
                  from Client__c where Account__c =: acct.Id LIMIT 1];
        opp = [SELECT Id, Client__c from Opportunity where AccountId =: acct.Id LIMIT 1];
        quote = [SELECT Id from Quote where AccountId =: acct.Id LIMIT 1];
    }

    static string BuildQuery(String obj){
        Map<String, Schema.SObjectField> SobjtField = Schema.getGlobalDescribe().get(obj).getDescribe().fields.getMap();
        string qry = '';
        for (Schema.SObjectField s : SobjtField.values()) {
            qry += s + ',';
        }
        qry = qry.removeEnd(',');
        qry = qry+' from '+ obj;
        return qry;
    }
    
    @isTest static void testGetQuote() {
		QueryStartingItems();                
                
        ApexPages.StandardController sc = new ApexPages.StandardController(quote);
        PageReference pageRef = Page.ava_AddressValidation;
        pageRef.getParameters().put('id',quote.Id);
        Test.setCurrentPage(pageRef);        
        ava_OverrideAddressValidation controller = new ava_OverrideAddressValidation(sc);
        
        Test.startTest();
        controller.getQuote();
        Test.stopTest();
        
        system.assertNotEquals(controller.quote,null);
    }
    
    @isTest static void testConstructor() {                                        
        QueryStartingItems();                
        
        ava_OverrideAddressValidation controller = new ava_OverrideAddressValidation(acct.Id);
    }
    
    @isTest static void testPageReference() {
        QueryStartingItems();                
                        
        PageReference pageRef = Page.ava_AddressValidation;
        pageRef.getParameters().put('id',quote.Id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(quote);
        ava_OverrideAddressValidation controller = new ava_OverrideAddressValidation(sc);
        
        Test.startTest();
        PageReference result = controller.CopyBillingToShipping();
        PageReference result1 = controller.UpdateAddress();
        Test.stopTest();
        
        System.assertEquals(null, result);        	    
        System.assertEquals(null, result1);
    }
    
    @isTest 
    static void testFetchOriginalAddress() {                       
        QueryStartingItems();                

        PageReference pageRef = Page.ava_AddressValidation;
        pageRef.getParameters().put('id',quote.Id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(quote);
        ava_OverrideAddressValidation controller = new ava_OverrideAddressValidation(sc);
        controller.FetchOriginalAddress();
        controller.UpdateCAddress();                
    }
    
    @isTest 
    static void testUpdateStdController() {
        QueryStartingItems();                
        
        PageReference pageRef = Page.ava_AddressValidation;
        pageRef.getParameters().put('id',quote.Id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(quote);
        ava_OverrideAddressValidation controller = new ava_OverrideAddressValidation(sc);
        AVA_SF_SDK.AddressSvc.ValidAddress ovaValAddr  = new AVA_SF_SDK.AddressSvc.ValidAddress();
        controller.UpdateStdController(ovaValAddr);
    }
    
}