public with sharing class ava_OverrideQuoteAddress extends AVA_SFQUOTES.Quote_GetTax {
    
    private ApexPages.StandardController std;
    public Quote quote;
    
    // query for the additional custom fields to be used in FetchDestinationAddress and FetchTaxData
    
    public ava_OverrideQuoteAddress(ApexPages.StandardController controller)
    {
        std = controller;
        getQuote();
        clopps = [select Id, Shipping_Street__c, Shipping_City__c, Shipping_Country__c,
                                      Shipping_State__c, Shipping_Zip_Postal_Code__c
                                      from Client__c where Id=:Quote.Client__c LIMIT 1]; 
    }
    
    public Quote getQuote() {
        Quote = [SELECT Id, Client__c from Quote WHERE ID =:ApexPages.currentPage().GetParameters().Get('Id')];
        system.debug('Quote: '+Quote.Id);
        return Quote;
        
    }
    
    
    private List<Client__c> clopps; 
    
    // Override FetchDestination Address to fetch from custom fields
    
    public override AVA_SF_SDK.TaxSvc.BaseAddress FetchDestinationAddress()
    {    
        AVA_SF_SDK.TaxSvc.BaseAddress oDestinationAddress = new AVA_SF_SDK.TaxSvc.BaseAddress();
        
        oDestinationAddress.AddressCode = 'D';
        // use client address
        
        oDestinationAddress.Line1 = clopps[0].Shipping_Street__c;
      //  oDestinationAddress.Line2 = clopps[0].Ship_to_Address_2__c;
        oDestinationAddress.City = clopps[0].Shipping_City__c;
        oDestinationAddress.Region = clopps[0].Shipping_State__c;
        oDestinationAddress.PostalCode = clopps[0].Shipping_Zip_Postal_Code__c;
        oDestinationAddress.Country = clopps[0].Shipping_Country__c;
        return oDestinationAddress;  
    }
    
}