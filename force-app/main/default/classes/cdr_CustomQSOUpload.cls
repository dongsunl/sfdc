public class cdr_CustomQSOUpload {
    /*  public static void transferdocs(FeedItem[] c){

List<FeedAttachment> fa = new List<FeedAttachment>();
fa = [SELECT ID, RecordId from FeedAttachment where FeedEntityId in: c];

Set<Id> faId = new Set<Id>();

for(FeedAttachment fas:fa){
faId.add(fas.RecordId);
}

Map<Id,ContentVersion> cMap = new Map<Id,ContentVersion>([Select Id, VersionData, Title from ContentVersion where id in: faId ]);

Set<id> cId = cMap.keySet();

List<attachment> newAtt = new List<Attachment>();
If(cId.size()>0){
for(id cv:cId){

String verData = encodingUtil.base64Encode(cMap.get(cv).VersionData);
String s1 = cMap.get(cv).Title;                
system.debug('s1: ' + s1);
String s2 = s1.substringBetween('QSO-','.pdf');
system.debug('s2: ' + s2);
Id i1 = Id.valueOf(s2);
newAtt.add(new Attachment (ParentId=i1,Description='Custom QSO', name=cMap.get(cv).Title, Body=encodingUtil.base64Decode(verData)));
}
insert newAtt;
}
}*/
    
    
    public static void attachContract(Attachment[] att){
        Set<Id> attSet = new Set<Id>();        
        for (Attachment a: att)
        {     
            if(a.Name.contains('QSO') && (a.Name.contains('exec') || a.Name.contains('Exec')) ){
                attSet.add(a.ParentId);     
            }            
        }
        
        if(!attSet.isEmpty()){
            Map<Id,Contract_Document__c> conMap = new Map<Id,Contract_Document__c>([
                SELECT Id, Status__c, Opportunity__c FROM Contract_Document__c WHERE ID in:attSet]);
            Set<Id> conSet = conMap.keySet();      
            
            List<Contract_Document__c> cons = new List<Contract_Document__c>();
            if(!conSet.isEmpty()){
                for(Id c:conSet){
                    Contract_Document__c con = new Contract_Document__c();
                    con.Id = c;
                    con.Status__c = 'Signed';
                    cons.add(con);
                }
                update cons;
            } 
        }        
    }
    
    public static void attachContractFinal(Attachment[] att){
        Set<Id> attSet = new Set<Id>();        
        for (Attachment a: att)
        {     
            if(a.Name.contains('QSO') && (a.Name.contains('final') || a.Name.contains('Final')) ){
                attSet.add(a.ParentId);     
            }            
        }
        
        if(!attSet.isEmpty()){
            Map<Id,Contract_Document__c> conMap = new Map<Id,Contract_Document__c>([
                SELECT Id, Status__c FROM Contract_Document__c WHERE ID in:attSet]);
            Set<Id> conSet = conMap.keySet();      
            List<Contract_Document__c> cons = new List<Contract_Document__c>();
            if(!conSet.isEmpty()){
                for(Id c:conSet){                
                    Contract_Document__c con = new Contract_Document__c();
                    con.Id = c;
                    con.Status__c = 'Ready for Signature';
                    con.Custom_Contract_Status__c = 'Ready for Signature';
                    cons.add(con);
                }
                update cons;
            } 
        }        
    }
    
}