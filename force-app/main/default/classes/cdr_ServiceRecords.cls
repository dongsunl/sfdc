global class cdr_ServiceRecords {
    public static void updateService(Service__c[] ser){
        Set<Id> opp = new Set<Id>();
        for(Service__c s:ser){
            opp.add(s.Opportunity__c);
        }
        List<Contract_Document__c> cd = [SELECT ID, Opportunity__c FROM Contract_Document__c WHERE Opportunity__c in:opp];
        for(Service__c se:ser){
            for(Contract_Document__c c:cd){
                if(se.Opportunity__c == c.Opportunity__c){
                    se.Contract_Document__c = c.Id;
                }
            }
        }
    }
    
/*    WebService static void updateSOW(Id ser){
        Service__c s = new Service__c();
        s = [SELECT Id, SOW_Project_Schedule__c, SOW_Assumptions_Constraints_Exclusions__c, SOW_Component_1_1__c,
             SOW_Component_1_2__c, SOW_Component_1_3__c, SOW_Fees_and_Invoice_Schedule__c, SOW_Project_Scope_and_Objectives__c,
             SOW_Subscriber_Responsibilities__c
             FROM Service__c where Id='a3c4C000000CiVw'];
        
        Service__c se = new Service__c();
        se.Id = ser;
        se.SOW_Project_Schedule__c = s.SOW_Project_Schedule__c;
        se.SOW_Assumptions_Constraints_Exclusions__c = s.SOW_Assumptions_Constraints_Exclusions__c;
        se.SOW_Component_1_1__c = s.SOW_Component_1_1__c ;
        se.SOW_Component_1_2__c = s.SOW_Component_1_2__c ;
        se.SOW_Component_1_3__c = s.SOW_Component_1_3__c ;
        se.SOW_Fees_and_Invoice_Schedule__c = s.SOW_Fees_and_Invoice_Schedule__c ;
        se.SOW_Project_Scope_and_Objectives__c = s.SOW_Project_Scope_and_Objectives__c ;
        se.SOW_Subscriber_Responsibilities__c = s.SOW_Subscriber_Responsibilities__c ;      
        update se;
        
    }*/
    
 /*   public static void moveSOW(Service__c[] ser){
        
        Set<Id> opp = new Set<Id>();
        for(Service__c s:ser){
            if(s.SOW_Approval_Status__c == 'Approved' && s.Service_Type__c == 'Professional Services'){
                opp.add(s.Opportunity__c);
            }
        }
        
        if(!opp.isEmpty()){
            Contract_Document__c cd = [SELECT ID, Opportunity__c FROM Contract_Document__c WHERE Opportunity__c in:opp];
            
            Attachment att = [SELECT Id, Name, Body from Attachment 
                              WHERE ParentId in:ser /*and Name LIKE 'Professional Services SOW'*/ 
            /*                 order by CreatedDate Desc LIMIT 1];
            
            Attachment attNew = new Attachment(name=att.Name, body=att.Body, parentid=cd.Id);
            
            insert attNew;
        }
    }*/
    
    public static void updateImp(Service__c[] ser){
        
        List<Contract_Document__c> cd = new List<Contract_Document__c>();
        
        For(Service__c s:ser){
            if(s.Contract_Document__c !=Null && s.Service_Type__c == 'Implementation Services' && s.EstimateNotesForRep__c!=Null){
                Contract_Document__c cdNew = new Contract_Document__c();
                cdNew.Id = s.Contract_Document__c;
                cdNew.Implementation__c = s.EstimateNotesforRep__c.left(255);
              //  cdNew.Implementation_Temp__c = s.EstimateNotesforRep__c.left(255);
          
                cd.add(cdNew);
            }            
        }
        update cd;
        
    }
}