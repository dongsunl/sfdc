public class cdrl_ContractTemplates {
    public static void createQSOStandalone(List<Contract_Document__c> newList, map<Id,Contract_Document__c> OldMap){
        
        Map<Id,APXTConga4__Conga_Template__c> ct = new Map<Id,APXTConga4__Conga_Template__c>([
            Select Id, APXTConga4__Name__c, APXTConga4__Template_Group__c, APXTConga4__Description__c 
            from APXTConga4__Conga_Template__c WHERE APXTConga4__Name__c ='QSO Standalone'
        ]);
        
        Set<Id> ctSet = ct.keySet();
        
        List<Contract_Document_Template__c> dt = new List<Contract_Document_Template__c>();
        
        for(Contract_Document__c cd:newList){
            
            for(Id c:ctSet){
                string tempid = ct.get(c).Id; 
                //decimal order = Decimal.valueOf(ct.get(c).APXTConga4__Description__c); 
                Contract_Document_Template__c dtNew = new Contract_Document_Template__c();
                dtNew.Name = ct.get(c).APXTConga4__Name__c;
                dtNew.Template_Type__c = 'QSO Legal';
                dtNew.Contract_Document__c = cd.Id;
                dtNew.Template_Id__c = tempid.left(15);                    
                if(ct.get(c).APXTConga4__Description__c != null){
                    dtNew.Sort_Order__c = Decimal.valueOf(ct.get(c).APXTConga4__Description__c);                    
                }
                dt.add(dtNew);                
            }
        }
        insert dt;
    }
    
    public static void createImpLines(List<Contract_Document__c> newList, map<Id,Contract_Document__c> OldMap){
       // system.debug('this worked too');
        List<Contract_Document__c> lstCd = new List<Contract_Document__c>();
        //system.debug('start here');
       // system.debug(newList);
       // system.debug(OldMap);
        for(Contract_Document__c obj:newList){
          //  if(OldMap != Null &&(obj.Implementation__c != OldMap.get(obj.Id).Implementation__c))
          //  {
          //      system.debug('prod: '+obj.Implementation__c);
          //      system.debug('oldprod: '+OldMap.get(obj.Id).Implementation__c);
                lstCd.add(obj);
           // }
           
           // else if (oldMap == NULL){
            //    lstCd.add(obj);
           // }
        }
        
        if(lstCd!=Null && lstCd.size()>0){
            List<Contract_Document_Template__c> dtdelete = [
                SELECT Id 
                FROM Contract_Document_Template__c 
                where Contract_Document__c in: lstCd and Template_Type__c = 'Implementation'];
            if(!dtdelete.isEmpty()){
                delete dtdelete;
            }
            
            
            Map<Id,APXTConga4__Conga_Template__c> ct = new Map<Id,APXTConga4__Conga_Template__c>([
                Select Id, APXTConga4__Name__c, APXTConga4__Template_Group__c, APXTConga4__Description__c  
                from APXTConga4__Conga_Template__c WHERE APXTConga4__Template_Group__c = 'Implementation'
            ]);
            
            Set<Id> ctSet = ct.keySet();
             
            List<Contract_Document_Template__c> dt = new List<Contract_Document_Template__c>();
            
            for(Contract_Document__c cd:lstCd){
                if(cd.Implementation__c!=NULL){
                    for(Id c:ctSet){
                        string tempid = ct.get(c).Id;   
                        //decimal order = Decimal.valueOf(ct.get(c).APXTConga4__Description__c); 
                        If(cd.Implementation__c.Contains(ct.get(c).APXTConga4__Name__c)){
                            Contract_Document_Template__c dtNew = new Contract_Document_Template__c();
                            dtNew.Name = ct.get(c).APXTConga4__Name__c;
                            dtNew.Template_Type__c = 'Implementation';
                            dtNew.Contract_Document__c = cd.Id;
                            if(ct.get(c).APXTConga4__Description__c != null){
                                dtNew.Sort_Order__c = Decimal.valueOf(ct.get(c).APXTConga4__Description__c);
                            }
                            dtNew.Template_Id__c = tempid.left(15);
                            dt.add(dtNew);
                        }
                    } 
                }
            }
            insert dt;
        }     
    }
    
    
    public static void createQSOLines(List<Contract_Document__c> newList, map<Id,Contract_Document__c> OldMap){
   
        //    if(lstCd!=Null && lstCd.size()>0){
        Map<Id,APXTConga4__Conga_Template__c> ct = new Map<Id,APXTConga4__Conga_Template__c>([
            Select Id, APXTConga4__Name__c, APXTConga4__Template_Group__c, APXTConga4__Description__c 
            from APXTConga4__Conga_Template__c WHERE APXTConga4__Template_Group__c ='QSO Required'
        ]);
        
        Set<Id> ctSet = ct.keySet();
        
        List<Contract_Document_Template__c> dt = new List<Contract_Document_Template__c>();
        
        for(Contract_Document__c cd:newList){
            //  if(cd.Products__c!=Null){
            for(Id c:ctSet){
                string tempid = ct.get(c).Id; 
                //decimal order = Decimal.valueOf(ct.get(c).APXTConga4__Description__c); 
                Contract_Document_Template__c dtNew = new Contract_Document_Template__c();
                dtNew.Name = ct.get(c).APXTConga4__Name__c;
                dtNew.Template_Type__c = 'QSO';
                dtNew.Contract_Document__c = cd.Id;
                dtNew.Template_Id__c = tempid.left(15);                    
                if(ct.get(c).APXTConga4__Description__c != null){
                    dtNew.Sort_Order__c = Decimal.valueOf(ct.get(c).APXTConga4__Description__c);                    
                }
                dt.add(dtNew);                
            }
            //  }        
        }
        insert dt;
        //  }     
    }
    
    public static void createQuoteLines(List<Contract_Document__c> newList, map<Id,Contract_Document__c> OldMap){
        
        List<Contract_Document__c> lstCd = new List<Contract_Document__c>();
        
        for(Contract_Document__c obj:newList){
            if(OldMap != Null &&(obj.Quote_Type__c != OldMap.get(obj.Id).Quote_Type__c))
            {                
                lstCd.add(obj);
            }
            else if (oldMap == NULL){
                lstCd.add(obj);
            }
        }
        
        if(lstCd!=Null && lstCd.size()>0){
            List<Contract_Document_Template__c> dtdelete = [
                SELECT Id 
                FROM Contract_Document_Template__c 
                where Contract_Document__c in: lstCd and Template_Type__c = 'Quote'];
            if(!dtdelete.isEmpty()){
                delete dtdelete;
            }
            
            Map<Id,APXTConga4__Conga_Template__c> ct = new Map<Id,APXTConga4__Conga_Template__c>([
                Select Id, APXTConga4__Name__c, APXTConga4__Template_Group__c, APXTConga4__Description__c 
                from APXTConga4__Conga_Template__c WHERE APXTConga4__Template_Group__c = 'Quote'
            ]);
            
            Set<Id> ctSet = ct.keySet();
            
            List<Contract_Document_Template__c> dt = new List<Contract_Document_Template__c>();
            
            for(Contract_Document__c cd:lstCd){
                if(cd.Quote_Type__c!=Null){
                    for(Id c:ctSet){
                        if(ct.get(c).APXTConga4__Name__c == cd.Quote_Type__c){
                            string tempid = ct.get(c).Id; 
                            //decimal order = Decimal.valueOf(ct.get(c).APXTConga4__Description__c); 
                            Contract_Document_Template__c dtNew = new Contract_Document_Template__c();
                            dtNew.Name = ct.get(c).APXTConga4__Name__c;
                            dtNew.Template_Type__c = 'Quote';
                            dtNew.Contract_Document__c = cd.Id;
                            dtNew.Template_Id__c = tempid.left(15);
                            if(ct.get(c).APXTConga4__Description__c != null){
                                dtNew.Sort_Order__c = Decimal.valueOf(ct.get(c).APXTConga4__Description__c);
                            }
                            dt.add(dtNew);    
                        }   
                        
                    }
                }  
            }
            insert dt;
        }     
    }  
    
    
    public static void createQSOLangLines(List<Contract_Document__c> newList, map<Id,Contract_Document__c> OldMap){
        

        List<Contract_Document_Template__c> dtdelete = [
            SELECT Id 
            FROM Contract_Document_Template__c 
            where Contract_Document__c in: newList and Template_Type__c = 'QSO Legal'];
        if(!dtdelete.isEmpty()){
            delete dtdelete;
        }
        
        Map<Id,APXTConga4__Conga_Template__c> ct = new Map<Id,APXTConga4__Conga_Template__c>([
            Select Id, APXTConga4__Name__c, APXTConga4__Template_Group__c, APXTConga4__Description__c  
            from APXTConga4__Conga_Template__c WHERE APXTConga4__Template_Group__c = 'QSO'
        ]);
        
        Set<Id> ctSet = ct.keySet();
        
        List<Contract_Document_Template__c> dt = new List<Contract_Document_Template__c>();
        
        for(Contract_Document__c cd:newList){
            if(cd.Language_Type__c!=NULL){
                for(Id c:ctSet){
                    string tempid = ct.get(c).Id;   
                    //decimal order = Decimal.valueOf(ct.get(c).APXTConga4__Description__c); 
                    If(cd.MSA__c == TRUE && ct.get(c).APXTConga4__Name__c == 'QSO with MSA')
                    {
                        Contract_Document_Template__c dtNew = new Contract_Document_Template__c();
                        dtNew.Name = ct.get(c).APXTConga4__Name__c;
                        dtNew.Template_Type__c = 'QSO Legal';
                        dtNew.Contract_Document__c = cd.Id;
                        if(ct.get(c).APXTConga4__Description__c != null){
                            dtNew.Sort_Order__c = Decimal.valueOf(ct.get(c).APXTConga4__Description__c);
                        }
                        dtNew.Template_Id__c = tempid.left(15);
                        dt.add(dtNew);
                    }
                    else If(cd.MSA__c == FALSE && ct.get(c).APXTConga4__Name__c == 'QSO Standalone')
                    {
                        Contract_Document_Template__c dtNew = new Contract_Document_Template__c();
                        dtNew.Name = ct.get(c).APXTConga4__Name__c;
                        dtNew.Template_Type__c = 'QSO Legal';
                        dtNew.Contract_Document__c = cd.Id;
                        if(ct.get(c).APXTConga4__Description__c != null){
                            dtNew.Sort_Order__c = Decimal.valueOf(ct.get(c).APXTConga4__Description__c);
                        }
                        dtNew.Template_Id__c = tempid.left(15);
                        dt.add(dtNew);
                    }
                }
            }
        }
        insert dt;
         
    }  
  
    
      public static void createMultiYearQuoteLines(List<Contract_Document__c> newList, map<Id,Contract_Document__c> OldMap){
        
        List<Contract_Document__c> lstCd = new List<Contract_Document__c>();
        
        for(Contract_Document__c obj:newList){
            if(OldMap != Null &&(obj.MultiYear_Quote__c != OldMap.get(obj.Id).MultiYear_Quote__c))
            {                
                lstCd.add(obj);
            }
            else if (oldMap == NULL){
                lstCd.add(obj);
            }
        }
        
        if(lstCd!=Null && lstCd.size()>0){
            List<Contract_Document_Template__c> dtdelete = [
                SELECT Id 
                FROM Contract_Document_Template__c 
                where Contract_Document__c in: lstCd and Template_Type__c = 'Multi-Year Quote'];
            if(!dtdelete.isEmpty()){
                delete dtdelete;
            }
            
            Map<Id,APXTConga4__Conga_Template__c> ct = new Map<Id,APXTConga4__Conga_Template__c>([
                Select Id, APXTConga4__Name__c, APXTConga4__Template_Group__c, APXTConga4__Description__c 
                from APXTConga4__Conga_Template__c WHERE APXTConga4__Template_Group__c = 'Multi-Year Quote'
            ]);
            
            Set<Id> ctSet = ct.keySet();
            
            List<Contract_Document_Template__c> dt = new List<Contract_Document_Template__c>();
            
            for(Contract_Document__c cd:lstCd){
                if(cd.Quote_Type__c!=Null){
                    for(Id c:ctSet){
                        if(cd.MultiYear_Quote__c == true){
                            string tempid = ct.get(c).Id; 
                            //decimal order = Decimal.valueOf(ct.get(c).APXTConga4__Description__c); 
                            Contract_Document_Template__c dtNew = new Contract_Document_Template__c();
                            dtNew.Name = ct.get(c).APXTConga4__Name__c;
                            dtNew.Template_Type__c = 'Multi-Year Quote';
                            dtNew.Contract_Document__c = cd.Id;
                            dtNew.Template_Id__c = tempid.left(15);
                            if(ct.get(c).APXTConga4__Description__c != null){
                                dtNew.Sort_Order__c = Decimal.valueOf(ct.get(c).APXTConga4__Description__c);
                            }
                            dt.add(dtNew);    
                        }   
                        
                    }
                }  
            }
            insert dt;
        }     
    }  
    
    
}