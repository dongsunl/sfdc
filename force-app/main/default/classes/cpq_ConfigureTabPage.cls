public with sharing class cpq_ConfigureTabPage {
    
    @auraEnabled
    public static List<XM_Experience__mdt> getExperiences(string selectedExperience){
        String query = 'SELECT MasterLabel, Code__c, Active__c, Core_Type__c, Core_Entry_Type__c, Dashboard_Users__c FROM XM_Experience__mdt WHERE Active__c = TRUE and Code__c = :selectedExperience';
        List<XM_Experience__mdt> sobjList = Database.query(query);
        system.debug('experiences: ' + sobjList);
        return sobjList;
    }
    
    @AuraEnabled
    public static List<Bundle__c> getBundles(string selectedExperience, list<string> excludedBundles){
        return [SELECT Id, Name, NS_Item_ID__c, Bundle__c, Experience__c, 
                Bundle_Full_Name__c, Unlimited_Responses__c, Academic_Tier_Comparison__c, Sort_Order__c
                FROM Bundle__c WHERE Experience__c =: selectedExperience and Active__c = true and Name not in :excludedBundles and Bundle__c != '0'  ORDER BY Sort_Order__c];
    }
    
    @AuraEnabled
    public static List<Bundle__c> getUpgradeBundles(string bundleId, list<string> excludedBundles, string bundleName) {
        Bundle__c currentBundle = [SELECT Id, Name, NS_Item_ID__c, Bundle__c, Experience__c, 
                                   Bundle_Full_Name__c, Unlimited_Responses__c, Upgrade_Bundles__c
                                   FROM Bundle__c WHERE (Id = :bundleId OR Name = :bundleName) and Active__c = true LIMIT 1];
        system.debug(currentBundle);
        List<String> upgradeBundles = currentBundle.Upgrade_Bundles__c.split(';');
        return [SELECT Id, Name, NS_Item_ID__c, Bundle__c, Experience__c, 
                Bundle_Full_Name__c, Unlimited_Responses__c, Upgrade_Bundles__c, Academic_Tier_Comparison__c, Sort_Order__c
                FROM Bundle__c WHERE Active__c = true and Name not in :excludedBundles and Name in :upgradeBundles ORDER BY Sort_Order__c];
    }
    
    @AuraEnabled
    public static List<Product_Bundle_Junction__c> getProducts(Id bundleId){       
        return [SELECT Id, Related_Product__c, Name, Related_Product__r.Name, Related_Product__r.ProductCode, PricingType__c, 
                Related_Bundle__r.Bundle__c, Related_Bundle__r.Experience__c, Availability__c, Bundle_Key__c, User_Input_Required__c,
                Quantity__c, Quantity_Multiplier__c, Related_Product__r.Revenue_Roll_up__c, Quantity_Multiplied_by_Core__c, 
                Display_Label__c, Display_Variable__c, Upgrade_Availability__c, Related_Product__r.Revenue_Recognition_Rule__c,
                Related_Product__r.Revenue_Type__c, Related_Product__r.Eligible_for_Proration__c, Related_Product__r.Upgrade_Comparison__c,
                Related_Product__r.NS_Item_ID__c, Max_Cap__c, Min_Cap__c, Type__c, Picklist_Value__c,
                Price_Based_On__c, Count_of_Service_Bundles__c, Related_Product__r.Unlimited_Responses__c, Custom_Service__c,
                Related_Bundle__r.NS_Item_ID__c, Default_Service__c, Related_Product__r.Maximum__c, Related_Product__r.Merge_Comparison__c, Related_Product__r.Eligible_for_Discount__c,
                Add_Multiple__c, Hide_in_Tech_Qlis__c, Bundle_NS_Id__c
                FROM Product_Bundle_Junction__c
                WHERE Related_Bundle__c =: bundleId and Active__c = TRUE
                ORDER BY Type__c DESC, Related_Product__r.Name];   
    }
    
    @AuraEnabled
    public static List<Pricing__c> getPricing(Id bundleId){
        return[SELECT Experience__c, Bundle__c, Minimum__c, Maximum__c, Pricing_Type__c, Pricing_Key__c, List_Price__c, Bundle_Discount_Amount__c, 
               Price_Multiplier__c, Max_Cap__c, Min_Cap__c, Quantity__c, Unlimited__c, GSA__c, FedRamp__c, CarahsoftID__c, FedRampCarahsoft__c,
               FedRAMP_CarahsoftID__c, GSACarahsoft__c
               FROM Pricing__c 
               WHERE Related_Bundle__c = :bundleId and Active_Pricing__c = TRUE Order By Maximum__c];
    }
    
    @AuraEnabled
    public static List<Service_Bundle__c> getServiceBundles(string bundle){
        return [SELECT Id, Name, NS_Item_ID__c, Bundle__c, Experience__c, 
                Bundle_Full_Name__c, Bundle_Type__c, Custom__c, Bundle_Product_Prerequisite__c, Conga_Template_Code__c, Duration__c
                FROM Service_Bundle__c WHERE Bundle__c =: bundle and Active__c = true  ORDER BY Name];
    }
    
    @AuraEnabled
    public static List<Service_Bundle_Product_Junction__c > getServiceProducts(string bundle){       
        return [SELECT Id, Related_Product__c, Name, Related_Product__r.Name, Related_Product__r.ProductCode, PricingType__c, 
                Related_Service_Bundle__r.Bundle__c, Related_Service_Bundle__r.Experience__c, Availability__c, Bundle_Key__c, User_Input_Required__c,
                Quantity__c, Quantity_Multiplier__c, Related_Product__r.Revenue_Roll_up__c, Quantity_Multiplied_by_Core__c, 
                Display_Label__c, Display_Variable__c, Upgrade_Availability__c, Related_Product__r.Revenue_Recognition_Rule__c,
                Related_Product__r.Revenue_Type__c, Related_Product__r.Eligible_for_Proration__c, Related_Product__r.Upgrade_Comparison__c,
                Related_Product__r.NS_Item_ID__c, Max_Cap__c, Min_Cap__c, Price_Based_On__c, Related_Service_Bundle__r.Bundle_Type__c,
                Maintenance__c, Template_Code__c
                FROM Service_Bundle_Product_Junction__c 
                WHERE Related_Service_Bundle__r.Bundle__c =: bundle and Active__c = TRUE
                ORDER BY Related_Product__r.Name];   
    }
    
    @AuraEnabled
    public static List<Product2> getSystemProducts(list<string> productIds){
        return[SELECT Id, Name, ProductCode, Revenue_Roll_up__c, Revenue_Type__c, Eligible_for_Proration__c,
               Upgrade_Comparison__c, NS_Item_ID__c
               FROM Product2 
               WHERE Id in :productIds];
    }
    
    
    @auraEnabled
    public static List<QuoteLineItem> getQuoteLineItems(String quoteId){
        List<QuoteLineItem> lineItems = new List<QuoteLineItem>([SELECT Id, 
                                                                 Quantity, 
                                                                 UnitPrice, 
                                                                 PricebookEntryId, 
                                                                 List_Price__c, 
                                                                 ListPrice, 
                                                                 TotalPrice,
                                                                 Max_Bundle_Discount__c, 
                                                                 NS_Item_ID__c, 
                                                                 License_Change__c,
                                                                 Related_License_Item__c, 
                                                                 Related_License_Item__r.Active__c,
                                                                 New_Business_Value__c, 
                                                                 Renewal_Value__c,
                                                                 Quota_Relief_Amount__c,
                                                                 Quote.Related_Client__c,
                                                                 Quote.CurrencyIsoCode,
                                                                 Quote.Culture_Code__c,
                                                                 Bundle_Lookup__c, 
                                                                 Bundle_Lookup__r.Bundle__c, 
                                                                 Bundle_Product__c, 
                                                                 Bundle_Lookup__r.Experience__c,
                                                                 Bundle_Product__r.Availability__c, 
                                                                 Bundle_Product__r.Upgrade_Availability__c, 
                                                                 Bundle_Product__r.PricingType__c, 
                                                                 Bundle_Product__r.User_Input_Required__c, 
                                                                 Bundle_Product__r.Bundle_Key__c, 
                                                                 Bundle_Product__r.Display_Variable__c, 
                                                                 Bundle_Product__r.Display_Label__c, 
                                                                 Bundle_Product__r.Name, 
                                                                 Product2Id, 
                                                                 Product2.Name, 
                                                                 Product2.ProductCode, 
                                                                 Product2.Revenue_Roll_up__c,
                                                                 Product2.Revenue_Recognition_Rule__c,
                                                                 Product2.Revenue_Type__c,
                                                                 Product2.Eligible_for_Proration__c,
                                                                 Product2.Upgrade_Comparison__c
                                                                 FROM QuoteLineItem
                                                                 WHERE QuoteId = :quoteId and Bundle_Lookup__c != null]);
        return lineItems;
    }
    
    @auraEnabled
    public static List<QService__c> getQService(String quoteId){
        List<QService__c> lineItems = new List<QService__c>([SELECT Id, 
                                                             Bundle__c, 
                                                             Custom__c,
                                                             List_Price__c,
                                                             Maintenance_Line_Item__c,
                                                             Markup_Line_Item__c,
                                                             Package__c,
                                                             Q_Partner__c,
                                                             Qualtrics_Invoicing__c,
                                                             Partner_Name__c,
                                                             Partner_Availability__c,
                                                             Partner_Percentage_of_Cost__c,
                                                             Provider_Selected__c,
                                                             Quote_Line_Item__c,
                                                             Service_Type__c,
                                                             Status__c,
                                                             Markup_Line_Item__r.Bundle_Product__c
                                                             FROM QService__c  
                                                             WHERE Quote__c = :quoteId]);
        return lineItems;
    }
    
    @auraEnabled
    public static List<Service_Component__c> getServiceComponents(String quoteId){
        List<Service_Component__c> lineItems = new List<Service_Component__c>([SELECT Id, 
                                                                               Cost__c, 
                                                                               Discount__c,
                                                                               List_Price__c,
                                                                               Maintenance__c,
                                                                               Quantity__c,
                                                                               Service__c,
                                                                               Service__r.Partner_Percentage_of_Cost__c,
                                                                               Service__r.Custom__c,
                                                                               Service_Bundle_Product__c,
                                                                               Total_Price__c
                                                                               FROM Service_Component__c  
                                                                               WHERE Service__r.Quote__c = :quoteId]);
        return lineItems;
    }
    
    
}