@isTest()
public class cpq_Configure_Test {
    //Static properties to be referenced in test methods
    
    static Account a;
    static Opportunity o;
    static Quote q;
    static Bundle__c b0;
    static Bundle__c b1;
    static Client__c c0;
    static Client__c c1;
    static Client__c c2;
    static Client_License__c cl0;
    static Client_License__c cl1;
    static Client_License__c cl2;
    static Client_License__c cl3;
    static Client_License__c cl4;
    static Client_License__c cl5;
    
    //Data Setup
    private static void dataSetup(){
        AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
        Insert avaCustomSetting;
        a = new Account(Name = 'Test Account',
                        BillingStreet = '123 Billing Street',
                        BillingCity = 'Billville',
                        BillingState = 'BL',
                        BillingPostalCode = '12345');
        insert a;
        RecordType xmtype = [SELECT ID FROM RecordType WHERE sObjectType = 'Opportunity' AND Name = 'XM'];
        o = new Opportunity(Name = 'Test Opportunity',
                            AccountId = a.Id,
                            StageName = 'Discover and Assess',
                            CloseDate = system.today(),
                            RecordTypeId = xmtype.Id,
                            Turn_Off_Initial_CX_EX_RC_Amounts__c = TRUE);
        insert o;
        q = new Quote (Name = 'Test Quote',
                       OpportunityId = o.Id);
        insert q;
        b0 = new Bundle__c(Name = 'Test Bundle 0',
                           Experience__c = 'CX',
                           Bundle__c = '0');
        insert b0;
        b1 = new Bundle__c(Name = 'Test Bundle 1',
                           Experience__c = 'CX',
                           Bundle__c = '1');
        insert b1;
        c0 = new Client__c(Name = 'Test Client 0',
                           Account__c = a.Id);
        insert c0;
        c1 = new Client__c(Name = 'Test Client 1',
                           Account__c = a.Id,
                           CurrencyIsoCode = 'USD');
        insert c1;
        c2 = new Client__c(Name = 'Test Client 2',
                           Account__c = a.Id);
        insert c2;
        cl0 = new Client_License__c(Name = 'Test Client License 0',
                                    Client__c = c0.Id,
                                    License_End_Date__c = system.today() + 10,
                                    Bundle__c = b0.Id,
                                    Status__c = 'Active',
                                    CurrencyIsoCode = 'USD',
                                    Invoice_Amount__c = 100);
        insert cl0;
        cl1 = new Client_License__c(Name = 'Test Client License 1',
                                    Client__c = c0.Id,
                                    License_End_Date__c = system.today() - 10,
                                    Bundle__c = b0.Id,
                                    Status__c = 'Expired');
        insert cl1;
        cl2 = new Client_License__c(Name = 'Test Client License 2',
                                    Client__c = c1.Id,
                                    License_End_Date__c = system.today() + 10,
                                    Bundle__c = b0.Id,
                                    Status__c = 'Active');
        insert cl2;
        cl3 = new Client_License__c(Name = 'Test Client License 3',
                                    Client__c = c0.Id,
                                    License_End_Date__c = system.today() + 10,
                                    Bundle__c = b1.Id,
                                    Status__c = 'Active');
        insert cl3;
        cl4 = new Client_License__c(Name = 'Test Client License 4',
                                    Client__c = c0.Id,
                                    License_End_Date__c = system.today() + 20,
                                    Bundle__c = b0.Id,
                                    Status__c = 'Active');
        insert cl4;
        cl5 = new Client_License__c(Name = 'Test Client License 5',
                                    Client__c = c0.Id,
                                    License_End_Date__c = system.today() + 10,
                                    Bundle__c = b0.Id,
                                    Status__c = 'Active',
                                    CurrencyIsoCode = 'EUR',
                                    Invoice_Amount__c = 100);
        insert cl5;
    }
    
    testMethod static void arthursTest() {
        Account a = new Account(Name = 'Test Account',
                                BillingStreet = '123 Billing Street',
                                BillingCity = 'Billville',
                                BillingState = 'BL',
                                BillingPostalCode = '12345');
        insert a;
    }
    
    //Test method for retrieving client license groupings
    private static testMethod void retrieveGroupings(){
        dataSetup();//Initial data setup
        Test.startTest();
        List<String> testlist = cpq_Configure.getLicenseGroupings(a.Id);//Get the groupings for the test account
        system.debug(testlist);
        system.assertEquals(4, testlist.size());//Verify that expected 4 groups are returned.
        Test.stopTest();
    } 
    /*
//Test method to retrieve an individual license grouping
private static testMethod void retrieveGroup1(){
dataSetup();//Initial data setup
Test.startTest();
List<Client_License__c> licenses = cpq_Configure.getLicenseItems(String.valueOf(c0.Id), String.valueOf(b0.Id), String.valueOf(system.today() + 10), 'Active');
system.debug(licenses);
system.assertEquals(2, licenses.size());//Make sure Test Client License 0 and Test Client License 5 were included but Test Client License 1 was not since it is expired.
Test.stopTest();
} 

//Test method to retrieve another individual license grouping
private static testMethod void retrieveGroup2(){
dataSetup();//Initial data setup
Test.startTest();
List<Client_License__c> licenses = cpq_Configure.getLicenseItems(String.valueOf(c0.Id), String.valueOf(b1.Id), String.valueOf(system.today() + 10), 'Active');
system.debug(licenses);
system.assertEquals(1, licenses.size());//Make sure only Test Client License 3 was retrieved.
Test.stopTest();
} */
    
    //Test method to retrieve value summary in translated currency for a license group
    private static testMethod void retrieveValueSum(){
        dataSetup();//Initial data setup
        Test.startTest();
        List<Client_License__c> licenses = cpq_Configure.getLicenseItems(String.valueOf(c0.Id), String.valueOf(b0.Id), String.valueOf(system.today() + 10), 'Active');
        Decimal valuesum = cpq_Configure.getLicenseValueSum(licenses);
        system.debug(valuesum);//Can only debug but can't really set out a hard assertEquals here since currency exchange rates are not static.
        Test.stopTest();
    }
    
    private static testMethod void createARTestCase() {
        dataSetup();
        Test.startTest();
        Entitlement ent = new Entitlement(Name = 'Deal Support', StartDate = Date.today().addDays(-1), AccountId = a.Id);
        insert ent;
        Id caseId = cpq_Configure.createARCase(a.Id, o.Id, c0.Id, 'This is a test case.');
        Test.stopTest();
    }
    
    @IsTest static void testGetRelatedClientLicenses(){
        
        
        //Create Account
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        //Create new bundle
        Bundle__c b = new Bundle__c();
        b.Name = 'Test bundle';
        b.Experience__c = 'CX';
        b.Bundle__c = '0';
        b.Active__c = TRUE;
        String bId = String.valueOf(b.Id);
        insert b;
        
        //Create new bundle
        Bundle__c b1 = new Bundle__c();
        b1.Name = 'Test bundle';
        b1.Experience__c = 'CX';
        b1.Bundle__c = '0';
        b1.Active__c = TRUE;
        String bId2 = String.valueOf(b1.Id);
        insert b1;
        
        //Create new Clinet
        Client__c c = new Client__c();
        c.Name = 'Test Client';
        c.Account__c = acc.Id;
        c.Client_Status__c = 'Active';
        insert c;
        
        string ttoday = String.valueOf(Date.today());
        string nextday = String.valueOf(Date.today().addDays(-1));
        
        List<String> dates = new List<String>{ttoday, nextday};
        List<String> bundles = new List<String>{b.Name, b1.Name};
        List<Id> clients = new List<Id>{c.Id};
            
        Test.startTest();
        List<Client_License__c> clientLicenses = cpq_Configure.getRelatedClientLicenses(dates, bundles, clients);
        Test.stopTest();
        
        system.assertNotEquals(null, clientLicenses.size());
    }
    
    @IsTest static void testGetExperiences(){
        List<String> qPerms = new List<String>{'XM', 'EX', 'RC', 'BX'};
            
        Test.startTest();
        List<XM_Experience__mdt> exps = cpq_Configure.getExperiences(qPerms);
        Test.stopTest();
        
        system.assertNotEquals(null, exps.size());
            
    }
    
    @IsTest static void testRecordSaveEditDelete(){
        //Create Account
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        //Create Client
        Client__c c = new Client__c();
        c.Name = 'Test Client';
        c.Account__c = acc.Id;
        c.Client_Status__c = 'Active';
        //insert c;
        
        //Create Client2
        Client__c c2 = new Client__c();
        c2.Name = 'Test Client';
        c2.Account__c = acc.Id;
        c2.Client_Status__c = 'Active';
        //insert c2;
        
        //Create Client2
        Client__c c3 = new Client__c();
        c3.Name = 'Test Client';
        c3.Account__c = acc.Id;
        c3.Client_Status__c = 'Active';
        //insert c3;
        
        List<sObject> newquery = cpq_Configure.saveRecords(new list<sObject>{c,c2}); //saveRecords
        List<sObject> editRecordsquery = cpq_Configure.editRecords(new list<sObject>{c,c2}); //editRecords
        sObject saveRecordquery = cpq_Configure.saveRecord(c3); //saveRecord
        sObject editRecordQuery = cpq_Configure.editRecord(c3); //editRecord
        sObject deleteRecordquery = cpq_Configure.deleteRecord(c3); //deleteRecord
        List<sObject> newquery1 = cpq_Configure.deleteRecords(new List<sObject>{c,c2}); //deleteRecords
    }
    
    @IsTest static void testQuoteManipulation(){
        AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
        Insert avaCustomSetting;
        //Create Account
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        //Create Contact
        Contact con = new Contact();
        con.FirstName = 'Kruse';
        con.LastName = 'Collins';
        con.AccountId = acc.Id;
        con.LeadSource = 'Event';
        con.Contact_Role__c = 'VP';
        insert con;
        
        //Create new bundle
        Bundle__c b = new Bundle__c();
        b.Name = 'Test bundle';
        b.Experience__c = 'CX';
        b.Bundle__c = '0';
        b.Active__c = TRUE;
        insert b;
        
        // Insert Product
        Product2 pr = new Product2();
        pr.Name='Moto - G1';
        pr.isActive=true;
        pr.Display_Order__c = 99;
        string prId = string.valueOf(pr.Id);
        insert pr;
        
        // Insert Pricebook
        PriceBook2 customPriceBook = new PriceBook2();
        customPriceBook.Name='Custom Pricebook';
        customPriceBook.IsActive=true;
        insert customPriceBook;
        
        // Query Standard and Custom Price Books
        Pricebook2 customPriceBookRec=[select Id from Pricebook2 where id=:customPriceBook.Id];
        Id stdPriceBookRecId = Test.getStandardPricebookId();
        
        // Create Standard PriceBookEntry
        PriceBookEntry stdPriceBookEntry = new PriceBookEntry();
        stdPriceBookEntry.Product2Id=pr.Id;
        stdPriceBookEntry.Pricebook2Id=stdPriceBookRecId;
        stdPriceBookEntry.UnitPrice=2000;
        stdPriceBookEntry.IsActive=true;
        insert stdPriceBookEntry;
        
        // Create Custom PriceBookEntry
        PriceBookEntry customPriceBookEntry = new PriceBookEntry();
        customPriceBookEntry.Product2Id=pr.Id;
        customPriceBookEntry.Pricebook2Id=customPriceBookRec.Id;
        customPriceBookEntry.UnitPrice=5000;
        customPriceBookEntry.IsActive=true;
        insert customPriceBookEntry;
        
        //Create Opportunity
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.CloseDate = date.today();
        opp.Accountid = acc.Id;
        opp.Contact2__c = con.Id;
        opp.Stagename = 'Discover and Assess';
        opp.Holdout_Opportunity__c = FALSE;
        opp.ForecastCategoryName = 'Commit';
        opp.Sales_Process_Stage__c = 'Discover and Assess';
        //opp.Pricebook2Id = stdPriceBookEntry.Id;
        opp.Forecast_Percentage_NEW__c = 15.00;
        opp.Turn_Off_Initial_CX_EX_RC_Amounts__c = TRUE;
        Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();
        insert opp;
        
        // Add product and Pricebook to the particular opportunity using OpportunityLineItem 
        OpportunityLineItem oppLineItem = new OpportunityLineItem();
        oppLineItem.OpportunityId = opp.Id;
        oppLineItem.PricebookEntryId = customPriceBookEntry.Id;
        oppLineItem.UnitPrice = 7000;
        oppLineItem.Quantity = 5;
        insert oppLineItem;
        
        //Create Quote 
        Quote qt = new Quote();
        qt.Name = 'Test Sync Quote';
        qt.OpportunityId = opp.Id;
        qt.License_Term_in_months__c = 12.00;
        qt.XM_Products__c = 'CX';
        qt.RecordTypeId = '01250000000EIDu'; //Record type ID is XM.      
        Quote saveQuoteRecordQuery = cpq_Configure.saveQuoteRecord(qt); //saveQuoteRecord
        
        List<Quote> newquery = cpq_Configure.getQuoteList(opp.Id); //getQuoteList
        
        //Create Client
        Client__c c = new Client__c();
        c.Name = 'Test Client';
        c.Account__c = acc.Id;
        c.Client_Status__c = 'Active';
        insert c;
        
        //Create client license
        Client_License__c clientlicense = new Client_License__c();
        clientlicense.Client__c = c.Id;
        clientlicense.Status__c = 'Active';
        clientlicense.Name = 'Test Client License';
        clientlicense.Original_Amount__c = 1000.00;
        clientlicense.Invoice_Amount__c = 1000.00;
        clientlicense.License_Start_Date__c = date.parse('01/01/2018');
        clientlicense.License_End_Date__c = date.parse('01/02/2019');
        clientlicense.Product_Family__c = 'Test Product Family';
        clientlicense.Product__c = pr.Id;
        clientlicense.Quantitiy__c = 1;
        clientlicense.Bundle__c = b.Id;
        insert clientlicense;
        
        Test.startTest();
        List<Client_License__c> getLicenseQuery = cpq_Configure.getLicenses(acc.Id); //getLicenses
        List<PriceBookEntry> getPriceBookEntryquery = cpq_Configure.getPriceBookEntry(new list<String>{'Test Product 1','Test Product 2','iPhone8'}, 'Test Price Book', 'USD'); //getPriceBookEntry
        Opportunity getOpportunityQuery = cpq_Configure.getOpportunity(opp.Id); //getOpportunity
        cpq_Configure.syncQuote(qt.Id, opp.Id, 100);
        cpq_Configure.fullQuoteSync(qt, opp.Id);
        //   fullSyncQuote = cpq_Confgur
        Test.stopTest();
        //syncQuotequery = cpq_Configure.syncQuote(qt.Id, opp.Id); //syncQuote
    }
    
    @isTest static void testgetgetQuoteLineItems() {
        AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
        Insert avaCustomSetting;
        //Create Account
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        //Create Contact
        Contact con = new Contact();
        con.FirstName = 'Kruse';
        con.LastName = 'Collins';
        con.AccountId = acc.Id;
        con.LeadSource = 'Event';
        con.Contact_Role__c = 'VP';
        insert con;
        
        //Create new bundle
        Bundle__c b = new Bundle__c();
        b.Name = 'Test bundle';
        b.Experience__c = 'CX';
        b.Bundle__c = '0';
        b.Active__c = TRUE;
        String bId = String.valueOf(b.Id);
        insert b;
        
        //Create new bundle
        Bundle__c b1 = new Bundle__c();
        b1.Name = 'Test bundle';
        b1.Experience__c = 'CX';
        b1.Bundle__c = '0';
        b1.Active__c = TRUE;
        String bId2 = String.valueOf(b1.Id);
        insert b1;
        
        // Insert Product
        Product2 pr = new Product2();
        pr.Name='Moto - G1';
        pr.isActive=true;
        pr.Display_Order__c = 99;
        insert pr;
        
        // Insert Pricebook
        //PriceBook2 customPriceBook = new PriceBook2();
        //customPriceBook.Name='Custom Pricebook';
        //customPriceBook.IsActive=true;
        //insert customPriceBook;
        
        // Query Standard and Custom Price Books
        //Pricebook2 customPriceBookRec=[select Id from Pricebook2 where id=:customPriceBook.Id];
        Id stdPriceBookRecId = Test.getStandardPricebookId();
        
        // Create Standard PriceBookEntry
        PriceBookEntry stdPriceBookEntry = new PriceBookEntry();
        stdPriceBookEntry.Product2Id=pr.Id;
        stdPriceBookEntry.Pricebook2Id=stdPriceBookRecId;
        stdPriceBookEntry.UnitPrice=2000;
        stdPriceBookEntry.IsActive=true;
        insert stdPriceBookEntry;
        
        // Create Custom PriceBookEntry
        //PriceBookEntry customPriceBookEntry = new PriceBookEntry();
        //customPriceBookEntry.Product2Id=pr.Id;
        //customPriceBookEntry.Pricebook2Id=customPriceBookRec.Id;
        //customPriceBookEntry.UnitPrice=5000;
        //customPriceBookEntry.IsActive=true;
        //insert customPriceBookEntry;
        
        //Create Opportunity
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.CloseDate = date.today();
        opp.Accountid = acc.Id;
        opp.Contact2__c = con.Id;
        opp.Stagename = 'Discover and Assess';
        opp.Holdout_Opportunity__c = FALSE;
        opp.ForecastCategoryName = 'Commit';
        opp.Sales_Process_Stage__c = 'Discover and Assess';
        opp.Forecast_Percentage_NEW__c = 15.00;
        opp.Turn_Off_Initial_CX_EX_RC_Amounts__c = TRUE;
        opp.PriceBook2Id = stdPriceBookRecId;
        Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();
        insert opp;
        
        // Add product and Pricebook to the particular opportunity using OpportunityLineItem 
        OpportunityLineItem oppLineItem = new OpportunityLineItem();
        oppLineItem.OpportunityId = opp.Id;
        oppLineItem.PricebookEntryId = stdPriceBookEntry.Id;
        oppLineItem.UnitPrice = 7000;
        oppLineItem.Quantity = 5;
        insert oppLineItem;
        
        //Create Quote 
        Quote qt = new Quote();
        qt.Name = 'Test Sync Quote';
        qt.OpportunityId = opp.Id;
        qt.License_Term_in_months__c = 12.00;
        qt.Quota_Relief_Adjustment_360__c = 1000;
        qt.EE_SMS_Text_Reserve__c = 1000;
        qt.Pricebook2Id = stdPriceBookRecId;
        Test.startTest();
        sObject saveUpgradeRecordQuery = cpq_Configure.saveUpgradeRecord(qt, 'Quota_Relief_Adjustment_360__c', 1000); //saveUpgradeRecord
        
        //Create Quote 2
        Quote qt2 = new Quote();
        qt2.Name = 'Test Sync Quote';
        qt2.OpportunityId = opp.Id;
        qt2.License_Term_in_months__c = 12.00;
        qt2.Quota_Relief_Adjustment_360__c = 1000;
        qt2.EE_SMS_Text_Reserve__c = 1000;
        Quote saveUpgradeQuoteRecordquery = cpq_Configure.saveUpgradeQuoteRecord(qt2, 'Quota_Relief_Adjustment_360__c', 1000); //saveUpgradeQuoteRecord
        sObject editUpgradeRecordQuery = cpq_Configure.editUpgradeRecord(qt2, 'Quota_Relief_Adjustment_360__c', 1500); //editUpgradeRecord
        
        //Create new quote line item
        QuoteLineItem qli = new QuoteLineItem();
        qli.Quantity = 1.00;
        qli.UnitPrice = 0.00;
        qli.Bundle_Lookup__c = b.Id;
        qli.QuoteId = qt.Id;
        qli.PricebookEntryId = stdPriceBookEntry.Id;
        qli.Product2Id = pr.Id;
        insert qli;
        
        //Create Bundle Product
        Product_Bundle_Junction__c pbj = new Product_Bundle_Junction__c();
        pbj.Name = 'Test Bundle Product';
        pbj.Availability__c = 'Available';
        //pbj.Related_Product__c = '';
        pbj.Related_Bundle__c = b.Id;
        insert pbj;
        
        //Create QService
        QService__c qserv = new QService__c();
        qserv.Name = 'Test QService Record';
        qserv.Service_Type__c = 'TAM';
        qserv.Quote__c = qt2.Id;
        //    insert qserv;
        
        //Create Contract Document
        Contract_Document__c cd = new Contract_Document__c();
        cd.Opportunity__c = opp.Id;
        insert cd;
        
        //Test.startTest();
        cpq_Configure.unsyncQuote(opp.Id);
        Boolean dda = cpq_Configure.getDealDeskAccess();
        Boolean cc = cpq_Configure.cpqComplete(qt.Id);
        XM_Quoting__c xmq = cpq_Configure.getXmQuoting();
        Boolean ot = cpq_Configure.getOppTeam(opp.Id, acc.Id);
        List<QuoteLineItem> newquery = cpq_Configure.getQuoteLines(qt.Id); //getQuoteLines
        List<Partner_Availability__c> getPartnerAvailabilityquery = cpq_Configure.getPartnerAvailability(new String[] {b.Id, b1.Id} , 'North America', 12000.00); //getPartnerAvailability
        map<string,decimal> getctQuery = cpq_Configure.getct(); //getct
        List<RecordType> getServiceRecordTypeQuery = cpq_Configure.getServiceRecordType(); //getServiceRecordType
        String getLocQuery = cpq_Configure.getLoc(); //getLoc
        List<QuoteLineItem> getQuoteLineItemQuery = cpq_Configure.getQuoteLineItems(qt.Id, 'CX'); //getQuoteLineItems
        Contract_Document__c getContractDocumentQuery = cpq_Configure.getContractDocument(opp.Id); //getContractDocument
        List<QuoteLineItem> deleteQlisQuery = cpq_Configure.deleteQlis(qt.Id, b.Id, 'CX'); //deleteQlis
        List<Product_Bundle_Junction__c> getProductsquery = cpq_Configure.getProducts(b.Id); //getProducts
        List<QService__c> deleteSlisQuery = cpq_Configure.deleteSlis(qt.Id); //deleteSlis
        List<XM_Experience__mdt> getAllExperiencesQuery = cpq_Configure.getAllExperiences(); //getAllExperiences
        boolean getOppTeamQuery = cpq_Configure.getOppTeam(opp.Id, acc.Id); //getOppTeam
        XM_Quoting__c getXMQuotingQuery = cpq_Configure.getXmQuoting(); //getXmQuoting
        //List<XM_Experience_mdt> getExperiencesQuery = cpq_Configure.getExperiences(List<String>{'Test'}); //getExperiences
        Test.stopTest();
        
    }
}