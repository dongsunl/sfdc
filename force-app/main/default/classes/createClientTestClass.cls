@isTest
public with sharing class createClientTestClass {
	
    
    @isTest public static void createRecordsNewBillingAdd(){
        
        //Create Account
		Account acc = new Account();
		acc.Name = 'Test Account';
        acc.BillingStreet = '333 W River Park Dr';
        acc.BillingCity = 'Provo';
        acc.BillingState = 'UT';
        acc.BillingPostalCode = '84604';
        insert acc;
        
        //Create Client
        Client__c client = new Client__c();
        client.Name = 'Test Client';
        client.Account__c = acc.Id;
        client.Client_Status__c = 'Merged';
        client.Billing_Street_Address__c = acc.BillingStreet;
        client.Billing_City__c = acc.BillingCity;
        client.Billing_State_Province__c = acc.BillingState;
        client.Billing_Zip_Postal_Code__c = acc.BillingPostalCode;
        insert client;
        
        //createClient c = new createClient();
        ID newclient = createClient.newClient(acc.Id, client, TRUE, FALSE);
        
    }
   
    @isTest static void createRecordsNewClientAdd(){
        //Create Account
        Account acc = new Account();
        acc.Name = 'Test Account 2';
        acc.BillingStreet = '333 W River Park Dr';
        acc.BillingCity = 'Provo';
        acc.BillingState = 'UT';
        acc.BillingPostalCode = '84604';
        insert acc;
        
        //Create Client
        Client__c client = new Client__c();
        client.Name = 'Test Client';
        client.Account__c = acc.Id;
        client.Client_Status__c = 'Merged';
        client.Billing_Street_Address__c = '333 W River Park Dr';
        client.Billing_City__c = 'Provo';
        client.Billing_State_Province__c = 'UT';
        client.Billing_Zip_Postal_Code__c = '84604';
        client.Shipping_Street__c = client.Billing_Street_Address__c;
        client.Shipping_City__c = client.Billing_City__c;
        client.Shipping_State__c = client.Billing_State_Province__c;
        client.Shipping_Zip_Postal_Code__c = client.Billing_Zip_Postal_Code__c;
        client.ShippingCountry2__c = client.BillingCountry2__c;
        insert client;
        
        ID newclient = createClient.newClient(acc.Id, client, FALSE, TRUE);
    }
    
    @isTest public static void createRecordsTryCatch(){
        
        //Create Account
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.BillingStreet = '333 W River Park Dr';
        acc.BillingCity = 'Provo';
        acc.BillingState = 'UT';
        acc.BillingPostalCode = '84604';
        insert acc;
        
        Client__c client = new Client__c();

        ID newclient = createClient.newClient(acc.Id, client, TRUE, FALSE);
        
    }
}