@istest
public class fab_ControllerTest {
    
    @testsetup
    static void test_setup(){
        AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
        insert avaCustomSetting;
        
        //Create Account
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.BillingStreet = '333 W River Park Dr';
        acc.BillingCity = 'Provo';
        acc.BillingState = 'UT';
        acc.BillingPostalCode = '84604';
        insert acc;
        
        //Create Contact
        Contact con = new Contact();
        con.FirstName = 'Kruse';
        con.LastName = 'Collins';
        con.AccountId = acc.Id;
        con.LeadSource = 'Event';
        con.Contact_Role__c = 'VP';
        con.Email = 'legal-sales@qualtrics.com';
        insert con;
        
        //Create Opportunity
        Id opptId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Panels').getRecordTypeId();
        
        list<Opportunity> oppList = new List<Opportunity>();
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.CloseDate = date.today();
        opp.Accountid = acc.Id;
        opp.Contact2__c = con.Id;
        opp.Stagename = 'Discover and Assess';
        opp.Holdout_Opportunity__c = FALSE;
        opp.ForecastCategoryName = 'Commit';
        opp.Sales_Process_Stage__c = 'Discover and Assess';
        opp.Forecast_Percentage_NEW__c = 15.00;
        opp.Turn_Off_Initial_CX_EX_RC_Amounts__c = TRUE;
        oppList.add(opp);
        //   insert opp;
        
        Opportunity opp2 = new Opportunity();
        opp2.Name = 'Test Opp2';
        opp2.CloseDate = date.today();
        opp2.Accountid = acc.Id;
        opp2.Contact2__c = con.Id;
        opp2.Stagename = 'Discover and Assess';
        opp2.Holdout_Opportunity__c = FALSE;
        opp2.ForecastCategoryName = 'Commit';
        opp2.Sales_Process_Stage__c = 'Discover and Assess';
        opp2.Forecast_Percentage_NEW__c = 15.00;
        opp2.Turn_Off_Initial_CX_EX_RC_Amounts__c = TRUE;
        oppList.add(opp2);
        //  insert opp2;
        
        insert oppList;
        
        //Create Quote 
        Quote qt = new Quote();
        qt.Name = 'Test Sync Quote';
        qt.OpportunityId = opp.Id;
        qt.License_Term_in_months__c = 12.00;
        insert qt;
        
        //Create QService
        QService__c qserv = new QService__c();
        qserv.Name = 'Test Service';
        qserv.Service_Type__c = 'TAM';
        qserv.Quote__c = qt.Id;
        qserv.Opportunity__c = opp.Id;
        insert qserv;
        
        //Create Client
        Client__c client = new Client__c();
        client.Name = 'Batman';
        client.Account__c = acc.Id;
        client.Client_Status__c = 'Merged';
        client.Billing_Street_Address__c = acc.BillingStreet;
        client.Billing_City__c = acc.BillingCity;
        client.Billing_State_Province__c = acc.BillingState;
        client.Billing_Zip_Postal_Code__c = acc.BillingPostalCode;
        insert client;
        
        //Cretae Conga Merge Query
        APXTConga4__Conga_Merge_Query__c cmq = new APXTConga4__Conga_Merge_Query__c();
        cmq.APXTConga4__Name__c = 'Test Conga Merge Query';
        insert cmq;
        
        APXTConga4__Conga_Template__c ct = new APXTConga4__Conga_Template__c();
        ct.APXTConga4__Name__c ='QSO Standalone';
        ct.APXTConga4__Description__c = '1';
        ct.APXTConga4__Template_Group__c = 'Quote';
        insert ct;
        
        APXTConga4__Conga_Email_Template__c cet = new APXTConga4__Conga_Email_Template__c();
        cet.APXTConga4__Name__c = 'Client Invoice';
        insert cet;
        
        Contract_Document__c cd = New Contract_Document__c();
        cd.Opportunity__c = opp.id;
        cd.Account__c = acc.id;
        insert cd;
        
        Contract_Document_Template__c dt = new Contract_Document_Template__c();
        dt.Name = ct.APXTConga4__Name__c;
        dt.Template_Type__c = 'Service Order';
        dt.Contract_Document__c = cd.Id;
        insert dt;
        
        ContentVersion cv = new ContentVersion(
                Title = 'Test',
                PathOnClient = 'Test.jpg',
                VersionData = blob.valueof('Test Content Data'),
                IsMajorVersion = true,
            Description = 'Service Order'
            );
            insert cv;
        
        cv = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id LIMIT 1];
        ContentDocumentLink cl = new ContentDocumentLink();
        cl.ContentDocumentId = cv.ContentDocumentId;
        cl.LinkedEntityId = opp.Id; 
        cl.ShareType = 'I';
        cl.Visibility = 'InternalUsers';
        
        insert cl;
        
        Retainer__c ret = new Retainer__c();
        ret.Name = 'Test Retainer';
        ret.Related_Account__c = acc.Id;
        ret.Related_Opportunity__c = opp.Id;
        ret.Related_Client__c = client.Id;       
        
        ret.Initial_Value__c = 5000;
       
        insert ret;
    }
    
    @isTest static void testCreateContractDocument() {
        
        Opportunity oppt = [SELECT Id, Name FROM Opportunity WHERE Name = 'Test Opp' AND Holdout_Opportunity__c = false LIMIT 1];
        
        Test.startTest();
        Boolean result = fab_Controller.createContractDocument(oppt.Id);
        List<Retainer_Link__c> rl = fab_Controller.getLinkedRetainers(oppt.Id);
        list<CurrencyType> ct = fab_Controller.getCurrencyCodes();
        Test.stopTest();
        
        System.assertEquals(true, result);
    }
    
    @isTest static void testCreateContractDocumentNoCD() {
        
        
        Opportunity oppt2 = [SELECT Id, Name FROM Opportunity WHERE Name = 'Test Opp2' AND Holdout_Opportunity__c = false LIMIT 1];
        Test.startTest();
        
        Boolean result2 = fab_Controller.createContractDocument(oppt2.Id);
        Test.stopTest();
        
        System.assertEquals(true, result2);
    }
    
    
    /*
@isTest static void testGetContractDocument() {

Opportunity oppt = [SELECT Id, Name FROM Opportunity WHERE Name = 'Test Opp' AND Holdout_Opportunity__c = false LIMIT 1];

Test.startTest();
Contract_Document__c contract = fab_Controller.getContractDocument(oppt.Id);
Test.stopTest();

System.assertNotEquals(null, contract);
}*/
    
    @isTest static void testGetOpportunity() {
        
        Opportunity oppt = [SELECT Id, Name FROM Opportunity WHERE Name = 'Test Opp' AND Holdout_Opportunity__c = false LIMIT 1];
        
        Test.startTest();
        Opportunity opp = fab_Controller.getOpportunity(oppt.Id);
        Test.stopTest();
        
        System.assertNotEquals(null, opp);
    }
    
    @isTest static void testGetQuoteList() {
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1];
        Client__c cli = [SELECT Id FROM Client__c WHERE Name = 'Batman' LIMIT 1];        
        Opportunity oppt = [SELECT Id, Name FROM Opportunity WHERE Name = 'Test Opp' AND Holdout_Opportunity__c = false LIMIT 1];
        Quote q = [SELECT Id FROM Quote WHERE OpportunityId =: oppt.Id];
        Test.startTest();
        List<Quote> quotes = fab_Controller.getQuoteList(oppt.Id);
        Opportunity op = fab_Controller.submitForApproval(oppt.Id, acc.Id, cli.Id, q.Id);
        Contract_Document__c cd = fab_Controller.getContractDocument(oppt.Id);
        List<ContentDocument> cdt = fab_Controller.getContentDocs(oppt.Id);
        
        String src = fab_Controller.searchDB('Retainer__c', 'Name', 'Id', 'Value_Remaining__c', 0, 'CurrencyIsoCode', 4, 'Name', 'test');
        
        Test.stopTest();
        
        System.assertNotEquals(0, quotes.size());
    }
    
    @isTest static void testGetService() {
        
        Quote qt = [SELECT Id, Name, OpportunityId, License_Term_in_months__c FROM Quote WHERE Name = 'Test Sync Quote' LIMIT 1];
        
        Test.startTest();
        List<QService__c> serviceList = fab_Controller.getService(qt.Id);
        String pi = fab_Controller.getProcessInstance(qt.Id);
        Test.stopTest();
        
        System.assertEquals(0, serviceList.size());
    }
    
    @isTest static void testGetServiceList() {
        
        Quote qt = [SELECT Id, Name, OpportunityId, License_Term_in_months__c FROM Quote WHERE Name = 'Test Sync Quote' LIMIT 1];
        
        Test.startTest();
        List<QService__c> serviceList = fab_Controller.getServiceList(qt.Id);
        Test.stopTest();
        
        System.assertEquals(0, serviceList.size());
    }
    
    @isTest static void testGetQuoteLines() {
        
        Quote qt = [SELECT Id, Name, OpportunityId, License_Term_in_months__c FROM Quote WHERE Name = 'Test Sync Quote' LIMIT 1];
        
        Test.startTest();
        List<QuoteLineItem> quoteLineList = fab_Controller.getQuoteLines(qt.Id);
        Test.stopTest();
        
        System.assertEquals(0, quoteLineList.size());
    }
    
    @isTest static void testGetClient() {
        
        Client__c client = [SELECT Id, Name FROM Client__c WHERE Name = 'Batman' LIMIT 1];
        
        Test.startTest();
        Client__c cl = fab_Controller.getClient(client.Id);
        Test.stopTest();
        
        System.assertNotEquals(null, cl);
    }
    
    @isTest static void testGetLegalContact() {
        
        Test.startTest();
        List<Contact> cont = fab_Controller.getLegalContact();
        Test.stopTest();
        
        System.assertNotEquals(null, cont);
    }
    
    @isTest static void testGetCongaQueries() {
        
        Test.startTest();
        List<APXTConga4__Conga_Merge_Query__c> qry = fab_Controller.getCongaQueries();
        Test.stopTest();
        
        System.assertEquals(1, qry.size());
    }
    
    @isTest static void testUpdateRecord() {
        
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Test Account' LIMIT 1];        
        
        Test.startTest();
        sObject sOb = fab_Controller.updateRecord(acc);
        Test.stopTest();
        
        System.assertNotEquals(null, sOb);
    }
    
    @isTest static void testUpdateQuoteRecord() {
        
        Quote qt = [SELECT Id, Name, OpportunityId, License_Term_in_months__c FROM Quote WHERE Name = 'Test Sync Quote' LIMIT 1];
        
        Test.startTest();
        sObject sOb = fab_Controller.updateQuoteRecord(qt);
        Test.stopTest();
        
        System.assertNotEquals(null, sOb);
    }
    
    @isTest static void testUpdateQuote() {
        
        Quote qt = [SELECT Id, Name, OpportunityId, License_Term_in_months__c FROM Quote WHERE Name = 'Test Sync Quote' LIMIT 1];
        date myDate = date.newInstance(1987, 12, 17);    
        
        Test.startTest();
        Quote quote = fab_Controller.updateQuote(qt, myDate, 1);
        Quote quote2 = fab_Controller.updateQuote(qt, myDate, 2);
        //  List<sObject> qlis = fab_Controller.saveQlis(records)
        Test.stopTest();
        
        System.assertNotEquals(null, quote);
    }
    
    @isTest static void testGetTemplateIds() {
        Opportunity oppt = [SELECT Id, Name FROM Opportunity WHERE Name = 'Test Opp'  LIMIT 1];
        Contract_Document__c cd = [SELECT Id FROM Contract_Document__c WHERE Opportunity__c = :oppt.Id LIMIT 1];
        Quote qt = [SELECT Id, Name, OpportunityId, License_Term_in_months__c FROM Quote WHERE Name = 'Test Sync Quote' LIMIT 1];
        List<Contract_Document_Template__c> cdts = new List<Contract_Document_Template__c>();
        Contract_Document_Template__c cdt1 = new Contract_Document_Template__c();
        cdt1.Contract_Document__c = cd.Id;
        cdt1.Name = 'new cdt';
        cdts.add(cdt1);
        String b='Unit Test Attachment Body';
        Test.startTest();
        List<Contract_Document_Template__c> cdt = fab_Controller.getTemplateIds(cd.Id);
        Boolean dct = fab_Controller.deleteTemplates(cd.Id);
        List<dsfs__DocuSign_Status__c> ds = fab_Controller.getDocusignStatus(cd.Id);
        List<Attachment> att = fab_Controller.getFileId(cd.Id);
        List<Contract_Document_Template__c> temins = fab_Controller.insertRecords(cdts,cd.Id); 
        Id stf = fab_Controller.saveTheFile('fileName', b, oppt.Id, 'description', qt.Id);
        Id upp2 = fab_Controller.uploadPO('fileName', b, oppt.Id, 'Purchase Order');
        Id stc = fab_Controller.saveTheChunk('fileName', b, oppt.Id, 'Purchase Order', '');
        Id stc2 = fab_Controller.saveTheChunk2('fileName', b, oppt.Id, 'Purchase Order', '', qt.Id);
        Test.stopTest();
        
        
    }
    @isTest static void testIds() {
        
        Test.startTest();
        String session = fab_Controller.getSessionId();
        Id uid = fab_Controller.getUserId();
        Id pid = fab_Controller.getProfileId();
        List<fab_Variable__mdt> fv = fab_Controller.getFabVariables();
        List<Conga_Merge__mdt> cm = fab_Controller.getCongaButton('QSO_Docusign');
        List<ContentVersion> cv = fab_Controller.getContentVersion();
        //List<TopicAssignment> ta = fab_Controller.getTopics();
        List<APXTConga4__Conga_Template__c>  ct = fab_Controller.getCongaTemplates();
        List<APXTConga4__Conga_Email_Template__c> cet = fab_Controller.getCongaEmailTemplates();
        List<APXTConga4__Conga_Merge_Query__c> cmq = fab_Controller.getCongaQueries();
        Test.stopTest();
        
        
    }
    
    @isTest static void testInsertRecord() {
        
        Contract_Document_Template__c cdt = [SELECT Name, Template_Type__c FROM Contract_Document_Template__c WHERE Template_Type__c = 'Service Order' LIMIT 1 ];
        cdt.Name = 'New Test CD 123987';
        Test.startTest();
        Contract_Document_Template__c cdtRecord = fab_Controller.insertRecord(cdt);
        
        Test.stopTest();
        
        System.assertNotEquals(null, cdtRecord);
    }
    
    @isTest static void testGetCongaTemplates() {
        
        Test.startTest();
        List<APXTConga4__Conga_Template__c> conTemplate = fab_Controller.getCongaTemplates();
        Test.stopTest();
        
        System.assertNotEquals(null, conTemplate);
    }
    
    @isTest static void testGetContentVersion() {
        
        Test.startTest();
        List<ContentVersion> contVersion = fab_Controller.getContentVersion();
        Test.stopTest();
        
       // System.assertEquals(0, contVersion.size());
    }
    
    @isTest static void testsaveQlis() {
        List<Quote> qtList = [SELECT Id, Name, OpportunityId, License_Term_in_months__c FROM Quote WHERE Name = 'Test Sync Quote'];
        
        Test.startTest();
        List<sObject> sObjs = fab_Controller.saveQlis(qtList);
        
        Test.stopTest();        
    }
    
    @isTest static void testCancelCC() {
        Opportunity oppt = [SELECT Id, Name FROM Opportunity WHERE Name = 'Test Opp' AND Holdout_Opportunity__c = false LIMIT 1];
        
        Test.startTest();
        Boolean result = fab_Controller.cancelCC(oppt.Id);     
        Test.stopTest();
        
        System.assertEquals(true, result);
    }
    
}