@isTest 
public class fab_LookupControllerTest {
    
   static testMethod void search_should_return_Account() {
        Id [] fixedResults = new Id[1];
        Account account = createTestAccount('Account');
       Contact contact = createTestContact('Contact', account.Id);
        fixedResults.add(contact.Id);
        Test.setFixedSearchResults(fixedResults);
        List<String> selectedIds = new List<String>();

        List<fab_LookupSearchResults> results = fab_Controller.search('Con', selectedIds, account.Id);
       
fab_LookupSearchResults results2 = new fab_LookupSearchResults(contact.Id, 'Contact', 'icon', 'title', 'subtitle', account.Id);
       results2.getId();
       results2.getAccountId();
       results2.getIcon();
       results2.getSObjectType();
       results2.getSubtitle();
       results2.getTitle();
       //  System.assertEquals(1, results.size());
       // System.assertEquals(account.Id, results.get(0).getId());
    }

    static testMethod void search_should_not_return_selected_item() {
        Id [] fixedResults = new Id[1];
        Account account1 = createTestAccount('Account1');
        
        Account account2 = createTestAccount('Account2');
       
        Contact contact1 = createTestContact('Contact1', account1.Id);
        Contact contact2 = createTestContact('Contact2', account2.Id);
        fixedResults.add(contact1.Id);
         fixedResults.add(contact2.Id);
        Test.setFixedSearchResults(fixedResults);
        List<String> selectedIds = new List<String>();
     //   selectedIds.add(account2.Id);

        List<fab_LookupSearchResults> results = fab_Controller.search('Con', selectedIds, account1.Id);

      //  System.assertEquals(1, results.size());
      //  System.assertEquals(account1.Id, results.get(0).getId());
    }

    private static Account createTestAccount(String name) {
        Account account = new Account(Name = name);
        insert account;
        return account;
    }
    
    private static Contact createTestContact(String name, Id account) {
        Contact contact = new Contact(LastName = name, AccountId = account);
        insert contact;
        return contact;
    }

}