public class getRelatedCases {
    
 public static List<Case> cse { get; set; } 
    @auraEnabled
    public static List<Case> getCases(Integer recordLimit, Integer recordOffset, Id oppId){
        
        Integer intLimit = Integer.valueof(recordLimit);
        Integer intOffset = Integer.valueof(recordOffset);
       
        cse = [SELECT Id, Subject, Primary_Support_Reason__c, Secondary_Support_Reason__c, CreatedDate, Status, Owner.Name
            FROM Case
            WHERE Related_Opportunity__c = :oppId
              ORDER BY CreatedDate desc
             LIMIT :intLimit Offset :intOffset];
        return cse;        
    }
    
    //Get Total Number of Contacts
    @AuraEnabled
    public static Integer getTotal(Id oppId){
        Id userId = UserInfo.getUserId();
        AggregateResult results = [SELECT Count(Id) ttl  FROM Case
            WHERE Related_Opportunity__c = :oppId];
        Integer ttl = (Integer)results.get('ttl') ; 
        return ttl;
    } 
}