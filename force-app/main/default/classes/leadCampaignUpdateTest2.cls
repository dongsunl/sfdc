@istest(seealldata=true)
public class leadCampaignUpdateTest2{
public static testmethod void leadCampaignUpdateTest2(){

    
    Profile p = [SELECT Id FROM Profile WHERE Name='Q-SDR-US and Enterprise']; 
 //   userrole ur = [select id from userrole where name = 'NA-GRL-SDR-Provo1-Rep'];
    User u = new User(userroleid='00E50000001Huox', Alias = 'standt', Email='standarduser@testorg987654.com', EmailEncodingKey='UTF-8', firstname='leadtest2', LastName='leadlasttest7', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='stanadfasdfasdfdarduser@testorg.com');
    System.runAs(u){
        campaign c1 = new campaign(name='OpDev - test 1');
        campaign c2 = new campaign(name='OpDev - test 2');
        campaign c3 = new campaign(name='OpDev - test 3');
        campaign c4 = new campaign(name='OpDev - test 4');
        campaign c5 = new campaign(name='AE OpDev Training');
        insert c1;
        insert c2;
        insert c3;
        insert c4;
        insert c5;
        lead l = new lead(firstname='fname', lastname='lname', company='comp');
        insert l;
        lead l1 = new lead(firstname='fname1', lastname='lname1', company='comp1');
        insert l1;
        CampaignMember cm2 = new CampaignMember(CampaignId = c2.id,LeadId = l.id);
        CampaignMember cm3 = new CampaignMember(CampaignId = c3.id,LeadId = l.id);
        CampaignMember cm4 = new CampaignMember(CampaignId = c4.id,LeadId = l.id);
        insert cm2;
        insert cm3;
        insert cm4;
        l.ownerid = u.id;
        update l;
        lead l2 = [select ownerid from lead where id = :l.id];
        system.debug(logginglevel.info, '*****u.id='+u.id);
        system.debug(logginglevel.info, '*****l2='+l2);
        CampaignMember[] cm = [select id from CampaignMember where leadid = :l.id AND CampaignId = :c5.id];
        //system.assertequals(cm.size(), 1);
        CampaignMember[] cm1 = [select id from CampaignMember where leadid = :l.id];
        //system.assertequals(cm1.size(), 1);
    }


}
}