@istest
public class leadUpdateFromTaskTest{
public static testmethod void leadUpdateFromTaskTest(){
    lead l = new lead(firstname='fname', lastname='lname', Product__c='Research Suite;360', company='cpny');
    insert l;

    lead l1 = [select First_Advancing_Call__c, First_Activity_Date__c from lead where id = :l.id];
    //system.assert(l1.First_Activity_Date__c == null);
    //system.assert(l1.First_Advancing_Call__c == null);

    task t = new task(whoid = l.id,Status = 'Completed', Outcome__c = 'Busy');
    insert t;

    lead l2 = [select First_Advancing_Call__c, First_Activity_Date__c from lead where id = :l.id];
    //system.assert(l2.First_Activity_Date__c == null);
    //system.assert(l2.First_Advancing_Call__c == null);

    task t1 = new task(whoid = l.id,Status = 'Completed', Subject='test non advancing call test', Outcome__c = 'Busy');
    insert t1;

    lead l3 = [select First_Advancing_Call__c, First_Activity_Date__c from lead where id = :l.id];
    //system.assert(l3.First_Activity_Date__c == null);
    //system.assert(l3.First_Advancing_Call__c == null);

    task t2 = new task(whoid = l.id,Status = 'Completed', Subject='test advancing call test', Outcome__c = 'Busy');
    insert t2;

    lead l4 = [select First_Advancing_Call__c, First_Activity_Date__c from lead where id = :l.id];
    //system.assert(l4.First_Activity_Date__c == null);
    //system.assert(l4.First_Advancing_Call__c == null);
}
}