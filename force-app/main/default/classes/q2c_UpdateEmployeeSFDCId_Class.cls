public class q2c_UpdateEmployeeSFDCId_Class {
    
    //get new employee records
    public static void updateSFDCId(List<Employee__c> emps){

        //create employee id and database instance
        Set<Decimal> eid = new Set<Decimal>();
        Database.SaveResult[] lsr; 
        
        //loop through new employee records and add records to emp id and dept sets
        for(Employee__c e:emps){
            eid.add(e.EmployeeIndentificationNumber__c);
        }
        System.debug('**eid list: ' + eid);
        try{
            //get the user information for the employee id and put it in a map
            Map<Id,User> userMap = new Map<Id,User>([
                SELECT Id, EIN__c FROM User WHERE EIN__c != null AND EIN__c in :eid
            ]);
            System.Debug(LoggingLevel.INFO, 'Message ' + userMap); 
            
            //set the user id for the map               
            Set<ID> uid = userMap.keySet();
            System.Debug('**userMap: ' + uid); 
                        
            List<Employee__c> employeeList = new List<Employee__c>();

            if(!uid.isEmpty()){
                //loop through the employee and user sets and update the employee table user name with the user id                
                for(Employee__c e:emps){                
                    for(Id u:uid){                  
                        if(e.EmployeeIndentificationNumber__c == userMap.get(u).EIN__c){
                            e.UserName__c = u;
                            employeeList.add(e);
                        }
                    } 
                }                                    
            }  
            System.Debug('**employeeList: ' + employeeList);

            //create a map that will hold the values of the list 
            map<id,Employee__c> employeeMap = new map<id,Employee__c>();

            //put all the values from the list to map. 
            employeeMap.putall(employeeList);
            if(employeeMap.size()>0){
                //update employeeMap.values();
                lsr = Database.update(employeeMap.values(),false);
            }

            // Iterate through each returned result
            for (Database.SaveResult sr : lsr) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully updated employee. Employee ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Employee fields that affected this error: ' + err.getFields());
                    }
                }
            }        
        }
        catch (System.NullPointerException e) {            
            system.debug(logginglevel.error, '***** e.getmessage()='+e.getmessage());            
        }
    }        
}