public class serviceEditBannerController {    
    
    public Qservice__c qserviceObj; //Create opp var
    public Opportunity opp; //Create opp var
    public List<QService__c> getQServices{get; set;} //List to pull in values from Qservice__c for related list.
    public boolean section1{get; set;} //used for deciding what to render on VF page.
    public boolean section2{get; set;} //used for deciding what to render on VF page.
    public boolean section3{get; set;} //used for deciding what to render on VF page.
    public boolean opp_is_won = false;
    
    //Controller Extension
    public serviceEditBannerController(ApexPages.StandardController controller) {
        this.qserviceObj = (Qservice__c)controller.getRecord();
        
        //Pull in columns for QService__c and load into getQServices
        
        getQServices = [SELECT Id, Opportunity__r.Sales_Process_Stage__c,Partner_Availability__r.Name   
                        FROM QService__c
                        WHERE Id = :qserviceObj.Id
                       ];
        
        if(getQservices[0].Partner_Availability__r.Name == 'Record NOT Found for Selected Provider'){
            section1 = FALSE;
            section2 = FALSE;
            section3 = TRUE;
        }else{
            section3 = FALSE;
            if(getQservices[0].Opportunity__r.Sales_Process_Stage__c == 'Won' || getQservices[0].Opportunity__r.Sales_Process_Stage__c == 'Invoice'){
                section1 = TRUE;
                section2 = FALSE;
            }else{
                section1 = FALSE;
                section2 = TRUE;
            }
        }
        //If records do exist, set section1 to TRUE to render the related list, otherwise display catch error VF display.
        /*if(opp_is_won == TRUE){
section1 = TRUE;
section2 = FALSE;
} else if(recordsExist == FALSE){
section1 = FALSE;
section2 = TRUE;
}*/
    }
}