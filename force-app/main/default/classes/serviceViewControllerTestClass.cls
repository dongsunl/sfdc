@isTest
private class serviceViewControllerTestClass {
    
    public static Opportunity opportunityInstance {get; set;}    
    
    @isTest static void createEverything() {
        AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
        Insert avaCustomSetting;
        //Create Account
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        //Create Contact
        Contact con = new Contact();
        con.FirstName = 'Kruse';
        con.LastName = 'Collins';
        con.AccountId = acc.Id;
        con.LeadSource = 'Event';
        con.Contact_Role__c = 'VP';
        insert con;
        
        //Create Opportunity
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.CloseDate = date.today();
        opp.Accountid = acc.Id;
        opp.Contact2__c = con.Id;
        opp.Stagename = 'Invoice';
        opp.Holdout_Opportunity__c = FALSE;
        opp.ForecastCategoryName = 'Commit';
        opp.Sales_Process_Stage__c = 'Invoice';
        opp.Forecast_Percentage_NEW__c = 15.00;
        opp.Turn_Off_Initial_CX_EX_RC_Amounts__c = TRUE;
        Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();
        //opportunityInstance.RecordTypeId = rtMapByName.get('XM').getRecordTypeId();
        insert opp;
        
        //Create Partner
        Q_Partner__c ptnr = new Q_Partner__c();
        ptnr.Name = 'Test Partner';
        ptnr.Category_New__c = 'Other';
        insert ptnr;
        
        //Create Bundle
        Bundle__c bndl = new Bundle__c();
        bndl.Name = 'Test Bundle';
        bndl.Experience__c = 'EX';
        bndl.Bundle__c = '1a';
        insert bndl;
        
        //Create Bundle Product
        Product_Bundle_Junction__c bndprd = new Product_Bundle_Junction__c();
        bndprd.Related_Bundle__c = bndl.Id;
        insert bndprd;
            
        //Create Service Bundle
        Service_Bundle__c svcb = new Service_Bundle__c();
        svcb.Name = 'Test Service Bundle';
        svcb.Bundle_Product_Prerequisite__c = bndprd.Id;
        insert svcb;
            
        //Create Partner Availability
        Partner_Availability__c pa = new Partner_Availability__c();
        pa.Partner__c = ptnr.Id;
        pa.Region__c = 'North America; Latin America; Canada; EMEA; APAC; LATAM';
        pa.Service_Bundle__c = svcb.Id;
        insert pa;
            
        //Create Quote 
        Quote qt = new Quote();
        qt.Name = 'Test Sync Quote';
        qt.OpportunityId = opp.Id;
        qt.License_Term_in_months__c = 12.00;
        insert qt;
        
        //Create Service Record
        QService__c srvc = new QService__c();
        srvc.Name = 'Synced Service';
        srvc.Service_Type__c = 'TAM';
        srvc.Quote__c = qt.Id;
        srvc.Opportunity__c = opp.Id;
        srvc.Status__c = 'Inactive';
        insert srvc;
        
        opp.SyncedQuoteId = qt.Id;
        update opp;
        
        serviceViewController sv = new serviceViewController(new ApexPages.StandardController(opp));
    }
    
    @isTest static void createTodo() {
        AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
        Insert avaCustomSetting;
        //Create Account
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        //Create Contact
        Contact con = new Contact();
        con.FirstName = 'Kruse';
        con.LastName = 'Collins';
        con.AccountId = acc.Id;
        con.LeadSource = 'Event';
        con.Contact_Role__c = 'VP';
        insert con;
        
        //Create Opportunity
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.CloseDate = date.today();
        opp.Accountid = acc.Id;
        opp.Contact2__c = con.Id;
        opp.Stagename = 'Invoice';
        opp.Holdout_Opportunity__c = FALSE;
        opp.ForecastCategoryName = 'Commit';
        opp.Sales_Process_Stage__c = 'Invoice';
        opp.Forecast_Percentage_NEW__c = 15.00;
        opp.Turn_Off_Initial_CX_EX_RC_Amounts__c = TRUE;
        Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();
        //opportunityInstance.RecordTypeId = rtMapByName.get('XM').getRecordTypeId();
        insert opp;
        
        //Create Partner
        Q_Partner__c ptnr = new Q_Partner__c();
        ptnr.Name = 'Test Partner';
        ptnr.Category_New__c = 'Other';
        insert ptnr;
        
        //Create Bundle
        Bundle__c bndl = new Bundle__c();
        bndl.Name = 'Test Bundle';
        bndl.Experience__c = 'EX';
        bndl.Bundle__c = '1a';
        insert bndl;
        
        //Create Bundle Product
        Product_Bundle_Junction__c bndprd = new Product_Bundle_Junction__c();
        bndprd.Related_Bundle__c = bndl.Id;
        insert bndprd;
            
        //Create Service Bundle
        Service_Bundle__c svcb = new Service_Bundle__c();
        svcb.Name = 'Test Service Bundle';
        svcb.Bundle_Product_Prerequisite__c = bndprd.Id;
        insert svcb;
            
        //Create Partner Availability
        Partner_Availability__c pa = new Partner_Availability__c();
        pa.Partner__c = ptnr.Id;
        pa.Region__c = 'North America; Latin America; Canada; EMEA; APAC; LATAM';
        pa.Service_Bundle__c = svcb.Id;
        insert pa;
                    
        //Create Quote 
        Quote qt = new Quote();
        qt.Name = 'Test Sync Quote';
        qt.OpportunityId = opp.Id;
        qt.License_Term_in_months__c = 12.00;
        insert qt;
        
        //Create Service Record
        QService__c srvc = new QService__c();
        srvc.Name = 'Synced Service';
        srvc.Service_Type__c = 'TAM';
        srvc.Quote__c = qt.Id;
        srvc.Opportunity__c = opp.Id;
        srvc.Status__c = 'Inactive';
        insert srvc;
        
        
        serviceEditBannerController sebc = new serviceEditBannerController(new ApexPages.StandardController(srvc));
    }    
    
    @isTest static void createNonInvoice() {
        AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
        Insert avaCustomSetting;
        //Create Account
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        //Create Contact
        Contact con = new Contact();
        con.FirstName = 'Kruse';
        con.LastName = 'Collins';
        con.AccountId = acc.Id;
        con.LeadSource = 'Event';
        con.Contact_Role__c = 'VP';
        insert con;
        
        //Create Opportunity
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.CloseDate = date.today();
        opp.Accountid = acc.Id;
        opp.Contact2__c = con.Id;
        opp.Stagename = 'Commit';
        opp.Holdout_Opportunity__c = FALSE;
        opp.ForecastCategoryName = 'Commit';
        opp.Sales_Process_Stage__c = 'Commit';
        opp.Forecast_Percentage_NEW__c = 15.00;
        opp.Turn_Off_Initial_CX_EX_RC_Amounts__c = TRUE;
        Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();
        //opportunityInstance.RecordTypeId = rtMapByName.get('XM').getRecordTypeId();
        insert opp;
        
        //Create Partner
        Q_Partner__c ptnr = new Q_Partner__c();
        ptnr.Name = 'Test Partner';
        ptnr.Category_New__c = 'Other';
        insert ptnr;
        
        //Create Bundle
        Bundle__c bndl = new Bundle__c();
        bndl.Name = 'Test Bundle';
        bndl.Experience__c = 'EX';
        bndl.Bundle__c = '1a';
        insert bndl;
        
        //Create Bundle Product
        Product_Bundle_Junction__c bndprd = new Product_Bundle_Junction__c();
        bndprd.Related_Bundle__c = bndl.Id;
        insert bndprd;
            
        //Create Service Bundle
        Service_Bundle__c svcb = new Service_Bundle__c();
        svcb.Name = 'Test Service Bundle';
        svcb.Bundle_Product_Prerequisite__c = bndprd.Id;
        insert svcb;
            
        //Create Partner Availability
        Partner_Availability__c pa = new Partner_Availability__c();
        pa.Partner__c = ptnr.Id;
        pa.Region__c = 'North America; Latin America; Canada; EMEA; APAC; LATAM';
        pa.Service_Bundle__c = svcb.Id;
        insert pa;
                    
        //Create Quote 
        Quote qt = new Quote();
        qt.Name = 'Test Sync Quote';
        qt.OpportunityId = opp.Id;
        qt.License_Term_in_months__c = 12.00;
        insert qt;
        
        //Create Service Record
        QService__c srvc = new QService__c();
        srvc.Name = 'Synced Service';
        srvc.Service_Type__c = 'TAM';
        srvc.Quote__c = qt.Id;
        srvc.Opportunity__c = opp.Id;
        srvc.Status__c = 'Inactive';
        insert srvc;
        
        
        serviceEditBannerController sebc = new serviceEditBannerController(new ApexPages.StandardController(srvc));
    }    
    
   
}