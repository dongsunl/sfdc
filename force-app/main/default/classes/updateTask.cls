public class updateTask {
    
    public static void updateTask(Task[] tasks) {
      String leadPrefix = Schema.SObjectType.Lead.getKeyPrefix();
      String contactPrefix = Schema.SObjectType.Contact.getKeyPrefix();
      for (Task t :tasks){
          if(t.Subject.toLowerCase().startsWith('email') && t.RecordTypeId != '012500000009kLGAAY') {
              //if the what id is not empty and is a Lead
              if(t.WhoId != null) {
                  String taskId = t.WhoId;
                  if(taskId.startsWith(leadPrefix) || taskId.startsWith(contactPrefix)) {
                      t.Type = 'Prospecting Email';
                      t.Outcome__c = 'Email To';
                  }
              }
          }
       }
    }
}