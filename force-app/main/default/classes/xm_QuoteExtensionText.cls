@isTest (SeeAllData=true)
public class xm_QuoteExtensionText {
 static Account acct {get;set;}
    static Opportunity opp {get;set;}
    static Quote quoteObj;
    static Quote quoteObj3;
    static list<QuotelineItem> lstQLI;
    static list<Bundle__c> lstBdl;
    static Client__c c {get;set;}
    static Bundle__c bdl {get;set;}
    static list<String> ex;
    static list<String> bundles;
    
    @isTest static void test_getqe() {
        User objUser = [SELECT ID
                        FROM User
                        WHERE Profile.Name = 'System Administrator'
                        AND isActive = true LIMIT 1];
        System.runAs(objUser) {
            ex = new list<String>();
            ex.add('X360');
            ex.add('CX');
            ex.add('RC');
            bundles = new list<String>();
            bundles.add('EX3');
            bundles.add('RC1');
            lstQLI = new list<QuotelineItem>();
            TestFactory.disableTriggers();
            acct = TestFactory.createAccount();
            insert acct;
            opp = new Opportunity();
            opp = TestFactory.createOpportunity(acct.Id, null);
            insert opp;
            
            quoteObj = new Quote();
            quoteObj = TestFactory.createQuote(opp.Id, Test.getStandardPricebookId());
            insert quoteObj;
            
            quoteObj3 = new Quote();
            quoteObj3 = TestFactory.createQuote(opp.Id, Test.getStandardPricebookId());
            insert quoteObj3;
            
            Product2 prod = new Product2(Name = 'Laptop X200',
                                         Family = 'Hardware',Display_Order__c=78, IsActive=true);
            insert prod;
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            
            
            QuoteLineItem qliObj = new QuoteLineItem();
            qliObj.QuoteId = quoteObj.Id;
            qliObj.Quantity = 1;
            qliObj.UnitPrice = 100;
            qliObj.pricebookentryId = standardPrice.Id;
            insert qliObj;
          
           
            
            c = new Client__c();
            c.Account__c = acct.Id;
            c.Name = 'Test XM Client';
            insert c;
            
            bdl = new Bundle__c();
            bdl.Name = 'EX1a';
            bdl.Experience__c = 'EX';
            bdl.Bundle__c = '1a';
            bdl.NS_Item_ID__c = '251';
            bdl.Upgrade_Bundles__c = 'EX1a;EX3;EX5q;EX5m;';
            insert bdl;
            
            Quote quoteObj2 = new Quote();
		quoteObj2.OpportunityId = opp.id;
		quoteObj2.Name = 'Test Quote2';
        quoteObj2.Pricebook2Id = '01s500000001tHM';
            
            
            
            datetime dt = datetime.now();
            date d = date.today();
            date ldt = date.today();
            
            string d2 = string.valueof(d);
            string dt2 = string.valueof(dt);
            string ldt2 = string.valueof(ldt);
            
            ApexPages.StandardController sc = new ApexPages.StandardController(opp);
            PageReference pageRef = Page.xm_QuotePage;
            System.Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('id',opp.Id);            
            
            System.Test.startTest();
            xm_QuoteExtension controller = new xm_QuoteExtension(sc);
            xm_QuoteExtension.getCt();
            xm_QuoteExtension.getXmexp(ex);
            xm_QuoteExtension.getQuoteList(opp.Id);
            xm_QuoteExtension.GetAccount(acct.Id);
            xm_QuoteExtension.GetClients(acct.Id);
            xm_QuoteExtension.getQliList(quoteObj.Id);
            xm_QuoteExtension.deleteQuote(quoteObj);
            xm_QuoteExtension.getBundles('EX',false, bundles);
            xm_QuoteExtension.getPrice('EX','3');
            xm_QuoteExtension.getProducts('EX','3');
            xm_QuoteExtension.GetClientBundles(c.id);
            xm_QuoteExtension.getCurBundle(bdl.Id);         
           xm_QuoteExtension.createQuote(quoteObj2, d2, dt2, ldt2);
            xm_QuoteExtension.editQuote(quoteObj3, dt2, ldt2);
            xm_QuoteExtension.updateQuoteConfig(quoteObj3, dt2);
            xm_QuoteExtension.GetClientLicense(c.id, 'EX', 'Active', true);
            xm_QuoteExtension.getXmq(); 
            
            
            System.Test.stopTest();
            
        }
    }
    
}