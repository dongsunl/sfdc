//TOGGLE BETWEEN NEW AND UPGRADE
        $('#toggle1').on('change',function() {
            //The syntax inside the if statement below returns true for toggle checked and false for toggle unchecked
            if($(this).prop('checked')){
                $('#toggleValue1').text('On');
                $('#newPanel').removeClass('slds-is-expanded');
                $('#newPanel').addClass('slds-is-collapsed');
                
            }else{
                $('#toggleValue1').text('Off');
                $('#newPanel').removeClass('slds-is-collapsed');
                $('#newPanel').addClass('slds-is-expanded');    
            }
        });
        
        //FORMATTING
        //Format Number
        function formatNumber(num, culture) {
          if(culture === 'ar-AE'){

            culture = 'en-US';
         }
            var n = num;
            s = new String(num);
            return s = n.toLocaleString(culture);               
        }
        
        //Format Currency
        function formatCurrency(amount, currency, culture) {
         if(culture === 'ar-AE'){

            culture = 'en-US';
         }
            var n = Number(amount);
            s = new String(amount);
            return s = n.toLocaleString(culture, {style: 'currency', currency: currency});               
        }           


        
        //MODALS            
        //Modal Open
        function openModal(){               
            $('#modal').addClass('slds-fade-in-open');    
            $('#backdrop').addClass('slds-backdrop--open'); 
        }         
        
        //Modal Close
        function closeModal(){
            $('#modal').removeClass('slds-fade-in-open');
            $('#backdrop').removeClass('slds-backdrop--open');
        }
        
        //Modal Open
        function openModal2(){                  
            $('#quoteConfigurator').removeClass('slds-hide'); 
            $('#quoteConfigurator').addClass('slds-show'); 
            $('#quoteManager').removeClass('slds-show'); 
            $('#quoteManager').addClass('slds-hide');  
        }         
        
        //Modal Close
        function closeModal2(){
           
            hideSpinner2();
            emptyArraysTables();
            $("aProdList").empty();
             $("#license").empty();
             $('#clttl').html('');
             $('#ucred').html('');  
            $('#container').empty();
            $('#tiercontainer').empty();                
            $('#UserTiercontainer').empty()
            $("#utiers").val(0); 
            $("#rtiers").val(0);
            $("#notify").empty();
            $('#aProdList').empty();
           $('#rProdList').empty();
           $('#hProdList').empty();
           $("#qliTable").empty();                
            $('#ao').empty();
            $('#clist').empty();
            $('#dlist').empty();
           $("#curlicense").addClass('slds-hide');
            $("#qlicol").addClass('slds-size--2-of-2');
            $("#qlicol").removeClass('slds-size--1-of-2');
$("#right-pane").addClass('slds-hide');
                $("#panes-separator").addClass('slds-hide');
                $("#left-pane").removeClass('left-pane');
            $('#quoteConfigurator').addClass('slds-hide'); 
            $('#quoteConfigurator').removeClass('slds-show'); 
            $('#quoteManager').addClass('slds-show'); 
            $('#quoteManager').removeClass('slds-hide');  
        }
        
        //SPINNERS
        //SHOW PAGE SPINNER
        function showSpinner(){  
         //   console.log('showSpinner');
            $('#spinner').addClass('slds-fade-in-open');    
            $('#spinnerbackdrop').addClass('slds-backdrop--open');   
        }   
        
        //HIDE PAGE SPINNER
        function hideSpinner(){
          //  console.log('hideSpinner');
            $('#spinner').removeClass('slds-fade-in-open');
            $('#spinnerbackdrop').removeClass('slds-backdrop--open');
        }
        
        //SHOW QUOTE SPINNER
        function showSpinner2(){  
         //   console.log('showSpinner2');
            $('#spinner2').addClass('slds-fade-in-open');    
            $('#spinnerbackdrop2').addClass('slds-backdrop--open');   
        }         
        
        //HIDE QUOTE SPINNER
        function hideSpinner2(){
         //   console.log('hideSpinner2');
            $('#spinner2').removeClass('slds-fade-in-open');
            $('#spinnerbackdrop2').removeClass('slds-backdrop--open');
        } 

         
        // REMOVE DATA FROM ALL ARRAYS AND TABLES
        function emptyArraysTables(){
            console.log('emptyArraysTables');
         curExp = '';
         curBdl = '';
         curBdlId = '';
         curQuoteType = ''; 
         curUpgradeDateType = '';
         curUpgradeType = '';
         curQlis = [];
         curLicenseStart;
         curLicenseEnd;
         curQuoteStart;
         curQuoteTerm;
         OriginalQuoteTerm;
         quoteend; 
         curQuoteEnd;
         curQid = '';
         curProds = [];
         curClient = '';
         curNsId = '';
         coreQty = 0;
         coreLicenseQty = 0;
         coreType = '';
         coreEntry = '';               
         coreDb = '';
         curLicenseCoreQty = 0;
         curLicenseCoreCost = 0;
         curLicenseCoreId = '';
         curLicenseTotalCost = 0;
         userQty = 0;
         alacarteBdl = 'false';
         productGroups = [];
         prodIds = [];
         allPriceTiers = [];
         upgradeBundles = [];
         delqlis = [];
         Proration = 0;
         ProrationTerm = 0;
         Maintenance = 0;
         UnusedCredits = 0;
         UnusedCredAmt = 0;
         selectedBundle = false;
         deletedLicenses = '';
            rtier = '';
            utier = '';
            curLicenseExpBdl = '';
            
        }
        

         //OPEN QUOTE VIEW PAGE
        function viewQuote(a){
            console.log('viewQuote');
            var openloc = '/apex/QuoteLayout?Id='+a.id;
            window.open(openloc);
        }
        
        //OPEN DISCOUNT PAGE
        function discountQuote(a){
            console.log('discountQuote');                
            var openloc = '/apex/XM_Discounts?Id='+a.id;
            window.open(openloc);
        }
       

           //CREATE TERM INPUT
        function createTermInput(){
            console.log('createTermInput');
            $('#term').empty(); 
            var cterm = '<div class="slds-form-element">'+
                '<label class="slds-form-element__label" for="cterm">Term: </label>'+
                '<div class="slds-form-element__control">'+
                '<input type="text" id="cterm" class="slds-input" value="'+curQuoteTerm+'" onchange="updateTerm(this.value)"/>'+
                '</div>'+
                '</div>';
            $('#term').html(cterm);  
        }
        
        //CREATE TERM OUTPUT
        function createTermOutput(){
             console.log('createTermOutput');
            $('#term').empty(); 
            var cterm = '<div class="slds-form-element">'+
                '<label class="slds-form-element__label" for="cterm">Term: </label>'+
                '<div class="slds-form-element__control">'+
                '<input type="text" disabled="" id="cterm" class="slds-input" value="'+curQuoteTerm+'" />'+
                '</div>'+
                '</div>';  
            $('#term').html(cterm);  
        }
        
        //CREATE QUOTE START DATE OUTPUT
        function createStartDateOutput(){  
         console.log('createStartDateOutput');              
            var cstart = '<div class="slds-form-element">'+
                '<label class="slds-form-element__label" for="cstart">Start Date: </label>'+
                '<div class="slds-form-element__control">'+
                '<input type="text" disabled="" id="cstart" class="slds-input" value="'+moment(curQuoteStart).format('l')+'" />'+                    
                '</div>'+
                '</div>'; 
            $('#start').html(cstart); 
        }           
        
        //CREATE QUOTE END DATE OUTPUT
        function createEndDateOutput(){  
         console.log('createEndDateOutput');  
         //console.log(curQuoteEnd);            
            var cend= '<div class="slds-form-element">'+
                '<label class="slds-form-element__label" for="cend">End Date: </label>'+
                '<div class="slds-form-element__control">'+
               '<input type="text" disabled="" id="cend" class="slds-input" value="'+moment(curQuoteEnd).format('l')+'" />'+
                '</div>'+
                '</div>'; 
            $('#enddt').html(cend); 
        }
        
        //CREATE LICENSE START DATE OUTPUT
        function createLStartDateOutput(){  
         console.log('createLStartDateOutput');              
            var cstart = '<div class="slds-form-element">'+
                '<label class="slds-form-element__label" for="clstart">Start Date: </label>'+
                '<div class="slds-form-element__control">'+
                moment(curLicenseStart).format('l')+
                '</div>'+
                '</div>'; 
            $('#curstdt').html(cstart); 
        }
        
        //CREATE LICENSE END DATE OUTPUT
        function createLEndDateOutput(){   
        console.log('createLEndDateOutput');               
            var cend= '<div class="slds-form-element">'+
                '<label class="slds-form-element__label" for="clend">End Date: </label>'+
                '<div class="slds-form-element__control">'+
                moment(curLicenseEnd).format('l')+
                '</div>'+
                '</div>'; 
            $('#curenddt').html(cend); 
        }

        //QUOTE ACTIONS 
        function viewActions(){
            if($('#newqtpanel').hasClass('slds-is-open')){
                $('#newqtpanel').removeClass('slds-is-open');
                $('#newqtpanel').find('.slds-section__content').hide();
            }
            else{
                $('#newqtpanel').removeClass('slds-is-open');
                $('#newqtpanel').toggleClass('slds-is-open');
            }  
        }
        function closeActions(){
                $('#newqtpanel').removeClass('slds-is-open');
                $('#newqtpanel').find('.slds-section__content').hide();
        }