trigger AttachmentTrigger on Attachment (
    before insert,
    before update,
    before delete,
    after insert,
    after update,
    after delete,
    after undelete) {


    if(Trigger_Settings__c.getInstance().Disable_All_Triggers__c){
        return;
    }

    
                
        if (trigger.isAfter && trigger.isUpdate){
           Q2C_QSOAttachedtoOpp.attachQSO(trigger.new);
            Q2C_QSOAttachedtoOpp.attachPO(trigger.new);
            
        }
        if(trigger.isAfter && trigger.isinsert){
            Q2C_QSOAttachedtoOpp.attachQSO(trigger.new);
            cdr_CustomQSOUpload.attachContract(trigger.new);
            cdr_CustomQSOUpload.attachContractFinal(trigger.new);
            Q2C_QSOAttachedtoOpp.attachPO(trigger.new);
        }
        if(trigger.isbefore && trigger.isdelete){
            Q2C_QSOAttachedtoOpp.deleteQSO(trigger.old);
        }

}