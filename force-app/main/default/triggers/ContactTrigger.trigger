trigger ContactTrigger on Contact (before insert, before update, before delete, after insert, after update, after delete, after undelete) {


    if(Trigger_Settings__c.getInstance().Disable_All_Triggers__c){
        return;
    }

    

	ContactTriggerHandler handler = new ContactTriggerHandler();

	if (Trigger.isBefore) {
    	if(Trigger.isInsert){
    		handler.OnBeforeInsert(Trigger.new);
    	}
    	else if(Trigger.isUpdate){
    		handler.OnBeforeUpdate(Trigger.old,Trigger.new,trigger.newMap, Trigger.oldMap);
    	}
    	else if(Trigger.isDelete){
    		handler.OnBeforeDelete(trigger.old,Trigger.newMap, Trigger.oldMap);
    	}    
	} 
	else if (Trigger.isAfter) {
    	if(Trigger.isInsert){
    		handler.OnAfterInsert(Trigger.new,trigger.newMap);
       	}
       	else if(Trigger.isUpdate){
    		handler.OnAfterUpdate(Trigger.old,Trigger.new,trigger.newMap, Trigger.oldMap);
    	}
    	else if(Trigger.isDelete){
    		handler.OnAfterDelete(trigger.old, Trigger.oldMap);
    	}    
	}
}