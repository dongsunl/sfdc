trigger EmailTeamMember on OpportunityTeamMember (before insert) {

    for (OpportunityTeamMember o : trigger.new) {
        if(o.TeamMemberRole == 'SME') {
            String[] showID = new String[] {o.UserID};
            List<User> u = [SELECT Id, Name, Email FROM User WHERE Id in :showID];
            Opportunity opp = [SELECT Id, Name, Owner.Email FROM Opportunity WHERE Id = :o.OpportunityId];
            String[] toAddresses = new String[] {u[0].email, opp.Owner.Email};
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(toAddresses);
            mail.setReplyTo ('noreply@salesforce.com');
            mail.setSenderDisplayname('Salesforce Support');
            mail.setSubject(opp.Name + ' SME Assignment');
            mail.setUseSignature(false);
            mail.setPlainTextBody('You have been added to an Opportunity Team ');
            mail.setHtmlBody('A SME has been assigned to this opportunity: ' + opp.Name + '<p><p/> '+ 
            'Please log into Salesforce to see more details: <a href="https://qualtrics.my.salesforce.com/'+opp.Id+'" target="_blank">click here<a/>');
            try {
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            }
            catch(exception e) {
                
            }
        }
    
    }
}