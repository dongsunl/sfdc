/* Trigger execute when Lead record gets created */
trigger Lead_AddAccount on Lead (after insert) {


    if(Trigger_Settings__c.getInstance().Disable_All_Triggers__c){
        return;
    }

    
   /* 
   if(Trigger.isAfter && Trigger.isInsert){
        //Lead_AddAccountHandler objHandler = new Lead_AddAccountHandler();
        //objHandler.onInsertLead(trigger.new);
        
        Set<ID> setLead = new Set<Id>();
        
        for(Lead objLead : [select Id From Lead Where Id =: trigger.new]){
            setLead.add(objLead.Id);
        }//for
        if(setLead != Null){
            Lead_AddAccountHandler.onInsertLead(setLead);
        }//if
    }//if  */
}//Lead_AddAccount