/***********
Description: This trigger is used for calculating the number of bid made by a vendor(i.e no. of panels project vendors per vendor)
and the number of bid own by a vendor(i.e no. of panels project vendor slected for use per vendor). This trigger will fire just before 
the delete event in  panels project vendor 
***********/
trigger PanelProjectVendorTrigger on Panels_Project_Vendor__c (before delete,before update,before insert) {
    // Cheking isBefore is true and is delete is true
    if(Trigger.isBefore && Trigger.isdelete){
        PanelProjectVendorHelper.deleteEventHandler(Trigger.oldMap);
    }
    
    if(Trigger.isUpdate || Trigger.isInsert){
        system.debug('@@@@');
        PanelProjectVendorHelper.upsertBeforeEventHandler(Trigger.new);
    }    
       
        
         
}