trigger PartnerShareLead on Lead (after insert, after update) {

    if(Trigger.isAfter && Trigger.isInsert){
        PartnerShareLeadHelper.afterInsert(trigger.newMap);
    }
    
    if(Trigger.isAfter && Trigger.isUpdate){
        PartnerShareLeadHelper.afterUpdate(Trigger.NewMap, Trigger.OldMap);
    }
    
    if(Trigger.isAfter && Trigger.isDelete){
    }    

}