trigger PartnerShareQPartner on Q_Partner__c (
	before insert,
	before update,
	before delete,
	after insert,
	after update,
	after delete,
	after undelete) {

		if (Trigger.isBefore) {
	    	//call your handler.before method
		} else if (Trigger.isAfter) {
	    	//call handler.after method
			if(Trigger.isUpdate){
				PartnerShareQPartnerHelper.afterUpdate(Trigger.NewMap, Trigger.OldMap);
			}
			if(Trigger.isInsert){
				PartnerShareQPartnerHelper.afterInsert(trigger.newMap);
			}
		}
}