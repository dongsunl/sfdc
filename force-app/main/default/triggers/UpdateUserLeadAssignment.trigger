trigger UpdateUserLeadAssignment on Lead (after update) {


  if(Trigger_Settings__c.getInstance().Disable_All_Triggers__c){
    return;
  }

  

    String U2Update;

    For(Lead L : Trigger.old){
        System.debug('This is Trigger.old OwnerId ' +Trigger.old);
        For(Lead L2 : Trigger.new){
            System.debug('This is Trigger.new OwnerId ' +Trigger.new);
            if(L.OwnerID != L2.OwnerID){
                U2Update = L2.OwnerID;
            }
        }
    }
    System.debug('This is U2Update  ' + U2Update);
    
    List<User> u = [Select ID, Last_Lead_Assigned__c from User where Id = :U2Update];
    
      For(User use: u){
          use.Last_Lead_Assigned__c = Datetime.now();
      }
      System.debug('This is the u list to update  ' +u);
      update u;
        

}