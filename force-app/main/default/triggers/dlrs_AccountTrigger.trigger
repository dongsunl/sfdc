/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
trigger dlrs_AccountTrigger on Account
    (before delete, before insert, before update, after delete, after insert, after undelete, after update)
{


	if(Trigger_Settings__c.getInstance().Disable_All_Triggers__c){
		return;
	}

	
    dlrs.RollupService.triggerHandler();
}