trigger leadProducts on Lead (before update) {


	if(Trigger_Settings__c.getInstance().Disable_All_Triggers__c){
		return;
	}

	
	for (lead l : trigger.new) {
		string s = trigger.oldmap.get(l.id).Product__c;
		string s1 = l.Product__c;
		string s2;
		if (s == null) {
			s = '';
		}
		if (s1 == null) {
			s1 = '';
		}
		set<string> prodSet = new set<string>();
		list<string> sl = s.split(';');
		for (string ss : sl) {
			prodSet.add(ss);
		}
		list<string> sl2 = s1.split(';');
		for (string ss : sl2) {
			prodSet.add(ss);
		}
		for (string ss : prodSet) {
			if (s2 == null) {
				s2 = ss;
			}
			else{
				s2 += ';'+ss;
			}
		}
		l.Product__c = s2;
	}
}